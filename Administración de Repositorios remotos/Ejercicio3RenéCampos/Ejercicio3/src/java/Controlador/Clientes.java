package Controlador;

import EJB.ClienteFacadeLocal;
import Entidades.Cliente;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class Clientes implements Serializable {

    @EJB
    private ClienteFacadeLocal cfl;
    private Cliente cliente;
    private List<Cliente> listClientes;

    @PostConstruct
    public void init() {
        cliente = new Cliente();
    }

    public void listar() {
        try {
            listClientes = cfl.findAll();
        } catch (Exception e) {
            System.out.println("Error en: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void crear() {
        try {
            cfl.create(cliente);
            listClientes = cfl.findAll();
            cliente = new Cliente();
        } catch (Exception e) {
            System.out.println("Error en: " + e.getMessage());
            e.printStackTrace();
        }
    }
    
    public void llenarid(Cliente c){
        cliente = c;
    }

    public void editar() {
        try {
            cfl.edit(cliente);
            listClientes = cfl.findAll();
            cliente = new Cliente();
        } catch (Exception e) {
            System.out.println("Error en: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void eliminar(Cliente c) {
        try {
            cfl.remove(c);
            listClientes = cfl.findAll();
            cliente = new Cliente();
        } catch (Exception e) {
            System.out.println("Error en: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<Cliente> getListClientes() {
        return listClientes;
    }

    public void setListClientes(List<Cliente> listClientes) {
        this.listClientes = listClientes;
    }
}
