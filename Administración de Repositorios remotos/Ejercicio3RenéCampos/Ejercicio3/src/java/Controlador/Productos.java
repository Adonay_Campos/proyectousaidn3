package Controlador;

import EJB.CategoriaFacadeLocal;
import EJB.ProductoFacadeLocal;
import Entidades.*;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class Productos implements Serializable {

    @EJB
    private ProductoFacadeLocal pfl;
    @EJB
    private CategoriaFacadeLocal cafl;
    private List<Producto> listProductos;
    private List<Categoria> listCategoria;
    private Producto producto;
    private Categoria categoria;

    @PostConstruct
    public void init() {
        categoria = new Categoria();
        producto = new Producto();
    }

    public void listarProductos() {
        try {
            listProductos = pfl.findAll();
        } catch (Exception e) {
            System.out.println("Error en: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void crearProductos() {
        try {
            producto.setIdCategoria(categoria);
            pfl.create(producto);
            producto = new Producto();
            categoria = new Categoria();
            listProductos = pfl.findAll();
        } catch (Exception e) {
            System.out.println("Error en: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void llenarIdCategoria(Producto p) {
        categoria.setIdCategoria(p.getIdCategoria().getIdCategoria());
        producto = p;
    }

    public void editarProductos() {
        try {
            producto.setIdCategoria(categoria);
            pfl.edit(producto);
            producto = new Producto();
            categoria = new Categoria();
            listProductos = pfl.findAll();
        } catch (Exception e) {
            System.out.println("Error en: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void eliminarProductos(Producto p) {
        try {
            pfl.edit(p);
            producto = new Producto();
            listProductos = pfl.findAll();
        } catch (Exception e) {
            System.out.println("Error en: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public List<Producto> getListProductos() {
        return listProductos;
    }

    public void setListProductos(List<Producto> listProductos) {
        this.listProductos = listProductos;
    }

    public List<Categoria> getListCategoria() {
        listCategoria = cafl.findAll();
        return listCategoria;
    }

    public void setListCategoria(List<Categoria> listCategoria) {
        this.listCategoria = listCategoria;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }
}
