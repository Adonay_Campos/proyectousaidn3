/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import Entidades.ModoPago;
import javax.annotation.PreDestroy;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author rene.camposusam
 */
@Stateless
public class ModoPagoFacade extends AbstractFacade<ModoPago> implements ModoPagoFacadeLocal {

    @PersistenceContext(unitName = "Ejercicio3PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ModoPagoFacade() {
        super(ModoPago.class);
    }
    
        @PreDestroy
    public void destruct() {
        em.close();
    }
}
