/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import Entidades.ModoPago;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author rene.camposusam
 */
@Local
public interface ModoPagoFacadeLocal {

    void create(ModoPago modoPago);

    void edit(ModoPago modoPago);

    void remove(ModoPago modoPago);

    ModoPago find(Object id);

    List<ModoPago> findAll();

    List<ModoPago> findRange(int[] range);

    int count();
    
}
