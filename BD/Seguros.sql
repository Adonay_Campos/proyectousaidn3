-- MySQL dump 10.13  Distrib 8.0.11, for Win64 (x86_64)
--
-- Host: localhost    Database: seguros
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `asignacion_seguros`
--

DROP TABLE IF EXISTS `asignacion_seguros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `asignacion_seguros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` int(11) DEFAULT NULL,
  `personas` int(11) DEFAULT NULL,
  `seguro` int(11) DEFAULT NULL,
  `vehiculo` int(11) DEFAULT NULL,
  `beneficiario` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `paquetes` int(11) DEFAULT NULL,
  `poliza` varchar(50) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT NULL,
  `surcusal` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario` (`usuario`),
  KEY `persona_seguro` (`personas`),
  KEY `seguro` (`seguro`),
  KEY `vehiculo` (`vehiculo`),
  KEY `seguros_surcusal` (`surcusal`),
  KEY `benefe_seguro` (`beneficiario`),
  KEY `paquete_seguros` (`paquetes`),
  CONSTRAINT `benefe_seguro` FOREIGN KEY (`beneficiario`) REFERENCES `beneficiarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `paquete_seguros` FOREIGN KEY (`paquetes`) REFERENCES `paquetes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `persona_seguro` FOREIGN KEY (`personas`) REFERENCES `personas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `seguro` FOREIGN KEY (`seguro`) REFERENCES `tipo_seguros` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `seguros_surcusal` FOREIGN KEY (`surcusal`) REFERENCES `sucursales` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `usuario` FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `vehiculo` FOREIGN KEY (`vehiculo`) REFERENCES `vehiculos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asignacion_seguros`
--

LOCK TABLES `asignacion_seguros` WRITE;
/*!40000 ALTER TABLE `asignacion_seguros` DISABLE KEYS */;
INSERT INTO `asignacion_seguros` VALUES (1,2,13,1,2,NULL,'2017-05-03 08:25:00',3,'SA1',1,3),(2,2,14,1,1,NULL,'2017-06-03 08:34:00',1,'SA2',1,5),(3,1,19,2,NULL,1,'2017-12-03 13:49:18',6,'SV1',1,5),(4,1,12,2,NULL,2,'2017-12-03 13:53:19',5,'SV2',1,2),(5,1,15,1,3,NULL,'2017-12-05 14:40:12',2,'SA3',1,3),(6,1,20,2,NULL,5,'2018-02-09 09:11:07',6,'SV3',1,4),(7,1,21,2,NULL,3,'2018-03-09 09:11:20',4,'SV4',1,2),(8,1,21,2,NULL,4,'2018-05-09 09:11:24',4,'SV5',1,2),(9,1,16,1,4,NULL,'2018-02-09 09:12:31',3,'SA4',1,1),(10,1,17,1,5,NULL,'2018-03-09 09:12:44',1,'SA5',1,3),(11,1,17,1,7,NULL,'2018-09-09 09:17:05',2,'SA6',1,4),(12,1,18,1,6,NULL,'2019-01-09 09:17:24',1,'SA7',1,5),(13,1,22,2,NULL,6,'2018-09-09 09:23:46',5,'SV6',1,1),(14,1,22,2,NULL,7,'2019-05-09 09:23:54',5,'SV7',1,1),(15,1,23,1,8,NULL,'2019-05-09 09:27:14',3,'SA8',1,2),(16,1,24,2,NULL,8,'2019-02-09 09:31:54',6,'SV8',1,1),(17,1,24,2,NULL,9,'2019-05-09 09:36:00',6,'SV9',1,1),(18,1,25,1,9,NULL,'2019-04-09 09:36:00',3,'SA9',1,3);
/*!40000 ALTER TABLE `asignacion_seguros` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `estado_asi` BEFORE INSERT ON `asignacion_seguros` FOR EACH ROW begin
set new.estado= 1;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `asistencias`
--

DROP TABLE IF EXISTS `asistencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `asistencias` (
  `idAsistencia` int(11) NOT NULL AUTO_INCREMENT,
  `asistencia` varchar(255) DEFAULT NULL,
  `detalles` varchar(255) DEFAULT NULL,
  `fecha` varchar(255) DEFAULT NULL,
  `switch1` varchar(255) DEFAULT NULL,
  `switch2` varchar(255) DEFAULT NULL,
  `nie` int(11) DEFAULT NULL,
  PRIMARY KEY (`idAsistencia`),
  KEY `FK_asistencias_nie` (`nie`),
  CONSTRAINT `FK_asistencias_nie` FOREIGN KEY (`nie`) REFERENCES `estudiantes` (`nie`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asistencias`
--

LOCK TABLES `asistencias` WRITE;
/*!40000 ALTER TABLE `asistencias` DISABLE KEYS */;
/*!40000 ALTER TABLE `asistencias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `beneficiarios`
--

DROP TABLE IF EXISTS `beneficiarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `beneficiarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `apellido` varchar(50) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `dui` varchar(11) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `telefono` varchar(9) DEFAULT NULL,
  `beneficiario` int(11) DEFAULT NULL,
  `parentesco` varchar(25) DEFAULT NULL,
  `porcentaje` double(5,2) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `beneficiario` (`beneficiario`),
  CONSTRAINT `beneficiario` FOREIGN KEY (`beneficiario`) REFERENCES `personas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `beneficiarios`
--

LOCK TABLES `beneficiarios` WRITE;
/*!40000 ALTER TABLE `beneficiarios` DISABLE KEYS */;
INSERT INTO `beneficiarios` VALUES (1,'Javier Eduardo','Flores Paz','2001-08-09','07021589-2','San Antonio','7111-2356',19,'Sobrino',15.00,1),(2,'Xoxhitl Mariela','Sibrián Cañas','1999-05-11','07002365-2','Turín','7789-6003',12,'Hija',50.00,1),(3,'José Luis','Sanchez Garcia','2000-08-19','06459857-2','San Salvador','7052-6230',21,'Hijo',50.00,1),(4,'Rosa Maria','Sanchez Garcia','1995-04-12','05580021-8','San Salvador','7005-6839',21,'Hija',50.00,1),(5,'Eduardo Kevin','Sanchez Sanchez','1998-08-26','06520018-7','San Felipe','7089-5600',20,'Hijo',100.00,1),(6,'Fredy Miguel','Rivas Castañeda','1990-05-28','07892000-5','Cojutepeque','7752-0399',22,'Hijo',50.00,1),(7,'Erlinda Beatriz','Rivas Castañeda','1999-12-17','07512001-9','Cojutepeque','7890-3680',22,'Hija',50.00,1),(8,'Mauricio Alejandro','Abrego Lopez','1999-10-03','06892002-1','Satelite','7803-2562',24,'Sobrino',50.00,1),(9,'Kenia Marina','AbregoLopez','2000-09-30','07820356-2','Satelite','6950-1549',24,'Sobrina',50.00,1);
/*!40000 ALTER TABLE `beneficiarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `estado_ben` BEFORE INSERT ON `beneficiarios` FOR EACH ROW begin
set new.estado= 1;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `cargos`
--

DROP TABLE IF EXISTS `cargos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cargos` (
  `idCargo` int(11) NOT NULL AUTO_INCREMENT,
  `cargo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idCargo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cargos`
--

LOCK TABLES `cargos` WRITE;
/*!40000 ALTER TABLE `cargos` DISABLE KEYS */;
/*!40000 ALTER TABLE `cargos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categoria_seguros`
--

DROP TABLE IF EXISTS `categoria_seguros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `categoria_seguros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_seguro` int(11) DEFAULT NULL,
  `anio` int(2) DEFAULT NULL,
  `paquetes` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tipo_seguro` (`tipo_seguro`),
  KEY `paquetes_categoria_seguros` (`paquetes`),
  CONSTRAINT `paquetes_categoria_seguros` FOREIGN KEY (`paquetes`) REFERENCES `paquetes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tipo_seguro` FOREIGN KEY (`tipo_seguro`) REFERENCES `tipo_seguros` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria_seguros`
--

LOCK TABLES `categoria_seguros` WRITE;
/*!40000 ALTER TABLE `categoria_seguros` DISABLE KEYS */;
INSERT INTO `categoria_seguros` VALUES (1,1,5,1),(2,1,7,2),(3,1,10,3),(4,2,7,4),(5,2,10,5),(6,2,15,6);
/*!40000 ALTER TABLE `categoria_seguros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cuentas`
--

DROP TABLE IF EXISTS `cuentas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cuentas` (
  `idCuenta` int(11) NOT NULL AUTO_INCREMENT,
  `correo` varchar(255) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `recuperacion` varchar(255) DEFAULT NULL,
  `tipoCuenta` int(11) DEFAULT NULL,
  PRIMARY KEY (`idCuenta`),
  KEY `FK_cuentas_tipoCuenta` (`tipoCuenta`),
  CONSTRAINT `FK_cuentas_tipoCuenta` FOREIGN KEY (`tipoCuenta`) REFERENCES `tipos_cuentas` (`idcuenta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cuentas`
--

LOCK TABLES `cuentas` WRITE;
/*!40000 ALTER TABLE `cuentas` DISABLE KEYS */;
/*!40000 ALTER TABLE `cuentas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departamentos`
--

DROP TABLE IF EXISTS `departamentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `departamentos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departamentos`
--

LOCK TABLES `departamentos` WRITE;
/*!40000 ALTER TABLE `departamentos` DISABLE KEYS */;
INSERT INTO `departamentos` VALUES (1,'Ahuachapan'),(2,'Santa Ana'),(3,'Sonsonate'),(4,'Chalatenango'),(5,'Cuscatlan'),(6,'San Salvador'),(7,'La Libertad'),(8,'San Vicente'),(9,'Cabañas'),(10,'La Paz'),(11,'Usulutan'),(12,'San Miguel'),(13,'Morazan'),(14,'La Union');
/*!40000 ALTER TABLE `departamentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalles_encargados`
--

DROP TABLE IF EXISTS `detalles_encargados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `detalles_encargados` (
  `idDe` int(11) NOT NULL AUTO_INCREMENT,
  `idDetalles` int(11) DEFAULT NULL,
  `idEncargado` int(11) DEFAULT NULL,
  PRIMARY KEY (`idDe`),
  KEY `FK_detalles_encargados_idEncargado` (`idEncargado`),
  KEY `FK_detalles_encargados_idDetalles` (`idDetalles`),
  CONSTRAINT `FK_detalles_encargados_idDetalles` FOREIGN KEY (`idDetalles`) REFERENCES `detalles_notificaciones` (`idnotificaciones`),
  CONSTRAINT `FK_detalles_encargados_idEncargado` FOREIGN KEY (`idEncargado`) REFERENCES `encargados` (`idencargado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalles_encargados`
--

LOCK TABLES `detalles_encargados` WRITE;
/*!40000 ALTER TABLE `detalles_encargados` DISABLE KEYS */;
/*!40000 ALTER TABLE `detalles_encargados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalles_notificaciones`
--

DROP TABLE IF EXISTS `detalles_notificaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `detalles_notificaciones` (
  `idNotificaciones` int(11) NOT NULL AUTO_INCREMENT,
  `detalles` longtext,
  `fecha` date DEFAULT NULL,
  `remitente` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idNotificaciones`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalles_notificaciones`
--

LOCK TABLES `detalles_notificaciones` WRITE;
/*!40000 ALTER TABLE `detalles_notificaciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `detalles_notificaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `encargados`
--

DROP TABLE IF EXISTS `encargados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `encargados` (
  `idEncargado` int(11) NOT NULL AUTO_INCREMENT,
  `dui` varchar(255) DEFAULT NULL,
  `nit` varchar(255) DEFAULT NULL,
  `parentesco` varchar(255) DEFAULT NULL,
  `telefonoF` varchar(255) DEFAULT NULL,
  `telefonoM` varchar(255) DEFAULT NULL,
  `idPersona` int(11) DEFAULT NULL,
  `subEncargado` int(11) DEFAULT NULL,
  PRIMARY KEY (`idEncargado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `encargados`
--

LOCK TABLES `encargados` WRITE;
/*!40000 ALTER TABLE `encargados` DISABLE KEYS */;
/*!40000 ALTER TABLE `encargados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estado_civil`
--

DROP TABLE IF EXISTS `estado_civil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `estado_civil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estado_civil` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado_civil`
--

LOCK TABLES `estado_civil` WRITE;
/*!40000 ALTER TABLE `estado_civil` DISABLE KEYS */;
INSERT INTO `estado_civil` VALUES (1,'Soltero/a'),(2,'Casado/a'),(3,'Divorciado/a'),(4,'Viudo/a');
/*!40000 ALTER TABLE `estado_civil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estudiantes`
--

DROP TABLE IF EXISTS `estudiantes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `estudiantes` (
  `nie` int(11) NOT NULL,
  `idEncargado` int(11) DEFAULT NULL,
  `idGs` int(11) DEFAULT NULL,
  `idPersona` int(11) DEFAULT NULL,
  PRIMARY KEY (`nie`),
  KEY `FK_estudiantes_idEncargado` (`idEncargado`),
  CONSTRAINT `FK_estudiantes_idEncargado` FOREIGN KEY (`idEncargado`) REFERENCES `encargados` (`idencargado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estudiantes`
--

LOCK TABLES `estudiantes` WRITE;
/*!40000 ALTER TABLE `estudiantes` DISABLE KEYS */;
/*!40000 ALTER TABLE `estudiantes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grados`
--

DROP TABLE IF EXISTS `grados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `grados` (
  `idGrado` int(11) NOT NULL AUTO_INCREMENT,
  `grado` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idGrado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grados`
--

LOCK TABLES `grados` WRITE;
/*!40000 ALTER TABLE `grados` DISABLE KEYS */;
/*!40000 ALTER TABLE `grados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grados_secciones`
--

DROP TABLE IF EXISTS `grados_secciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `grados_secciones` (
  `idGs` int(11) NOT NULL AUTO_INCREMENT,
  `year` int(11) DEFAULT NULL,
  `idGrado` int(11) DEFAULT NULL,
  `idPersonalAcademico` int(11) DEFAULT NULL,
  `idSeccion` int(11) DEFAULT NULL,
  PRIMARY KEY (`idGs`),
  KEY `FK_grados_secciones_idPersonalAcademico` (`idPersonalAcademico`),
  KEY `FK_grados_secciones_idGrado` (`idGrado`),
  KEY `FK_grados_secciones_idSeccion` (`idSeccion`),
  CONSTRAINT `FK_grados_secciones_idGrado` FOREIGN KEY (`idGrado`) REFERENCES `grados` (`idgrado`),
  CONSTRAINT `FK_grados_secciones_idPersonalAcademico` FOREIGN KEY (`idPersonalAcademico`) REFERENCES `personal_academico` (`idpersonalacademico`),
  CONSTRAINT `FK_grados_secciones_idSeccion` FOREIGN KEY (`idSeccion`) REFERENCES `secciones` (`idseccion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grados_secciones`
--

LOCK TABLES `grados_secciones` WRITE;
/*!40000 ALTER TABLE `grados_secciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `grados_secciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `historial_notificiaciones`
--

DROP TABLE IF EXISTS `historial_notificiaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `historial_notificiaciones` (
  `idNotificiacion` int(11) NOT NULL AUTO_INCREMENT,
  `cuerpo` longtext,
  `fecha` date DEFAULT NULL,
  `idPersonal` int(11) DEFAULT NULL,
  `prioridad` int(11) DEFAULT NULL,
  `tipo` int(11) DEFAULT NULL,
  PRIMARY KEY (`idNotificiacion`),
  KEY `FK_historial_notificiaciones_tipo` (`tipo`),
  KEY `FK_historial_notificiaciones_idPersonal` (`idPersonal`),
  KEY `FK_historial_notificiaciones_prioridad` (`prioridad`),
  CONSTRAINT `FK_historial_notificiaciones_idPersonal` FOREIGN KEY (`idPersonal`) REFERENCES `personal_academico` (`idpersonalacademico`),
  CONSTRAINT `FK_historial_notificiaciones_prioridad` FOREIGN KEY (`prioridad`) REFERENCES `prioridades` (`idprioridad`),
  CONSTRAINT `FK_historial_notificiaciones_tipo` FOREIGN KEY (`tipo`) REFERENCES `tipos_notificaciones` (`idtipo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `historial_notificiaciones`
--

LOCK TABLES `historial_notificiaciones` WRITE;
/*!40000 ALTER TABLE `historial_notificiaciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `historial_notificiaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `incidencias`
--

DROP TABLE IF EXISTS `incidencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `incidencias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(20) DEFAULT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `incidencias`
--

LOCK TABLES `incidencias` WRITE;
/*!40000 ALTER TABLE `incidencias` DISABLE KEYS */;
INSERT INTO `incidencias` VALUES (1,'SV001','Daños a Terceros en Bienes'),(2,'SV002','Servicio de Grúa'),(3,'SV003','Daño Superior'),(4,'SV004','Reposición de Llave'),(5,'SV005','Daños a Terceros'),(6,'SV006','Muerte Accidental'),(7,'SV007','Gastos Funerarios'),(8,'SV008','Ambulancia'),(9,'SV009','Maternidad'),(10,'SV10','Protesis'),(11,'SV11','Gastos Médicos');
/*!40000 ALTER TABLE `incidencias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `incidencias_cubiertas`
--

DROP TABLE IF EXISTS `incidencias_cubiertas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `incidencias_cubiertas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `n_seguro` int(11) DEFAULT NULL,
  `codigo` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `taller` int(11) DEFAULT NULL,
  `estado` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `n_seguro` (`n_seguro`),
  KEY `codigo` (`codigo`),
  KEY `taller` (`taller`),
  CONSTRAINT `codigo` FOREIGN KEY (`codigo`) REFERENCES `incidencias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `n_seguro` FOREIGN KEY (`n_seguro`) REFERENCES `asignacion_seguros` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `taller` FOREIGN KEY (`taller`) REFERENCES `talleres` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `incidencias_cubiertas`
--

LOCK TABLES `incidencias_cubiertas` WRITE;
/*!40000 ALTER TABLE `incidencias_cubiertas` DISABLE KEYS */;
/*!40000 ALTER TABLE `incidencias_cubiertas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marcas`
--

DROP TABLE IF EXISTS `marcas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `marcas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `marca` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=272 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marcas`
--

LOCK TABLES `marcas` WRITE;
/*!40000 ALTER TABLE `marcas` DISABLE KEYS */;
INSERT INTO `marcas` VALUES (1,'Abarth'),(2,'AC'),(3,'Acura'),(4,'Alfa Romero'),(5,'Alpina'),(6,'Alpine'),(7,'Apollo'),(8,'Arcfox'),(9,'Aria'),(10,'Ariel'),(11,'Aro'),(12,'Artega'),(13,'Asia'),(14,'Aspark'),(15,'Aston Martin'),(16,'Astro'),(17,'Audi'),(18,'Aurus'),(19,'Austin'),(20,'Autobianchi'),(21,'B.Engineering'),(22,'BAC'),(23,'Baltijas Dxips'),(24,'Baojun'),(25,'Bee Bee'),(26,'Beijing'),(27,'Bentley'),(28,'Bertone'),(29,'Bestune'),(30,'Bitter'),(31,'Blonell'),(32,'BMW'),(33,'Bordrin'),(34,'Borgward'),(35,'Brabham'),(36,'Brilliance'),(37,'Bristol'),(38,'Bufori'),(39,'Bugatti'),(40,'Buick'),(41,'BYD'),(42,'Cadillac'),(43,'Callaway'),(44,'Campagna'),(45,'Carbodies'),(46,'Caterham'),(47,'ChangAn'),(48,'ChangFeng'),(49,'Chery'),(50,'Chevrolet'),(51,'Chrysler'),(52,'Citroen'),(53,'Cizeta'),(54,'Corbellati'),(55,'Cupra'),(56,'Dacia'),(57,'Dadi'),(58,'Daewoo'),(59,'DAF'),(60,'Daihatsu'),(61,'Daimler'),(62,'Dallara'),(63,'Dallas'),(64,'Datsun'),(65,'David Brown'),(66,'DC'),(67,'De Lorean'),(68,'De Tomaso'),(69,'Derway'),(70,'Dodge'),(71,'Dongfeng'),(72,'Doninvest'),(73,'Donkervoort'),(74,'DR Automobiles'),(75,'DS'),(76,'e.GO'),(77,'Eadon Green'),(78,'Eagle'),(79,'Elemental'),(80,'Engler'),(81,'FAW'),(82,'Felino'),(83,'Ferrari'),(84,'Fiat'),(85,'Fittipaldi'),(86,'FOMM'),(87,'Force Motors'),(88,'Ford'),(89,'FSO'),(90,'Fuqi'),(91,'GAZ'),(92,'Geely'),(93,'Genesis'),(94,'Geo'),(95,'GFG Style'),(96,'Ginetta'),(97,'Gleagle'),(98,'GMC'),(99,'Great Wall'),(100,'Hafei'),(101,'Haima'),(102,'Haval'),(103,'Hennessey'),(104,'Hindustan'),(105,'Hispano Suiza'),(106,'Holden'),(107,'Honda'),(108,'HSV'),(109,'HuangHai'),(110,'Hummer'),(111,'Hurtan'),(112,'Hyundai'),(113,'IMSA'),(114,'Infiniti'),(115,'Innocenti'),(116,'Invicta'),(117,'Iran Khodro'),(118,'Irmscher'),(119,'Isdera'),(120,'IsoRivolta'),(121,'Isuzu'),(122,'Italdesign'),(123,'Iveco'),(124,'Izh'),(125,'JAC'),(126,'Jaguar'),(127,'Jeep'),(128,'Jiangling'),(129,'Karlmann King'),(130,'Karma'),(131,'Kia'),(132,'Koenigsegg'),(133,'KTM'),(134,'Lada'),(135,'Lamborghini'),(136,'Lancia'),(137,'Land Rover'),(138,'Landwind'),(139,'Lexus'),(140,'Lincoln'),(141,'Lister'),(142,'Lotus'),(143,'LTI'),(144,'LUAZ'),(145,'Luxgen'),(146,'Lvchi'),(147,'Mahindra'),(148,'Marcos'),(149,'Maruti'),(150,'Maserati'),(151,'Maxus'),(152,'Maybach'),(153,'Mazda'),(154,'MCC'),(155,'McLaren'),(156,'Mega'),(157,'Mercedes-Benz'),(158,'Mercury'),(159,'Metrocab'),(160,'MG'),(161,'Milan'),(162,'Minelli'),(163,'Minemobility'),(164,'Mini'),(165,'Mitsubishi'),(166,'Mitsuoka'),(167,'Monte Carlo'),(168,'Morgan'),(169,'Morris'),(170,'Moskvich'),(171,'MW motors'),(172,'Nissan'),(173,'Noble'),(174,'O.S.C.A.'),(175,'Oldsmobile'),(176,'Opel'),(177,'Pagani'),(178,'Panoz'),(179,'Pariss'),(180,'Paykan'),(181,'Perodua'),(182,'Peugeot'),(183,'Pininfarina'),(184,'Plymouth'),(185,'Polaris'),(186,'Polestar'),(187,'Pontiac'),(188,'Porsche'),(189,'Praga'),(190,'Premier'),(191,'Proton'),(192,'PUCH'),(193,'Puma'),(194,'Puritalia'),(195,'Quiantu'),(196,'Qoros'),(197,'Qvale'),(198,'RAM'),(199,'Ravon'),(200,'Reliant'),(201,'Renault'),(202,'Renault Samsung'),(203,'Rimac'),(204,'Rinspeed'),(205,'Rivian'),(206,'Roewe'),(207,'Rolls-Royce'),(208,'Ronart'),(209,'Rover'),(210,'RUF'),(211,'Saab'),(212,'Saleen'),(213,'Saturn'),(214,'Sbarro'),(215,'SCG'),(216,'Scion'),(217,'Seat'),(218,'SeAZ'),(219,'ShuangHuan'),(220,'Sin Cars'),(221,'Skoda'),(222,'SMA'),(223,'Smart'),(224,'Soueast'),(225,'Spectre'),(226,'Spyker'),(227,'SsangYong'),(228,'SSC'),(229,'Subaru'),(230,'Suzuki'),(231,'TagAz'),(232,'Talbot'),(233,'Tata'),(234,'Tatra'),(235,'Techrules'),(236,'Tesla'),(237,'Tianma'),(238,'Tianye'),(239,'Tofas'),(240,'Tonggong'),(241,'Toyota'),(242,'Trabant'),(243,'Triumph'),(244,'Trumpchi'),(245,'TVR'),(246,'UAZ'),(247,'Vanderhall'),(248,'Vauxhall'),(249,'Vector'),(250,'Vencer'),(251,'Venturi'),(252,'Vespa'),(253,'Vinfast'),(254,'Volkswagen'),(255,'Volvo'),(256,'VUHL'),(257,'VW-Porsche'),(258,'W Motors'),(259,'Wartburg'),(260,'Westfield'),(261,'WEY'),(262,'Wiesmann'),(263,'Xin Kai'),(264,'XPENG'),(265,'Zastava'),(266,'ZAZ'),(267,'Zenvo'),(268,'Zhindou'),(269,'ZIL'),(270,'Zotye'),(271,'ZX');
/*!40000 ALTER TABLE `marcas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modelos`
--

DROP TABLE IF EXISTS `modelos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `modelos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modelo` varchar(50) DEFAULT NULL,
  `marca` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `marcas` (`marca`),
  CONSTRAINT `marcas` FOREIGN KEY (`marca`) REFERENCES `marcas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=276 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modelos`
--

LOCK TABLES `modelos` WRITE;
/*!40000 ALTER TABLE `modelos` DISABLE KEYS */;
INSERT INTO `modelos` VALUES (1,'124 Spider',1),(2,'Aceca',2),(3,'Integra',3),(4,'Sensation',4),(5,'8C Competizione',5),(6,'Roadster S',6),(7,'IE',7),(8,'ECF',8),(9,'FXE',9),(10,'Atom',10),(11,'Spartana',11),(12,'Scalo',12),(13,'Retona',13),(14,'OWL',14),(15,'Rapide',15),(16,'11301',16),(17,'Coupe',17),(18,'Senat',18),(19,'Ambassador',19),(20,'A 112',20),(21,'Edonis',21),(22,'Mono',22),(23,'BD',23),(24,'E100',24),(25,'XS',25),(26,'BJ 2020',26),(27,'Brooklands',27),(28,'Freeclimber',28),(29,'T77',29),(30,'Type 3 Cabriolet',30),(31,'TF 2000 MK!',31),(32,'M2',32),(33,'iV6',33),(34,'BX5',34),(35,'BT62',35),(36,'A1',36),(37,'Fighter',37),(38,'La Joya',38),(39,'Veyron',39),(40,'Century',40),(41,'F6',41),(42,'Allante',42),(43,'C12',43),(44,'T-REX',44),(45,'Taxi',45),(46,'CSR',46),(47,'CS15',47),(48,'Flying',48),(49,'Tiggo',49),(50,'Blazer',50),(51,'Nassau',51),(52,'Berlingo',52),(53,'V16t',53),(54,'Corbellati',54),(55,'Ateca',55),(56,'Duster',56),(57,'Shuttle',57),(58,'Arcadia',58),(59,'66',59),(60,'Altis',60),(61,'Coupe',61),(62,'Stradale',62),(63,'Fun',63),(64,'GO',64),(65,'Mini Remastered',65),(66,'Avanti',66),(67,'Dmc-12',67),(68,'Guara',68),(69,'Aurora',69),(70,'Avenger',70),(71,'Rich',71),(72,'Kondor',72),(73,'D8',73),(74,'3',74),(75,'3',75),(76,'Life',76),(77,'Zanturi',77),(78,'Premier',78),(79,'Rp1',79),(80,'F.F Superquad',80),(81,'A6',81),(82,'cB7',82),(83,'208/308',83),(84,'124',84),(85,'EF7',85),(86,'Concept One',86),(87,'Gurkha',87),(88,'Aspire',88),(89,'Polonez',89),(90,'6500 (Land King)',90),(91,'Siber',91),(92,'Coolray',92),(93,'G70',93),(94,'Metro',94),(95,'Kangaroo',95),(96,'Akula',96),(97,'EV K12',97),(98,'Sierra HD',98),(99,'Pegasus',99),(100,'Brio',100),(101,'S5',101),(102,'F5',102),(103,'Venom F5',103),(104,'Lancer',104),(105,'Carmen',105),(106,'Astra',106),(107,'Accord',107),(108,'Avalanche',108),(109,'Antelope',109),(110,'H1',110),(111,'Albaycin',111),(112,'Accent',112),(113,'G-Class',113),(114,'Project Black S',114),(115,'Elba',115),(116,'S1',116),(117,'Samand',117),(118,'Coupe',118),(119,'Commendatore',119),(120,'Vision',120),(121,'D-Max',121),(122,'Da Vinci',122),(123,'Massif 4x4',123),(124,'2125',124),(125,'Rein',125),(126,'E-Pace',126),(127,'Cherokee',127),(128,'E-series',128),(129,'Stealth',129),(130,'Revero',130),(131,'Forte',131),(132,'Agera',132),(133,'X-Bow',133),(134,'1111 Oka',134),(135,'Asterion',135),(136,'Beta',136),(137,'Defender',137),(138,'SUV',138),(139,'CT',139),(140,'Corsair',140),(141,'LFT-666',141),(142,'3-Eleven',142),(143,'TX',143),(144,'969',144),(145,'M7',145),(146,'Venere',146),(147,'Marazzo',147),(148,'Mantis',148),(149,'Esteem',149),(150,'Chubasco',150),(151,'G10',151),(152,'Landaulet',152),(153,'818 Kombi',153),(154,'Smart',154),(155,'Senna',155),(156,'Monte Carlo',156),(157,'G-class',157),(158,'Capri',158),(159,'Taxi',159),(160,'Midget',160),(161,'Red',161),(162,'TF 1800',162),(163,'City EV',163),(164,'Covertible',164),(165,'Outandler',165),(166,'Galue',166),(167,'Monte Carlos',167),(168,'Plus Eight',168),(169,'Marina',169),(170,'2137 Kombi',170),(171,'Luka EV',171),(172,'200 SX',172),(173,'M12 GTO',173),(174,'2500 GT',174),(175,'Alero',175),(176,'Ampera',176),(177,'Huayra',177),(178,'Esperante',178),(179,'Pariss Electric',179),(180,'Paykan Saloon',180),(181,'Alza',181),(182,'408',182),(183,'HK GT',183),(184,'Acclaim',184),(185,'Slingshot',185),(186,'1',186),(187,'GTO',187),(188,'911',188),(189,'R1',189),(190,'Padmini',190),(191,'Iriz',191),(192,'G-modell',192),(193,'AM4',193),(194,'Berlinetta',194),(195,'K50',195),(196,'3',196),(197,'Mangusta',197),(198,'1500',198),(199,'Centra',199),(200,'Scimitar Sabre',200),(201,'30',201),(202,'QM3',202),(203,'CTwo',203),(204,'Etos',204),(205,'R1S',205),(206,'360',206),(207,'Ghost',207),(208,'Lightning',208),(209,'45',209),(210,'RtR',210),(211,'9-2X',211),(212,'S7',212),(213,'Aura',213),(214,'GTC',214),(215,'003',215),(216,'FR-S',216),(217,'Arona',217),(218,'1111',218),(219,'Sceo',219),(220,'R1',220),(221,'Roomster',221),(222,'C32',222),(223,'Fortwo',223),(224,'Lioncel',224),(225,'R42',225),(226,'C8',226),(227,'Chairman',227),(228,'Tuatara',228),(229,'BRZ',229),(230,'Cappuccino',230),(231,'Aquila',231),(232,'Murena',232),(233,'Aria',233),(234,'T700',234),(235,'Ren',235),(236,'Model S',236),(237,'Century',237),(238,'Admiral',238),(239,'131',239),(240,'TG',240),(241,'Avalon',241),(242,'P 601',242),(243,'TR 8',243),(244,'GS5',244),(245,'Cerbera',245),(246,'23632 Pickup',246),(247,'Venice',247),(248,'Astra',248),(249,'M12',249),(250,'Sarthe',250),(251,'210',251),(252,'Porter',252),(253,'Fadil',253),(254,'Amarok',254),(255,'460L',155),(256,'05',256),(257,'914',257),(258,'Lykan',258),(259,'353',259),(260,'Westfield',260),(261,'VV7',261),(262,'Roadster',262),(263,'SR-V X3',263),(264,'G3',264),(265,'10',265),(266,'1102',266),(267,'ST1',267),(268,'D1',268),(269,'114',269),(270,'T700',270),(271,'GrandTiger',271),(272,'Yaris',241),(273,'Juke',172),(274,'CBR 650 F',107),(275,'Harrier',241);
/*!40000 ALTER TABLE `modelos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `municipios`
--

DROP TABLE IF EXISTS `municipios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `municipios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `departamento` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `depto` (`departamento`),
  CONSTRAINT `depto` FOREIGN KEY (`departamento`) REFERENCES `departamentos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=255 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `municipios`
--

LOCK TABLES `municipios` WRITE;
/*!40000 ALTER TABLE `municipios` DISABLE KEYS */;
INSERT INTO `municipios` VALUES (1,'San Lorenzo',1),(2,'San Pedro Puxtla',1),(3,'Tacuba',1),(4,'Turín',1),(5,'Candelaria de la Frontera',2),(6,'Chalchuapa',2),(7,'Coatepeque',2),(8,'El Congo',2),(9,'El Porvenir',2),(10,'Masahuat',2),(11,'Metapán',2),(12,'San Antonio Pajonal',2),(13,'San Sebastián Salitrillo',2),(14,'Santa Ana',2),(15,'Santa Rosa Guachipilín',2),(16,'Santiago de la Frontera',2),(17,'Texistepeque',2),(18,'Acajutla',3),(19,'Armenia',3),(20,'Caluco',3),(21,'Cuisnahuat',3),(22,'Izalco',3),(23,'Juayúa',3),(24,'Nahuizalco',3),(25,'Nahulingo',3),(26,'Salcoatitán',3),(27,'San Antonio del Monte',3),(28,'San Julián',3),(29,'Santa Catarina Masahuat',3),(30,'Santa Isabel Ishuatán',3),(31,'Santo Domingo Guzmán',3),(32,'Sonsonate',3),(33,'Sonzacate',3),(34,'Agua Caliente',4),(35,'Arcatao',4),(36,'Azacualpa',4),(37,'Chalatenango (ciudad)',4),(38,'Comalapa',4),(39,'Citalá',4),(40,'Concepción Quezaltepeque',4),(41,'Dulce Nombre de María',4),(42,'El Carrizal',4),(43,'El Paraíso',4),(44,'La Laguna',4),(45,'La Palma',4),(46,'La Reina',4),(47,'Las Vueltas',4),(48,'Nueva Concepción',4),(49,'Nueva Trinidad',4),(50,'Nombre de Jesús',4),(51,'Ojos de Agua',4),(52,'Potonico',4),(53,'San Antonio de la Cruz',4),(54,'San Antonio Los Ranchos',4),(55,'San Fernando',4),(56,'San Francisco Lempa',4),(57,'San Francisco Morazán',4),(58,'San Ignacio',4),(59,'San Isidro Labrador',4),(60,'San José Cancasque',4),(61,'San José Las Flores',4),(62,'San Luis del Carmen',4),(63,'San Miguel de Mercedes',4),(64,'San Rafael',4),(65,'Santa Rita',4),(66,'Tejutla',4),(67,'Candelaria',5),(68,'Cojutepeque',5),(69,'El Carmen',5),(70,'El Rosario',5),(71,'Monte San Juan',5),(72,'Oratorio de Concepción',5),(73,'San Bartolomé Perulapía',5),(74,'San Cristóbal',5),(75,'San José Guayabal',5),(76,'San Pedro Perulapán',5),(77,'San Rafael Cedros',5),(78,'San Ramón',5),(79,'Santa Cruz Analquito',5),(80,'Santa Cruz Michapa',5),(81,'Suchitoto',5),(82,'Tenancingo',5),(83,'Aguilares',6),(84,'Apopa',6),(85,'Ayutuxtepeque',6),(86,'Cuscatancingo',6),(87,'Ciudad Delgado',6),(88,'El Paisnal',6),(89,'Guazapa',6),(90,'Ilopango',6),(91,'Mejicanos',6),(92,'Nejapa',6),(93,'Panchimalco',6),(94,'Rosario de Mora',6),(95,'San Marcos',6),(96,'San Martín',6),(97,'San Salvador',6),(98,'Santiago Texacuangos',6),(99,'Santo Tomás',6),(100,'Soyapango',6),(101,'Tonacatepeque',6),(102,'Antiguo Cuscatlán',7),(103,'Chiltiupán',7),(104,'Ciudad Arce',7),(105,'Colón',7),(106,'Comasagua',7),(107,'Huizúcar',7),(108,'Jayaque',7),(109,'Jicalapa',7),(110,'La Libertad',7),(111,'Nueva San Salvador (Santa Tecla)',7),(112,'Nuevo Cuscatlán',7),(113,'San Juan Opico',7),(114,'Quezaltepeque',7),(115,'Sacacoyo',7),(116,'San José Villanueva',7),(117,'San Matías',7),(118,'San Pablo Tacachico',7),(119,'Talnique',7),(120,'Tamanique',7),(121,'Teotepeque',7),(122,'Tepecoyo',7),(123,'Zaragoza',7),(124,'Apastepeque',8),(125,'Guadalupe',8),(126,'San Cayetano Istepeque',8),(127,'San Esteban Catarina',8),(128,'San Ildefonso',8),(129,'San Lorenzo',8),(130,'San Sebastián',8),(131,'San Vicente',8),(132,'Santa Clara',8),(133,'Santo Domingo',8),(134,'Tecoluca',8),(135,'Tepetitán',8),(136,'Verapaz',8),(137,'Cinquera',9),(138,'Dolores',9),(139,'Guacotecti',9),(140,'Ilobasco',9),(141,'Jutiapa',9),(142,'San Isidro',9),(143,'Sensuntepeque',9),(144,'Tejutepeque',9),(145,'Victoria',9),(146,'Cuyultitán',10),(147,'El Rosario',10),(148,'Jerusalén',10),(149,'Mercedes La Ceiba',10),(150,'Olocuilta',10),(151,'Paraíso de Osorio',10),(152,'San Antonio Masahuat',10),(153,'San Emigdio',10),(154,'San Francisco Chinameca',10),(155,'San Juan Nonualco',10),(156,'San Juan Talpa',10),(157,'San Juan Tepezontes',10),(158,'San Luis Talpa',10),(159,'San Luis La Herradura',10),(160,'San Miguel Tepezontes',10),(161,'San Pedro Masahuat',10),(162,'San Pedro Nonualco',10),(163,'San Rafael Obrajuelo',10),(164,'Santa María Ostuma',10),(165,'Santiago Nonualco',10),(166,'Tapalhuaca',10),(167,'Zacatecoluca',10),(168,'Alegría',11),(169,'Berlín',11),(170,'California',11),(171,'Concepción Batres',11),(172,'El Triunfo',11),(173,'Ereguayquín',11),(174,'Estanzuelas',11),(175,'Jiquilisco',11),(176,'Jucuapa',11),(177,'Jucuarán',11),(178,'Mercedes Umaña',11),(179,'Nueva Granada',11),(180,'Ozatlán',11),(181,'Puerto El Triunfo',11),(182,'San Agustín',11),(183,'San Buenaventura',11),(184,'San Dionisio',11),(185,'San Francisco Javier',11),(186,'Santa Elena',11),(187,'Santa María',11),(188,'Santiago de María',11),(189,'Tecapán',11),(190,'Usulután',11),(191,'Carolina',12),(192,'Chapeltique',12),(193,'Chinameca',12),(194,'Chirilagua',12),(195,'Ciudad Barrios',12),(196,'Comacarán',12),(197,'El Tránsito',12),(198,'Lolotique',12),(199,'Moncagua',12),(200,'Nueva Guadalupe',12),(201,'Nuevo Edén de San Juan',12),(202,'Quelepa',12),(203,'San Antonio del Mosco',12),(204,'San Gerardo',12),(205,'San Jorge',12),(206,'San Luis de la Reina',12),(207,'San Miguel',12),(208,'San Rafael Oriente',12),(209,'Sesori',12),(210,'Uluazapa',12),(211,'Arambala',13),(212,'Cacaopera',13),(213,'Chilanga',13),(214,'Corinto',13),(215,'Delicias de Concepción',13),(216,'El Divisadero',13),(217,'El Rosario',13),(218,'Gualococti',13),(219,'Guatajiagua',13),(220,'Joateca',13),(221,'Jocoaitique',13),(222,'Jocoro',13),(223,'Lolotiquillo',13),(224,'Meanguera',13),(225,'Osicala',13),(226,'Perquín',13),(227,'San Carlos',13),(228,'San Fernando',13),(229,'San Francisco Gotera',13),(230,'San Isidro',13),(231,'San Simón',13),(232,'Sensembra',13),(233,'Sociedad',13),(234,'Torola',13),(235,'Yamabal',13),(236,'Yoloaiquín',13),(237,'Anamorós',14),(238,'Bolivar',14),(239,'Concepción de Oriente',14),(240,'Conchagua',14),(241,'El Carmen',14),(242,'El Sauce',14),(243,'Intipucá',14),(244,'La Unión',14),(245,'Lislique',14),(246,'Meanguera del Golfo',14),(247,'Nueva Esparta',14),(248,'Pasaquina',14),(249,'Polorós',14),(250,'San Alejo',14),(251,'San José',14),(252,'Santa Rosa de Lima',14),(253,'Yayantique',14),(254,'Yucuaiquín',14);
/*!40000 ALTER TABLE `municipios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pago_seguros`
--

DROP TABLE IF EXISTS `pago_seguros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `pago_seguros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poliza` int(11) DEFAULT NULL,
  `cantidad` double(6,2) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `usuario` int(11) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT NULL,
  `surcusal` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `poliza` (`poliza`),
  KEY `usuario_pago` (`usuario`),
  KEY `surcusal_pago` (`surcusal`),
  CONSTRAINT `poliza` FOREIGN KEY (`poliza`) REFERENCES `asignacion_seguros` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `surcusal_pago` FOREIGN KEY (`surcusal`) REFERENCES `sucursales` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `usuario_pago` FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=312 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pago_seguros`
--

LOCK TABLES `pago_seguros` WRITE;
/*!40000 ALTER TABLE `pago_seguros` DISABLE KEYS */;
INSERT INTO `pago_seguros` VALUES (1,1,250.00,'2017-06-03 13:05:06',10,1,2),(2,1,250.00,'2017-07-12 09:56:12',10,1,2),(3,1,250.00,'2017-08-01 15:06:50',10,1,2),(4,1,267.50,'2017-09-16 10:50:49',10,2,2),(5,1,250.00,'2017-10-15 14:29:23',10,1,2),(6,1,250.00,'2017-11-07 15:50:02',10,1,2),(7,1,250.00,'2017-12-14 17:00:00',10,1,3),(8,1,267.50,'2018-01-27 10:01:49',10,2,3),(9,1,250.00,'2018-02-09 15:00:49',10,1,2),(10,1,250.00,'2018-03-03 11:49:03',10,1,2),(11,1,250.00,'2018-04-11 16:15:00',10,1,2),(12,1,267.50,'2018-05-25 16:49:50',10,2,2),(13,1,250.00,'2018-06-05 16:00:50',10,1,2),(14,1,250.00,'2018-07-09 15:19:50',10,1,2),(15,1,250.00,'2018-08-12 16:39:50',10,1,2),(16,1,267.50,'2018-09-20 13:29:50',10,2,2),(17,1,250.00,'2018-10-11 16:49:50',10,1,2),(18,1,250.00,'2018-11-03 16:49:50',10,1,2),(19,1,250.00,'2018-12-15 10:49:50',10,1,2),(20,1,267.50,'2019-01-20 13:49:50',10,2,2),(21,1,250.00,'2019-02-10 12:49:50',10,1,2),(22,1,250.00,'2019-03-02 11:49:50',10,1,2),(23,1,267.50,'2019-04-27 14:49:50',10,2,2),(24,1,250.00,'2019-05-13 14:49:50',10,1,2),(25,1,250.00,'2019-06-15 10:49:50',10,1,2),(26,1,267.50,'2019-07-20 08:49:50',10,2,2),(27,1,250.00,'2019-08-10 13:49:50',10,1,2),(28,1,250.00,'2019-09-02 09:49:50',10,1,2),(29,1,267.50,'2019-10-27 11:49:50',10,2,2),(30,2,69.45,'2017-07-09 09:48:09',10,1,3),(31,2,69.45,'2017-08-09 09:48:13',10,1,3),(32,2,69.45,'2017-09-09 09:48:17',10,1,3),(33,2,69.45,'2017-10-09 09:48:24',10,1,3),(34,2,69.45,'2017-11-09 09:48:25',10,1,3),(35,2,69.45,'2017-12-09 09:48:29',10,1,4),(36,2,69.45,'2018-01-09 09:48:33',10,1,3),(37,2,69.45,'2018-02-09 09:48:34',10,1,3),(38,2,69.45,'2018-03-02 09:48:09',10,1,3),(39,2,69.45,'2018-04-06 09:48:13',10,1,3),(40,2,69.45,'2018-05-09 09:48:17',10,1,3),(41,2,69.45,'2018-06-09 09:48:24',10,1,3),(42,2,69.45,'2018-07-09 09:48:25',10,1,3),(43,2,69.45,'2018-08-09 09:48:29',10,1,4),(44,2,69.45,'2018-09-08 09:48:33',10,1,3),(45,2,74.31,'2018-10-20 09:48:34',10,2,3),(46,2,69.45,'2018-11-09 09:48:09',10,1,3),(47,2,69.45,'2018-12-15 09:48:13',10,1,3),(48,2,69.45,'2019-01-15 09:48:17',10,1,3),(49,2,69.45,'2019-02-08 09:48:24',10,1,3),(50,2,69.45,'2019-03-05 09:48:25',10,1,3),(51,2,69.45,'2019-04-01 09:48:29',10,1,4),(52,2,74.31,'2019-05-19 09:48:33',10,2,3),(53,2,69.45,'2019-06-11 09:48:34',10,1,3),(54,2,74.31,'2019-07-20 09:48:09',10,2,3),(55,2,69.45,'2019-08-09 09:48:13',10,1,3),(56,2,69.45,'2019-09-02 09:48:17',10,1,3),(57,2,69.45,'2019-10-10 09:48:24',10,1,3),(58,2,74.31,'2019-11-29 09:48:25',10,2,3),(59,3,194.45,'2019-08-15 09:56:13',10,1,1),(60,3,194.45,'2018-01-09 10:45:50',10,1,1),(61,3,194.45,'2018-02-15 13:29:56',10,1,1),(62,3,194.45,'2018-04-02 09:20:39',10,1,1),(63,3,194.45,'2018-05-04 11:49:50',10,1,1),(64,3,208.06,'2018-06-18 15:40:59',10,2,1),(65,3,194.45,'2018-07-15 14:20:56',10,1,1),(66,3,194.45,'2018-08-09 00:00:00',10,1,1),(67,3,208.06,'2018-09-20 00:00:00',10,2,1),(68,3,194.45,'2018-10-13 16:12:15',10,1,1),(69,3,208.06,'2018-11-16 10:45:58',10,2,1),(70,3,208.06,'2018-03-16 10:50:49',10,2,2),(71,3,194.45,'2018-12-03 13:20:05',10,1,1),(72,3,194.45,'2019-01-09 10:45:50',10,1,1),(73,3,194.45,'2019-02-15 13:29:56',10,1,1),(74,3,194.45,'2019-04-02 09:20:39',10,1,1),(75,3,194.45,'2019-05-04 11:49:50',10,1,1),(76,3,208.06,'2019-06-18 15:40:59',10,2,1),(77,3,194.45,'2019-07-15 14:20:56',10,1,1),(78,3,194.45,'2019-08-09 00:00:00',10,1,1),(79,3,208.06,'2019-09-20 00:00:00',10,2,1),(80,3,194.45,'2019-10-13 16:12:15',10,1,1),(81,3,208.06,'2019-11-16 10:45:58',10,2,1),(82,3,208.06,'2019-03-16 10:50:49',10,2,2),(83,4,166.67,'2018-01-09 09:55:24',10,1,1),(84,4,166.67,'2018-02-09 09:55:24',10,1,2),(85,4,166.67,'2018-03-09 09:55:24',10,1,2),(86,4,166.67,'2018-04-09 09:55:26',10,1,2),(87,4,166.67,'2018-05-05 09:55:29',10,1,2),(88,4,166.67,'2018-06-09 09:55:31',10,1,2),(89,4,166.67,'2018-07-09 09:55:33',10,1,2),(90,4,166.67,'2018-08-09 09:55:35',10,1,2),(91,4,178.34,'2018-09-19 09:55:38',10,2,2),(92,4,166.67,'2018-10-15 09:55:40',10,1,3),(93,4,178.34,'2018-11-20 09:55:45',10,2,3),(94,4,166.67,'2018-12-09 09:55:48',10,1,3),(95,4,166.67,'2019-01-09 09:55:50',10,1,3),(96,4,166.67,'2019-02-14 09:55:52',10,1,3),(97,4,166.67,'2019-03-09 09:55:55',10,1,3),(98,4,166.67,'2019-04-09 09:55:59',10,1,3),(99,4,166.67,'2019-05-09 09:56:02',10,1,1),(100,4,166.67,'2019-06-09 09:56:05',10,1,1),(101,4,166.67,'2019-07-09 09:56:07',10,1,1),(102,4,166.67,'2019-08-15 09:56:13',10,1,1),(103,4,166.67,'2019-11-13 10:49:50',10,1,1),(104,4,166.67,'2019-09-09 09:56:05',10,1,1),(105,4,166.67,'2019-10-09 09:56:07',10,1,1),(106,5,178.00,'2018-01-09 10:25:52',10,1,1),(107,5,178.00,'2018-02-09 10:25:53',10,1,1),(108,5,178.00,'2018-03-09 10:25:54',10,1,1),(109,5,178.00,'2018-04-09 10:25:55',10,1,1),(110,5,178.00,'2018-05-09 10:25:56',10,1,1),(111,5,178.00,'2018-06-09 10:25:57',10,1,1),(112,5,178.00,'2018-07-09 10:25:58',10,1,1),(113,5,178.00,'2018-08-09 10:25:59',10,1,1),(114,5,178.00,'2018-09-09 10:25:20',10,1,1),(115,5,178.00,'2018-10-09 10:25:50',10,1,1),(116,5,178.00,'2018-11-09 10:25:50',10,1,1),(117,5,178.00,'2018-12-09 10:25:50',10,1,1),(118,5,178.00,'2019-01-09 10:25:50',10,1,2),(119,5,178.00,'2019-02-09 10:25:50',10,1,2),(120,5,178.00,'2019-03-09 10:25:50',10,1,1),(121,5,178.00,'2019-04-09 10:25:50',10,1,1),(122,5,178.00,'2019-05-09 10:25:50',10,1,1),(123,5,178.00,'2019-06-09 10:25:50',10,1,1),(124,5,178.00,'2019-07-09 10:25:50',10,1,1),(125,5,178.00,'2019-08-09 10:25:50',10,1,1),(126,5,178.00,'2019-10-09 10:25:50',10,1,1),(127,5,178.00,'2019-11-09 10:25:50',10,1,1),(128,6,194.45,'2018-02-09 10:25:53',10,1,1),(129,6,194.45,'2018-03-09 10:25:54',10,1,1),(130,6,194.45,'2018-04-09 10:25:55',10,1,1),(131,6,194.45,'2018-05-09 10:25:56',10,1,1),(132,6,194.45,'2018-06-09 10:25:57',10,1,1),(133,6,194.45,'2018-07-09 10:25:58',10,1,1),(134,6,194.45,'2018-08-09 10:25:59',10,1,1),(135,6,194.45,'2018-09-09 10:25:06',10,1,1),(136,6,194.45,'2018-10-09 10:25:40',10,1,1),(137,6,194.45,'2018-11-09 10:25:26',10,1,1),(138,6,194.45,'2018-12-09 10:25:02',10,1,1),(139,6,194.45,'2019-01-09 10:25:50',10,1,1),(140,6,194.45,'2019-02-09 10:25:50',10,1,1),(141,6,194.45,'2019-03-09 10:25:50',10,1,1),(142,6,194.45,'2019-04-09 10:25:50',10,1,1),(143,6,194.45,'2019-05-09 10:25:50',10,1,1),(144,6,194.45,'2019-06-09 10:25:50',10,1,1),(145,6,194.45,'2019-07-09 10:25:50',10,1,1),(146,6,194.45,'2019-08-09 10:25:50',10,1,1),(147,6,194.45,'2019-10-09 10:25:50',10,1,1),(148,6,194.45,'2019-11-09 10:25:50',10,1,3),(149,7,104.17,'2018-03-10 10:25:54',10,1,3),(150,7,104.17,'2018-04-09 10:25:55',10,1,3),(151,7,104.17,'2018-05-09 10:25:56',10,1,3),(152,7,104.17,'2018-06-09 10:25:57',10,1,3),(153,7,104.17,'2018-07-09 10:25:58',10,1,3),(154,7,104.17,'2018-08-09 10:25:59',10,1,3),(155,7,104.17,'2018-09-09 10:25:25',10,1,3),(156,7,104.17,'2018-10-09 10:25:50',10,1,3),(157,7,104.17,'2018-11-09 10:25:50',10,1,3),(158,7,104.17,'2018-12-09 10:25:50',10,1,3),(159,7,104.17,'2019-01-09 10:25:50',10,1,3),(160,7,104.17,'2019-02-09 10:25:50',10,1,3),(161,7,104.17,'2019-03-09 10:25:50',10,1,3),(162,7,104.17,'2019-04-09 10:25:50',10,1,3),(163,7,104.17,'2019-05-09 10:25:50',10,1,3),(164,7,104.17,'2019-06-09 10:25:50',10,1,3),(165,7,104.17,'2019-07-09 10:25:50',10,1,3),(166,7,104.17,'2019-08-09 10:25:50',10,1,3),(167,7,104.17,'2019-10-09 10:25:50',10,1,3),(168,7,104.17,'2019-11-09 10:25:50',10,1,3),(169,8,104.17,'2018-04-09 10:25:55',10,1,2),(170,8,104.17,'2018-05-09 10:25:56',10,1,2),(171,8,104.17,'2018-06-09 10:25:57',10,1,2),(172,8,104.17,'2018-07-09 10:25:58',10,1,2),(173,8,104.17,'2018-08-09 10:25:59',10,1,2),(174,8,104.17,'2018-09-09 10:25:15',10,1,2),(175,8,104.17,'2018-10-09 10:25:50',10,1,2),(176,8,104.17,'2018-11-09 10:25:50',10,1,2),(177,8,104.17,'2018-12-09 10:25:50',10,1,2),(178,8,104.17,'2019-01-09 10:25:50',10,1,2),(179,8,104.17,'2019-02-09 10:25:50',10,1,2),(180,8,104.17,'2019-03-09 10:25:50',10,1,2),(181,8,104.17,'2019-04-09 10:25:50',10,1,2),(182,8,104.17,'2019-05-09 10:25:50',10,1,2),(183,8,104.17,'2019-06-09 10:25:50',10,1,2),(184,8,104.17,'2019-07-09 10:25:50',10,1,2),(185,8,104.17,'2019-08-09 10:25:50',10,1,2),(186,8,104.17,'2019-10-09 10:25:50',10,1,2),(187,8,104.17,'2019-11-09 10:25:50',10,1,2),(188,9,250.00,'2018-03-09 10:25:54',10,1,2),(189,9,250.00,'2018-04-09 10:25:55',10,1,2),(190,9,250.00,'2018-05-09 10:25:56',10,1,2),(191,9,250.00,'2018-06-09 10:25:57',10,1,2),(192,9,250.00,'2018-07-09 10:25:58',10,1,2),(193,9,250.00,'2018-08-09 10:25:59',10,1,2),(194,9,250.00,'2018-09-09 10:25:05',10,1,2),(195,9,250.00,'2018-10-09 10:25:50',10,1,2),(196,9,250.00,'2018-11-09 10:25:50',10,1,2),(197,9,250.00,'2018-12-09 10:25:50',10,1,2),(198,9,250.00,'2019-01-09 10:25:50',10,1,2),(199,9,250.00,'2019-02-09 10:25:50',10,1,2),(200,9,250.00,'2019-03-09 10:25:50',10,1,2),(201,9,250.00,'2019-04-09 10:25:50',10,1,2),(202,9,250.00,'2019-05-09 10:25:50',10,1,2),(203,9,250.00,'2019-06-09 10:25:50',10,1,2),(204,9,250.00,'2019-07-09 10:25:50',10,1,2),(205,9,250.00,'2019-08-09 10:25:50',10,1,2),(206,9,250.00,'2019-10-09 10:25:50',10,1,2),(207,9,250.00,'2019-11-09 10:25:50',10,1,2),(208,10,69.45,'2018-04-09 10:25:55',10,1,2),(209,10,69.45,'2018-05-09 10:25:56',10,1,2),(210,10,69.45,'2018-06-09 10:25:57',10,1,2),(211,10,69.45,'2018-07-09 10:25:58',10,1,2),(212,10,69.45,'2018-08-09 10:25:59',10,1,2),(213,10,69.45,'2018-09-09 10:25:00',10,1,2),(214,10,69.45,'2018-10-09 10:25:50',10,1,2),(215,10,69.45,'2018-11-09 10:25:50',10,1,2),(216,10,69.45,'2018-12-09 10:25:50',10,1,2),(217,10,69.45,'2019-01-09 10:25:50',10,1,2),(218,10,69.45,'2019-02-09 10:25:50',10,1,2),(219,10,69.45,'2019-03-09 10:25:50',10,1,2),(220,10,69.45,'2019-04-09 10:25:50',10,1,2),(221,10,69.45,'2019-05-09 10:25:50',10,1,2),(222,10,69.45,'2019-06-09 10:25:50',10,1,2),(223,10,69.45,'2019-07-09 10:25:50',10,1,2),(224,10,69.45,'2019-08-09 10:25:50',10,1,2),(225,10,69.45,'2019-10-09 10:25:50',10,1,2),(226,10,69.45,'2019-11-09 10:25:50',10,1,2),(227,11,178.00,'2018-10-09 10:25:50',10,1,1),(228,11,178.00,'2018-11-09 10:25:50',10,1,1),(229,11,178.00,'2018-12-09 10:25:50',10,1,1),(230,11,178.00,'2019-01-09 10:25:50',10,1,1),(231,11,178.00,'2019-02-09 10:25:50',10,1,1),(232,11,178.00,'2019-03-09 10:25:50',10,1,1),(233,11,178.00,'2019-04-09 10:25:50',10,1,1),(234,11,178.00,'2019-05-09 10:25:50',10,1,1),(235,11,178.00,'2019-06-09 10:25:50',10,1,1),(236,11,178.00,'2019-07-09 10:25:50',10,1,1),(237,11,178.00,'2019-08-09 10:25:50',10,1,1),(238,11,178.00,'2019-10-09 10:25:50',10,1,1),(239,11,178.00,'2019-11-09 10:25:50',10,1,1),(240,12,69.45,'2019-02-09 10:25:06',10,1,2),(241,12,69.45,'2019-03-09 10:25:09',10,1,2),(242,12,69.45,'2019-04-09 10:25:25',10,1,2),(243,12,69.45,'2019-05-09 10:25:29',10,1,2),(244,12,69.45,'2019-06-09 10:25:10',10,1,2),(245,12,69.45,'2019-07-09 10:25:28',10,1,2),(246,12,69.45,'2019-08-09 10:25:00',10,1,2),(247,12,69.45,'2019-09-09 10:25:00',10,1,2),(248,12,69.45,'2019-10-09 10:25:19',10,1,2),(249,12,69.45,'2019-11-09 10:25:12',10,1,2),(250,13,166.67,'2018-10-09 10:25:50',10,1,2),(251,13,166.67,'2018-11-09 10:25:50',10,1,2),(252,13,166.67,'2018-12-09 10:25:50',10,1,2),(253,13,166.67,'2019-01-09 10:25:50',10,1,2),(254,13,166.67,'2019-02-09 10:25:50',10,1,2),(255,13,166.67,'2019-03-09 10:25:50',10,1,2),(256,13,166.67,'2019-04-09 10:25:50',10,1,2),(257,13,166.67,'2019-05-09 10:25:50',10,1,2),(258,13,166.67,'2019-06-09 10:25:50',10,1,2),(259,13,166.67,'2019-07-09 10:25:50',10,1,2),(260,13,166.67,'2019-08-09 10:25:50',10,1,2),(261,13,166.67,'2019-10-09 10:25:02',10,1,2),(262,13,166.67,'2019-11-09 10:25:58',10,1,2),(263,13,166.67,'2019-12-09 09:47:55',10,1,2);
/*!40000 ALTER TABLE `pago_seguros` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `mora_pago` BEFORE INSERT ON `pago_seguros` FOR EACH ROW BEGIN
if dayofmonth(new.fecha) >  15 
then
set new.cantidad = new.cantidad+(new.cantidad*0.07);
set new.estado = 2;
else
set new.estado =1;
end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `paquetes`
--

DROP TABLE IF EXISTS `paquetes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `paquetes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` int(11) DEFAULT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `monto` double(8,2) DEFAULT NULL,
  `incidencia_paquetes` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `incidencia_paquetes` (`incidencia_paquetes`),
  KEY `tipo_seguro_paquete` (`tipo`),
  CONSTRAINT `incidencia_paquetes` FOREIGN KEY (`incidencia_paquetes`) REFERENCES `incidencias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tipo_seguro_paquete` FOREIGN KEY (`tipo`) REFERENCES `tipo_seguros` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paquetes`
--

LOCK TABLES `paquetes` WRITE;
/*!40000 ALTER TABLE `paquetes` DISABLE KEYS */;
INSERT INTO `paquetes` VALUES (1,1,'Básico',69.45,1),(2,1,'Plus',178.00,2),(3,1,'Premium',250.00,3),(4,2,'Básico',104.17,6),(5,2,'Plus',166.67,7),(6,2,'Premium',194.45,8);
/*!40000 ALTER TABLE `paquetes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_academico`
--

DROP TABLE IF EXISTS `personal_academico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `personal_academico` (
  `idPersonalAcademico` int(11) NOT NULL AUTO_INCREMENT,
  `detalles` varchar(255) DEFAULT NULL,
  `estadoPersonal` varchar(255) DEFAULT NULL,
  `idCargo` int(11) DEFAULT NULL,
  `idPersona` int(11) DEFAULT NULL,
  PRIMARY KEY (`idPersonalAcademico`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_academico`
--

LOCK TABLES `personal_academico` WRITE;
/*!40000 ALTER TABLE `personal_academico` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_academico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personas`
--

DROP TABLE IF EXISTS `personas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `personas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `apellido` varchar(50) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `edad` int(11) DEFAULT NULL,
  `sexo` varchar(15) DEFAULT NULL,
  `dui` varchar(11) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `municipio` int(11) DEFAULT NULL,
  `telefono` varchar(9) DEFAULT NULL,
  `correo` text,
  `tipo` int(11) DEFAULT NULL,
  `estado_civil` int(11) DEFAULT NULL,
  `profesion` varchar(50) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tipo_user` (`tipo`),
  KEY `estado_civi` (`estado_civil`),
  KEY `municipio_persona` (`municipio`),
  CONSTRAINT `estado_civi` FOREIGN KEY (`estado_civil`) REFERENCES `estado_civil` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `municipio_persona` FOREIGN KEY (`municipio`) REFERENCES `municipios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tipo_user` FOREIGN KEY (`tipo`) REFERENCES `tipo_usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personas`
--

LOCK TABLES `personas` WRITE;
/*!40000 ALTER TABLE `personas` DISABLE KEYS */;
INSERT INTO `personas` VALUES (1,'Amanda','Quintanilla','2001-12-18',18,'Femenino','102457896-9','Col. Las Rosas',1,'7878-9696','amanda@gmail.com',5,1,'Estudiante',1),(2,'René Adonay','Campos Meléndez','2000-07-27',19,'Masculino','060841570-2','Mariona',97,'7237-3671','adocampos27@gmail.com',5,1,'Estudiante',1),(3,'Erick Uvaldo','Gómez','2000-06-14',26,'Masculino','012458963-6','Col. Monte Carmelo',2,'6356-8989','erick45@gmail.com',5,2,'Albañil',1),(4,'Alejandro','Saenz','1990-07-27',25,'Masculino','060841570-2','Mariona',3,'7237-3671','ale20@gmail.com',5,1,'Estudiante',1),(5,'José','Larios','1980-06-15',30,'Masculino','0124578-2','Apopa',13,'7201-3671','larios1@gmail.com',5,1,'Estudiante',1),(6,'Luis','Enrique','1999-10-01',20,'Masculino','012478569-8','Caserio Los Angeles',6,'6547-5603','luis12@gmail.com',5,3,'Albañil',1),(7,'Tamara Sofia','Lopez','2000-10-21',19,'Femenino','2415457-8','Col. Los Altos',12,'6547-5603','sofia1254@gmail.com',5,3,'Albañil',1),(8,'Rene Alejandro','Peraza Fuentes','2000-10-21',20,'Masculino','2415457-8','Col. America #2',12,'6547-5603','Ale@gmail.com',5,3,'Programador',1),(9,'Carlos Rodrigo','Garcia Sigüenza','2000-09-19',19,'Masculino','058987988-1','San Martin',96,'7895-0236','carRodrigo@gmail.com',1,1,'Estudiante',1),(10,'Sarai','Aguilera Hernandez','1999-01-25',20,'Femenino','070269812-5','Zacamil',91,'7036-8920','sarai@hotmail.com',3,1,'Estudiante',1),(11,'Raúl Orlando','Ramirez Pineda','2000-08-18',19,'Masculino','069032615-2','Antiguo Cuscatlan',102,'7369-8900','raor@gmail.com',4,1,'Estudiante',1),(12,'Mario Eduardo','Sibrián Rodriguez','1990-12-03',29,'Masculino','05098713-2','Turín',4,'7896-2036','maredua@gmail.com',2,2,'Profesor en Matemáticas',1),(13,'Gabriela Patricia','Barahona Sorto','1989-12-01',30,'Femenino','03565988-9','Las Margaritas',100,'6890-3780','paty08@gmail.com',2,2,'Recepcionista',1),(14,'Samuel Alejandro','Romero Aguilar','1999-06-08',20,'Masculino','06890323-0','Zacamil',91,'7023-9870','sam286@hotmail.com',2,1,'Estudiante',1),(15,'Daniela Sofia','Lopez Martinez','1990-08-05',29,'Femenino','04659801-1','San Salvador',97,'7895-0030','daniSo@gmail.com',2,1,'Cantante',1),(16,'Mauricio Alejandro','Villalta Gomez','1989-12-01',40,'Masculino','03456220-1','Jiquilisco',175,'6989-0312','mauriGome_08@gmailcom',2,2,'Contador',1),(17,'Carlos Edmundo','Escobar Alas','1990-09-01',29,'Masculino','05100359-3','San Bartolo',90,'7012-3690','edmundo58@gmail.com',2,1,'Repartidor de Comida Rápida',1),(18,'Diana Luna','Flores Perez','1995-05-05',24,'Femenino','06103985-0','Santa Maria',85,'7004-9803','lunaF@hotmail.com',2,1,'Contador',1),(19,'Tatiana Guadalupe','Cartagena Martinez','1985-02-27',34,'Femenino','05012980-1','Santa Tecla',111,'6230-8970','tatGuada26@gmail.com',2,1,'Arquitecto',1),(20,'Karla Olivia','Sanchez Ortiz','1989-05-01',30,'Femenino','05231068-0','San Felipe',90,'7009-1855','kolivia10@gmail.com',2,1,'Recepcionista',1),(21,'Fatima Lizzet','Garcia Castro','1980-04-02',39,'Femenino','04589512-1','San Salvador',97,'7889-0356','lizzet59_f@hotmail.com',2,2,'Arquitecto',1),(22,'Saul Alexis','Rivas Villalta','1979-02-09',40,'Masculino','05698001-1','Cojutepeque',68,'6201-2890','sal_Alex@hotmail.com',2,2,'Albañil',1),(23,'Miguel Andres','Martinez Marinero','1995-10-08',24,'Masculino','07520061-4','San Martin',111,'7003-5491','miguel1452@gmail.com',2,1,'Cajero',1),(24,'Ailyn Fernanda','Lopez Cuellar','1990-03-18',29,'Femenino','05852006-5','Satelite',97,'7056-5986','aiFerna@gmail.com',2,1,'Supervisora',1),(25,'Alberto Jorge','Meléndez Rodriguez','1990-09-25',29,'Masculino','06898620-0','Ayutuxtepeque',85,'7357-0156','jorgAlb@hotmail.com',2,1,'Mecánico',1);
/*!40000 ALTER TABLE `personas` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `estado_pe` BEFORE INSERT ON `personas` FOR EACH ROW begin
set new.estado= 1;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `prioridades`
--

DROP TABLE IF EXISTS `prioridades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `prioridades` (
  `idPrioridad` int(11) NOT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idPrioridad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prioridades`
--

LOCK TABLES `prioridades` WRITE;
/*!40000 ALTER TABLE `prioridades` DISABLE KEYS */;
/*!40000 ALTER TABLE `prioridades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `secciones`
--

DROP TABLE IF EXISTS `secciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `secciones` (
  `idSeccion` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idSeccion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `secciones`
--

LOCK TABLES `secciones` WRITE;
/*!40000 ALTER TABLE `secciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `secciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sucursales`
--

DROP TABLE IF EXISTS `sucursales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `sucursales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `municipio` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `munp` (`municipio`),
  CONSTRAINT `munp` FOREIGN KEY (`municipio`) REFERENCES `municipios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sucursales`
--

LOCK TABLES `sucursales` WRITE;
/*!40000 ALTER TABLE `sucursales` DISABLE KEYS */;
INSERT INTO `sucursales` VALUES (1,'Seguros Web San Lorenzo',1),(2,'Seguros Web Candelaria de la Frontera',2),(3,'Seguros Web Acajutla',3),(4,'Seguros Web Comalapa',4),(5,'Seguros Web San Cristóbal',5);
/*!40000 ALTER TABLE `sucursales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `talleres`
--

DROP TABLE IF EXISTS `talleres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `talleres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_taller` varchar(50) DEFAULT NULL,
  `codigo` varchar(20) DEFAULT NULL,
  `direccion` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `talleres`
--

LOCK TABLES `talleres` WRITE;
/*!40000 ALTER TABLE `talleres` DISABLE KEYS */;
INSERT INTO `talleres` VALUES (1,'autorepairSV','at250','urb. la paz #255'),(2,'motorACM','at251','calle la mascote local4 ss'),(3,'fibras la bencidion','at252','calle donrua. local 155');
/*!40000 ALTER TABLE `talleres` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_autos`
--

DROP TABLE IF EXISTS `tipo_autos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tipo_autos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_autos`
--

LOCK TABLES `tipo_autos` WRITE;
/*!40000 ALTER TABLE `tipo_autos` DISABLE KEYS */;
INSERT INTO `tipo_autos` VALUES (1,'Autobus'),(2,'Automovil'),(3,'camion'),(4,'cabezal'),(5,'Microbus'),(6,'Motocicleta'),(7,'Panel'),(8,'Pick-up');
/*!40000 ALTER TABLE `tipo_autos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_seguros`
--

DROP TABLE IF EXISTS `tipo_seguros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tipo_seguros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_seguros`
--

LOCK TABLES `tipo_seguros` WRITE;
/*!40000 ALTER TABLE `tipo_seguros` DISABLE KEYS */;
INSERT INTO `tipo_seguros` VALUES (1,'Seguro de Auto'),(2,'Seguro de Vida');
/*!40000 ALTER TABLE `tipo_seguros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_usuarios`
--

DROP TABLE IF EXISTS `tipo_usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tipo_usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_usuarios`
--

LOCK TABLES `tipo_usuarios` WRITE;
/*!40000 ALTER TABLE `tipo_usuarios` DISABLE KEYS */;
INSERT INTO `tipo_usuarios` VALUES (1,'Empleado'),(2,'Cliente'),(3,'Cajero'),(4,'Supervisor'),(5,'Gerente');
/*!40000 ALTER TABLE `tipo_usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipos_cuentas`
--

DROP TABLE IF EXISTS `tipos_cuentas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tipos_cuentas` (
  `idCuenta` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idCuenta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipos_cuentas`
--

LOCK TABLES `tipos_cuentas` WRITE;
/*!40000 ALTER TABLE `tipos_cuentas` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipos_cuentas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipos_notificaciones`
--

DROP TABLE IF EXISTS `tipos_notificaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tipos_notificaciones` (
  `idTipo` int(11) NOT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idTipo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipos_notificaciones`
--

LOCK TABLES `tipos_notificaciones` WRITE;
/*!40000 ALTER TABLE `tipos_notificaciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipos_notificaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(50) DEFAULT NULL,
  `pass` blob,
  `personas` int(11) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `persona` (`personas`),
  CONSTRAINT `persona` FOREIGN KEY (`personas`) REFERENCES `personas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'AdoCampos','���@\Z\�/���hn�',2,1),(2,'AmandaQ','T\��\�l�\�v\�_\�\0',1,1),(3,'ErickU','\�\�#}���fLkV��Z',3,1),(4,'AleSaenz','�h\�zOk�%P�5AR���',4,1),(5,'JoseL','M�(&:ط\�9�(ZJ',5,1),(6,'LuisE','o����D�Ä\��\�\�',6,1),(7,'TamaraS','�T�\�J�uy���$\�',7,1),(8,'ReneAle','����D/�苮\�\'JT\n',8,1),(9,'Empleado','[;m<��n�(��q\�od',9,1),(10,'Cajero','��$\�|\0�BT\�%��',10,1),(11,'Supervisor','�V\�ݙ>\��\\\��\�',11,1);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehiculos`
--

DROP TABLE IF EXISTS `vehiculos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `vehiculos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `placa` varchar(15) DEFAULT NULL,
  `color` varchar(15) DEFAULT NULL,
  `anio` char(4) DEFAULT NULL,
  `modelo` int(11) DEFAULT NULL,
  `tipo` int(11) DEFAULT NULL,
  `chasis` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `modelo_vehiculo` (`modelo`),
  KEY `tipo_aut` (`tipo`),
  CONSTRAINT `modelo_vehiculo` FOREIGN KEY (`modelo`) REFERENCES `modelos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tipo_aut` FOREIGN KEY (`tipo`) REFERENCES `tipo_autos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehiculos`
--

LOCK TABLES `vehiculos` WRITE;
/*!40000 ALTER TABLE `vehiculos` DISABLE KEYS */;
INSERT INTO `vehiculos` VALUES (1,'P569-890','Azul','2015',1,2,'5689-8902'),(2,'P805-036','Rojo','2010',1,2,'2505-8695'),(3,'P023-569','Azul','2005',272,2,'9632-1089'),(4,'P880-980','Blanco','2015',273,2,'9063-1700'),(5,'M080-890','Rojo','2017',274,6,'2689-1206'),(6,'P009-892','Verde','2010',230,2,'7895-5001'),(7,'P789-200','Gris','2017',1,7,'6456-8789'),(8,'P782-920','Rojo','2018',127,2,'8521-5410'),(9,'P905-120','Negro','2018',153,2,'8012-4521');
/*!40000 ALTER TABLE `vehiculos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'seguros'
--

--
-- Dumping routines for database 'seguros'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-03 15:53:42
