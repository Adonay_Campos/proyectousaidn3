use bd1;
INSERT INTO jefe (primer_nombre, segundo_nombre, primer_apellido, segundo_apellido) VALUES ('Raúl', 'Alexis', 'Ramirez', 'Perez');
INSERT INTO jefe (primer_nombre, segundo_nombre, primer_apellido, segundo_apellido) VALUES ('Samuel', 'Eduardo', 'Castañeda', 'Aguilar');
INSERT INTO jefe (primer_nombre, segundo_nombre, primer_apellido, segundo_apellido) VALUES ('Saúl', 'Rodrigo', 'Merino', 'Sanchez');
INSERT INTO jefe (primer_nombre, segundo_nombre, primer_apellido, segundo_apellido) VALUES ('Jorge', 'Roberto', 'Barahona', 'Sibrian');
INSERT INTO empleados (primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, correo, tel_movil, jefe) VALUES ('René', 'Adonay', 'Campos', 'Meléndez', 'adocampos27@gmail.com', '72373671', '2');
INSERT INTO empleados (primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, correo, tel_fijo, tel_movil, jefe) VALUES ('Katty', 'Michelle', 'Rodriguez', 'Campos', 'kattcampos@outlook.com', '22558000', '78962302', '1');
INSERT INTO empleados (primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, correo, tel_fijo, jefe) VALUES ('Eduardo', 'Alexis', 'Meléndez', 'Cardona', 'edua28@hotmail.com', '25688902', '1');
INSERT INTO empleados (primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, correo, tel_fijo, tel_movil, jefe) VALUES ('Cristopher', 'Alexander', 'Mejia', 'Linares', 'crisLi@gmail.com', '22305980', '66892310', '2');
INSERT INTO empleados (primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, correo, tel_movil, jefe) VALUES ('Tatina', 'Maria', 'Pineda', 'Letona', 'tatianaMari@gmail.com', '70098925', '3');
INSERT INTO empleados (primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, correo, tel_fijo, tel_movil, jefe) values ('Maria','Fernanda','Perez','Escamilla','mariaferp@gmail.com','25403378','72148762','4')
