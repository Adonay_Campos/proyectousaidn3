create database bd2;
use bd2;

create table if not exists jefe(
id_jefe int not null auto_increment primary key,
primer_nombre varchar(50),
segundo_nombre varchar(50),
primer_apellido varchar(50),
segundo_apellido varchar(50)
);

create table if not exists empleados(
id_empleado int not null primary key auto_increment,
primer_nombre varchar(50),
segundo_nombre varchar(50),
primer_apellido varchar(50),
segundo_apellido varchar(50),
correo text,
tel_fijo int(10),
tel_movil int(10),
jefe int not null,
constraint fk_jefe foreign key (jefe) references jefe(id_jefe) on delete cascade on update cascade 
);