package Controlador;

import Entidad.Empleados;
import Entidad.Jefe;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class EmpleadosVista implements Serializable {

    private List<Empleados> listEmpleados;
    private List<Empleados> listaEmpleados;
    private List<Jefe> listJefe;
    private List<Jefe> listaJefeBD2;
    private Empleados empleados;
    private Jefe jefe;

    @PostConstruct
    public void init() {
        empleados = new Empleados();
        jefe = new Jefe();
    }

    //CRUD BD1
    public void listar() {
        try {
            empleados = new Empleados();
            listEmpleados = empleados.findAll();
            System.out.println("Se listó");
        } catch (Exception e) {
            System.out.println("Error en: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void migrar() {
        try {
            empleados.migrarDatos(empleados);
            System.out.println("Lo inserto");
            listaEmpleados = empleados.mostrar();
        } catch (Exception e) {
            System.out.println("ERROR EN: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void llenaridEmpleado(Empleados e) {
        jefe.setId_jefe(e.getJefe().getId_jefe());
        System.out.println("Prueba de id Jefe: " + e.getJefe().getId_jefe());
        empleados = e;
    }

    public void registar() {
        try {
            empleados.insertarEmpleado(empleados, jefe);
            listEmpleados = empleados.findAll();
            empleados = new Empleados();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void editarBD1() {
        try {
            empleados.setJefe(jefe);
            empleados.editar(empleados, jefe);
            listEmpleados = empleados.findAll();
            empleados = new Empleados();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void eliminarBD1(Empleados e) {
        try {
            empleados.eliminarEmpleadoBD1(e.getId_empleado());
            listEmpleados = empleados.findAll();
            empleados = new Empleados();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    //CRUD BD2
    public void mostrar() {
        try {
            empleados = new Empleados();
            listaEmpleados = empleados.mostrar();
            System.out.println("Se listó");
        } catch (Exception e) {
            System.out.println("Error en: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void llenaridEmpleadoBD2(Empleados e) {
        jefe.setId_jefe(e.getJefe().getId_jefe());
        System.out.println("Prueba de id Jefe: " + e.getJefe().getId_jefe());
        empleados = e;
    }

    public void registarBD2() {
        try {
            empleados.insertarEmpleadoBD2(empleados, jefe);
           listaEmpleados = empleados.mostrar();
            empleados = new Empleados();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void editarBD2() {
        try {
            empleados.setJefe(jefe);
            empleados.editarBD2(empleados, jefe);
            listaEmpleados = empleados.mostrar();
            empleados = new Empleados();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void eliminarBD2(Empleados e) {
        try {
            empleados.eliminarEmpleadoBD2(e.getId_empleado());
            listaEmpleados = empleados.mostrar();
            empleados = new Empleados();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void truncateEmpleadosBD2() {
        try {
            empleados.truncateBD2();
            listaEmpleados = empleados.mostrar();
        } catch (Exception e) {
            System.out.println("Error en: " + e.getMessage());
            e.printStackTrace();
        }
    }

    //JEFE
    public void llenaridJefe(Jefe j) {
        jefe = j;
    }

    public void editarJefe() {
        try {
            jefe.editarJefe(jefe);
            listJefe = jefe.findAll();
            jefe = new Jefe();
            System.out.println("Se edito jefe");
        } catch (Exception e) {
            System.out.println("Error en: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void eliminarJefe(Jefe j) {
        try {
            jefe.eliminarJefe(j.getId_jefe());
            listJefe = jefe.findAll();
            jefe = new Jefe();
            System.out.println("Se elimino jefe");
        } catch (Exception e) {
            System.out.println("Error en: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void insertJefe() {
        try {
            jefe.insertarJefe(jefe);
            listJefe = jefe.findAll();
            jefe = new Jefe();
        } catch (Exception e) {
            System.out.println("Error en: " + e.getMessage());
            e.printStackTrace();
        }
    }

    //BD2
    public void llenaridJefeBD2(Jefe j) {
        jefe = j;
    }

    public void editarJefeBD2() {
        try {
            jefe.editarJefeBD2(jefe);
            listaJefeBD2 = jefe.findAllDB2();
            jefe = new Jefe();
            System.out.println("Se edito jefe");
        } catch (Exception e) {
            System.out.println("Error en: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void eliminarJefeBD2(Jefe j) {
        try {
            jefe.eliminarJefeBD2(j.getId_jefe());
            listaJefeBD2 = jefe.findAllDB2();
            jefe = new Jefe();
            System.out.println("Se elimino jefe");
        } catch (Exception e) {
            System.out.println("Error en: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void insertJefeBD2() {
        try {
            jefe.insertarJefeBD2(jefe);
            listaJefeBD2 = jefe.findAllDB2();
            jefe = new Jefe();
        } catch (Exception e) {
            System.out.println("Error en: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public List<Empleados> getListEmpleados() {
        return listEmpleados;
    }

    public void setListEmpleados(List<Empleados> listEmpleados) {
        this.listEmpleados = listEmpleados;
    }

    public List<Empleados> getListaEmpleados() {
        return listaEmpleados;
    }

    public void setListaEmpleados(List<Empleados> listaEmpleados) {
        this.listaEmpleados = listaEmpleados;
    }

    public Empleados getEmpleados() {
        return empleados;
    }

    public void setEmpleados(Empleados empleados) {
        this.empleados = empleados;
    }

    public Jefe getJefe() {
        return jefe;
    }

    public void setJefe(Jefe jefe) {
        this.jefe = jefe;
    }

    public List<Jefe> getListJefe() {
        try {
            listJefe = jefe.findAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listJefe;
    }

    public void setListJefe(List<Jefe> listJefe) {
        this.listJefe = listJefe;
    }

    public List<Jefe> getListaJefeBD2() {
        try {
            listaJefeBD2 = jefe.findAllDB2();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listaJefeBD2;
    }

    public void setListaJefeBD2(List<Jefe> listaJefeBD2) {
        this.listaJefeBD2 = listaJefeBD2;
    }

}
