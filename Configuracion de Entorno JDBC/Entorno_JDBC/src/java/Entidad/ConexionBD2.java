package Entidad;

import java.sql.*;

public class ConexionBD2 {
    
    private static String db = "bd2";
    private static String user = "root";
    private static String pass = "root";
    private static String ruta = "jdbc:mysql://localhost:3306/"+db+"?useSSL=false";
    public Connection conn;

    public Connection getConn() {
        return conn;
    }

    public void setConn(Connection conn) {
        this.conn = conn;
    }

    public Connection conectar() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(ruta, user, pass);
            System.out.println("Entro");
            if(conn!=null){
                System.out.println("Exito");
            }
            return conn;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void desconectar() {
        try {
            conn.close();
        } catch (Exception e) {
        }
    }

}
