package Entidad;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class Empleados extends ConexionBD1 {

    private int id_empleado;
    private String primer_nombre;
    private String segundo_nombre;
    private String primer_apellido;
    private String segundo_apellido;
    private String correo;
    private int tel_fijo;
    private int tel_movil;
    private Jefe jefe;

    public Empleados() {
    }

    public Empleados(int id_empleado, String primer_nombre, String segundo_nombre, String primer_apellido, String segundo_apellido, String correo, int tel_fijo, int tel_movil, Jefe jefe) {
        this.id_empleado = id_empleado;
        this.primer_nombre = primer_nombre;
        this.segundo_nombre = segundo_nombre;
        this.primer_apellido = primer_apellido;
        this.segundo_apellido = segundo_apellido;
        this.correo = correo;
        this.tel_fijo = tel_fijo;
        this.tel_movil = tel_movil;
        this.jefe = jefe;
    }

    public int getId_empleado() {
        return id_empleado;
    }

    public void setId_empleado(int id_empleado) {
        this.id_empleado = id_empleado;
    }

    public String getPrimer_nombre() {
        return primer_nombre;
    }

    public void setPrimer_nombre(String primer_nombre) {
        this.primer_nombre = primer_nombre;
    }

    public String getSegundo_nombre() {
        return segundo_nombre;
    }

    public void setSegundo_nombre(String segundo_nombre) {
        this.segundo_nombre = segundo_nombre;
    }

    public String getPrimer_apellido() {
        return primer_apellido;
    }

    public void setPrimer_apellido(String primer_apellido) {
        this.primer_apellido = primer_apellido;
    }

    public String getSegundo_apellido() {
        return segundo_apellido;
    }

    public void setSegundo_apellido(String segundo_apellido) {
        this.segundo_apellido = segundo_apellido;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public int getTel_fijo() {
        return tel_fijo;
    }

    public void setTel_fijo(int tel_fijo) {
        this.tel_fijo = tel_fijo;
    }

    public int getTel_movil() {
        return tel_movil;
    }

    public void setTel_movil(int tel_movil) {
        this.tel_movil = tel_movil;
    }

    public Jefe getJefe() {
        return jefe;
    }

    public void setJefe(Jefe jefe) {
        this.jefe = jefe;
    }

    //CRUD DE LA BASE DE DATOS NUMERO 1
    public List<Empleados> findAll() throws Exception {
        List<Empleados> list = new ArrayList<>();
        try {
            conectar();
            String sql = "SELECT * FROM empleados e\n"
                    + "inner join jefe j on j.id_jefe = e.jefe;";
            PreparedStatement ps = getConn().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Empleados em = new Empleados();
                Jefe j = new Jefe();
                em.setId_empleado(rs.getInt("id_empleado"));
                em.setPrimer_nombre(rs.getString("primer_nombre"));
                em.setSegundo_nombre(rs.getString("segundo_nombre"));
                em.setPrimer_apellido(rs.getString("primer_apellido"));
                em.setSegundo_apellido(rs.getString("segundo_apellido"));
                em.setCorreo(rs.getString("correo"));
                em.setTel_fijo(rs.getInt("tel_fijo"));
                em.setTel_movil(rs.getInt("tel_movil"));
                j.setId_jefe(rs.getInt("id_jefe"));
                j.setPrimer_nombre(rs.getString("j.primer_nombre"));
                j.setSegundo_nombre(rs.getString("j.segundo_nombre"));
                j.setPrimer_apellido(rs.getString("j.primer_apellido"));
                j.setSegundo_apellido(rs.getString("j.segundo_apellido"));
                em.setJefe(j);
                list.add(em);
            }
        } catch (Exception e) {
            throw e;
        }
        return list;
    }

    public void migrarDatos(Empleados em) throws Exception {
        ConexionBD2 cnn = new ConexionBD2();
        try {
            conectar();
            cnn.conectar();
            System.out.println("Insertar empleados");
            String query2 = "INSERT INTO bd2.jefe (primer_nombre, segundo_nombre, primer_apellido, segundo_apellido)\n "
                    + "SELECT primer_nombre, segundo_nombre, primer_apellido, segundo_apellido FROM bd1.jefe;";
            String query1 = "INSERT INTO bd2.empleados (primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, correo, tel_fijo, tel_movil, jefe)\n "
                    + "SELECT primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, correo, tel_fijo, tel_movil, jefe FROM bd1.empleados;";
            PreparedStatement ps = cnn.getConn().prepareStatement(query2);
            ps.executeUpdate();
            PreparedStatement p = cnn.getConn().prepareStatement(query1);
            p.executeUpdate();
        } catch (Exception e) {
            System.out.println("Error en: " + e.getMessage());
            throw e;
        }
    }

    public void insertarEmpleado(Empleados em, Jefe j) throws Exception {
        try {
            conectar();
            System.out.println("Antes de insertar");
            String query = "INSERT INTO empleados (primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, correo, tel_fijo, tel_movil, jefe) VALUES (?,?,?,?,?,?,?,?)";
            PreparedStatement ps = getConn().prepareStatement(query);
            ps.setString(1, em.getPrimer_nombre());
            ps.setString(2, em.getSegundo_nombre());
            ps.setString(3, em.getPrimer_apellido());
            ps.setString(4, em.getSegundo_apellido());
            ps.setString(5, em.getCorreo());
            ps.setInt(6, em.getTel_fijo());
            ps.setInt(7, em.getTel_movil());
            ps.setInt(8, j.getId_jefe());
            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println("Error en: " + e.getMessage());
            throw e;
        }
    }

    public void editar(Empleados em, Jefe j) throws Exception {
        try {
            conectar();
            String sql = "UPDATE empleados SET primer_nombre = ?, segundo_nombre = ?, primer_apellido = ?, segundo_apellido= ?, correo= ?, tel_fijo = ?, tel_movil = ?, jefe = ? WHERE (id_empleado = ?);";
            PreparedStatement ps = getConn().prepareStatement(sql);
            ps.setString(1, em.getPrimer_nombre());
            ps.setString(2, em.getSegundo_nombre());
            ps.setString(3, em.getPrimer_apellido());
            ps.setString(4, em.getSegundo_apellido());
            ps.setString(5, em.getCorreo());
            ps.setInt(6, em.getTel_fijo());
            ps.setInt(7, em.getTel_movil());
            ps.setInt(8, j.getId_jefe());
            ps.setInt(9, em.getId_empleado());
            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

    public void eliminarEmpleadoBD1(int id) throws Exception {
        try {
            conectar();
            String query = "delete from empleados where id_empleado = ?";
            PreparedStatement ps = getConn().prepareStatement(query);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }
    //CRUD DE LA BASE DE DATOS 2

    ConexionBD2 cnn = new ConexionBD2();

    public List<Empleados> mostrar() throws Exception {
        List<Empleados> list = new ArrayList<>();
        try {
            cnn.conectar();
            String sql = "SELECT * FROM empleados e\n"
                    + "inner join jefe j on j.id_jefe = e.jefe order by e.id_empleado;";
            PreparedStatement ps = cnn.getConn().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Empleados em = new Empleados();
                Jefe j = new Jefe();
                em.setId_empleado(rs.getInt("id_empleado"));
                em.setPrimer_nombre(rs.getString("primer_nombre"));
                em.setSegundo_nombre(rs.getString("segundo_nombre"));
                em.setPrimer_apellido(rs.getString("primer_apellido"));
                em.setSegundo_apellido(rs.getString("segundo_apellido"));
                em.setCorreo(rs.getString("correo"));
                em.setTel_fijo(rs.getInt("tel_fijo"));
                em.setTel_movil(rs.getInt("tel_movil"));
                j.setPrimer_nombre(rs.getString("j.primer_nombre"));
                j.setSegundo_nombre(rs.getString("j.segundo_nombre"));
                j.setPrimer_apellido(rs.getString("j.primer_apellido"));
                j.setSegundo_apellido(rs.getString("j.segundo_apellido"));
                em.setJefe(j);
                list.add(em);
            }
        } catch (Exception e) {
            throw e;
        }
        return list;
    }

    public void truncateBD2() throws Exception {
        try {
            cnn.conectar();
            String query = "truncate table jefe; ";
            String query1 = "truncate table empleados; ";
            PreparedStatement p = cnn.getConn().prepareStatement(query);
            p.executeUpdate();
            System.out.println("Elimino jefe");
            PreparedStatement ps = cnn.getConn().prepareStatement(query1);
            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println("Error en: " + e.getMessage());
            throw e;
        }
    }

    public void insertarEmpleadoBD2(Empleados em, Jefe j) throws Exception {
        try {
            cnn.conectar();
            System.out.println("Antes de insertar");
            String query = "INSERT INTO empleados (primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, correo, tel_fijo, tel_movil, jefe) VALUES (?,?,?,?,?,?,?,?)";
            PreparedStatement ps = getConn().prepareStatement(query);
            ps.setString(1, em.getPrimer_nombre());
            ps.setString(2, em.getSegundo_nombre());
            ps.setString(3, em.getPrimer_apellido());
            ps.setString(4, em.getSegundo_apellido());
            ps.setString(5, em.getCorreo());
            ps.setInt(6, em.getTel_fijo());
            ps.setInt(7, em.getTel_movil());
            ps.setInt(8, j.getId_jefe());
            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println("Error en: " + e.getMessage());
            throw e;
        }
    }

    public void editarBD2(Empleados em, Jefe j) throws Exception {
        try {
            cnn.conectar();
            String sql = "UPDATE empleados SET primer_nombre = ?, segundo_nombre = ?, primer_apellido = ?, segundo_apellido= ?, correo= ?, tel_fijo = ?, tel_movil = ?, jefe = ? WHERE (id_empleado = ?);";
            PreparedStatement ps = getConn().prepareStatement(sql);
            ps.setString(1, em.getPrimer_nombre());
            ps.setString(2, em.getSegundo_nombre());
            ps.setString(3, em.getPrimer_apellido());
            ps.setString(4, em.getSegundo_apellido());
            ps.setString(5, em.getCorreo());
            ps.setInt(6, em.getTel_fijo());
            ps.setInt(7, em.getTel_movil());
            ps.setInt(8, j.getId_jefe());
            ps.setInt(9, em.getId_empleado());
            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

    public void eliminarEmpleadoBD2(int id) throws Exception {
        try {
            cnn.conectar();
            String query = "delete from empleados where id_empleado = ?";
            PreparedStatement ps = getConn().prepareStatement(query);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

}
