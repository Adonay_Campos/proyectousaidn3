package Entidad;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class Jefe extends ConexionBD1 {

    private int id_jefe;
    private String primer_nombre;
    private String segundo_nombre;
    private String primer_apellido;
    private String segundo_apellido;

    public Jefe() {
    }

    public Jefe(int id_jefe, String primer_nombre, String segundo_nombre, String primer_apellido, String segundo_apellido) {
        this.id_jefe = id_jefe;
        this.primer_nombre = primer_nombre;
        this.segundo_nombre = segundo_nombre;
        this.primer_apellido = primer_apellido;
        this.segundo_apellido = segundo_apellido;
    }

    public int getId_jefe() {
        return id_jefe;
    }

    public void setId_jefe(int id_jefe) {
        this.id_jefe = id_jefe;
    }

    public String getPrimer_nombre() {
        return primer_nombre;
    }

    public void setPrimer_nombre(String primer_nombre) {
        this.primer_nombre = primer_nombre;
    }

    public String getSegundo_nombre() {
        return segundo_nombre;
    }

    public void setSegundo_nombre(String segundo_nombre) {
        this.segundo_nombre = segundo_nombre;
    }

    public String getPrimer_apellido() {
        return primer_apellido;
    }

    public void setPrimer_apellido(String primer_apellido) {
        this.primer_apellido = primer_apellido;
    }

    public String getSegundo_apellido() {
        return segundo_apellido;
    }

    public void setSegundo_apellido(String segundo_apellido) {
        this.segundo_apellido = segundo_apellido;
    }

    public List<Jefe> findAll() throws Exception {
        List<Jefe> list = new ArrayList<>();
        try {
            conectar();
            String sql = "SELECT * FROM jefe";
            PreparedStatement ps = getConn().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Jefe j = new Jefe();
                j.setId_jefe(rs.getInt("id_jefe"));
                j.setPrimer_nombre(rs.getString("primer_nombre"));
                j.setSegundo_nombre(rs.getString("segundo_nombre"));
                j.setPrimer_apellido(rs.getString("primer_apellido"));
                j.setSegundo_apellido(rs.getString("segundo_apellido"));
                list.add(j);
            }
        } catch (Exception e) {
            throw e;
        }
        return list;
    }

    public void editarJefe(Jefe j) throws Exception {
        try {
            conectar();
            String sql = "UPDATE jefe SET primer_nombre = ?, segundo_nombre = ?, primer_apellido = ?, segundo_apellido= ? WHERE (id_jefe = ?);";
            PreparedStatement ps = getConn().prepareStatement(sql);
            ps.setString(1, j.getPrimer_nombre());
            ps.setString(2, j.getSegundo_nombre());
            ps.setString(3, j.getPrimer_apellido());
            ps.setString(4, j.getSegundo_apellido());
            ps.setInt(5, j.getId_jefe());
            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

    public void insertarJefe(Jefe j) throws Exception {
        try {
            conectar();
            String query = "INSERT INTO jefe (primer_nombre, segundo_nombre, primer_apellido, segundo_apellido) VALUES (?,?,?,?)";
            PreparedStatement ps = getConn().prepareStatement(query);
            ps.setString(1, j.getPrimer_nombre());
            ps.setString(2, j.getSegundo_nombre());
            ps.setString(3, j.getPrimer_apellido());
            ps.setString(4, j.getSegundo_apellido());
            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println("Error en: " + e.getMessage());
            throw e;
        }
    }

    public void eliminarJefe(int id) throws Exception {
        try {
            conectar();
            String query = "delete from jefe where id_jefe = ?";
            PreparedStatement ps = getConn().prepareStatement(query);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

    //BD2
    ConexionBD2 cnn = new ConexionBD2();

    public List<Jefe> findAllDB2() throws Exception {
        List<Jefe> list = new ArrayList<>();
        try {
            cnn.conectar();
            String sql = "SELECT * FROM jefe";
            PreparedStatement ps = getConn().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Jefe j = new Jefe();
                j.setId_jefe(rs.getInt("id_jefe"));
                j.setPrimer_nombre(rs.getString("primer_nombre"));
                j.setSegundo_nombre(rs.getString("segundo_nombre"));
                j.setPrimer_apellido(rs.getString("primer_apellido"));
                j.setSegundo_apellido(rs.getString("segundo_apellido"));
                list.add(j);
            }
        } catch (Exception e) {
            throw e;
        }
        return list;
    }

    public void editarJefeBD2(Jefe j) throws Exception {
        try {
            cnn.conectar();
            String sql = "UPDATE jefe SET primer_nombre = ?, segundo_nombre = ?, primer_apellido = ?, segundo_apellido= ? WHERE (id_jefe = ?);";
            PreparedStatement ps = getConn().prepareStatement(sql);
            ps.setString(1, j.getPrimer_nombre());
            ps.setString(2, j.getSegundo_nombre());
            ps.setString(3, j.getPrimer_apellido());
            ps.setString(4, j.getSegundo_apellido());
            ps.setInt(5, j.getId_jefe());
            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

    public void insertarJefeBD2(Jefe j) throws Exception {
        try {
            cnn.conectar();
            String query = "INSERT INTO jefe (primer_nombre, segundo_nombre, primer_apellido, segundo_apellido) VALUES (?,?,?,?)";
            PreparedStatement ps = getConn().prepareStatement(query);
            ps.setString(1, j.getPrimer_nombre());
            ps.setString(2, j.getSegundo_nombre());
            ps.setString(3, j.getPrimer_apellido());
            ps.setString(4, j.getSegundo_apellido());
            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println("Error en: " + e.getMessage());
            throw e;
        }
    }

    public void eliminarJefeBD2(int id) throws Exception {
        try {
            cnn.conectar();
            String query = "delete from jefe where id_jefe = ?";
            PreparedStatement ps = getConn().prepareStatement(query);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

}
