create database cuestionario;
use cuestionario;

create table nivel_personal(
id_nivel int not null auto_increment primary key,
nombre_nivel varchar(50)
)engine innodb;

create table departamento_trabajo(
id_dtrabajo int not null auto_increment primary key,
nombre_deptrabajo varchar(50)
)engine innodb;

create table area_trabajo(
id_area int not null auto_increment primary key,
nombre_area varchar(50),
departamento int,
constraint dept_fk foreign key (departamento) references departamento_trabajo(id_dtrabajo) on update cascade
)engine innodb;

create table empleado(
id_empleado int not null auto_increment primary key,
sexo varchar(20),
edad int(3),
nivel_escolaridad varchar(50),
area int,
nivel int,
constraint area_fk foreign key (area) references area_trabajo(id_area) on update cascade,
constraint nivel_personal_fk foreign key (nivel) references nivel_personal(id_nivel)
)engine innodb;

create table secciones(
id_secciones int not null auto_increment primary key,
nombre_secciones varchar(100),
codigo varchar(5)
)engine InnoDB;

create table preguntas(
id_preguntas int not null auto_increment primary key,
pregunta text,
seccion int,
constraint seccion_fk foreign key (seccion) references secciones(id_secciones) on update cascade
)engine InnoDB;

create table criterios(
id_criterios int not null auto_increment primary key,
num_seccion int,
numero_criterio int(2),
constraint id_seccion_fk foreign key (num_seccion) references secciones(id_secciones) on update cascade
)engine InnoDB;

create table tipo_user(
id_tipo int not null auto_increment primary key,
tipo varchar(25)
)engine InnoDB;

create table usuario(
id_us int not null auto_increment primary key,
usuario varchar(25),
pass text not null,
tipo_user int,
constraint tipo_fk foreign key (tipo_user) references tipo_user(id_tipo) on update cascade
)engine InnoDB;

create table hoja(
id_cuestionario int not null auto_increment primary key,
us int,
id_pregunta int not null,
fecha date,
id_empleado int not null,
id_criterio int not null,
constraint pregunta_fk foreign key (id_pregunta) references preguntas(id_preguntas) on update cascade,
constraint emp_fk foreign key (id_empleado) references empleado(id_empleado) on update cascade,
constraint criterio_fk foreign key (id_criterio) references criterios(id_criterios) on update cascade,
constraint user_fk foreign key (us) references usuario(id_us) on update cascade
)engine InnoDB;