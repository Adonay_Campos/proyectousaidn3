package com.exportes.ctrl;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperRunManager;

@RequestScoped
@ManagedBean
public class Reporte implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void generarPDF() throws JRException, IOException {

		File file = new File(FacesContext.getCurrentInstance().getExternalContext()
				.getRealPath("jasper/Reportes de Productos.jasper"));
		System.out.println("////////////" + file.getPath());
		byte[] bytes = JasperRunManager.runReportToPdf(file.getPath(), new HashMap<String, Object>(), Enlace.conecta());
		HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance()
				.getExternalContext().getResponse();
		httpServletResponse.setContentType("application/pdf");
		httpServletResponse.setContentLength(bytes.length);
		ServletOutputStream outputStream = httpServletResponse.getOutputStream();
		outputStream.write(bytes, 0, bytes.length);
		outputStream.flush();
		outputStream.close();
		FacesContext.getCurrentInstance().responseComplete();
	}

}
