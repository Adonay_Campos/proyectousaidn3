create database grupo8sql;
use grupo8sql;

CREATE TABLE prioridades (
  idPrioridad int PRIMARY KEY auto_increment NOT NULL,
  descripcion varchar(150)
) ENGINE InnoDB;

CREATE TABLE detalles_notificaciones (
  idNotificaciones int primary KEY NOT NULL AUTO_INCREMENT,
  detalles text,
  fecha date,
  remitente varchar(255)
)engine InnoDB;

CREATE TABLE tipos_notificaciones (
  idTipo int NOT NULL PRIMARY KEY auto_increment,
  descripcion varchar(255)
) ENGINE InnoDB;

CREATE TABLE tipos_cuentas (
  idCuenta int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  descripcion varchar(100)
) ENGINE InnoDB;

CREATE TABLE cargos (
  idCargo int NOT NULL AUTO_INCREMENT primary key,
  cargo varchar(100)
)engine innoDB;

CREATE TABLE grados (
  idGrado int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  grado varchar(100)
) engine InnoDB;

CREATE TABLE secciones (
  idSeccion int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  descripcion varchar(100)
) ENGINE InnoDB;

CREATE TABLE cuentas (
  idCuenta int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  correo varchar(150) UNIQUE NOT NULL,
  pass varchar(100) NOT NULL,
  recuperacion varchar(100),
  tipoCuenta int NOT NULL,
  CONSTRAINT `FK_cuentas_tipoCuenta` FOREIGN KEY (tipoCuenta) REFERENCES tipos_cuentas(idcuenta) ON DELETE CASCADE ON UPDATE CASCADE
)engine InnoDB;

CREATE TABLE personas (
  idPersona int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  apellido varchar(150) NOT NULL,
  direccion text,
  fechaNac date,
  foto longblob,
  fototext varchar(150),
  nombre varchar(150) NOT NULL,
  idCuenta int,
  CONSTRAINT `FK_personas_idCuenta` FOREIGN KEY (idCuenta) REFERENCES cuentas (idcuenta) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE InnoDB;

CREATE TABLE personal_academico (
  idPersonalAcademico int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  detalles varchar(150),
  estadoPersonal varchar(150),
  idCargo int,
  idPersona int,
  CONSTRAINT `FK_personal_academico_idCargo` FOREIGN KEY (idCargo) REFERENCES cargos (idcargo) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_personal_academico_idPersona` FOREIGN KEY (idPersona) REFERENCES personas (idpersona) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE InnoDB;

CREATE TABLE grados_secciones (
  idGs int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  year int(4),
  idGrado int NOT NULL,
  idPersonalAcademico int NOT NULL,
  idSeccion int NOT NULL,
  CONSTRAINT `FK_grados_secciones_idGrado` FOREIGN KEY (idGrado) REFERENCES grados (idgrado) ON UPDATE CASCADE,
  CONSTRAINT `FK_grados_secciones_idPersonalAcademico` FOREIGN KEY (idPersonalAcademico) REFERENCES personal_academico (idpersonalacademico) ON UPDATE CASCADE,
  CONSTRAINT `FK_grados_secciones_idSeccion` FOREIGN KEY (idSeccion) REFERENCES secciones (idseccion) ON UPDATE CASCADE
) ENGINE InnoDB;

CREATE TABLE encargados (
  idEncargado int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  dui varchar(11),
  nit varchar(18),
  parentesco varchar(100),
  telefonoF varchar(10),
  telefonoM varchar(10),
  idPersona int NOT NULL,
  subEncargado int,
  CONSTRAINT `FK_encargados_idPersona` FOREIGN KEY (idPersona) REFERENCES personas (idpersona) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_encargados_subEncargado` FOREIGN KEY (subEncargado) REFERENCES encargados(idencargado) ON UPDATE CASCADE
)engine InnoDB;

CREATE TABLE estudiantes (
  nie int(11) PRIMARY KEY NOT NULL,
  idEncargado int NOT NULL,
  idGs int NOT NULL,
  idPersona int NOT NULL,
  CONSTRAINT `FK_estudiantes_idEncargado` FOREIGN KEY (idEncargado) REFERENCES encargados (idencargado) ON UPDATE CASCADE,
  CONSTRAINT `FK_estudiantes_idGs` FOREIGN KEY (idGs) REFERENCES grados_secciones (idgs) ON UPDATE CASCADE,
  CONSTRAINT `FK_estudiantes_idPersona` FOREIGN KEY (idPersona) REFERENCES personas (idpersona) ON UPDATE CASCADE
)engine InnoDB;

CREATE TABLE asistencias (
  idAsistencia int NOT NULL AUTO_INCREMENT primary key,
  asistencia varchar(100),
  detalles varchar(150),
  fecha varchar(150),
  switch1 varchar(150),
  switch2 varchar(150),
  nie int NOT NULL,
  CONSTRAINT `FK_asistencias_nie` FOREIGN KEY (nie) REFERENCES estudiantes(nie) ON UPDATE CASCADE
)engine innodb;

CREATE TABLE historial_notificiaciones (
  idNotificiacion int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  cuerpo text,
  fecha date,
  idPersonal int NOT NULL,
  prioridad int NOT NULL,
  tipo int NOT NULL,
  CONSTRAINT `FK_historial_notificiaciones_idPersonal` FOREIGN KEY (idPersonal) REFERENCES personal_academico (idpersonalacademico) ON UPDATE CASCADE,
  CONSTRAINT `FK_historial_notificiaciones_prioridad` FOREIGN KEY (prioridad) REFERENCES prioridades (idprioridad) ON UPDATE CASCADE,
  CONSTRAINT `FK_historial_notificiaciones_tipo` FOREIGN KEY (tipo) REFERENCES tipos_notificaciones (idtipo) ON UPDATE CASCADE
) ENGINE InnoDB;

CREATE TABLE detalles_encargados (
  idDe int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  idDetalles int,
  idEncargado int,
  CONSTRAINT `FK_detalles_encargados_idDetalles` FOREIGN KEY (idDetalles) REFERENCES detalles_notificaciones (idnotificaciones) ON UPDATE CASCADE,
  CONSTRAINT `FK_detalles_encargados_idEncargado` FOREIGN KEY (idEncargado) REFERENCES encargados (idencargado) ON UPDATE CASCADE
)engine InnoDB;
