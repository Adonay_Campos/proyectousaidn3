use grupo8sql;

INSERT INTO `tipos_cuentas` VALUES (1,'Personal Administrativo'),(2,'Encargados');

INSERT INTO `cargos` VALUES (1,'Administrativo'),(2,'Personal Academico'),(3,'Profesor');

INSERT INTO `grados` VALUES (1,'Primer Año de Bachillerato'),(2,'Segundo Año de Bachillerato'),(3,'Tercer Año de Bachillerato');

INSERT INTO `secciones` VALUES (1,'A'),(2,'B'),(3,'C'),(4,'D'),(5,'E'),(6,'F'),(7,'G'),(8,'H'),(9,'I');

INSERT INTO `cuentas` VALUES (1,'jhadhe58@gmail.com','ado123',NULL,1),(2,'carlGS@gmail.com','carlos123',NULL,1),
(3,'camMT@gmail.com','camila123',NULL,1),(4,'allanML@gmail.com','allan123',NULL,1),(5,'carmenVP@gmail.com','carmen123',NULL,1),
(6,'guadatat@gmail.com','guadalupe1234',NULL,1),(7,'samrober@gmail.com','samuel123',NULL,1),(8,'mauri20@gmail.com','mau123',NULL,1),
(9,'michael@gmail.com','mi123',NULL,2),(10,'karlamirian58@gmail.com','karla123',NULL,2),(11,'rub45@gmail.com','ruben1234',NULL,2),
(12,'carnalia@gmail.com','carolina123',NULL,2);

INSERT INTO `personas` VALUES (1,'Campos Meléndez','Mariona','2000-07-27',NULL,NULL,'René Adonay',1),
(2,'Garcia Sigüenza','Ilopango','1980-02-18',NULL,NULL,'Carlos Edmundo',2),
(3,'Molina Torres','Quezaltepeque','1980-02-18',NULL,NULL,'Andrea Camila',3),(4,'Mejia Lozano','Mejicanos','1989-03-07',NULL,NULL,'Allan Jose',4),
(5,'Villalta Paz','San Salvador','1990-08-24',NULL,NULL,'Carmen Guadalupe',5),
(6,'Morales Rodriguez','San Salvador','1985-05-20',NULL,NULL,'Guadalupe Tatiana',6),
(7,'Castañeda Castro','San Salvador','1980-10-25',NULL,NULL,'Samuel Roberto',7),
(8,'Flores Herrara','Zacamil','1979-12-09',NULL,NULL,'Mauricio Javier',8),(9,'Gomez Soriano','Lourdes','1970-03-29',NULL,NULL,'Michael Jose',9),
(10,'Aguirres Pineda','Cuscatancingo','1975-01-03',NULL,NULL,'Karla Mirian',10),
(11,'Sanchez Quijano','Apopa','1990-09-19',NULL,NULL,'Ruben Alejandro',11),
(12,'Grimaldy Gonzalez','Apopa','1976-11-30',NULL,NULL,'Carolina Nathalia',12),
(13,'Guitierrez Aguirres','Csucatancingo','1998-01-31',NULL,NULL,'Rosa Maria',NULL),
(14,'Guitierrez Aguirres','Cuscatancingo','1995-08-18',NULL,NULL,'Kevin Adalberto',NULL),
(15,'Sanchez Quijano','Apopa','1999-11-23',NULL,NULL,'Adonai Alejandro',NULL),
(16,'Cañas Gonzalez','Apopa','1999-12-03',NULL,NULL,'Ailyn Sarai',NULL),(17,'Gomez Lopez','Lourdes','1995-09-30',NULL,NULL,'Oscar Enrique',NULL),
(18,'Gomez Lopez','Lourdes','2000-01-15',NULL,NULL,'Maria Eugenia',NULL);

INSERT INTO `personal_academico` VALUES (1,'Ingles','Licenciada',3,3),(2,'Basica','Licenciado',3,2),
(3,'Basica','Licenciado',3,4),(4,'Informática','Licenciado',3,5),(5,'Ingles','Licenciado',3,7),(6,'Basica','Licenciado',3,6),
(7,'Director','Licenciado',1,1),(8,'Contabilidad','Licenciado',3,8);

INSERT INTO `grados_secciones` VALUES (1,2019,1,1,1),(2,2019,1,2,2),(3,2019,2,3,1),(4,2019,2,5,2),(5,2019,3,6,1);

INSERT INTO `encargados` VALUES (1,'06565232-1','0423-123132-123-0','Padre','2265-9899','6902-0078',9,NULL),
(2,'01354679-8','0454-612316-546-4','Madre','2285-9610','6007-8891',10,NULL),
(3,'07894110-0','0641-225412-222-5','Hermano','2015-7806','7892-0001',11,NULL),
(4,'06411224-5','0562-211234-898-7','Madre','2589-3017','7045-2019',12,NULL);

INSERT INTO `estudiantes` VALUES (32556,1,1,18),(300445,1,2,17),(314032,2,2,14),(314567,2,3,13),(335541,3,5,15),(345622,4,4,16);

INSERT INTO `asistencias` VALUES (1,'SI Asistio','NA','2019-12-02','false','false',314567),
(2,'No Asistio','NA','2019-12-03','false','false',314567),(3,'SI Asistio','NA','2019-12-04','false','false',314567),
(4,'SI Asistio','NA','2019-12-05','false','false',314567),(5,'SI Asistio','NA','2019-12-09','false','false',314567),
(6,'SI Asistio','NA','2019-12-10','false','false',314567),(7,'SI Asistio','NA','2019-12-11','false','false',314567),
(8,'No Asistio','NA','2019-12-12','false','false',314567),(9,'SI Asistio','NA','2019-12-13','false','false',314567),
(10,'SI Asistio','NA','2019-12-16','false','false',314567),(11,'SI Asistio','NA','2019-12-02','false','false',335541),
(12,'Permiso','NA','2019-12-02','false','false',345622),(13,'No Asistio','NA','2019-12-03','false','false',345622),
(14,'SI Asistio','NA','2019-12-04','false','false',335541),(15,'SI Asistio','NA','2019-12-05','false','false',335541),
(16,'Permiso','NA','2019-12-16','false','false',345622),(17,'No Asistio','NA','2019-12-05','false','false',345622),
(18,'SI Asistio','NA','2019-12-09','false','false',335541),(19,'SI Asistio','NA','2019-12-10','false','false',335541),
(20,'Permiso','NA','2019-12-11','false','false',345622),(21,'SI Asistio','NA','2019-12-12','false','false',345622),
(22,'SI Asistio','NA','2019-12-12','false','false',335541);

