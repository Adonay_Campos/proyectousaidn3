package com.control.asistencia.persistencias;

import com.control.asistencia.persistencias.Estudiantes;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-12-19T14:36:50")
@StaticMetamodel(Asistencias.class)
public class Asistencias_ { 

    public static volatile SingularAttribute<Asistencias, String> fecha;
    public static volatile SingularAttribute<Asistencias, String> asistencia;
    public static volatile SingularAttribute<Asistencias, String> switch2;
    public static volatile SingularAttribute<Asistencias, Integer> idAsistencia;
    public static volatile SingularAttribute<Asistencias, String> switch1;
    public static volatile SingularAttribute<Asistencias, String> detalles;
    public static volatile SingularAttribute<Asistencias, Estudiantes> nie;

}