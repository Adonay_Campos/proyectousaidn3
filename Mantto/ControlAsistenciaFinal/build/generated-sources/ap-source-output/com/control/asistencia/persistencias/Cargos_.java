package com.control.asistencia.persistencias;

import com.control.asistencia.persistencias.PersonalAcademico;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-12-19T14:36:50")
@StaticMetamodel(Cargos.class)
public class Cargos_ { 

    public static volatile SingularAttribute<Cargos, Integer> idCargo;
    public static volatile ListAttribute<Cargos, PersonalAcademico> personalAcademicoList;
    public static volatile SingularAttribute<Cargos, String> cargo;

}