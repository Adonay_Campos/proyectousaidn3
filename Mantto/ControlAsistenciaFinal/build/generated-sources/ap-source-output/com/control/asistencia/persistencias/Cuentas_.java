package com.control.asistencia.persistencias;

import com.control.asistencia.persistencias.Personas;
import com.control.asistencia.persistencias.TiposCuentas;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-12-19T14:36:50")
@StaticMetamodel(Cuentas.class)
public class Cuentas_ { 

    public static volatile SingularAttribute<Cuentas, String> recuperacion;
    public static volatile SingularAttribute<Cuentas, String> pass;
    public static volatile ListAttribute<Cuentas, Personas> personasList;
    public static volatile SingularAttribute<Cuentas, String> correo;
    public static volatile SingularAttribute<Cuentas, TiposCuentas> tipoCuenta;
    public static volatile SingularAttribute<Cuentas, Integer> idCuenta;

}