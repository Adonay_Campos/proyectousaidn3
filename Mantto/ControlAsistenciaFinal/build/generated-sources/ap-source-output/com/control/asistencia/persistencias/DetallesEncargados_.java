package com.control.asistencia.persistencias;

import com.control.asistencia.persistencias.DetallesNotificaciones;
import com.control.asistencia.persistencias.Encargados;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-12-19T14:36:50")
@StaticMetamodel(DetallesEncargados.class)
public class DetallesEncargados_ { 

    public static volatile SingularAttribute<DetallesEncargados, Encargados> idEncargado;
    public static volatile SingularAttribute<DetallesEncargados, DetallesNotificaciones> idDetalles;
    public static volatile SingularAttribute<DetallesEncargados, Integer> idDe;

}