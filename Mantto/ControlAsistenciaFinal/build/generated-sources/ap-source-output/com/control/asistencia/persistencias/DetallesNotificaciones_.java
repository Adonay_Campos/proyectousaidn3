package com.control.asistencia.persistencias;

import com.control.asistencia.persistencias.DetallesEncargados;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-12-19T14:36:50")
@StaticMetamodel(DetallesNotificaciones.class)
public class DetallesNotificaciones_ { 

    public static volatile SingularAttribute<DetallesNotificaciones, Date> fecha;
    public static volatile ListAttribute<DetallesNotificaciones, DetallesEncargados> detallesEncargadosList;
    public static volatile SingularAttribute<DetallesNotificaciones, String> remitente;
    public static volatile SingularAttribute<DetallesNotificaciones, String> detalles;
    public static volatile SingularAttribute<DetallesNotificaciones, Integer> idNotificaciones;

}