package com.control.asistencia.persistencias;

import com.control.asistencia.persistencias.DetallesEncargados;
import com.control.asistencia.persistencias.Encargados;
import com.control.asistencia.persistencias.Estudiantes;
import com.control.asistencia.persistencias.Personas;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-12-19T14:36:50")
@StaticMetamodel(Encargados.class)
public class Encargados_ { 

    public static volatile SingularAttribute<Encargados, Integer> idEncargado;
    public static volatile ListAttribute<Encargados, Estudiantes> estudiantesList;
    public static volatile SingularAttribute<Encargados, String> telefonoF;
    public static volatile SingularAttribute<Encargados, String> parentesco;
    public static volatile ListAttribute<Encargados, DetallesEncargados> detallesEncargadosList;
    public static volatile SingularAttribute<Encargados, String> nit;
    public static volatile SingularAttribute<Encargados, String> dui;
    public static volatile ListAttribute<Encargados, Encargados> encargadosList;
    public static volatile SingularAttribute<Encargados, Encargados> subEncargado;
    public static volatile SingularAttribute<Encargados, String> telefonoM;
    public static volatile SingularAttribute<Encargados, Personas> idPersona;

}