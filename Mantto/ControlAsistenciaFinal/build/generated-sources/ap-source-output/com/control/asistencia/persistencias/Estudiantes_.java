package com.control.asistencia.persistencias;

import com.control.asistencia.persistencias.Asistencias;
import com.control.asistencia.persistencias.Encargados;
import com.control.asistencia.persistencias.GradosSecciones;
import com.control.asistencia.persistencias.Personas;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-12-19T14:36:50")
@StaticMetamodel(Estudiantes.class)
public class Estudiantes_ { 

    public static volatile SingularAttribute<Estudiantes, Encargados> idEncargado;
    public static volatile ListAttribute<Estudiantes, Asistencias> asistenciasList;
    public static volatile SingularAttribute<Estudiantes, GradosSecciones> idGs;
    public static volatile SingularAttribute<Estudiantes, Integer> nie;
    public static volatile SingularAttribute<Estudiantes, Personas> idPersona;

}