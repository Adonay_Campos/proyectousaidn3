package com.control.asistencia.persistencias;

import com.control.asistencia.persistencias.Estudiantes;
import com.control.asistencia.persistencias.Grados;
import com.control.asistencia.persistencias.PersonalAcademico;
import com.control.asistencia.persistencias.Secciones;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-12-19T14:36:50")
@StaticMetamodel(GradosSecciones.class)
public class GradosSecciones_ { 

    public static volatile ListAttribute<GradosSecciones, Estudiantes> estudiantesList;
    public static volatile SingularAttribute<GradosSecciones, Integer> year;
    public static volatile SingularAttribute<GradosSecciones, Integer> idGs;
    public static volatile SingularAttribute<GradosSecciones, Grados> idGrado;
    public static volatile SingularAttribute<GradosSecciones, PersonalAcademico> idPersonalAcademico;
    public static volatile SingularAttribute<GradosSecciones, Secciones> idSeccion;

}