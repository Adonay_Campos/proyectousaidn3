package com.control.asistencia.persistencias;

import com.control.asistencia.persistencias.GradosSecciones;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-12-19T14:36:50")
@StaticMetamodel(Grados.class)
public class Grados_ { 

    public static volatile SingularAttribute<Grados, String> grado;
    public static volatile SingularAttribute<Grados, Integer> idGrado;
    public static volatile ListAttribute<Grados, GradosSecciones> gradosSeccionesList;

}