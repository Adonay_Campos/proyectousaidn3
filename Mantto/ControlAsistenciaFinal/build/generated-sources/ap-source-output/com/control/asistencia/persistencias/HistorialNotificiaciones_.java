package com.control.asistencia.persistencias;

import com.control.asistencia.persistencias.PersonalAcademico;
import com.control.asistencia.persistencias.Prioridades;
import com.control.asistencia.persistencias.TiposNotificaciones;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-12-19T14:36:50")
@StaticMetamodel(HistorialNotificiaciones.class)
public class HistorialNotificiaciones_ { 

    public static volatile SingularAttribute<HistorialNotificiaciones, Date> fecha;
    public static volatile SingularAttribute<HistorialNotificiaciones, TiposNotificaciones> tipo;
    public static volatile SingularAttribute<HistorialNotificiaciones, Integer> idNotificiacion;
    public static volatile SingularAttribute<HistorialNotificiaciones, PersonalAcademico> idPersonal;
    public static volatile SingularAttribute<HistorialNotificiaciones, String> cuerpo;
    public static volatile SingularAttribute<HistorialNotificiaciones, Prioridades> prioridad;

}