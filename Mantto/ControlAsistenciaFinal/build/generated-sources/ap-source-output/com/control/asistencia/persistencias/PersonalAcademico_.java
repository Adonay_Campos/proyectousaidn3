package com.control.asistencia.persistencias;

import com.control.asistencia.persistencias.Cargos;
import com.control.asistencia.persistencias.GradosSecciones;
import com.control.asistencia.persistencias.HistorialNotificiaciones;
import com.control.asistencia.persistencias.Personas;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-12-19T14:36:50")
@StaticMetamodel(PersonalAcademico.class)
public class PersonalAcademico_ { 

    public static volatile SingularAttribute<PersonalAcademico, Cargos> idCargo;
    public static volatile SingularAttribute<PersonalAcademico, String> estadoPersonal;
    public static volatile ListAttribute<PersonalAcademico, HistorialNotificiaciones> historialNotificiacionesList;
    public static volatile SingularAttribute<PersonalAcademico, Integer> idPersonalAcademico;
    public static volatile SingularAttribute<PersonalAcademico, String> detalles;
    public static volatile ListAttribute<PersonalAcademico, GradosSecciones> gradosSeccionesList;
    public static volatile SingularAttribute<PersonalAcademico, Personas> idPersona;

}