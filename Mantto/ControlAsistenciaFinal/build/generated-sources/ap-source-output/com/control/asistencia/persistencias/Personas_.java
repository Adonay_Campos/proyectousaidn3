package com.control.asistencia.persistencias;

import com.control.asistencia.persistencias.Cuentas;
import com.control.asistencia.persistencias.Encargados;
import com.control.asistencia.persistencias.Estudiantes;
import com.control.asistencia.persistencias.PersonalAcademico;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-12-19T14:36:50")
@StaticMetamodel(Personas.class)
public class Personas_ { 

    public static volatile SingularAttribute<Personas, Date> fechaNac;
    public static volatile SingularAttribute<Personas, String> fototext;
    public static volatile SingularAttribute<Personas, byte[]> foto;
    public static volatile ListAttribute<Personas, Estudiantes> estudiantesList;
    public static volatile ListAttribute<Personas, PersonalAcademico> personalAcademicoList;
    public static volatile SingularAttribute<Personas, String> apellido;
    public static volatile SingularAttribute<Personas, String> direccion;
    public static volatile ListAttribute<Personas, Encargados> encargadosList;
    public static volatile SingularAttribute<Personas, Cuentas> idCuenta;
    public static volatile SingularAttribute<Personas, String> nombre;
    public static volatile SingularAttribute<Personas, Integer> idPersona;

}