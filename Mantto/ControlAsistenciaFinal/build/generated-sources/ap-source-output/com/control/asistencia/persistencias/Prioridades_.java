package com.control.asistencia.persistencias;

import com.control.asistencia.persistencias.HistorialNotificiaciones;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-12-19T14:36:50")
@StaticMetamodel(Prioridades.class)
public class Prioridades_ { 

    public static volatile SingularAttribute<Prioridades, String> descripcion;
    public static volatile ListAttribute<Prioridades, HistorialNotificiaciones> historialNotificiacionesList;
    public static volatile SingularAttribute<Prioridades, Integer> idPrioridad;

}