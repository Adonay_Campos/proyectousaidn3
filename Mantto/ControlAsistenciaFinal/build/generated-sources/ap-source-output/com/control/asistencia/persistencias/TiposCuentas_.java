package com.control.asistencia.persistencias;

import com.control.asistencia.persistencias.Cuentas;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-12-19T14:36:50")
@StaticMetamodel(TiposCuentas.class)
public class TiposCuentas_ { 

    public static volatile SingularAttribute<TiposCuentas, String> descripcion;
    public static volatile SingularAttribute<TiposCuentas, Integer> idCuenta;
    public static volatile ListAttribute<TiposCuentas, Cuentas> cuentasList;

}