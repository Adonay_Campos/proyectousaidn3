package com.control.asistencia.persistencias;

import com.control.asistencia.persistencias.HistorialNotificiaciones;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-12-19T14:36:50")
@StaticMetamodel(TiposNotificaciones.class)
public class TiposNotificaciones_ { 

    public static volatile SingularAttribute<TiposNotificaciones, String> descripcion;
    public static volatile ListAttribute<TiposNotificaciones, HistorialNotificiaciones> historialNotificiacionesList;
    public static volatile SingularAttribute<TiposNotificaciones, Integer> idTipo;

}