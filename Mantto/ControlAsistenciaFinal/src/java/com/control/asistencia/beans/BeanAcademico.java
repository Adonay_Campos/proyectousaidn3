package com.control.asistencia.beans;

import com.control.asistencia.mantenimiento.CargosJpaController;
import com.control.asistencia.mantenimiento.CuentasJpaController;
import com.control.asistencia.mantenimiento.PersonalAcademicoJpaController;
import com.control.asistencia.mantenimiento.PersonasJpaController;
import com.control.asistencia.mantenimiento.TiposCuentasJpaController;
import com.control.asistencia.persistencias.Cargos;
import com.control.asistencia.persistencias.Cuentas;
import com.control.asistencia.persistencias.PersonalAcademico;
import com.control.asistencia.persistencias.Personas;
import com.control.asistencia.persistencias.TiposCuentas;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;

@ManagedBean
@RequestScoped
public class BeanAcademico implements Serializable {

    public BeanAcademico() {
    }

    public Cargos getObCargo() {
        return obCargo;
    }

    public void setObCargo(Cargos obCargo) {
        this.obCargo = obCargo;
    }

    public Cuentas getObCuentas() {
        return obCuentas;
    }

    public void setObCuentas(Cuentas obCuentas) {
        this.obCuentas = obCuentas;
    }

    public Personas getObPersona() {
        return obPersona;
    }

    public void setObPersona(Personas obPersona) {
        this.obPersona = obPersona;
    }

    public TiposCuentas getObTipo() {
        return obTipo;
    }

    public void setObTipo(TiposCuentas obTipo) {
        this.obTipo = obTipo;
    }

    public PersonalAcademico getObAcademico() {
        return obAcademico;
    }

    public void setObAcademico(PersonalAcademico obAcademico) {
        this.obAcademico = obAcademico;
    }

    public ArrayList getArrayPersonalAca() {
        return ArrayPersonalAca;
    }

    public void setArrayPersonalAca(ArrayList ArrayPersonalAca) {
        this.ArrayPersonalAca = ArrayPersonalAca;
    }

    public ArrayList getArrayListCargos() {
        return ArrayListCargos;
    }

    public void setArrayListCargos(ArrayList ArrayListCargos) {
        this.ArrayListCargos = ArrayListCargos;
    }

    public ArrayList getCuentasperTipo() {
        return cuentasperTipo;
    }

    public void setCuentasperTipo(ArrayList cuentasperTipo) {
        this.cuentasperTipo = cuentasperTipo;
    }

    public ArrayList getPersonasCuentas() {
        return personasCuentas;
    }

    public void setPersonasCuentas(ArrayList personasCuentas) {
        this.personasCuentas = personasCuentas;
    }

    public ArrayList getCuentasArrayList() {
        return cuentasArrayList;
    }

    public void setCuentasArrayList(ArrayList cuentasArrayList) {
        this.cuentasArrayList = cuentasArrayList;
    }

    public ArrayList getTiposCuentaArrayList() {
        return tiposCuentaArrayList;
    }

    public void setTiposCuentaArrayList(ArrayList tiposCuentaArrayList) {
        this.tiposCuentaArrayList = tiposCuentaArrayList;
    }

    public CargosJpaController getConcargo() {
        return concargo;
    }

    public void setConcargo(CargosJpaController concargo) {
        this.concargo = concargo;
    }

    public CuentasJpaController getConcuentas() {
        return concuentas;
    }

    public void setConcuentas(CuentasJpaController concuentas) {
        this.concuentas = concuentas;
    }

    public PersonasJpaController getConPersonas() {
        return conPersonas;
    }

    public void setConPersonas(PersonasJpaController conPersonas) {
        this.conPersonas = conPersonas;
    }

    public TiposCuentasJpaController getContipo() {
        return contipo;
    }

    public void setContipo(TiposCuentasJpaController contipo) {
        this.contipo = contipo;
    }

    public PersonalAcademicoJpaController getConPersonalacademic() {
        return conPersonalacademic;
    }

    public void setConPersonalacademic(PersonalAcademicoJpaController conPersonalacademic) {
        this.conPersonalacademic = conPersonalacademic;
    }

    private Cargos obCargo = new Cargos();
    private Cuentas obCuentas = new Cuentas();
    private Personas obPersona = new Personas();
    private TiposCuentas obTipo = new TiposCuentas();
    private PersonalAcademico obAcademico = new PersonalAcademico();
    private ArrayList ArrayPersonalAca = new ArrayList();
    private ArrayList ArrayListCargos = new ArrayList();
    private ArrayList cuentasperTipo = new ArrayList();
    private ArrayList personasCuentas = new ArrayList();
    private ArrayList cuentasArrayList = new ArrayList();
    private ArrayList tiposCuentaArrayList = new ArrayList();
    private ArrayList personasArray = new ArrayList();
    private CargosJpaController concargo = new CargosJpaController();
    private CuentasJpaController concuentas = new CuentasJpaController();
    private PersonasJpaController conPersonas = new PersonasJpaController();
    private TiposCuentasJpaController contipo = new TiposCuentasJpaController();
    private PersonalAcademicoJpaController conPersonalacademic = new PersonalAcademicoJpaController();

    public ArrayList<PersonalAcademico> mostrarPersonalAcademico() {
        List<PersonalAcademico> personasList = conPersonalacademic.mostrarPersonalAcademico();
        ArrayPersonalAca = new ArrayList();
        Iterator it = personasList.iterator();
        while (it.hasNext()) {
            PersonalAcademico pa = (PersonalAcademico) it.next();
            ArrayPersonalAca.add(pa);
        }
        return ArrayPersonalAca;
    }

    public ArrayList<PersonalAcademico> mostrarDocentes() {
        List<PersonalAcademico> personasList = conPersonalacademic.mostrarAcademicoDocente();
        cuentasperTipo = new ArrayList();

        Iterator it = personasList.iterator();
        while (it.hasNext()) {
            PersonalAcademico pa = (PersonalAcademico) it.next();
            cuentasperTipo.add(pa);
        }
        return cuentasperTipo;
    }

    public ArrayList<Personas> personaperTipoCuenta(ArrayList Cuentas) {
        List<Personas> personasList = concuentas.cuentasperTipo();
        personasCuentas = new ArrayList();
        Iterator it = personasList.iterator();
        while (it.hasNext()) {
            Personas p = (Personas) it.next();
            personasCuentas.add(p);
        }
        return personasCuentas;
    }

    //Metodo para crear y editar
    public void nuevoAcademico(PersonalAcademico pa) {
        try {
            System.out.println(obAcademico.getIdPersonalAcademico());
            System.out.println(obPersona.getIdPersona());
            if (pa.getIdPersonalAcademico() != null) {

                System.out.println("Editar");
                System.out.println(obAcademico.getIdPersonalAcademico());
                obCuentas.setTipoCuenta(obTipo);
                concuentas.editar(obCuentas);
                obPersona.setIdCuenta(obCuentas);
                conPersonas.editar(obPersona);
                obAcademico.setIdPersona(obPersona);
                obAcademico.setIdCargo(obCargo);
                conPersonalacademic.editar(obAcademico);
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Se editado con Éxito", "");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                PrimeFaces.current().ajax().update("form");
            } else {
                System.out.println(obAcademico.getIdPersonalAcademico());
                System.out.println("NUevo");
                System.out.println(obTipo.getIdCuenta());
                obCuentas.setTipoCuenta(obTipo);
                concuentas.crear(obCuentas);
                System.out.println("Creo cuenta");
                int idc = concuentas.selectMax();
                obCuentas.setIdCuenta(idc);

                obPersona.setIdCuenta(obCuentas);
                conPersonas.crear(obPersona);
                int max = conPersonas.selectMax();

                obPersona.setIdPersona(max);

                obAcademico.setIdPersona(obPersona);
                obAcademico.setIdCargo(obCargo);
                conPersonalacademic.crear(obAcademico);

                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Se Guardó con Éxito", "");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                PrimeFaces.current().ajax().update("form");
            }
        } catch (Exception e) {
            System.out.println("Error :" + e.getMessage());
            e.printStackTrace();
        }

    }

    private int num;

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public void academicosperId(int id) {
        num = 1;
        System.out.println(num);
        PersonalAcademico pa = new PersonalAcademico();
        pa.setIdPersonalAcademico(id);

        obAcademico = conPersonalacademic.encontrar(pa.getIdPersonalAcademico());
        obPersona = obAcademico.getIdPersona();
        System.out.println(obPersona.getIdPersona());
        obCargo = obAcademico.getIdCargo();
        obCuentas = obAcademico.getIdPersona().getIdCuenta();
        obTipo = obAcademico.getIdPersona().getIdCuenta().getTipoCuenta();
        PrimeFaces.current().executeScript("PF('modale').show();");
        PrimeFaces.current().ajax().update("modale");
    }

    public void valute() {
        FacesMessage msg = null;
        boolean valuteIn;
        System.out.println("/**/*" + this.obPersona.getNombre());
        System.out.println("/*///*" + obPersona.getApellido());
        System.out.println("/**/*" + this.obPersona.getFechaNac());
        System.out.println("/*///*" + this.obPersona.getDireccion());
        System.out.println("/**/*" + this.obCuentas.getCorreo());
        System.out.println("/*///*" + this.obTipo.getDescripcion());
        System.out.println("/**/*" + this.obCargo.getCargo());
        System.out.println("/*///*" + this.obCuentas.getPass());
        if (this.obPersona.getNombre() == null || this.obPersona.getApellido() == null
                || this.obPersona.getFechaNac() == null || this.obPersona.getDireccion() == null || this.obCuentas.getCorreo() == null
                || this.obCuentas.getPass() == null) {
            valuteIn = false;
            msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Error", "Todos los campos son requeridos");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } else {
            valuteIn = true;
            obAcademico.getIdPersonalAcademico();
            obPersona.getIdPersona();
            System.out.println("/*/*/*/*/*" + obPersona.getIdPersona());
            System.out.println(obAcademico.getIdPersonalAcademico());
            nuevoAcademico(obAcademico);
        }
        PrimeFaces.current().ajax().addCallbackParam("valuteIn", valuteIn);
    }

    public void clean() {
        obCuentas = new Cuentas();
        obTipo = new TiposCuentas();
        obCargo = new Cargos();
        obPersona = new Personas();
        obAcademico = new PersonalAcademico();

        PrimeFaces.current().executeScript("PF('modal').show();");
        PrimeFaces.current().ajax().update("modal");
    }
    
        public void cleane() {
        obCuentas = new Cuentas();
        obTipo = new TiposCuentas();
        obCargo = new Cargos();
        obPersona = new Personas();
        obAcademico = new PersonalAcademico();

        PrimeFaces.current().executeScript("PF('modale').show();");
        PrimeFaces.current().ajax().update("modale");
    }
}
