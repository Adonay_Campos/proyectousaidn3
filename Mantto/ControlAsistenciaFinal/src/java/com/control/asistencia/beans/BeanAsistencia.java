package com.control.asistencia.beans;

import static com.control.asistencia.beans.test.count;
import com.control.asistencia.mantenimiento.AsistenciasJpaController;
import com.control.asistencia.mantenimiento.EstudiantesJpaController;
import com.control.asistencia.mantenimiento.GradosSeccionesJpaController;
import com.control.asistencia.mantenimiento.PersonalAcademicoJpaController;
import com.control.asistencia.persistencias.Asistencias;
import com.control.asistencia.persistencias.Encargados;
import com.control.asistencia.persistencias.Estudiantes;
import com.control.asistencia.persistencias.GradosSecciones;
import com.control.asistencia.persistencias.PersonalAcademico;
import java.applet.Applet;
import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

@ManagedBean
@SessionScoped
public class BeanAsistencia extends Applet implements Serializable {

    public BeanAsistencia() {
    }

    public String javaArray[]
            = {"array 1", "array 2", "array 3"};

    public String[] getJavaArray() {
        return javaArray;
    }

    public AsistenciasJpaController getConAsistencia() {
        return conAsistencia;
    }

    public void setConAsistencia(AsistenciasJpaController conAsistencia) {
        this.conAsistencia = conAsistencia;
    }

    public Asistencias getObAsistencias() {
        return obAsistencias;
    }

    public void setObAsistencias(Asistencias obAsistencias) {
        this.obAsistencias = obAsistencias;
    }

    public ArrayList getArrayAsistencia() {
        return arrayAsistencia;
    }

    public void setArrayAsistencia(ArrayList arrayAsistencia) {
        this.arrayAsistencia = arrayAsistencia;
    }

    public Estudiantes getObEstudiantes() {
        return obEstudiantes;
    }

    public void setObEstudiantes(Estudiantes obEstudiantes) {
        this.obEstudiantes = obEstudiantes;
    }

    public String getStringswitch1() {
        return Stringswitch1;
    }

    public void setStringswitch1(String Stringswitch1) {
        this.Stringswitch1 = Stringswitch1;
    }

    public String getStringswitch2() {
        return Stringswitch2;
    }

    public void setStringswitch2(String Stringswitch2) {
        this.Stringswitch2 = Stringswitch2;
    }

    public ArrayList getArrayestudiantePer() {
        return arrayestudiantePer;
    }

    public void setArrayestudiantePer(ArrayList arrayestudiantePer) {
        this.arrayestudiantePer = arrayestudiantePer;
    }

    public EstudiantesJpaController getConEstudia() {
        return conEstudia;
    }

    public void setConEstudia(EstudiantesJpaController conEstudia) {
        this.conEstudia = conEstudia;
    }

    public PersonalAcademico getAcademicoSession() {
        return academicoSession;
    }

    public void setAcademicoSession(PersonalAcademico academicoSession) {
        this.academicoSession = academicoSession;
    }

    public PersonalAcademicoJpaController getConPersonalacademic() {
        return conPersonalacademic;
    }

    public void setConPersonalacademic(PersonalAcademicoJpaController conPersonalacademic) {
        this.conPersonalacademic = conPersonalacademic;
    }

    public ArrayList getArrayEstudianter() {
        return arrayEstudianter;
    }

    public void setArrayEstudianter(ArrayList arrayEstudianter) {
        this.arrayEstudianter = arrayEstudianter;
    }

    public Encargados getEncargadoSession() {
        return encargadoSession;
    }

    public void setEncargadoSession(Encargados encargadoSession) {
        this.encargadoSession = encargadoSession;
    }

    public String getCadena() {
        return cadena;
    }

    public void setCadena(String cadena) {
        this.cadena = cadena;
    }

    private Encargados encargadoSession = (Encargados) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("Encargados");
    private PersonalAcademico academicoSession = (PersonalAcademico) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("personalAca");
    private EstudiantesJpaController conEstudia = new EstudiantesJpaController();
    private AsistenciasJpaController conAsistencia = new AsistenciasJpaController();
    private GradosSeccionesJpaController conGradosSecc = new GradosSeccionesJpaController();
    private PersonalAcademicoJpaController conPersonalacademic = new PersonalAcademicoJpaController();
    private Asistencias obAsistencias = new Asistencias();
    private ArrayList arrayAsistencia;
    private ArrayList arrayEstudianter;
    private ArrayList arrayestudiantePer;
    private Estudiantes obEstudiantes = new Estudiantes();
    private String Stringswitch1;
    private String Stringswitch2;
    private String cadena = ",";

    public GradosSeccionesJpaController getConGradosSecc() {
        return conGradosSecc;
    }

    public void setConGradosSecc(GradosSeccionesJpaController conGradosSecc) {
        this.conGradosSecc = conGradosSecc;
    }

    private String carstext = "[2019-05-01,2019-05-02,2019-05-03,2019-05-04,2019-05-15,]";

    public int getTest() {
        return test;
    }

    public String getCarstext() {
        return carstext;
    }

    public void setCarstext(String carstext) {
        this.carstext = carstext;
    }

    public void setTest(int test) {
        this.test = test;
    }
    private int test = 11;

    public void asisPeralumno(int nie) throws IOException {

        cadena = ",";
        obEstudiantes.setNie(nie);
        String tipoasistencia = "No Asistio";
        List<Asistencias> asisList = conAsistencia.asisPernie(obEstudiantes, tipoasistencia);
        arrayAsistencia = new ArrayList();
        Iterator it = asisList.iterator();

        while (it.hasNext()) {
            Asistencias a = (Asistencias) it.next();

            if (a.getAsistencia().equalsIgnoreCase("No Asistio")) {
                cadena = cadena + a.getFecha() + ",";
            }
        }
        FacesContext.getCurrentInstance().getExternalContext().redirect("asistenciaPerEstudiante.xhtml");

    }

    public ArrayList<Estudiantes> estudiantesPerDOcentes() {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);

        arrayestudiantePer = new ArrayList();
        List<Estudiantes> estuList = conEstudia.estudiantePerDocente(academicoSession, year);
        Iterator it = estuList.iterator();

        while (it.hasNext()) {
            Estudiantes a = (Estudiantes) it.next();
            arrayestudiantePer.add(a);
        }
        return arrayestudiantePer;
    }

    public ArrayList<Estudiantes> estudianteperEncargado() {
        arrayestudiantePer = new ArrayList();

        Encargados e = new Encargados();
        e.setIdEncargado(encargadoSession.getIdEncargado());

        List<Estudiantes> estudianteList = conEstudia.estudiantesPerEncar(e);
        Iterator it = estudianteList.iterator();
        while (it.hasNext()) {
            Estudiantes es = (Estudiantes) it.next();
            arrayestudiantePer.add(es);
        }
        return arrayestudiantePer;
    }

    public void switchValues() {
        obAsistencias.setSwitch1("inactivo");
        obAsistencias.setSwitch2("false");
    }

    public void switchChange(Estudiantes ob) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String fechaD = simpleDateFormat.format(new Date());

        String asistenciavar = "";
        if (obAsistencias.getSwitch1().equalsIgnoreCase("true")) {
            asistenciavar = "SI Asistio";
        } else if (obAsistencias.getSwitch2().equalsIgnoreCase("TRUE")) {
            asistenciavar = "Permiso";
        } else if ((obAsistencias.getSwitch1().equalsIgnoreCase("false"))) {
            asistenciavar = "No Asistio";
        }

        Asistencias obnewasis = new Asistencias();
        obnewasis = conAsistencia.asisPerfechaynie(ob, fechaD);

        if (obAsistencias.getSwitch1() != null) {
            obAsistencias.setSwitch2("false");

            if (obnewasis != null) {
                int idasistencia = obnewasis.getIdAsistencia();
                obAsistencias.setIdAsistencia(idasistencia);
                obAsistencias.setDetalles("NA");
                obAsistencias.setAsistencia(asistenciavar);
                obAsistencias.setFecha(fechaD);
                obAsistencias.setNie(ob);
                conAsistencia.editar(obAsistencias);

            } else {

                obAsistencias.setAsistencia(asistenciavar);
                obAsistencias.setDetalles("NA");
                obAsistencias.setFecha(fechaD);
                obAsistencias.setNie(ob);
                conAsistencia.crear(obAsistencias);
            }
        }

        obnewasis = conAsistencia.asisPerfechaynie(ob, fechaD);
        if (obAsistencias.getSwitch2() != null) {
            obAsistencias.setSwitch1("false");

            if (obnewasis != null) {
                int idasistencia = obnewasis.getIdAsistencia();
                obAsistencias.setIdAsistencia(idasistencia);
                obAsistencias.setDetalles("NA");
                obAsistencias.setAsistencia(asistenciavar);
                obAsistencias.setFecha(fechaD);
                obAsistencias.setNie(ob);
                conAsistencia.editar(obAsistencias);

            } else {

                obAsistencias.setAsistencia(asistenciavar);
                obAsistencias.setDetalles("NA");
                obAsistencias.setFecha(fechaD);
                obAsistencias.setNie(ob);
                conAsistencia.crear(obAsistencias);
            }
        }
        obAsistencias = new Asistencias();
    }

    public ArrayList<Asistencias> mostrarAsistencias() {
        List<Asistencias> asisList = conAsistencia.mostrarTodo();
        arrayAsistencia = new ArrayList();
        Iterator it = asisList.iterator();

        while (it.hasNext()) {
            Asistencias a = (Asistencias) it.next();
            arrayAsistencia.add(a);
        }
        return arrayAsistencia;
    }

    public void guardarAsistencia(int id) throws AddressException, MessagingException {

        int cant = 0;
        List<Asistencias> emailNot = conAsistencia.emailNot();
        if (emailNot.isEmpty()) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Asistencia se ah Guardada con Exito!!", "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } else {

            GradosSecciones gsecc = conGradosSecc.gradoseccPerIdAcademico(id);
            System.out.println("Paso 1");
            Iterator it = emailNot.iterator();
            while (it.hasNext()) {
                obAsistencias = (Asistencias) it.next();
                int NIE = obAsistencias.getNie().getNie();
                if (NIE != 0) {
                    boolean valor = conEstudia.seleccionEstudiantes(NIE, gsecc);
                    if (valor == true) {
                        count += 1;
                    } else {
                    }
                } else {
                }
            }
            System.out.println("Paso 2");
            obAsistencias = null;
            InternetAddress[] internet = new InternetAddress[count];

            Iterator ite = emailNot.iterator();
            while (ite.hasNext()) {
                obAsistencias = (Asistencias) ite.next();
                int NIE = obAsistencias.getNie().getNie();
                if (NIE != 0) {
                    boolean valor = conEstudia.seleccionEstudiantes(NIE, gsecc);
                    System.out.println("Paso 4");
                    if (valor == true) {
                        String correo = obAsistencias.getNie().getIdEncargado().getIdPersona().getIdCuenta().getCorreo();
                        for (int i = 0; i < count; i++) {

                            if (cant == i && correo != null) {
                                internet[i] = new InternetAddress(correo);
                            } else if (cant == i && correo == null) {
                                internet[i] = new InternetAddress(" ");
                            } else {

                            }

                        }
                        cant += 1;
                        System.out.println(cant);
                        System.out.println("Paso 4");

                    } else {
                    }

                } else {

                }
                System.out.println("Paso 5");
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Asistencia se ah Guardada con Exito!!", "");
                FacesContext.getCurrentInstance().addMessage(null, msg);

            }

            String subject = "Aviso de Notificacion de Inasistencia para:" + gsecc.getIdGrado().getGrado() + "" + gsecc.getIdSeccion().getDescripcion();
            String body = "Actualmente su hijo/a  del grado antes mencionado no ha asistido en dia de Ahora, por lo cual es necesario que comunique \n"
                    + "lo antes posible alguna razon del porque no ha podido presentarse, si Usted ya ha notificado\n"
                    + "puede hacer caso omiso de este mensaje";

            FacesContext context;
            System.out.println("Paso 6");

            //Prodpiedades de la conexion 
            Properties props = new Properties();

            props.setProperty("mail.smtp.host", "smtp.gmail.com");// Nombre del host de correo, es smtp.gmail.com
            props.setProperty("mail.smtp.starttls.enable", "true");// TLS si está disponible
            props.setProperty("mail.smtp.port", "587");// Puerto de gmail para envio de correos
            props.setProperty("mail.smtp.user", "Adonay campos melendez");// Nombre del usuario
            props.setProperty("mail.smtp.auth", "true");// Si requiere o no usuario y password para conectarse.

            // Preparando sesion
            Session session = Session.getDefaultInstance(props);
            session.setDebug(true);
            System.out.println("Paso 7");

            //Mensaje
            MimeMessage message = new MimeMessage(session);
//            message.setFrom(new InternetAddress("cruzrojaaplicacion@gmail.com"));// Remitente
            message.setFrom(new InternetAddress("jhadhe58@gmail.com"));
            message.addRecipients(Message.RecipientType.TO, internet); //Receptor
            // Datos que llevara el mensaje
            System.out.println("Paso 8");

            message.setSubject(subject);
            message.setText(body);
            Transport t = session.getTransport("smtp");
//            t.connect("cruzrojaaplicacion@gmail.com", "cruzroja01");
            t.connect("jhadhe58@gmail.com", "Clashroyale1007");
            t.sendMessage(message, message.getAllRecipients());

            // Cierre
            t.close();

            System.out.println("Paso Final");

        }

    }
}
