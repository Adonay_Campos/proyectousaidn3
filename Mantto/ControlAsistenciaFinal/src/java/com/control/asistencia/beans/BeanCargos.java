package com.control.asistencia.beans;

import com.control.asistencia.mantenimiento.CargosJpaController;
import com.control.asistencia.persistencias.Cargos;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean
@RequestScoped
public class BeanCargos {

    public BeanCargos() {
    }

    public CargosJpaController getConCargos() {
        return conCargos;
    }

    public void setConCargos(CargosJpaController conCargos) {
        this.conCargos = conCargos;
    }

    public Cargos getObCargos() {
        return obCargos;
    }

    public void setObCargos(Cargos obCargos) {
        this.obCargos = obCargos;
    }

    public ArrayList getArrayCargos() {
        return arrayCargos;
    }

    public void setArrayCargos(ArrayList arrayCargos) {
        this.arrayCargos = arrayCargos;
    }

    private CargosJpaController conCargos = new CargosJpaController();
    private Cargos obCargos = new Cargos();
    private ArrayList arrayCargos;

    public ArrayList<Cargos> mostrarCargos() {
        List<Cargos> cargosList = conCargos.mostrarTodo();
        arrayCargos = new ArrayList();

        Iterator it = cargosList.iterator();
        while (it.hasNext()) {
            Cargos c = (Cargos) it.next();
            arrayCargos.add(c);
        }
        return arrayCargos;
    }
}
