/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.control.asistencia.beans;

import java.util.Properties;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.primefaces.PrimeFaces;

/**
 *
 * @author luis.castillousam
 */
@ManagedBean
@RequestScoped
public class BeanContact {

    private String tel;
    private String name;
    private String correo;
    private String body;

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
    
    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void contactUs() throws Exception {
        FacesContext context;
        //Prodpiedades de la conexion 
        Properties props = new Properties();

        props.setProperty("mail.smtp.host", "smtp.gmail.com");// Nombre del host de correo, es smtp.gmail.com
        props.setProperty("mail.smtp.starttls.enable", "true");// TLS si está disponible
        props.setProperty("mail.smtp.port", "587");// Puerto de gmail para envio de correos
        props.setProperty("mail.smtp.user", "cruzrojaaplicacion@gmail.com");// Nombre del usuario
        props.setProperty("mail.smtp.auth", "true");// Si requiere o no usuario y password para conectarse.

        // Preparando sesion
        Session session = Session.getDefaultInstance(props);
        session.setDebug(true);

        //Mensaje
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress("cruzrojaaplicacion@gmail.com"));// Remitente
        message.addRecipient(Message.RecipientType.TO, new InternetAddress("cruzrojaaplicacion@gmail.com")); //Receptor
        // Datos que llevara el mensaje
        message.setSubject("Opinion Publica");
        message.setText(name + "---" + tel + "\n--" + correo + "\n--"+ body);

        Transport t = session.getTransport("smtp");
        t.connect("cruzrojaaplicacion@gmail.com", "cruzroja01");
        t.sendMessage(message, message.getAllRecipients());

        // Cierre
        t.close();
        
        tel = null;
        name = null;
        correo = null;
        body = null;
        PrimeFaces.current().ajax().update("form"); 
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje Enviado!", ""); 
        FacesContext.getCurrentInstance().addMessage(null, msg); 

    }

    public BeanContact() {
    }

}
