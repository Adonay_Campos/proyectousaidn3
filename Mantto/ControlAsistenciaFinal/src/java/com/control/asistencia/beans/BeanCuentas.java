/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.control.asistencia.beans;

import com.control.asistencia.mantenimiento.CuentasJpaController;
import com.control.asistencia.mantenimiento.EncargadosJpaController;
import com.control.asistencia.mantenimiento.PersonalAcademicoJpaController;
import com.control.asistencia.mantenimiento.PersonasJpaController;
import com.control.asistencia.mantenimiento.TiposCuentasJpaController;
import com.control.asistencia.persistencias.Cuentas;
import com.control.asistencia.persistencias.Encargados;
import com.control.asistencia.persistencias.PersonalAcademico;
import com.control.asistencia.persistencias.Personas;
import com.control.asistencia.persistencias.TiposCuentas;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;

/**
 *
 * @author luis.castillousam
 */
@ManagedBean
@RequestScoped
public class BeanCuentas implements Serializable {

    public BeanCuentas() {
    }

    public Cuentas getCuentas() {
        return cuentas;
    }

    public void setCuentas(Cuentas cuentas) {
        this.cuentas = cuentas;
    }

    public Personas getObPersona() {
        return obPersona;
    }

    public void setObPersona(Personas obPersona) {
        this.obPersona = obPersona;
    }

    public TiposCuentas getTipo() {
        return tipo;
    }

    public void setTipo(TiposCuentas tipo) {
        this.tipo = tipo;
    }

    public Encargados getObjEncargados() {
        return objEncargados;
    }

    public void setObjEncargados(Encargados objEncargados) {
        this.objEncargados = objEncargados;
    }

    public ArrayList getCuentasArrayList() {
        return cuentasArrayList;
    }

    public void setCuentasArrayList(ArrayList cuentasArrayList) {
        this.cuentasArrayList = cuentasArrayList;
    }

    public ArrayList getTiposCuentaArrayList() {
        return tiposCuentaArrayList;
    }

    public void setTiposCuentaArrayList(ArrayList tiposCuentaArrayList) {
        this.tiposCuentaArrayList = tiposCuentaArrayList;
    }

    public CuentasJpaController getConcuentas() {
        return concuentas;
    }

    public void setConcuentas(CuentasJpaController concuentas) {
        this.concuentas = concuentas;
    }

    public PersonasJpaController getConPersonas() {
        return conPersonas;
    }

    public void setConPersonas(PersonasJpaController conPersonas) {
        this.conPersonas = conPersonas;
    }

    public TiposCuentasJpaController getContipo() {
        return contipo;
    }

    public void setContipo(TiposCuentasJpaController contipo) {
        this.contipo = contipo;
    }

    public EncargadosJpaController getConEncargados() {
        return conEncargados;
    }

    public void setConEncargados(EncargadosJpaController conEncargados) {
        this.conEncargados = conEncargados;
    }

    private PersonalAcademico personalAcademico = new PersonalAcademico();
    private PersonalAcademicoJpaController pac = new PersonalAcademicoJpaController();
    private Cuentas cuentas = new Cuentas();
    private Personas obPersona = new Personas();
    private TiposCuentas tipo = new TiposCuentas();
    private Encargados objEncargados = new Encargados();
    private ArrayList cuentasArrayList = new ArrayList();
    private ArrayList tiposCuentaArrayList = new ArrayList();
    private CuentasJpaController concuentas = new CuentasJpaController();
    private PersonasJpaController conPersonas = new PersonasJpaController();
    private TiposCuentasJpaController contipo = new TiposCuentasJpaController();
    private EncargadosJpaController conEncargados = new EncargadosJpaController();

    public void clean() {
        cuentas = new Cuentas();
        tipo = new TiposCuentas();
        obPersona = new Personas();
        objEncargados = new Encargados();

        PrimeFaces.current().executeScript("PF('modalData').show();");
        PrimeFaces.current().ajax().update("formCuenta");
    }

    public ArrayList<TiposCuentas> mostrarTipos() {
        List<TiposCuentas> cuentaslList = contipo.mostrarTodo();
        tiposCuentaArrayList = new ArrayList();
        Iterator it = cuentaslList.iterator();

        while (it.hasNext()) {
            TiposCuentas t = (TiposCuentas) it.next();
            tiposCuentaArrayList.add(t);
        }
        return tiposCuentaArrayList;
    }

    public ArrayList<Cuentas> mostrarCuentas() {
        List<Cuentas> cuentasList = concuentas.mostrarTodo();
        cuentasArrayList = new ArrayList();
        Iterator it = cuentasList.iterator();
        while (it.hasNext()) {
            Cuentas ct = (Cuentas) it.next();
            cuentasArrayList.add(ct);
        }
        return cuentasArrayList;
    }

    public void dataCuentas() {
        cuentas.setTipoCuenta(tipo);
        if (cuentas.getIdCuenta() != null) {
            concuentas.editar(cuentas);
            obPersona.setIdCuenta(cuentas);
            conPersonas.editar(obPersona);

            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cambios Guardados", "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            PrimeFaces.current().ajax().update("formCuenta:table");
        } else {
            concuentas.crear(cuentas);  //Nueva Cuenta
            int idmax = concuentas.selectMax();

            cuentas = new Cuentas();
            cuentas.setIdCuenta(idmax);

            obPersona.setIdCuenta(cuentas);
            conPersonas.crear(obPersona);   //NUeva Persona
            if (cuentas.getTipoCuenta().getIdCuenta().equals(1)) {
                personalAcademico.setIdPersona(obPersona);
                pac.crear(personalAcademico);

                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Se Agregó con Éxito", "");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                PrimeFaces.current().ajax().update("formCuenta:table");
            } else {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Se Agregó con Éxito", "");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                PrimeFaces.current().ajax().update("formCuenta:table");
            }
        }
    }

    //Metodo de llenar ID
    public void cuentasperID(Personas a) {
        try {
            Cuentas obCt = new Cuentas();
            obCt.setIdCuenta(a.getIdCuenta().getIdCuenta());
            cuentas = concuentas.encontrar(obCt.getIdCuenta());
            tipo = cuentas.getTipoCuenta();
            System.out.println("A mitad");
            Personas obper = new Personas();
            obper.setIdPersona(a.getIdPersona());

            obPersona = conPersonas.encontrar(obper.getIdPersona());
            PrimeFaces.current().executeScript("PF('modalData').show();");
            PrimeFaces.current().ajax().update("formCuenta");
        } catch (Exception e) {
            System.out.println("Error al llenar, cuenta por: " + e.getMessage());
        }

    }

    //Se elimina la persona y su cuenta
    public void destroyCuenta(Personas a) {
        Cuentas cu = new Cuentas();
        Encargados enca = new Encargados();
        try {
            int id = a.getIdCuenta().getIdCuenta();
            cu.setIdCuenta(id);
            concuentas.eliminar(cu);

            PrimeFaces.current().ajax().update("formCuenta");
        } catch (Exception e) {
            System.out.println("Error al eliminar, Cuenta por: " + e.getMessage());
        }

    }

    public boolean isDisable() {
        return FacesContext.getCurrentInstance().getRenderResponse();
    }

    public void valute() {
        FacesMessage msg = null;
        boolean valuteIn;

        if (this.obPersona.getIdPersona() == null || this.objEncargados.getIdEncargado() == null
                || this.cuentas.getIdCuenta() == null || this.tipo.getIdCuenta() == null) {
            valuteIn = false;
            msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Error", "Todos los campos son requeridos");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } else {
            valuteIn = true;
            dataCuentas();
        }
        PrimeFaces.current().ajax().addCallbackParam("valuteIn", valuteIn);
    }
}
