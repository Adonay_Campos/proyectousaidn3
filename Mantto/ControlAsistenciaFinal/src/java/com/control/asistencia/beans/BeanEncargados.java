/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.control.asistencia.beans;

import com.control.asistencia.mantenimiento.EncargadosJpaController;
import com.control.asistencia.persistencias.Encargados;
import com.control.asistencia.persistencias.Personas;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;

/**
 *
 * @author diego.mendezUSAM
 */
@ManagedBean
@RequestScoped
public class BeanEncargados {

    public BeanEncargados() {
    }
    private ArrayList encargadosArrayList;
    private Personas objPersonas = new Personas();
    private Encargados objEncargados = new Encargados();
    private EncargadosJpaController conEncargados = new EncargadosJpaController();

    public ArrayList getEncargadosArrayList() {
        return encargadosArrayList;
    }

    public void setEncargadosArrayList(ArrayList encargadosArrayList) {
        this.encargadosArrayList = encargadosArrayList;
    }

    public Encargados getObjEncargados() {
        return objEncargados;
    }

    public void setObjEncargados(Encargados objEncargados) {
        this.objEncargados = objEncargados;
    }

    public Personas getObjPersonas() {
        return objPersonas;
    }

    public void setObjPersonas(Personas objPersonas) {
        this.objPersonas = objPersonas;
    }

    public EncargadosJpaController getConEncargados() {
        return conEncargados;
    }

    public void setConEncargados(EncargadosJpaController conEncargados) {
        this.conEncargados = conEncargados;
    }

    public void clean() {
        objEncargados = new Encargados();
        objPersonas = new Personas();

        PrimeFaces.current().executeScript("PF('modal').show();");
        PrimeFaces.current().ajax().update("form");
    }

    public void nuevoEncargado() {

        objEncargados.setIdPersona(objPersonas);

        if (objEncargados.getIdEncargado() == null) {
            conEncargados.crear(objEncargados);

        } else {
            conEncargados.editar(objEncargados);
        }
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Se Guardó con Éxito", "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
        PrimeFaces.current().ajax().update("form");
    }

    public ArrayList<Encargados> mostrarEncargados() {
        List<Encargados> encargadosList = conEncargados.mostrarTodo();
        encargadosArrayList = new ArrayList();
        Iterator it = encargadosList.iterator();
        while (it.hasNext()) {
            Encargados enc = (Encargados) it.next();
            encargadosArrayList.add(enc);
        }
        return encargadosArrayList;
    }

    public void encargadosperId(int id) {

        Encargados objEnc = new Encargados();
        objEnc.setIdEncargado(id);

        objEncargados = conEncargados.encontrar(objEnc.getIdEncargado());
        objPersonas = objEncargados.getIdPersona();

        PrimeFaces.current().executeScript("PF('modal').show();");
        PrimeFaces.current().ajax().update("form");
    }

    public void destroyEncargados(Encargados id) {
        conEncargados.eliminar(id);
        PrimeFaces.current().ajax().update("form");
    }

    public void valute() {
        FacesMessage msg = null;
        boolean valuteIn;

        if (this.objEncargados.getDui() == null || this.objEncargados.getNit() == null || this.objEncargados.getTelefonoF() == null || this.objEncargados.getTelefonoM() == null
                || this.objEncargados.getParentesco() == null) {
            valuteIn = false;
            msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Error", "Todos los campos son requeridos");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } else {
            valuteIn = true;
            nuevoEncargado();
        }
        //FacesContext.getCurrentInstance().addMessage(null, msg);
        PrimeFaces.current().ajax().addCallbackParam("valuteIn", valuteIn);
    }
}
