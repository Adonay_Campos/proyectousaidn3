package com.control.asistencia.beans;

import com.control.asistencia.mantenimiento.EstudiantesJpaController;
import com.control.asistencia.mantenimiento.GradosSeccionesJpaController;
import com.control.asistencia.mantenimiento.PersonasJpaController;
import com.control.asistencia.persistencias.Encargados;
import com.control.asistencia.persistencias.Estudiantes;
import com.control.asistencia.persistencias.Grados;
import com.control.asistencia.persistencias.GradosSecciones;
import com.control.asistencia.persistencias.PersonalAcademico;
import com.control.asistencia.persistencias.Personas;
import com.control.asistencia.persistencias.Secciones;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;

@ManagedBean
@RequestScoped
public class BeanEstudiantes {

    public BeanEstudiantes() {
    }

    public GradosSeccionesJpaController getConGradoseccion() {
        return conGradoseccion;
    }

    public void setConGradoseccion(GradosSeccionesJpaController conGradoseccion) {
        this.conGradoseccion = conGradoseccion;
    }

    public EstudiantesJpaController getConEstudiantes() {
        return conEstudiantes;
    }

    public void setConEstudiantes(EstudiantesJpaController conEstudiantes) {
        this.conEstudiantes = conEstudiantes;
    }

    public PersonasJpaController getConPersonas() {
        return conPersonas;
    }

    public void setConPersonas(PersonasJpaController conPersonas) {
        this.conPersonas = conPersonas;
    }

    public GradosSecciones getObGradoSecciones() {
        return obGradoSecciones;
    }

    public void setObGradoSecciones(GradosSecciones obGradoSecciones) {
        this.obGradoSecciones = obGradoSecciones;
    }

    public PersonalAcademico getObAcademico() {
        return obAcademico;
    }

    public void setObAcademico(PersonalAcademico obAcademico) {
        this.obAcademico = obAcademico;
    }

    public Estudiantes getObEstudiantes() {
        return obEstudiantes;
    }

    public void setObEstudiantes(Estudiantes obEstudiantes) {
        this.obEstudiantes = obEstudiantes;
    }

    public Encargados getObEncargados() {
        return obEncargados;
    }

    public void setObEncargados(Encargados obEncargados) {
        this.obEncargados = obEncargados;
    }

    public Personas getObPersona() {
        return obPersona;
    }

    public void setObPersona(Personas obPersona) {
        this.obPersona = obPersona;
    }

    public Secciones getObSecciones() {
        return obSecciones;
    }

    public void setObSecciones(Secciones obSecciones) {
        this.obSecciones = obSecciones;
    }

    public Grados getObGrados() {
        return obGrados;
    }

    public void setObGrados(Grados obGrados) {
        this.obGrados = obGrados;
    }

    public ArrayList getArrayEstudiante() {
        return arrayEstudiante;
    }

    public void setArrayEstudiante(ArrayList arrayEstudiante) {
        this.arrayEstudiante = arrayEstudiante;
    }

    public PersonalAcademico getEncargadoSession() {
        return encargadoSession;
    }

    public void setEncargadoSession(PersonalAcademico encargadoSession) {
        this.encargadoSession = encargadoSession;
    }

    private PersonalAcademico encargadoSession = (PersonalAcademico) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("Encargados");
    private GradosSeccionesJpaController conGradoseccion = new GradosSeccionesJpaController();
    private EstudiantesJpaController conEstudiantes = new EstudiantesJpaController();
    private PersonasJpaController conPersonas = new PersonasJpaController();
    private GradosSecciones obGradoSecciones = new GradosSecciones();
    private PersonalAcademico obAcademico = new PersonalAcademico();
    private Estudiantes obEstudiantes = new Estudiantes();
    private Encargados obEncargados = new Encargados();
    private Personas obPersona = new Personas();
    private Secciones obSecciones = new Secciones();
    private Grados obGrados = new Grados();
    private ArrayList arrayEstudiante;

    public void clean() {
        obGradoSecciones = new GradosSecciones();
        obEstudiantes = new Estudiantes();
        obEncargados = new Encargados();
        obPersona = new Personas();
        PrimeFaces.current().ajax().update("form");
        PrimeFaces.current().executeScript("PF('modalNew').show();");
    }

    public boolean isDisable() { // TODO: rename to isReadonly().  
        return FacesContext.getCurrentInstance().getRenderResponse();
    }

    public List<Estudiantes> mostraEstudiante() {
        List<Estudiantes> estudiantelist = conEstudiantes.mostrarTodo();
        arrayEstudiante = new ArrayList();

        Iterator it = estudiantelist.iterator();
        while (it.hasNext()) {
            Estudiantes e = (Estudiantes) it.next();
            arrayEstudiante.add(e);
        }
        return arrayEstudiante;
    }

    public void eliminarEstudiante(Estudiantes a) {
        try {
            conEstudiantes.eliminar(a);
            PrimeFaces.current().ajax().update("form");
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Se eliminado con Exito!", "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } catch (Exception e) {
            System.out.println("Error en: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void nuevoEstudiante() {

        conPersonas.crear(obPersona);  /////Persona  
        int max = conPersonas.selectMax();

        obPersona.setIdPersona(max);

        obEstudiantes.setIdEncargado(obEncargados);
        obEstudiantes.setIdPersona(obPersona);
        obEstudiantes.setIdGs(obGradoSecciones);
        conEstudiantes.crear(obEstudiantes);  /// Estudiante  

        PrimeFaces.current().ajax().update("form");
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Se Guardo con Exito!", "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void editEstudiante() {
        obEstudiantes.setIdEncargado(obEncargados);
        obEstudiantes.setIdPersona(obPersona);
        obEstudiantes.setIdGs(obGradoSecciones);
        conPersonas.editar(obPersona);
        conEstudiantes.editar(obEstudiantes);  /// Estudiante  
        PrimeFaces.current().ajax().update("form");
        PrimeFaces.current().ajax().update("form2");
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Se Guardo con Exito!", "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void estudiantesperID(int id) {

        Estudiantes obe = new Estudiantes();
        obe.setNie(id);

        obEstudiantes = conEstudiantes.encontrar(obe.getNie());
        obEncargados = obEstudiantes.getIdEncargado();
        obPersona = obEstudiantes.getIdPersona();
        obGradoSecciones = obEstudiantes.getIdGs();

        PrimeFaces.current().executeScript("PF('modalEdit').show();");
        PrimeFaces.current().ajax().update("form2");

    }

}
