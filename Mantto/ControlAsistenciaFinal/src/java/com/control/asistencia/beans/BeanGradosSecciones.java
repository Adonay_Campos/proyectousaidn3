
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.control.asistencia.beans;

import com.control.asistencia.mantenimiento.GradosSeccionesJpaController;
import com.control.asistencia.mantenimiento.PersonalAcademicoJpaController;
import com.control.asistencia.mantenimiento.PersonasJpaController;
import com.control.asistencia.persistencias.Grados;
import com.control.asistencia.persistencias.GradosSecciones;
import com.control.asistencia.persistencias.PersonalAcademico;
import com.control.asistencia.persistencias.Personas;
import com.control.asistencia.persistencias.Secciones;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;

/**
 *
 * @author diego.mendezUSAM
 */
@ManagedBean
@RequestScoped
public class BeanGradosSecciones {

    //private int year;
    private ArrayList arrayGrados;
    private ArrayList arraySecciones;
    private ArrayList gradosseccionesArrayList;

    private Grados obGrados = new Grados();
    private Personas obPersonas = new Personas();
    private Secciones obSecciones = new Secciones();
    private GradosSecciones obGradosSecciones = new GradosSecciones();
    private PersonalAcademico obPersonalAcademico = new PersonalAcademico();

    private PersonasJpaController conPersonas = new PersonasJpaController();
    private GradosSeccionesJpaController conGradosSecciones = new GradosSeccionesJpaController();
    private PersonalAcademicoJpaController conPersonalAcademico = new PersonalAcademicoJpaController();

    public Grados getObGrados() {
        return obGrados;
    }

    public void setObGrados(Grados obGrados) {
        this.obGrados = obGrados;
    }

    public Personas getObPersonas() {
        return obPersonas;
    }

    public void setObPersonas(Personas obPersonas) {
        this.obPersonas = obPersonas;
    }

    public Secciones getObSecciones() {
        return obSecciones;
    }

    public void setObSecciones(Secciones obSecciones) {
        this.obSecciones = obSecciones;
    }

    public GradosSecciones getObGradosSecciones() {
        return obGradosSecciones;
    }

    public void setObGradosSecciones(GradosSecciones obGradosSecciones) {
        this.obGradosSecciones = obGradosSecciones;
    }

    public PersonalAcademico getObPersonalAcademico() {
        return obPersonalAcademico;
    }

    public void setObPersonalAcademico(PersonalAcademico obPersonalAcademico) {
        this.obPersonalAcademico = obPersonalAcademico;
    }

    public ArrayList getArrayGrados() {
        return arrayGrados;
    }

    public void setArrayGrados(ArrayList arrayGrados) {
        this.arrayGrados = arrayGrados;
    }

    public ArrayList getArraySecciones() {
        return arraySecciones;
    }

    public void setArraySecciones(ArrayList arraySecciones) {
        this.arraySecciones = arraySecciones;
    }

    public ArrayList getGradosseccionesArrayList() {
        return gradosseccionesArrayList;
    }

    public void setGradosseccionesArrayList(ArrayList gradosseccionesArrayList) {
        this.gradosseccionesArrayList = gradosseccionesArrayList;
    }

    public PersonasJpaController getConPersonas() {
        return conPersonas;
    }

    public void setConPersonas(PersonasJpaController conPersonas) {
        this.conPersonas = conPersonas;
    }

    public GradosSeccionesJpaController getConGradosSecciones() {
        return conGradosSecciones;
    }

    public void setConGradosSecciones(GradosSeccionesJpaController conGradosSecciones) {
        this.conGradosSecciones = conGradosSecciones;
    }

    public PersonalAcademicoJpaController getConPersonalAcademico() {
        return conPersonalAcademico;
    }

    public void setConPersonalAcademico(PersonalAcademicoJpaController conPersonalAcademico) {
        this.conPersonalAcademico = conPersonalAcademico;
    }

    public ArrayList<Secciones> mostrarSecciones() {
        List<Secciones> seccionList = conGradosSecciones.mostrarSecciones();
        arraySecciones = new ArrayList();

        Iterator it = seccionList.iterator();
        while (it.hasNext()) {
            Secciones s = (Secciones) it.next();
            arraySecciones.add(s);
        }
        return arraySecciones;
    }

    public ArrayList<Grados> mostrarGrados() {
        List<Grados> gradosList = conGradosSecciones.mostrarGrados();
        arrayGrados = new ArrayList();
        Iterator it = gradosList.iterator();

        while (it.hasNext()) {
            Grados g = (Grados) it.next();
            arrayGrados.add(g);
        }
        return arrayGrados;
    }

    public void clean() {
        obGrados = new Grados();
        obSecciones = new Secciones();
        obGradosSecciones = new GradosSecciones();
        obPersonalAcademico = new PersonalAcademico();

        PrimeFaces.current().executeScript("PF('modal').show();");
        PrimeFaces.current().ajax().update("form");
    }

    public void nuevoGradoSeccion() {
        obGradosSecciones.setIdGrado(obGrados);
        obGradosSecciones.setIdSeccion(obSecciones);
        obGradosSecciones.setIdPersonalAcademico(obPersonalAcademico);

        if (obGradosSecciones.getIdGs() == null) {
            conGradosSecciones.crear(obGradosSecciones);
        } else {
            conGradosSecciones.editar(obGradosSecciones);
        }
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Se Guardó con Éxito", "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
        PrimeFaces.current().ajax().update("form");
    }

    public ArrayList<GradosSecciones> mostrarGradoSeccion() {
        List<GradosSecciones> gradoseccionList = conGradosSecciones.mostrarTodo();
        gradosseccionesArrayList = new ArrayList();
        Iterator it = gradoseccionList.iterator();
        while (it.hasNext()) {
            GradosSecciones gs = (GradosSecciones) it.next();
            gradosseccionesArrayList.add(gs);
        }
        return gradosseccionesArrayList;
    }

    public void gradoseccionperId(int id) {

        GradosSecciones obgs = new GradosSecciones();
        obgs.setIdGs(id);

        obGradosSecciones = conGradosSecciones.encontrar(obgs.getIdGs());
        obGrados = obGradosSecciones.getIdGrado();
        obSecciones = obGradosSecciones.getIdSeccion();
        obPersonalAcademico = obGradosSecciones.getIdPersonalAcademico();

        PrimeFaces.current().executeScript("PF('modal').show();");
        PrimeFaces.current().ajax().update("form");
    }

    public void destroyGradoSeccion(GradosSecciones id) {
        conGradosSecciones.eliminar(id);
        PrimeFaces.current().ajax().update("form");
    }

    public void valute() {
        FacesMessage msg = null;
        boolean valuteIn;

        if (this.obGrados.getIdGrado() == null && this.obSecciones.getIdSeccion() == null
                && this.obPersonalAcademico.getIdPersonalAcademico() == null) {
            valuteIn = false;
            msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Error", "Todos los campos son requeridos");
            FacesContext.getCurrentInstance().addMessage(null, msg);

        } else {
            valuteIn = true;
            nuevoGradoSeccion();
        }

        //FacesContext.getCurrentInstance().addMessage(null, msg);
        PrimeFaces.current().ajax().addCallbackParam("valuteIn", valuteIn);
    }

}
