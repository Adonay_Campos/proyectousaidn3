package com.control.asistencia.beans;

import com.control.asistencia.mantenimiento.CuentasJpaController;
import com.control.asistencia.mantenimiento.EncargadosJpaController;
import com.control.asistencia.mantenimiento.PersonalAcademicoJpaController;
import com.control.asistencia.mantenimiento.PersonasJpaController;
import com.control.asistencia.persistencias.Cuentas;
import com.control.asistencia.persistencias.Encargados;
import com.control.asistencia.persistencias.PersonalAcademico;
import com.control.asistencia.persistencias.Personas;
import java.io.IOException;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@SessionScoped
public class BeanLogin implements Serializable {

    public BeanLogin() {
    }

    public PersonasJpaController getConPersona() {
        return conPersona;
    }

    public void setConPersona(PersonasJpaController conPersona) {
        this.conPersona = conPersona;
    }

    public CuentasJpaController getConCuentas() {
        return conCuentas;
    }

    public void setConCuentas(CuentasJpaController conCuentas) {
        this.conCuentas = conCuentas;
    }

    public Personas getObPersonas() {
        return obPersonas;
    }

    public void setObPersonas(Personas obPersonas) {
        this.obPersonas = obPersonas;
    }

    public Cuentas getObCuentas() {
        return obCuentas;
    }

    public void setObCuentas(Cuentas obCuentas) {
        this.obCuentas = obCuentas;
    }

    public PersonalAcademicoJpaController getConPersonalacademic() {
        return conPersonalacademic;
    }

    public void setConPersonalacademic(PersonalAcademicoJpaController conPersonalacademic) {
        this.conPersonalacademic = conPersonalacademic;
    }

    public PersonalAcademico getObAcademico() {
        return obAcademico;
    }

    public void setObAcademico(PersonalAcademico obAcademico) {
        this.obAcademico = obAcademico;
    }

    public EncargadosJpaController getConEncargados() {
        return conEncargados;
    }

    public void setConEncargados(EncargadosJpaController conEncargados) {
        this.conEncargados = conEncargados;
    }

    public Encargados getObEncargados() {
        return obEncargados;
    }

    public void setObEncargados(Encargados obEncargados) {
        this.obEncargados = obEncargados;
    }

    private PersonalAcademicoJpaController conPersonalacademic = new PersonalAcademicoJpaController();
    private PersonasJpaController conPersona = new PersonasJpaController();
    private EncargadosJpaController conEncargados = new EncargadosJpaController();
    private Encargados obEncargados = new Encargados();
    private CuentasJpaController conCuentas = new CuentasJpaController();
    private PersonalAcademico obAcademico = new PersonalAcademico();
    private Personas obPersonas = new Personas();
    private Cuentas obCuentas = new Cuentas();

    public void validarLogin() throws IOException {
        Cuentas obc = new Cuentas();
        obPersonas = new Personas();
        System.out.println("Antes de validar");
        obc = conCuentas.validarLogin(obCuentas.getCorreo(), obCuentas.getPass());
        System.out.println("Paso/***-/*/*/*");
        String dbcorreo = "";
        String dbpass = "";
        if (obc != null) {
            dbcorreo = obc.getCorreo();
            dbpass = obc.getPass();
            if (obCuentas.getCorreo().equals(dbcorreo)) {
                if (obCuentas.getPass().equals(dbpass)) {
                    System.out.println("Antes de obPersonas");
                    obPersonas = conPersona.mostrarPersonasperIdc(obc);
                    System.out.println("Antes del swicth");
                    switch (obc.getTipoCuenta().getIdCuenta()) {
                        case 1:
                            PersonalAcademico obPersonalAcademico = new PersonalAcademico();
                            System.out.println("Antes de conPersonas");
                            obPersonalAcademico = conPersonalacademic.academicoPerhuman(obPersonas);
                            System.out.println("Despues");
                            if (obPersonalAcademico.getIdCargo().getIdCargo() == 1) {
                                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Bienvenido Director", "");
                                FacesContext context = FacesContext.getCurrentInstance();
                                context.getExternalContext().getSessionMap().put("Director", obPersonalAcademico);
                                FacesContext.getCurrentInstance().getExternalContext().redirect("Administrativo.xhtml");
                                FacesContext.getCurrentInstance().addMessage(null, msg);

                            } else if (obPersonalAcademico.getIdCargo().getIdCargo() == 2) {
                                FacesContext context = FacesContext.getCurrentInstance();
                                context.getExternalContext().getSessionMap().put("personalAdmin", obPersonalAcademico);
                                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Bienvenido Personal Administrativo", "");
                                FacesContext.getCurrentInstance().getExternalContext().redirect("Administrativo.xhtml");
                            } else {
                                FacesContext context = FacesContext.getCurrentInstance();
                                context.getExternalContext().getSessionMap().put("personalAca", obPersonalAcademico);
                                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Bienvenido Personal Academico", "");
                                FacesContext.getCurrentInstance().getExternalContext().redirect("AsistenciaPerDocente.xhtml");
                            }
                            break;
                        case 2:
                            obEncargados = conEncargados.encargadosperPersona(obPersonas);
                            FacesContext context = FacesContext.getCurrentInstance();
                            context.getExternalContext().getSessionMap().put("Encargados", obEncargados);
                            FacesContext.getCurrentInstance().getExternalContext().redirect("asistenciaEstudiante.xhtml");
                            break;
                        default:
                            FacesMessage msg2 = new FacesMessage(FacesMessage.SEVERITY_INFO, "paso", "");
                            FacesContext.getCurrentInstance().addMessage(null, msg2);
                            break;
                    }
                } else {
                    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Usuario o Contraseña Incorrectas", "");
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                }
            } else {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Usuario o Contraseña Incorrectas", "");
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }
        } else {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Usuario o Contraseña Incorrectas", "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }

    }

    public void logOut() throws IOException {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
    }

}
