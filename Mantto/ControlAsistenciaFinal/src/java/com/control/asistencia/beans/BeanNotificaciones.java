/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.control.asistencia.beans;

import com.control.asistencia.mantenimiento.AsistenciasJpaController;
import com.control.asistencia.mantenimiento.CuentasJpaController;
import com.control.asistencia.mantenimiento.EncargadosJpaController;
import com.control.asistencia.mantenimiento.EstudiantesJpaController;
import com.control.asistencia.mantenimiento.PersonasJpaController;
import com.control.asistencia.persistencias.Asistencias;
import com.control.asistencia.persistencias.Cuentas;
import com.control.asistencia.persistencias.Encargados;
import com.control.asistencia.persistencias.Estudiantes;
import com.control.asistencia.persistencias.Personas;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.mail.Address;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 *
 * @author luis.castillousam
 */
@ManagedBean
@RequestScoped
public class BeanNotificaciones {

    private List<DataMail> email;
    private DataMail mail;
    private TreeNode mailboxes;
    private TreeNode mailbox;

    private Asistencias obAsistencia = new Asistencias();
    private AsistenciasJpaController conAsistencias = new AsistenciasJpaController();

    private Estudiantes obEstudiantes = new Estudiantes();
    private EstudiantesJpaController conEstudiantes = new EstudiantesJpaController();

    private Encargados obEncargados = new Encargados();
    private EncargadosJpaController conEncargados = new EncargadosJpaController();

    private Personas obPersonas = new Personas();
    private PersonasJpaController conPersonas = new PersonasJpaController();

    private Cuentas obCuentas = new Cuentas();
    private CuentasJpaController conCuentas = new CuentasJpaController();

    ;
    
    
    

//    private Properties getServerProperties(String protocol, String host, String port) {
//        Properties properties = new Properties();
//        properties.put(String.format("mail.%s.host", protocol), host);
//        properties.put(String.format("mail.%s.port", protocol), port);
//        properties.setProperty(String.format("mail.%s.socketFactory.class", protocol), "javax.net.ssl.SSLSocketFactory");
//        properties.setProperty(String.format("mail.%s.socketFactory.fallback", protocol), "false");
//        properties.setProperty(String.format("mail.%s.socketFactory.port", protocol), String.valueOf(port));
//        return properties;
//    }
    
        private Properties getServerProperties() {
        Properties properties = new Properties();
        properties.setProperty("mail.store.protocol", "imaps");
        properties.put(String.format("mail.smtp.host"), "smtp.gmail.com");
        properties.put(String.format("mail.smtp.port"), "587");
        properties.setProperty(String.format("mail.smtp.socketFactory.class"), "javax.net.ssl.SSLSocketFactory");
        properties.setProperty(String.format("mail.smtp.socketFactory.fallback"), "false");
        properties.setProperty(String.format("mail.smtp.socketFactory.port"), String.valueOf("587"));
        return properties;
    }

    @PostConstruct
    public void inicio() {

        mailboxes = new DefaultTreeNode("root", null);
        TreeNode inbox = new DefaultTreeNode("i", "Inbox", mailboxes);
        TreeNode sent = new DefaultTreeNode("s", "Sent", mailboxes);
        TreeNode trash = new DefaultTreeNode("t", "Trash", mailboxes);
        TreeNode junk = new DefaultTreeNode("j", "Junk", mailboxes);
        TreeNode gmail = new DefaultTreeNode("Gmail", inbox);
        TreeNode hotmail = new DefaultTreeNode("Hotmail", inbox);

        String protocol = "imaps";
        String host = "imap.gmail.com";
        String port = "993";
//        String userName = "Adonay campos melendez";
        String userName = "adocampos27@gmail.com";
        String password = "Clash royale10";

        Properties properties = getServerProperties();
        Session session = Session.getDefaultInstance(properties);

        try {
            String from = "";
            String to = "";
            String subject = "";
            String date = "";
            String body = "";
            email = new ArrayList<DataMail>();

            Store store = session.getStore(protocol);
            store.connect(host,userName, password);
            System.out.println("Paso la conexion");
            Folder msg = store.getFolder("INBOX");
            msg.open(Folder.READ_WRITE);

            
            int count = msg.getMessageCount();
            Message[] messages = msg.getMessages(1, count);
            int i = 1;
            for (Message message : messages) {
                i++;
                System.out.println("for"+i);
                if (!message.getFlags().contains(Flags.Flag.SEEN) || message.getFlags().contains(Flags.Flag.SEEN)) {
                    Address[] fromAddresses = message.getFrom();

                    if (!(fromAddresses[0].toString().isEmpty())) {
                        from = fromAddresses[0].toString();
                        to = parseAddresses(message.getRecipients(RecipientType.TO));
                        subject = message.getSubject();
                        date = message.getSentDate().toString();

                        try {
                            body = message.getContent().toString();
                        } catch (Exception e) {
                            body = "Error reading content!!";
                        }
                        /*    System.out.println("...................");
                    System.out.println("\t From: " + fromAddresses[0].toString());
                    System.out.println("\t To: " + parseAddresses(message.getRecipients(RecipientType.TO)));
                    System.out.println("\t Subject: " + message.getSubject());
                    System.out.println("\t Sent Date:" + message.getSentDate().toString());
                        try {
                        System.out.println("Cuerpo:"+ message.getContent().toString());
                    } catch (Exception ex) {
                        System.out.println("Error reading content!!");
                        ex.printStackTrace();
                    }
                        dm.setFrom(from);
                        dm.setTo(to);
                        dm.setSubject(subject);
                        dm.setDate(date);
                        dm.setBody(body);*/

                        email.add(new DataMail(from, to, subject, date, body));
                    } else {
                    }
                }
            }
            msg.close(false);
            store.close();
        } catch (NoSuchProviderException ex) {
            System.out.println("No provider for protocol: " + protocol);
            ex.printStackTrace();
        } catch (MessagingException ex) {
            System.out.println("Could not connect to the message store");
            ex.printStackTrace();
        }
    }

    public TreeNode getMailbox() {
        return mailbox;
    }

    public void setMailbox(TreeNode mailbox) {
        this.mailbox = mailbox;
    }

    public TreeNode getMailboxes() {
        return mailboxes;
    }

    public void setMailboxes(TreeNode mailboxes) {
        this.mailboxes = mailboxes;
    }

    public DataMail getMail() {
        return mail;
    }

    public void setMail(DataMail mail) {
        this.mail = mail;
    }

    public List<DataMail> getEmail() {
        return email;
    }

    public void setEmail(List<DataMail> email) {
        this.email = email;
    }

    private String parseAddresses(Address[] address) {
        String listOfAddress = "";
        if ((address == null) || (address.length < 1)) {
            return null;
        }
        if (!(address[0] instanceof InternetAddress)) {
            return null;
        }
        for (int i = 0; i < address.length;
                i++) {
            InternetAddress internetAddress = (InternetAddress) address[0];
            listOfAddress += internetAddress.getAddress() + ",";
        }
        return listOfAddress;
    }

}
