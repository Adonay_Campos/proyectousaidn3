package com.control.asistencia.beans;

import com.control.asistencia.mantenimiento.CuentasJpaController;
import com.control.asistencia.mantenimiento.PersonasJpaController;
import com.control.asistencia.persistencias.Cuentas;
import com.control.asistencia.persistencias.Personas;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;

@ManagedBean
@RequestScoped

public class BeanPersona {

    public BeanPersona() {
    }

    public PersonasJpaController getConPersonas() {
        return conPersonas;
    }

    public void setConPersonas(PersonasJpaController conPersonas) {
        this.conPersonas = conPersonas;
    }

    public Personas getObPersona() {
        return obPersona;
    }

    public void setObPersona(Personas obPersona) {
        this.obPersona = obPersona;
    }

    public ArrayList getPersonasArray() {
        return personasArray;
    }

    public void setPersonasArray(ArrayList personasArray) {
        this.personasArray = personasArray;
    }

    public CuentasJpaController getConcuentas() {
        return concuentas;
    }

    public void setConcuentas(CuentasJpaController concuentas) {
        this.concuentas = concuentas;
    }

    public ArrayList getArrayPrueba() {
        return arrayPrueba;
    }

    public void setArrayPrueba(ArrayList arrayPrueba) {
        this.arrayPrueba = arrayPrueba;
    }

    public int getContador() {
        return contador;
    }

    public void setContador(int contador) {
        this.contador = contador;
    }

    public String[] getFechasArray() {
        return fechasArray;
    }

    public void setFechasArray(String[] fechasArray) {
        this.fechasArray = fechasArray;
    }

    private ArrayList arrayPrueba;
    private PersonasJpaController conPersonas = new PersonasJpaController();
    private Personas obPersona = new Personas();
    private CuentasJpaController concuentas = new CuentasJpaController();
    private Cuentas obCuentas = new Cuentas();

    public Cuentas getObCuentas() {
        return obCuentas;
    }

    public void setObCuentas(Cuentas obCuentas) {
        this.obCuentas = obCuentas;
    }

    private ArrayList personasArray;
    private int contador;
    private String[] fechasArray;

    public ArrayList<Personas> personasCuentasnull() {
        List<Personas> personasList = new ArrayList<>();
        personasList = conPersonas.personasCuentasnonNull();
        personasArray = new ArrayList();
        Iterator it = personasList.iterator();

        while (it.hasNext()) {
            Personas p = (Personas) it.next();
            personasArray.add(p);
        }
        return personasArray;
    }

    public ArrayList<Personas> mostrarPersonas() {
        List<Personas> personasList = conPersonas.mostrarTodo();
        personasArray = new ArrayList();
        Iterator it = personasList.iterator();

        while (it.hasNext()) {
            Personas p = (Personas) it.next();
            personasArray.add(p);
        }
        return personasArray;
    }

    public void nuevaPersona() {

        if (obPersona.getIdPersona() != null) {
            obCuentas.getIdCuenta();
            obPersona.setIdCuenta(obCuentas);
            conPersonas.editar(obPersona);
        } else {
            conPersonas.crear(obPersona);
        }
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Se Guardó con Éxito", "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
        PrimeFaces.current().ajax().update("form");
    }

    public void personaperId(int id) {
        Personas obper = new Personas();
        obper.setIdPersona(id);

        obPersona = conPersonas.encontrar(obper.getIdPersona());
        obCuentas.setIdCuenta(obPersona.getIdCuenta().getIdCuenta());
        PrimeFaces.current().executeScript("PF('modal').show();");
        PrimeFaces.current().ajax().update("form");
    }

    public void clean() {
        obPersona = new Personas();

        PrimeFaces.current().executeScript("PF('modal').show();");
        PrimeFaces.current().ajax().update("form");
    }

    public void eliminarper(Personas a) {
        try {
            conPersonas.eliminar(a);
            PrimeFaces.current().ajax().update("form");
        } catch (Exception e) {
        }
    }

    public void valute() {
        FacesMessage msg = null;
        boolean valuteIn;
        System.out.println(this.obPersona.getNombre());
        System.out.println(obPersona.getNombre());
        if (this.obPersona.getNombre() == null || this.obPersona.getApellido() == null
                || this.obPersona.getFechaNac() == null || this.obPersona.getDireccion() == null) {
            valuteIn = false;
            msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Error", "Todos los campos son requeridos");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } else {
            valuteIn = true;
            nuevaPersona();
        }
        PrimeFaces.current().ajax().addCallbackParam("valuteIn", valuteIn);
    }
}
