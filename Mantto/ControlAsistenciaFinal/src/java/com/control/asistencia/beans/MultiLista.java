/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.control.asistencia.beans;

import com.control.asistencia.mantenimiento.EstudiantesJpaController;
import com.control.asistencia.mantenimiento.GradosSeccionesJpaController;
import com.control.asistencia.persistencias.Cuentas;
import com.control.asistencia.persistencias.Estudiantes;
import com.control.asistencia.persistencias.GradosSecciones;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.model.SelectItemGroup;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.primefaces.context.RequestContext;


/**
 *
 * @author luis.castillousam
 */
@ManagedBean
@RequestScoped
public class MultiLista {

    private int[] gradoseccion;
    private List<SelectItem> grasecc;
    private int count;
    private ArrayList dataEstudiantes;
    private ArrayList direcciones;
    private GradosSecciones grss = new GradosSecciones();
    private GradosSeccionesJpaController conGradosSecciones = new GradosSeccionesJpaController();
    private Estudiantes estudiante = new Estudiantes();
    private Cuentas cuenta = new Cuentas();
    private EstudiantesJpaController conestudio = new EstudiantesJpaController();

    private String cc;
    private String subject;
    private String body;

    @PostConstruct
    public void inicio() {

        GradosSeccionesJpaController gss = new GradosSeccionesJpaController();
        grasecc = new ArrayList<SelectItem>();

        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);

        long contar = gss.contador(year);
        int counter = (int) contar;
        List<GradosSecciones> gradoseccionList = conGradosSecciones.mostrarGradoSeccion2(year);

        SelectItemGroup gradosSecc = new SelectItemGroup("Grados - Secciones");

        SelectItem[] itemlist = new SelectItem[counter];

        Iterator it = gradoseccionList.iterator();
        count = 0;
        while (it.hasNext()) {
            grss = (GradosSecciones) it.next();
            for (int a = 0; a < counter; a++) {
                if (a == count) {
                    itemlist[count] = new SelectItem(grss.getIdGs(), "" + grss.getIdGrado().getGrado() + " - " + grss.getIdSeccion().getDescripcion());
                } else {

                }
            }
            count += 1;
        }
        gradosSecc.setSelectItems(itemlist);
        grasecc.add(gradosSecc);

    }

    public void sendData() throws Exception {
        //Me complace decirles que todos seran mis leales subordinados.
        dataEstudiantes = new ArrayList();
        direcciones = new ArrayList();
        count = 0;
        int cant2 = 0;
        int cant = gradoseccion.length;

        for (int i = 0; i < cant; i++) {
            int id = gradoseccion[i];
            List<Estudiantes> datosEstudiantes = conestudio.datosCompletos(id);
            Iterator it = datosEstudiantes.iterator();
            while (it.hasNext()) {
                Estudiantes e = (Estudiantes) it.next();
                count += 1;
            }
        }
        InternetAddress[] internet = new InternetAddress[count];
        
        for (int i = 0; i < cant; i++) {
            int id = gradoseccion[i];
            List<Estudiantes> datosEstudiantes = conestudio.datosCompletos(id);

            Iterator it = datosEstudiantes.iterator();
            while (it.hasNext()) {
                estudiante = (Estudiantes) it.next();
                dataEstudiantes.add(estudiante);
              
                String direccion = "";
                direccion = estudiante.getIdEncargado().getIdPersona().getIdCuenta().getCorreo();
                for (int j = 0; j < count; j++) {
                    if(cant2==j && direccion !=null){
                        internet[j] = new InternetAddress(direccion);
                    }else if (cant2==j && direccion ==null){
                        internet[j] = new InternetAddress("");

                    }else{
                        
                    }
                }
                cant2 += 1;
            }
        }


        FacesContext context;

        //Prodpiedades de la conexion 
        Properties props = new Properties();

        props.setProperty("mail.smtp.host", "smtp.gmail.com");// Nombre del host de correo, es smtp.gmail.com
        props.setProperty("mail.smtp.starttls.enable", "true");// TLS si está disponible
        props.setProperty("mail.smtp.port", "587");// Puerto de gmail para envio de correos
        props.setProperty("mail.smtp.user", "cruzrojaaplicacion@gmail.com");// Nombre del usuario
        props.setProperty("mail.smtp.auth", "true");// Si requiere o no usuario y password para conectarse.

        // Preparando sesion
        Session session = Session.getDefaultInstance(props);
        session.setDebug(true);

        //Mensaje
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress("cruzrojaaplicacion@gmail.com"));// Remitente

        message.addRecipients(Message.RecipientType.TO, internet); //Receptor
        // Datos que llevara el mensaje
        message.setSubject(subject);
        message.setText(body);
        Transport t = session.getTransport("smtp");
        t.connect("cruzrojaaplicacion@gmail.com", "cruzroja01");
        t.sendMessage(message, message.getAllRecipients());

        // Cierre
        t.close();
        RequestContext.getCurrentInstance().update("msg");
        context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Correos Enviados..."));

    }
    

    public ArrayList getDirecciones() {
        return direcciones;
    }

    public void setDirecciones(ArrayList direcciones) {
        this.direcciones = direcciones;
    }

    public Cuentas getCuenta() {
        return cuenta;
    }

    public void setCuenta(Cuentas cuenta) {
        this.cuenta = cuenta;
    }


    public ArrayList getDataEstudiantes() {
        return dataEstudiantes;
    }

    public void setDataEstudiantes(ArrayList dataEstudiantes) {
        this.dataEstudiantes = dataEstudiantes;
    }

    public Estudiantes getEstudiante() {
        return estudiante;
    }

    public void setEstudiante(Estudiantes estudiante) {
        this.estudiante = estudiante;
    }

    public EstudiantesJpaController getConestudio() {
        return conestudio;
    }

    public void setConestudio(EstudiantesJpaController conestudio) {
        this.conestudio = conestudio;
    }

    public GradosSecciones getGrss() {
        return grss;
    }

    public void setGrss(GradosSecciones grss) {
        this.grss = grss;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int[] getGradoseccion() {
        return gradoseccion;
    }

    public void setGradoseccion(int[] gradoseccion) {
        this.gradoseccion = gradoseccion;
    }

    public List<SelectItem> getGrasecc() {
        return grasecc;
    }

    public void setGrasecc(List<SelectItem> grasecc) {
        this.grasecc = grasecc;
    }

    public GradosSeccionesJpaController getConGradosSecciones() {
        return conGradosSecciones;
    }

    public void setConGradosSecciones(GradosSeccionesJpaController conGradosSecciones) {
        this.conGradosSecciones = conGradosSecciones;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }


    
}



