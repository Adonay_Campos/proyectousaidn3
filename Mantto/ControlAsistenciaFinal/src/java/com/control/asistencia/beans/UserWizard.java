/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.control.asistencia.beans;

import java.io.IOException;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.FlowEvent;

@ManagedBean
@ViewScoped

public class UserWizard {
    
    public BeanPersona getUser() {
        return user;
    }
    
    public void setUser(BeanPersona user) {
        this.user = user;
    }
    
    private BeanPersona user = new BeanPersona();
    
    private boolean skip;
    
    public void save() throws IOException {
        
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Correcto", "La Solicitud se Proceso con Exito");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public boolean isSkip() {
        return skip;
    }
    
    public void setSkip(boolean skip) {
        this.skip = skip;
    }
    
    public String onFlowProcess(FlowEvent event) throws IOException {
        if (skip) {
            skip = false;   //reset in case user goes back
            return "confirm";
        } else {
            return event.getNewStep();
        }
        
    }
}
