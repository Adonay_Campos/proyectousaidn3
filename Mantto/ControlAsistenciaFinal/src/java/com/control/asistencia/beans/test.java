/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.control.asistencia.beans;

import com.control.asistencia.mantenimiento.AsistenciasJpaController;
import com.control.asistencia.mantenimiento.EstudiantesJpaController;
import com.control.asistencia.mantenimiento.GradosSeccionesJpaController;
import com.control.asistencia.persistencias.Asistencias;
import com.control.asistencia.persistencias.Estudiantes;
import com.control.asistencia.persistencias.GradosSecciones;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import javax.faces.context.FacesContext;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author luis.castillousam
 */
public class test {

    public static GradosSeccionesJpaController conGradosSecc = new GradosSeccionesJpaController();
    public static AsistenciasJpaController conAsistencia = new AsistenciasJpaController();
    public static Estudiantes obEstudiantes = new Estudiantes();
    public static Asistencias obAsistencias = new Asistencias();
    public static EstudiantesJpaController conEstudia = new EstudiantesJpaController();
    public static int count = 0;

    public static GradosSeccionesJpaController getConGradosSecc() {
        return conGradosSecc;
    }

    public static void setConGradosSecc(GradosSeccionesJpaController conGradosSecc) {
        test.conGradosSecc = conGradosSecc;
    }

    public static AsistenciasJpaController getConAsistencia() {
        return conAsistencia;
    }

    public static void setConAsistencia(AsistenciasJpaController conAsistencia) {
        test.conAsistencia = conAsistencia;
    }

    public static Estudiantes getObEstudiantes() {
        return obEstudiantes;
    }

    public static void setObEstudiantes(Estudiantes obEstudiantes) {
        test.obEstudiantes = obEstudiantes;
    }

    public static EstudiantesJpaController getConEstudia() {
        return conEstudia;
    }

    public static void setConEstudia(EstudiantesJpaController conEstudia) {
        test.conEstudia = conEstudia;
    }

    public static Asistencias getObAsistencias() {
        return obAsistencias;
    }

    public static void setObAsistencias(Asistencias obAsistencias) {
        test.obAsistencias = obAsistencias;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        test.count = count;
    }

    public static void main(String[] args) throws AddressException, MessagingException {
        Integer id = 5;
        int cant = 0;
        List<Asistencias> emailNot = conAsistencia.emailNot();
        GradosSecciones gsecc = conGradosSecc.gradoseccPerIdAcademico(id);

        Iterator it = emailNot.iterator();
        while (it.hasNext()) {
            obAsistencias = (Asistencias) it.next();
            int NIE = obAsistencias.getNie().getNie();
            if (NIE != 0) {
                boolean valor = conEstudia.seleccionEstudiantes(NIE, gsecc);
                if (valor == true) {
                    count += 1;
                } else {
                }
            } else {
            }
        }
        
        obAsistencias = null;
        InternetAddress[] internet = new InternetAddress[count];
        
        Iterator ite = emailNot.iterator();
        while (ite.hasNext()) {
            obAsistencias = (Asistencias) ite.next();
            int NIE = obAsistencias.getNie().getNie();
            if (NIE != 0) {
                boolean valor = conEstudia.seleccionEstudiantes(NIE, gsecc);
                if (valor == true) {
                    String correo = obAsistencias.getNie().getIdEncargado().getIdPersona().getIdCuenta().getCorreo();
                    for (int i = 0; i < count; i++) {

                        if (cant == i && correo != null) {
                            internet[i] = new InternetAddress(correo);
                        } else if (cant == i && correo == null) {
                            internet[i] = new InternetAddress(" ");
                        } else {

                        }

                    }
                    cant += 1;
                    System.out.println(cant);
                } else {
                }
            } else {

            }

        }
        
        String subject = "Aviso de Notificacion de Inasistencia para:"+ gsecc.getIdGrado().getGrado()+""+gsecc.getIdSeccion().getDescripcion();
        String body = "Actualmente su Hijo/a del grado antes mencionado no ha asistido en dia de Ahora, por lo cual es necesario que comunique \n"
                + "lo antes posible alguna razon del porque no ha podido presentarse, si Usted ya ha notificado\n"
                + "puede hacer caso omiso de este mensaje";
        
        FacesContext context;

        //Prodpiedades de la conexion 
        Properties props = new Properties();

        props.setProperty("mail.smtp.host", "smtp.gmail.com");// Nombre del host de correo, es smtp.gmail.com
        props.setProperty("mail.smtp.starttls.enable", "true");// TLS si está disponible
        props.setProperty("mail.smtp.port", "587");// Puerto de gmail para envio de correos
        props.setProperty("mail.smtp.user", "cruzrojaaplicacion@gmail.com");// Nombre del usuario
        props.setProperty("mail.smtp.auth", "true");// Si requiere o no usuario y password para conectarse.

        // Preparando sesion
        Session session = Session.getDefaultInstance(props);
        session.setDebug(true);

        //Mensaje
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress("cruzrojaaplicacion@gmail.com"));// Remitente

        message.addRecipients(Message.RecipientType.TO, internet); //Receptor
        // Datos que llevara el mensaje
        message.setSubject(subject);
        message.setText(body);
        Transport t = session.getTransport("smtp");
        t.connect("cruzrojaaplicacion@gmail.com", "cruzroja01");
        t.sendMessage(message, message.getAllRecipients());

        // Cierre
        t.close();
//        RequestContext.getCurrentInstance().update("msg");
//        context = FacesContext.getCurrentInstance();
//        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Correos Enviados..."));

    }
}

