package com.control.asistencia.mantenimiento;

import com.control.asistencia.persistencias.Asistencias;
import com.control.asistencia.persistencias.Estudiantes;
import com.control.asistencia.util.AbsFacade;
import com.control.asistencia.util.DAO;
import javax.persistence.Query;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class AsistenciasJpaController extends AbsFacade<Asistencias> implements DAO<Asistencias> {

    public AsistenciasJpaController() {
        super(Asistencias.class);
        this.em = Persistence.createEntityManagerFactory("ControlAsistenciaFinalPU").createEntityManager();
    }
    private EntityManager em;

    @Override
    protected EntityManager entityManager() {
        return em;
    }
//    
//    public static void main(String[] args) {
//        AsistenciasJpaController a = new AsistenciasJpaController();
//        Estudiantes e = new Estudiantes();
//        e.setNie(12312);
//        java.util.Date date = new Date();
//        SimpleDateFormat formateador = new SimpleDateFormat("YYYY-MM-dd");
//        String Fecha = formateador.format(date);
//        Asistencias obAsistencias =  a.asisPerfechaynie(e, Fecha);
//        
//        System.out.println("NIE:" + obAsistencias.getNie().getNie() );
//        System.out.println("Fecha:" + obAsistencias.getFecha() );
//    }

    public List asisPernie(Estudiantes nie, String asistencia) {
        List<Asistencias> asistenciasList = null;
        try {
            Query query = em.createQuery("SELECT a FROM Asistencias a JOIN a.nie b WHERE a.nie = :nie AND a.asistencia =:asistencia ");
            query.setParameter("asistencia", asistencia);
            query.setParameter("nie", nie);
            asistenciasList = query.getResultList();
        } catch (Exception e) {
            System.out.println("Error en: " + e.getMessage());
            return asistenciasList;
        }
        return asistenciasList;
    }

    public Asistencias asisPerfechaynie(Estudiantes nie, String fecha) {
        Asistencias obAsistencias = null;
        try {
            Query query = em.createQuery("SELECT a FROM Asistencias a JOIN a.nie b WHERE a.nie = :nie AND a.fecha = :fecha ");
            query.setParameter("fecha", fecha);
            query.setParameter("nie", nie);
            obAsistencias = (Asistencias) query.getSingleResult();
        } catch (Exception e) {
            return null;
        }
        return obAsistencias;
    }

    public Asistencias asisteperNIE(Estudiantes NIE) {
        Asistencias obAsistencias = null;
        try {
            Query query = em.createQuery("select a from Asistencias a WHERE a.nie = :nie");
            query.setParameter("nie", NIE);
            obAsistencias = (Asistencias) query.getSingleResult();
        } catch (Exception e) {
            return null;
        }
        return obAsistencias;
    }

    public int switchUp(Asistencias a) {
        Asistencias obAsistencias = null;
        int flag = 0;

        try {
            obAsistencias = em.find(Asistencias.class, a.getAsistencia());
            obAsistencias.setSwitch1(a.getSwitch1());
            obAsistencias.setSwitch2(a.getSwitch2());
            em.merge(obAsistencias);
            flag = 1;
        } catch (Exception e) {
            return 0;
        }
        return flag;

    }

    public List emailNot() {
        java.util.Date date = new Date();
        SimpleDateFormat formateador = new SimpleDateFormat("YYYY-MM-dd");
        String Fecha = formateador.format(date);
        String asistencia = "No asistio";
        List<Asistencias> listaNot = null;
        try {
            Query query = em.createQuery("select a from Asistencias a WHERE a.asistencia=:asistencia AND a.fecha = :fecha");
            query.setParameter("asistencia", asistencia);
            query.setParameter("fecha", Fecha);
            listaNot = query.getResultList();
        } catch (Exception e) {
            return null;
        }
        return listaNot;
    }

}
