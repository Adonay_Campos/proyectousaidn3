package com.control.asistencia.mantenimiento;

import com.control.asistencia.persistencias.Cargos;
import com.control.asistencia.util.AbsFacade;
import com.control.asistencia.util.DAO;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author victor.penausam
 */
public class CargosJpaController extends AbsFacade<Cargos> implements DAO<Cargos> {

    public CargosJpaController() {
        super(Cargos.class);
        this.em = Persistence.createEntityManagerFactory("ControlAsistenciaFinalPU").createEntityManager();
    }
    private EntityManager em;

    @Override
    public EntityManager entityManager() {
        return em;
    }
}
