package com.control.asistencia.mantenimiento;

import com.control.asistencia.persistencias.Cuentas;
import com.control.asistencia.persistencias.Personas;
import com.control.asistencia.persistencias.TiposCuentas;
import com.control.asistencia.util.AbsFacade;
import com.control.asistencia.util.DAO;
import javax.persistence.Query;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class CuentasJpaController extends AbsFacade<Cuentas> implements DAO<Cuentas> {

    public CuentasJpaController() {
        super(Cuentas.class);
        this.em = Persistence.createEntityManagerFactory("ControlAsistenciaFinalPU").createEntityManager();
    }
    private EntityManager em;

    @Override
    public EntityManager entityManager() {
        return em;
    }

    public Personas personaPercuenta(Cuentas idCuenta) {
        Personas obPersonas = null;
        try {
            Query query = em.createQuery("select c from Cuentas c WHERE c.idCuenta = :idCuenta");
            query.setParameter("idCuenta", idCuenta);
            obPersonas = (Personas) query.getSingleResult();
        } catch (Exception e) {
            return null;
        }
        return obPersonas;
    }

    public Cuentas validarLogin(String correo, String pass) {
        Cuentas obCuentas = null;
        try {
            System.out.println("Hara query");
            System.out.println(correo + " " + pass);
            Query query = em.createQuery("SELECT c FROM Cuentas c WHERE c.correo = :correo and c.pass =:pass");
            query.setParameter("correo", correo);
            query.setParameter("pass", pass);
            obCuentas = (Cuentas) query.getSingleResult();
        } catch (Exception e) {
            System.out.println("Error********" + e.getMessage());
            return null;
        }
        return obCuentas;
    }

    public int selectMax() {
        int max = 0;
        try {
            Query query = em.createQuery("select max(c.idCuenta) from Cuentas c");
            max = (int) query.getSingleResult();
        } catch (Exception e) {
            em.getTransaction().rollback();
            return 0;
        }
        return max;
    }

    public List cuentasperTipo() {
        TiposCuentas tp = new TiposCuentas();
        tp.setIdCuenta(1);
        List<Cuentas> listaCuentas = null;
        try {
            Query query = em.createQuery("select c from Cuentas c where c.tipoCuenta = :tipoCuenta");
            query.setParameter("tipoCuenta", tp);
            listaCuentas = query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return listaCuentas;
        }
        return listaCuentas;
    }
}
