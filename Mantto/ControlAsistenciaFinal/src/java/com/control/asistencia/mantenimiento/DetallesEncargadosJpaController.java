/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.control.asistencia.mantenimiento;

import com.control.asistencia.persistencias.DetallesEncargados;
import com.control.asistencia.util.AbsFacade;
import com.control.asistencia.util.DAO;
import java.io.Serializable;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author victor.penausam
 */
public class DetallesEncargadosJpaController extends AbsFacade<DetallesEncargados> implements DAO<DetallesEncargados> {

    public DetallesEncargadosJpaController() {
        super(DetallesEncargados.class);
        this.em = Persistence.createEntityManagerFactory("ControlAsistenciaFinalPU").createEntityManager();
    }
    private EntityManager em;

    @Override
    public EntityManager entityManager() {
        return em;
    }

}
