/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.control.asistencia.mantenimiento;

import com.control.asistencia.persistencias.DetallesNotificaciones;
import com.control.asistencia.util.AbsFacade;
import com.control.asistencia.util.DAO;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author victor.penausam
 */
public class DetallesNotificacionesJpaController extends AbsFacade<DetallesNotificaciones> implements DAO<DetallesNotificaciones> {

    public DetallesNotificacionesJpaController() {
        super(DetallesNotificaciones.class);
        this.em = Persistence.createEntityManagerFactory("ControlAsistenciaFinalPU").createEntityManager();
    }
    private EntityManager em;

    @Override
    public EntityManager entityManager() {
        return em;
    }

}
