/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.control.asistencia.mantenimiento;

import com.control.asistencia.persistencias.Encargados;
import com.control.asistencia.persistencias.Personas;
import com.control.asistencia.util.AbsFacade;
import com.control.asistencia.util.DAO;
import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author victor.penausam
 */
public class EncargadosJpaController extends AbsFacade<Encargados> implements DAO<Encargados> {

    public EncargadosJpaController() {
        super(Encargados.class);
        this.em = Persistence.createEntityManagerFactory("ControlAsistenciaFinalPU").createEntityManager();
    }
    private EntityManager em;

    @Override
    public EntityManager entityManager() {
        return em;
    }

    public Encargados encargadosperPersona(Personas idPersona) {
        Encargados objEncargados = null;
        em.getTransaction().begin();
        try {
            Query query = em.createQuery("SELECT e FROM Encargados e WHERE e.idPersona = :idPersona");
            query.setParameter("idPersona", idPersona);
            em.getTransaction().commit();
            objEncargados = (Encargados) query.getSingleResult();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return objEncargados;
    }

}
