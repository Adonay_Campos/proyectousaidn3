package com.control.asistencia.mantenimiento;

import com.control.asistencia.persistencias.Encargados;
import com.control.asistencia.persistencias.Estudiantes;
import com.control.asistencia.persistencias.GradosSecciones;
import com.control.asistencia.persistencias.PersonalAcademico;
import com.control.asistencia.util.AbsFacade;
import com.control.asistencia.util.DAO;
import javax.persistence.Query;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class EstudiantesJpaController extends AbsFacade<Estudiantes> implements DAO<Estudiantes> {

    public EstudiantesJpaController() {
        super(Estudiantes.class);
        this.em = Persistence.createEntityManagerFactory("ControlAsistenciaFinalPU").createEntityManager();
    }
    private EntityManager em;

    @Override
    public EntityManager entityManager() {
        return em;
    }

    public List estudiantePerDocente(PersonalAcademico idPersonalAcademico, int year) {
        List<Estudiantes> estudiantesList = null;
        try {
            Query query = em.createQuery("SELECT e FROM Estudiantes e JOIN e.idGs gs WHERE gs.idPersonalAcademico = :idPersonalAcademico AND gs.year = :year ");
            query.setParameter("idPersonalAcademico", idPersonalAcademico);
            query.setParameter("year", year);
            estudiantesList = query.getResultList();

        } catch (Exception e) {
            System.out.println("Error en: "+e.getMessage());
            return estudiantesList;
        }
        return estudiantesList;
    }

    public List<Estudiantes> estudiantesPerEncar(Encargados idEncargado) {
        List<Estudiantes> eslist = null;
        em.getTransaction().begin();
        try {
            Query query = em.createQuery("SELECT e FROM Estudiantes e JOIN e.idEncargado c WHERE e.idEncargado = :idEncargado");
            query.setParameter("idEncargado", idEncargado);
            em.getTransaction().commit();
            eslist = query.getResultList();

        } catch (Exception e) {
            System.out.println("Error en: "+e.getMessage());
            return null;
        }
        return eslist;
    }

    public List datosCompletos(/*GradosSecciones*/int id) {
        List<Estudiantes> datos = null;
        em.getTransaction().begin();
        GradosSecciones idGs = new GradosSecciones();
        idGs.setIdGs(id);
        try {
            Query query = em.createQuery("SELECT e FROM Estudiantes e JOIN e.idGs g WHERE e.idGs =:idGs");
            query.setParameter("idGs", idGs);
            em.getTransaction().commit();
            datos = query.getResultList();
        } catch (Exception e) {
            em.getTransaction().rollback();
        }
        return datos;
    }

    public boolean seleccionEstudiantes(int nie, GradosSecciones idGs) {
        Estudiantes es;
        boolean value = false;
        try {
            Query query = em.createQuery("SELECT e FROM Estudiantes e WHERE e.nie=:nie AND e.idGs=:idGs");
            query.setParameter("nie", nie);
            query.setParameter("idGs", idGs);
            es = (Estudiantes) query.getSingleResult();
            if (es != null) {
                value = true;
            }
        } catch (Exception e) {
            return value;
        }
        return value;
    }

    public boolean findNie(int n) {
        Estudiantes obEstudiante = null;
        boolean value = false;
        try {
            Query query = em.createQuery("SELECT e FROM Estudiantes e WHERE e.nie = :nie");
            if (obEstudiante != null) {
                value = true;
            } else {
                value = false;
            }
        } catch (Exception e) {
            System.out.println("Error en: "+e.getMessage());
            return value;
        }
        return value;
    }
}
