package com.control.asistencia.mantenimiento;

import com.control.asistencia.persistencias.Grados;
import com.control.asistencia.persistencias.GradosSecciones;
import com.control.asistencia.persistencias.PersonalAcademico;
import com.control.asistencia.persistencias.Secciones;
import com.control.asistencia.util.AbsFacade;
import com.control.asistencia.util.DAO;
import javax.persistence.Query;
import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class GradosSeccionesJpaController extends AbsFacade<GradosSecciones> implements DAO<GradosSecciones> {

    public GradosSeccionesJpaController() {
        super(GradosSecciones.class);
        this.em = Persistence.createEntityManagerFactory("ControlAsistenciaFinalPU").createEntityManager();
    }
    private EntityManager em;

    @Override
    public EntityManager entityManager() {
        return em;
    }

    public List mostrarGrados() {
        List<Grados> gradoList = null;
        try {
            Query query = em.createQuery("select g from Grados g");
            gradoList = query.getResultList();

        } catch (Exception e) {
            em.getTransaction().rollback();
        }
        return gradoList;
    }

    public List mostrarSecciones() {
        List<Secciones> seccionList = null;
        try {
            Query query = em.createQuery("select s from Secciones s");
            seccionList = query.getResultList();
        } catch (Exception e) {
            em.getTransaction().rollback();
        }
        return seccionList;
    }

    public List mostrarGradoSeccion2(int year) {
        List<GradosSecciones> listaGradoSeccion = null;
        try {
            Query query = em.createQuery("SELECT g FROM GradosSecciones g WHERE g.year =:year");
            query.setParameter("year", year);
            listaGradoSeccion = query.getResultList();
        } catch (Exception e) {
            em.getTransaction().rollback();
        }
        return listaGradoSeccion;
    }

    public long contador(int year) {
        long cantidad = 0;
        try {
            Query query = em.createQuery("SELECT COUNT(g.idGs) FROM GradosSecciones g WHERE g.year =:year");
            query.setParameter("year", year);
            cantidad = (long) query.getSingleResult();
        } catch (Exception e) {
            em.getTransaction().rollback();
        }
        return cantidad;
    }

    public GradosSecciones gradoseccPerIdAcademico(int idPersonal) {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        PersonalAcademico perA = new PersonalAcademico();
        perA.setIdPersonalAcademico(idPersonal);
        GradosSecciones obGs = null;
        try {
            Query query = em.createQuery("SELECT g from GradosSecciones g WHERE g.idPersonalAcademico=:idPersonalAcademico AND g.year=:year");
            query.setParameter("idPersonalAcademico", perA);
            query.setParameter("year", year);
            obGs = (GradosSecciones) query.getSingleResult();
        } catch (Exception e) {
            System.out.println("Error en: "+e.getMessage());
            return null;
        }
        return obGs;
    }

    public int maxGS() {
        int max = 0;
        try {
            Query query = em.createQuery("select max(g.idGs) from GradosSecciones g ");
            max = (int) query.getSingleResult();
        } catch (Exception e) {
            em.getTransaction().rollback();
        }
        return max;
    }

}
