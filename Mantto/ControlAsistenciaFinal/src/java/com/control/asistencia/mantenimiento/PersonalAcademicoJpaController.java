/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.control.asistencia.mantenimiento;

import com.control.asistencia.persistencias.Cargos;
import com.control.asistencia.persistencias.Cuentas;
import com.control.asistencia.persistencias.PersonalAcademico;
import com.control.asistencia.persistencias.Personas;
import com.control.asistencia.persistencias.TiposCuentas;
import com.control.asistencia.util.AbsFacade;
import com.control.asistencia.util.DAO;
import javax.persistence.Query;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author victor.penausam
 */
public class PersonalAcademicoJpaController extends AbsFacade<PersonalAcademico> implements DAO<PersonalAcademico> {

    public PersonalAcademicoJpaController() {
        super(PersonalAcademico.class);
        this.em = Persistence.createEntityManagerFactory("ControlAsistenciaFinalPU").createEntityManager();
    }
    private EntityManager em;

    @Override
    public EntityManager entityManager() {
        return em;
    }

    public PersonalAcademico academicoPerhuman(Personas idPersona) {
        PersonalAcademico obAcademico = null;
        System.out.println("******" + idPersona.getIdPersona());
        try {
            System.out.println("Antes");
            Query query = em.createQuery("select p from PersonalAcademico p WHERE p.idPersona = :idPersona");
            query.setParameter("idPersona", idPersona);
            System.out.println("///////" + idPersona);
            obAcademico = (PersonalAcademico) query.getSingleResult();
        } catch (Exception e) {
            System.out.println("Error********" + e.getMessage());
            return null;
        }
        System.out.println("Finalizo");
        return obAcademico;
    }

    public Personas mostrarPersonasperIdc(Cuentas idCuenta) {
        Personas obPersonas = null;
        try {
            Query query = em.createQuery("select p from Personas p where p.idCuenta =:idCuenta");
            query.setParameter("idCuenta", idCuenta);
            obPersonas = (Personas) query.getSingleResult();

        } catch (Exception e) {
            em.getTransaction().rollback();
        }
        return obPersonas;
    }

    public List<PersonalAcademico> mostrarAcademicoDocente() {
        List<PersonalAcademico> academicoPersonal = null;
        Cargos c = new Cargos();
        c.setIdCargo(3);
        try {
            Query query = em.createQuery("select p from PersonalAcademico p WHERE p.idCargo = :idCargo");
            query.setParameter("idCargo", c);
            academicoPersonal = query.getResultList();

        } catch (Exception e) {
            em.getTransaction().rollback();
        }
        return academicoPersonal;
    }

    public List<PersonalAcademico> mostrarPersonalAcademico() {
        List<PersonalAcademico> academicoPersonal = null;
        TiposCuentas c = new TiposCuentas();
        c.setIdCuenta(1);
        try {
            Query query = em.createQuery("select p from PersonalAcademico p JOIN p.idPersona e JOIN e.idCuenta c WHERE c.tipoCuenta = :idCuenta");
            query.setParameter("idCuenta", c);
            academicoPersonal = query.getResultList();

        } catch (Exception e) {
            em.getTransaction().rollback();
        }
        return academicoPersonal;
    }
}
