package com.control.asistencia.mantenimiento;

import com.control.asistencia.persistencias.Cuentas;
import com.control.asistencia.persistencias.Personas;
import com.control.asistencia.util.AbsFacade;
import com.control.asistencia.util.DAO;
import javax.persistence.Query;
import java.util.List;
import java.util.ArrayList;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author victor.penausam
 */
public class PersonasJpaController extends AbsFacade<Personas> implements DAO<Personas> {

    public PersonasJpaController() {
        super(Personas.class);
        this.em = Persistence.createEntityManagerFactory("ControlAsistenciaFinalPU").createEntityManager();
    }
    private EntityManager em;

    @Override
    public EntityManager entityManager() {
        return em;
    }

    public List personasCuentasnonNull() {
        List<Personas> personasList = null;
        try {
            Query query = em.createQuery("select p from Personas p WHERE p.idCuenta IS NOT NULL");
            personasList = query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return personasList;
    }

    public Personas mostrarPersonasperIdc(Cuentas idCuenta) {
        Personas obPersonas = null;
        try {
            Query query = em.createQuery("select p from Personas p where p.idCuenta =:idCuenta");
            query.setParameter("idCuenta", idCuenta);
            obPersonas = (Personas) query.getSingleResult();

        } catch (Exception e) {
            em.getTransaction().rollback();
        }
        return obPersonas;
    }

    public List mostrarPersonasperCuenta(ArrayList Cuentas) {
        List<Personas> personasList = null;
        em.getTransaction().begin();
        try {
            Query query = em.createQuery("select p from Personas p where p.idCuenta =:idCuenta");
            query.setParameter("idCuenta", Cuentas);
            em.getTransaction().commit();
            personasList = query.getResultList();

        } catch (Exception e) {
            em.getTransaction().rollback();
        }
        return personasList;
    }

    public int selectMax() {
        int max = 0;
        em.getTransaction().begin();
        try {
            Query query = em.createQuery("select max(p.idPersona) from Personas p");
            em.getTransaction().commit();
            max = (int) query.getSingleResult();
        } catch (Exception e) {
            em.getTransaction().rollback();
        }
        return max;
    }
}
