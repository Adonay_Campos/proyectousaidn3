package com.control.asistencia.mantenimiento;

import com.control.asistencia.persistencias.Secciones;
import com.control.asistencia.util.AbsFacade;
import com.control.asistencia.util.DAO;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;


public class SeccionesJpaController extends AbsFacade<Secciones> implements DAO<Secciones> {

    public SeccionesJpaController() {
        super(Secciones.class);
        this.em = Persistence.createEntityManagerFactory("ControlAsistenciaFinalPU").createEntityManager();
    }
    private EntityManager em;

    @Override
    public EntityManager entityManager() {
        return em;
    }
}
