/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.control.asistencia.mantenimiento;

import com.control.asistencia.persistencias.TiposNotificaciones;
import com.control.asistencia.util.AbsFacade;
import com.control.asistencia.util.DAO;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author victor.penausam
 */
public class TiposNotificacionesJpaController extends AbsFacade<TiposNotificaciones> implements DAO<TiposNotificaciones> {

    public TiposNotificacionesJpaController() {
        super(TiposNotificaciones.class);
        this.em = Persistence.createEntityManagerFactory("ControlAsistenciaFinalPU").createEntityManager();
    }
    private EntityManager em;

    @Override
    public EntityManager entityManager() {
        return em;
    }

}
