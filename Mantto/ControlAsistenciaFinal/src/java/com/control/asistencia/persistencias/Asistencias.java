/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.control.asistencia.persistencias;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Karito y Vict
 */
@Entity
@Table(name = "asistencias")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Asistencias.findAll", query = "SELECT a FROM Asistencias a")
    , @NamedQuery(name = "Asistencias.findByIdAsistencia", query = "SELECT a FROM Asistencias a WHERE a.idAsistencia = :idAsistencia")
    , @NamedQuery(name = "Asistencias.findByFecha", query = "SELECT a FROM Asistencias a WHERE a.fecha = :fecha")
    , @NamedQuery(name = "Asistencias.findByAsistencia", query = "SELECT a FROM Asistencias a WHERE a.asistencia = :asistencia")
    , @NamedQuery(name = "Asistencias.findByDetalles", query = "SELECT a FROM Asistencias a WHERE a.detalles = :detalles")
    , @NamedQuery(name = "Asistencias.findBySwitch1", query = "SELECT a FROM Asistencias a WHERE a.switch1 = :switch1")
    , @NamedQuery(name = "Asistencias.findBySwitch2", query = "SELECT a FROM Asistencias a WHERE a.switch2 = :switch2")})
public class Asistencias implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idAsistencia")
    private Integer idAsistencia;
    @Basic(optional = false)
    @Column(name = "fecha")
    private String fecha;
    @Basic(optional = false)
    @Column(name = "asistencia")
    private String asistencia;
    @Basic(optional = false)
    @Column(name = "detalles")
    private String detalles;
    @Column(name = "switch1")
    private String switch1;
    @Column(name = "switch2")
    private String switch2;
    @JoinColumn(name = "nie", referencedColumnName = "nie")
    @ManyToOne(optional = false)
    private Estudiantes nie;

    public Asistencias() {
    }

    public Asistencias(Integer idAsistencia) {
        this.idAsistencia = idAsistencia;
    }

    public Asistencias(Integer idAsistencia, String fecha, String asistencia, String detalles) {
        this.idAsistencia = idAsistencia;
        this.fecha = fecha;
        this.asistencia = asistencia;
        this.detalles = detalles;
    }

    public Integer getIdAsistencia() {
        return idAsistencia;
    }

    public void setIdAsistencia(Integer idAsistencia) {
        this.idAsistencia = idAsistencia;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getAsistencia() {
        return asistencia;
    }

    public void setAsistencia(String asistencia) {
        this.asistencia = asistencia;
    }

    public String getDetalles() {
        return detalles;
    }

    public void setDetalles(String detalles) {
        this.detalles = detalles;
    }

    public String getSwitch1() {
        return switch1;
    }

    public void setSwitch1(String switch1) {
        this.switch1 = switch1;
    }

    public String getSwitch2() {
        return switch2;
    }

    public void setSwitch2(String switch2) {
        this.switch2 = switch2;
    }

    public Estudiantes getNie() {
        return nie;
    }

    public void setNie(Estudiantes nie) {
        this.nie = nie;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAsistencia != null ? idAsistencia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Asistencias)) {
            return false;
        }
        Asistencias other = (Asistencias) object;
        if ((this.idAsistencia == null && other.idAsistencia != null) || (this.idAsistencia != null && !this.idAsistencia.equals(other.idAsistencia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Com.Persistencia.Asistencias[ idAsistencia=" + idAsistencia + " ]";
    }
    
}
