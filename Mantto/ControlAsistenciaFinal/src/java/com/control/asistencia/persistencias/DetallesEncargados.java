/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.control.asistencia.persistencias;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Karito y Vict
 */
@Entity
@Table(name = "detalles_encargados")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetallesEncargados.findAll", query = "SELECT d FROM DetallesEncargados d")
    , @NamedQuery(name = "DetallesEncargados.findByIdDe", query = "SELECT d FROM DetallesEncargados d WHERE d.idDe = :idDe")})
public class DetallesEncargados implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idDe")
    private Integer idDe;
    @JoinColumn(name = "idDetalles", referencedColumnName = "idNotificaciones")
    @ManyToOne(optional = false)
    private DetallesNotificaciones idDetalles;
    @JoinColumn(name = "idEncargado", referencedColumnName = "idEncargado")
    @ManyToOne(optional = false)
    private Encargados idEncargado;

    public DetallesEncargados() {
    }

    public DetallesEncargados(Integer idDe) {
        this.idDe = idDe;
    }

    public Integer getIdDe() {
        return idDe;
    }

    public void setIdDe(Integer idDe) {
        this.idDe = idDe;
    }

    public DetallesNotificaciones getIdDetalles() {
        return idDetalles;
    }

    public void setIdDetalles(DetallesNotificaciones idDetalles) {
        this.idDetalles = idDetalles;
    }

    public Encargados getIdEncargado() {
        return idEncargado;
    }

    public void setIdEncargado(Encargados idEncargado) {
        this.idEncargado = idEncargado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDe != null ? idDe.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetallesEncargados)) {
            return false;
        }
        DetallesEncargados other = (DetallesEncargados) object;
        if ((this.idDe == null && other.idDe != null) || (this.idDe != null && !this.idDe.equals(other.idDe))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Com.Persistencia.DetallesEncargados[ idDe=" + idDe + " ]";
    }
    
}
