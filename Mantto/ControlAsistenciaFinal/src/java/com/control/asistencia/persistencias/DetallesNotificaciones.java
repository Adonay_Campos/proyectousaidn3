/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.control.asistencia.persistencias;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Karito y Vict
 */
@Entity
@Table(name = "detalles_notificaciones")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetallesNotificaciones.findAll", query = "SELECT d FROM DetallesNotificaciones d")
    , @NamedQuery(name = "DetallesNotificaciones.findByIdNotificaciones", query = "SELECT d FROM DetallesNotificaciones d WHERE d.idNotificaciones = :idNotificaciones")
    , @NamedQuery(name = "DetallesNotificaciones.findByRemitente", query = "SELECT d FROM DetallesNotificaciones d WHERE d.remitente = :remitente")
    , @NamedQuery(name = "DetallesNotificaciones.findByFecha", query = "SELECT d FROM DetallesNotificaciones d WHERE d.fecha = :fecha")})
public class DetallesNotificaciones implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idNotificaciones")
    private Integer idNotificaciones;
    @Basic(optional = false)
    @Lob
    @Column(name = "detalles")
    private String detalles;
    @Basic(optional = false)
    @Column(name = "remitente")
    private String remitente;
    @Basic(optional = false)
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idDetalles")
    private List<DetallesEncargados> detallesEncargadosList;

    public DetallesNotificaciones() {
    }

    public DetallesNotificaciones(Integer idNotificaciones) {
        this.idNotificaciones = idNotificaciones;
    }

    public DetallesNotificaciones(Integer idNotificaciones, String detalles, String remitente, Date fecha) {
        this.idNotificaciones = idNotificaciones;
        this.detalles = detalles;
        this.remitente = remitente;
        this.fecha = fecha;
    }

    public Integer getIdNotificaciones() {
        return idNotificaciones;
    }

    public void setIdNotificaciones(Integer idNotificaciones) {
        this.idNotificaciones = idNotificaciones;
    }

    public String getDetalles() {
        return detalles;
    }

    public void setDetalles(String detalles) {
        this.detalles = detalles;
    }

    public String getRemitente() {
        return remitente;
    }

    public void setRemitente(String remitente) {
        this.remitente = remitente;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @XmlTransient
    public List<DetallesEncargados> getDetallesEncargadosList() {
        return detallesEncargadosList;
    }

    public void setDetallesEncargadosList(List<DetallesEncargados> detallesEncargadosList) {
        this.detallesEncargadosList = detallesEncargadosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNotificaciones != null ? idNotificaciones.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetallesNotificaciones)) {
            return false;
        }
        DetallesNotificaciones other = (DetallesNotificaciones) object;
        if ((this.idNotificaciones == null && other.idNotificaciones != null) || (this.idNotificaciones != null && !this.idNotificaciones.equals(other.idNotificaciones))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Com.Persistencia.DetallesNotificaciones[ idNotificaciones=" + idNotificaciones + " ]";
    }
    
}
