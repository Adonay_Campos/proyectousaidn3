/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.control.asistencia.persistencias;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Karito y Vict
 */
@Entity
@Table(name = "encargados")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Encargados.findAll", query = "SELECT e FROM Encargados e")
    , @NamedQuery(name = "Encargados.findByIdEncargado", query = "SELECT e FROM Encargados e WHERE e.idEncargado = :idEncargado")
    , @NamedQuery(name = "Encargados.findByDui", query = "SELECT e FROM Encargados e WHERE e.dui = :dui")
    , @NamedQuery(name = "Encargados.findByNit", query = "SELECT e FROM Encargados e WHERE e.nit = :nit")
    , @NamedQuery(name = "Encargados.findByTelefonoF", query = "SELECT e FROM Encargados e WHERE e.telefonoF = :telefonoF")
    , @NamedQuery(name = "Encargados.findByTelefonoM", query = "SELECT e FROM Encargados e WHERE e.telefonoM = :telefonoM")
    , @NamedQuery(name = "Encargados.findByParentesco", query = "SELECT e FROM Encargados e WHERE e.parentesco = :parentesco")})
public class Encargados implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idEncargado")
    private Integer idEncargado;
    @Basic(optional = false)
    @Column(name = "dui")
    private String dui;
    @Basic(optional = false)
    @Column(name = "nit")
    private String nit;
    @Basic(optional = false)
    @Column(name = "telefonoF")
    private String telefonoF;
    @Basic(optional = false)
    @Column(name = "telefonoM")
    private String telefonoM;
    @Basic(optional = false)
    @Column(name = "parentesco")
    private String parentesco;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEncargado")
    private List<DetallesEncargados> detallesEncargadosList;
    @OneToMany(mappedBy = "idEncargado")
    private List<Estudiantes> estudiantesList;
    @JoinColumn(name = "idPersona", referencedColumnName = "idPersona")
    @ManyToOne(optional = false)
    private Personas idPersona;
    @OneToMany(mappedBy = "subEncargado")
    private List<Encargados> encargadosList;
    @JoinColumn(name = "subEncargado", referencedColumnName = "idEncargado")
    @ManyToOne
    private Encargados subEncargado;

    public Encargados() {
    }

    public Encargados(Integer idEncargado) {
        this.idEncargado = idEncargado;
    }

    public Encargados(Integer idEncargado, String dui, String nit, String telefonoF, String telefonoM, String parentesco) {
        this.idEncargado = idEncargado;
        this.dui = dui;
        this.nit = nit;
        this.telefonoF = telefonoF;
        this.telefonoM = telefonoM;
        this.parentesco = parentesco;
    }

    public Integer getIdEncargado() {
        return idEncargado;
    }

    public void setIdEncargado(Integer idEncargado) {
        this.idEncargado = idEncargado;
    }

    public String getDui() {
        return dui;
    }

    public void setDui(String dui) {
        this.dui = dui;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getTelefonoF() {
        return telefonoF;
    }

    public void setTelefonoF(String telefonoF) {
        this.telefonoF = telefonoF;
    }

    public String getTelefonoM() {
        return telefonoM;
    }

    public void setTelefonoM(String telefonoM) {
        this.telefonoM = telefonoM;
    }

    public String getParentesco() {
        return parentesco;
    }

    public void setParentesco(String parentesco) {
        this.parentesco = parentesco;
    }

    @XmlTransient
    public List<DetallesEncargados> getDetallesEncargadosList() {
        return detallesEncargadosList;
    }

    public void setDetallesEncargadosList(List<DetallesEncargados> detallesEncargadosList) {
        this.detallesEncargadosList = detallesEncargadosList;
    }

    @XmlTransient
    public List<Estudiantes> getEstudiantesList() {
        return estudiantesList;
    }

    public void setEstudiantesList(List<Estudiantes> estudiantesList) {
        this.estudiantesList = estudiantesList;
    }

    public Personas getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Personas idPersona) {
        this.idPersona = idPersona;
    }

    @XmlTransient
    public List<Encargados> getEncargadosList() {
        return encargadosList;
    }

    public void setEncargadosList(List<Encargados> encargadosList) {
        this.encargadosList = encargadosList;
    }

    public Encargados getSubEncargado() {
        return subEncargado;
    }

    public void setSubEncargado(Encargados subEncargado) {
        this.subEncargado = subEncargado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEncargado != null ? idEncargado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Encargados)) {
            return false;
        }
        Encargados other = (Encargados) object;
        if ((this.idEncargado == null && other.idEncargado != null) || (this.idEncargado != null && !this.idEncargado.equals(other.idEncargado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Com.Persistencia.Encargados[ idEncargado=" + idEncargado + " ]";
    }
    
}
