/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.control.asistencia.persistencias;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Karito y Vict
 */
@Entity
@Table(name = "grados")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Grados.findAll", query = "SELECT g FROM Grados g")
    , @NamedQuery(name = "Grados.findByIdGrado", query = "SELECT g FROM Grados g WHERE g.idGrado = :idGrado")
    , @NamedQuery(name = "Grados.findByGrado", query = "SELECT g FROM Grados g WHERE g.grado = :grado")})
public class Grados implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idGrado")
    private Integer idGrado;
    @Basic(optional = false)
    @Column(name = "grado")
    private String grado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idGrado")
    private List<GradosSecciones> gradosSeccionesList;

    public Grados() {
    }

    public Grados(Integer idGrado) {
        this.idGrado = idGrado;
    }

    public Grados(Integer idGrado, String grado) {
        this.idGrado = idGrado;
        this.grado = grado;
    }

    public Integer getIdGrado() {
        return idGrado;
    }

    public void setIdGrado(Integer idGrado) {
        this.idGrado = idGrado;
    }

    public String getGrado() {
        return grado;
    }

    public void setGrado(String grado) {
        this.grado = grado;
    }

    @XmlTransient
    public List<GradosSecciones> getGradosSeccionesList() {
        return gradosSeccionesList;
    }

    public void setGradosSeccionesList(List<GradosSecciones> gradosSeccionesList) {
        this.gradosSeccionesList = gradosSeccionesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idGrado != null ? idGrado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Grados)) {
            return false;
        }
        Grados other = (Grados) object;
        if ((this.idGrado == null && other.idGrado != null) || (this.idGrado != null && !this.idGrado.equals(other.idGrado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Com.Persistencia.Grados[ idGrado=" + idGrado + " ]";
    }
    
}
