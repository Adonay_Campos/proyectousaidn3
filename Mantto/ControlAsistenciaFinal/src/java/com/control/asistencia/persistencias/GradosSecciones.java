/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.control.asistencia.persistencias;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Karito y Vict
 */
@Entity
@Table(name = "grados_secciones")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GradosSecciones.findAll", query = "SELECT g FROM GradosSecciones g")
    , @NamedQuery(name = "GradosSecciones.findByIdGs", query = "SELECT g FROM GradosSecciones g WHERE g.idGs = :idGs")
    , @NamedQuery(name = "GradosSecciones.findByYear", query = "SELECT g FROM GradosSecciones g WHERE g.year = :year")})
public class GradosSecciones implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idGs")
    private Integer idGs;
    @Basic(optional = false)
    @Column(name = "year")
    private int year;
    @OneToMany(mappedBy = "idGs")
    private List<Estudiantes> estudiantesList;
    @JoinColumn(name = "idPersonalAcademico", referencedColumnName = "idPersonalAcademico")
    @ManyToOne(optional = false)
    private PersonalAcademico idPersonalAcademico;
    @JoinColumn(name = "idGrado", referencedColumnName = "idGrado")
    @ManyToOne(optional = false)
    private Grados idGrado;
    @JoinColumn(name = "idSeccion", referencedColumnName = "idSeccion")
    @ManyToOne(optional = false)
    private Secciones idSeccion;

    public GradosSecciones() {
    }

    public GradosSecciones(Integer idGs) {
        this.idGs = idGs;
    }

    public GradosSecciones(Integer idGs, int year) {
        this.idGs = idGs;
        this.year = year;
    }

    public Integer getIdGs() {
        return idGs;
    }

    public void setIdGs(Integer idGs) {
        this.idGs = idGs;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @XmlTransient
    public List<Estudiantes> getEstudiantesList() {
        return estudiantesList;
    }

    public void setEstudiantesList(List<Estudiantes> estudiantesList) {
        this.estudiantesList = estudiantesList;
    }

    public PersonalAcademico getIdPersonalAcademico() {
        return idPersonalAcademico;
    }

    public void setIdPersonalAcademico(PersonalAcademico idPersonalAcademico) {
        this.idPersonalAcademico = idPersonalAcademico;
    }

    public Grados getIdGrado() {
        return idGrado;
    }

    public void setIdGrado(Grados idGrado) {
        this.idGrado = idGrado;
    }

    public Secciones getIdSeccion() {
        return idSeccion;
    }

    public void setIdSeccion(Secciones idSeccion) {
        this.idSeccion = idSeccion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idGs != null ? idGs.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GradosSecciones)) {
            return false;
        }
        GradosSecciones other = (GradosSecciones) object;
        if ((this.idGs == null && other.idGs != null) || (this.idGs != null && !this.idGs.equals(other.idGs))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Com.Persistencia.GradosSecciones[ idGs=" + idGs + " ]";
    }
    
}
