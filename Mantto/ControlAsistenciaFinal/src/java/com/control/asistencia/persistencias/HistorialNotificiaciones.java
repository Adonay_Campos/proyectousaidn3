/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.control.asistencia.persistencias;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Karito y Vict
 */
@Entity
@Table(name = "historial_notificiaciones")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HistorialNotificiaciones.findAll", query = "SELECT h FROM HistorialNotificiaciones h")
    , @NamedQuery(name = "HistorialNotificiaciones.findByIdNotificiacion", query = "SELECT h FROM HistorialNotificiaciones h WHERE h.idNotificiacion = :idNotificiacion")
    , @NamedQuery(name = "HistorialNotificiaciones.findByFecha", query = "SELECT h FROM HistorialNotificiaciones h WHERE h.fecha = :fecha")})
public class HistorialNotificiaciones implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idNotificiacion")
    private Integer idNotificiacion;
    @Basic(optional = false)
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Basic(optional = false)
    @Lob
    @Column(name = "cuerpo")
    private String cuerpo;
    @JoinColumn(name = "idPersonal", referencedColumnName = "idPersonalAcademico")
    @ManyToOne(optional = false)
    private PersonalAcademico idPersonal;
    @JoinColumn(name = "prioridad", referencedColumnName = "idPrioridad")
    @ManyToOne(optional = false)
    private Prioridades prioridad;
    @JoinColumn(name = "tipo", referencedColumnName = "idTipo")
    @ManyToOne(optional = false)
    private TiposNotificaciones tipo;

    public HistorialNotificiaciones() {
    }

    public HistorialNotificiaciones(Integer idNotificiacion) {
        this.idNotificiacion = idNotificiacion;
    }

    public HistorialNotificiaciones(Integer idNotificiacion, Date fecha, String cuerpo) {
        this.idNotificiacion = idNotificiacion;
        this.fecha = fecha;
        this.cuerpo = cuerpo;
    }

    public Integer getIdNotificiacion() {
        return idNotificiacion;
    }

    public void setIdNotificiacion(Integer idNotificiacion) {
        this.idNotificiacion = idNotificiacion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }

    public PersonalAcademico getIdPersonal() {
        return idPersonal;
    }

    public void setIdPersonal(PersonalAcademico idPersonal) {
        this.idPersonal = idPersonal;
    }

    public Prioridades getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(Prioridades prioridad) {
        this.prioridad = prioridad;
    }

    public TiposNotificaciones getTipo() {
        return tipo;
    }

    public void setTipo(TiposNotificaciones tipo) {
        this.tipo = tipo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNotificiacion != null ? idNotificiacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HistorialNotificiaciones)) {
            return false;
        }
        HistorialNotificiaciones other = (HistorialNotificiaciones) object;
        if ((this.idNotificiacion == null && other.idNotificiacion != null) || (this.idNotificiacion != null && !this.idNotificiacion.equals(other.idNotificiacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Com.Persistencia.HistorialNotificiaciones[ idNotificiacion=" + idNotificiacion + " ]";
    }
    
}
