/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.control.asistencia.persistencias;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Karito y Vict
 */
@Entity
@Table(name = "personal_academico")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PersonalAcademico.findAll", query = "SELECT p FROM PersonalAcademico p")
    , @NamedQuery(name = "PersonalAcademico.findByIdPersonalAcademico", query = "SELECT p FROM PersonalAcademico p WHERE p.idPersonalAcademico = :idPersonalAcademico")
    , @NamedQuery(name = "PersonalAcademico.findByDetalles", query = "SELECT p FROM PersonalAcademico p WHERE p.detalles = :detalles")
    , @NamedQuery(name = "PersonalAcademico.findByEstadoPersonal", query = "SELECT p FROM PersonalAcademico p WHERE p.estadoPersonal = :estadoPersonal")})
public class PersonalAcademico implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idPersonalAcademico")
    private Integer idPersonalAcademico;
    @Column(name = "detalles")
    private String detalles;
    @Column(name = "estadoPersonal")
    private String estadoPersonal;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPersonal")
    private List<HistorialNotificiaciones> historialNotificiacionesList;
    @JoinColumn(name = "idCargo", referencedColumnName = "idCargo")
    @ManyToOne(optional = false)
    private Cargos idCargo;
    @JoinColumn(name = "idPersona", referencedColumnName = "idPersona")
    @ManyToOne(optional = false)
    private Personas idPersona;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPersonalAcademico")
    private List<GradosSecciones> gradosSeccionesList;

    public PersonalAcademico() {
    }

    public PersonalAcademico(Integer idPersonalAcademico) {
        this.idPersonalAcademico = idPersonalAcademico;
    }

    public Integer getIdPersonalAcademico() {
        return idPersonalAcademico;
    }

    public void setIdPersonalAcademico(Integer idPersonalAcademico) {
        this.idPersonalAcademico = idPersonalAcademico;
    }

    public String getDetalles() {
        return detalles;
    }

    public void setDetalles(String detalles) {
        this.detalles = detalles;
    }

    public String getEstadoPersonal() {
        return estadoPersonal;
    }

    public void setEstadoPersonal(String estadoPersonal) {
        this.estadoPersonal = estadoPersonal;
    }

    @XmlTransient
    public List<HistorialNotificiaciones> getHistorialNotificiacionesList() {
        return historialNotificiacionesList;
    }

    public void setHistorialNotificiacionesList(List<HistorialNotificiaciones> historialNotificiacionesList) {
        this.historialNotificiacionesList = historialNotificiacionesList;
    }

    public Cargos getIdCargo() {
        return idCargo;
    }

    public void setIdCargo(Cargos idCargo) {
        this.idCargo = idCargo;
    }

    public Personas getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Personas idPersona) {
        this.idPersona = idPersona;
    }

    @XmlTransient
    public List<GradosSecciones> getGradosSeccionesList() {
        return gradosSeccionesList;
    }

    public void setGradosSeccionesList(List<GradosSecciones> gradosSeccionesList) {
        this.gradosSeccionesList = gradosSeccionesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPersonalAcademico != null ? idPersonalAcademico.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PersonalAcademico)) {
            return false;
        }
        PersonalAcademico other = (PersonalAcademico) object;
        if ((this.idPersonalAcademico == null && other.idPersonalAcademico != null) || (this.idPersonalAcademico != null && !this.idPersonalAcademico.equals(other.idPersonalAcademico))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Com.Persistencia.PersonalAcademico[ idPersonalAcademico=" + idPersonalAcademico + " ]";
    }
    
}
