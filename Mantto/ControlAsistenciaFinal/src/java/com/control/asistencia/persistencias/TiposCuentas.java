/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.control.asistencia.persistencias;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Karito y Vict
 */
@Entity
@Table(name = "tipos_cuentas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TiposCuentas.findAll", query = "SELECT t FROM TiposCuentas t")
    , @NamedQuery(name = "TiposCuentas.findByIdCuenta", query = "SELECT t FROM TiposCuentas t WHERE t.idCuenta = :idCuenta")
    , @NamedQuery(name = "TiposCuentas.findByDescripcion", query = "SELECT t FROM TiposCuentas t WHERE t.descripcion = :descripcion")})
public class TiposCuentas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idCuenta")
    private Integer idCuenta;
    @Basic(optional = false)
    @Column(name = "descripcion")
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tipoCuenta")
    private List<Cuentas> cuentasList;

    public TiposCuentas() {
    }

    public TiposCuentas(Integer idCuenta) {
        this.idCuenta = idCuenta;
    }

    public TiposCuentas(Integer idCuenta, String descripcion) {
        this.idCuenta = idCuenta;
        this.descripcion = descripcion;
    }

    public Integer getIdCuenta() {
        return idCuenta;
    }

    public void setIdCuenta(Integer idCuenta) {
        this.idCuenta = idCuenta;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<Cuentas> getCuentasList() {
        return cuentasList;
    }

    public void setCuentasList(List<Cuentas> cuentasList) {
        this.cuentasList = cuentasList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCuenta != null ? idCuenta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TiposCuentas)) {
            return false;
        }
        TiposCuentas other = (TiposCuentas) object;
        if ((this.idCuenta == null && other.idCuenta != null) || (this.idCuenta != null && !this.idCuenta.equals(other.idCuenta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Com.Persistencia.TiposCuentas[ idCuenta=" + idCuenta + " ]";
    }

}
