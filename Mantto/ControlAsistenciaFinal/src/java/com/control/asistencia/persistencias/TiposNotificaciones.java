/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.control.asistencia.persistencias;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Karito y Vict
 */
@Entity
@Table(name = "tipos_notificaciones")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TiposNotificaciones.findAll", query = "SELECT t FROM TiposNotificaciones t")
    , @NamedQuery(name = "TiposNotificaciones.findByIdTipo", query = "SELECT t FROM TiposNotificaciones t WHERE t.idTipo = :idTipo")
    , @NamedQuery(name = "TiposNotificaciones.findByDescripcion", query = "SELECT t FROM TiposNotificaciones t WHERE t.descripcion = :descripcion")})
public class TiposNotificaciones implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "idTipo")
    private Integer idTipo;
    @Basic(optional = false)
    @Column(name = "descripcion")
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tipo")
    private List<HistorialNotificiaciones> historialNotificiacionesList;

    public TiposNotificaciones() {
    }

    public TiposNotificaciones(Integer idTipo) {
        this.idTipo = idTipo;
    }

    public TiposNotificaciones(Integer idTipo, String descripcion) {
        this.idTipo = idTipo;
        this.descripcion = descripcion;
    }

    public Integer getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(Integer idTipo) {
        this.idTipo = idTipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<HistorialNotificiaciones> getHistorialNotificiacionesList() {
        return historialNotificiacionesList;
    }

    public void setHistorialNotificiacionesList(List<HistorialNotificiaciones> historialNotificiacionesList) {
        this.historialNotificiacionesList = historialNotificiacionesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipo != null ? idTipo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TiposNotificaciones)) {
            return false;
        }
        TiposNotificaciones other = (TiposNotificaciones) object;
        if ((this.idTipo == null && other.idTipo != null) || (this.idTipo != null && !this.idTipo.equals(other.idTipo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Com.Persistencia.TiposNotificaciones[ idTipo=" + idTipo + " ]";
    }
    
}
