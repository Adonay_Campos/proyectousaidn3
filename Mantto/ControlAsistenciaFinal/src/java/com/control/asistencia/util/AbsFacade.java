package com.control.asistencia.util;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;

public abstract class AbsFacade<T> {

    private final Class<T> entityClass;

    public AbsFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    protected abstract EntityManager entityManager();

    public void crear(T entity) {
        entityManager().getTransaction().begin();
        entityManager().persist(entity);
        entityManager().getTransaction().commit();
    }

    public void editar(T entity) {
        entityManager().getTransaction().begin();
        entityManager().merge(entity);
        entityManager().getTransaction().commit();
    }

    public void eliminar(T entity) {
        entityManager().getTransaction().begin();
        entityManager().remove(entityManager().merge(entity));
        entityManager().getTransaction().commit();
    }

    public T encontrar(Object id) {
        return entityManager().find(entityClass, id);
    }

    public List<T> mostrarTodo() {
        CriteriaQuery crq = entityManager().getCriteriaBuilder().createQuery();
        crq.select(crq.from(entityClass));
        return entityManager().createQuery(crq).getResultList();
    }

}
