/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.control.asistencia.util;

import java.util.List;

/**
 *
 * @author adonay.campos
 */
public interface DAO<T> {
   
    public void crear(T e);
    public void eliminar(T e);
    public void editar(T e);
    public T encontrar(Object e);
    public List<T> mostrarTodo();
    
}
