   function mostrar(){
                $('#navbar').toggle();
              }
            
       
   
$(document).ready(function (){
 //agregar paginacion   
 var imgItems = $('.slider li').length; //asignamos la longitud de los slides a una variable 
 var imgPos=1;
for(i=1; i<=imgItems; i++){//recorremos la longitud y le asignamos las etiquetas al paginator
  $('.paginator').append('<li><span class="fa fa-circle pag"></span></li>');  
}

 
 
 $('.slider li').hide();//esconder slides
  $('.slider li:first').show();//mostrar primer slide
  $('.paginator li:first').css({'color':'white'}); //agregar color a primer paginacion
  //declaramos las funciones
    $('.paginator li').click(pagination);
    $('.right span').click(nextSlider);
     $('.left span').click(prevSlider);
     
     
     setInterval(function (){
         nextSlider();
     }, 5000);
   //inicializamos las funciones 
   //funcion que maneja el paginator 
    function pagination(){
        var paginationPos=$(this).index()+1; //obtenemos un index de los li que hay segun los clics
       $('.slider li').hide(); 
       $('.slider li:nth-child('+paginationPos +')').fadeIn();
       
       $('.paginator li').css({'color':'black'});//regresar los colores del paginator a su normal
       $(this).css({'color':'white'});//dar color rojo al que se cliqueo
       imgPos=paginationPos;
    }
    
    function nextSlider(){
         if(imgPos>=imgItems){
             imgPos=1;
         }else{
           imgPos++;  
         }
         
         $('.paginator li').css({'color':'black'});//cambia a blanco los que no se han seleccionado
        $('.paginator li:nth-child('+imgPos+')').css({'color':'white'});//cambia a rojo el seleccionado
         $('.slider li').hide(); //esconde las demas imagenes
       $('.slider li:nth-child('+imgPos+')').fadeIn();// desvanece la imagen previa a cambiarse
    }
    
     function prevSlider(){
         if(imgPos<=1){
             imgPos=imgItems;
         }else{
           imgPos--;  
         }
         
         $('.paginator li').css({'color':'white'});
        $('.paginator li:nth-child('+imgPos+')').css({'color':'red'});
         $('.slider li').hide(); 
       $('.slider li:nth-child('+imgPos+')').fadeIn();
    }
 });
 