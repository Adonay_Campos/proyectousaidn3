package com.spring.ctrl;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spring.impl.ViviendaImpl;
import com.spring.model.Vivienda;

@Component
@ManagedBean
@SessionScoped
public class CarteraVivienda implements Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	private ViviendaImpl viviendaImpl;
	private List<Vivienda> listVivienda;
	private Vivienda vivienda;

	@PostConstruct
	public void init() {
		vivienda = new Vivienda();
	}

	public List<Vivienda> getListVivienda() {
		listVivienda = viviendaImpl.findAll();
		return listVivienda;
	}

	public void setListVivienda(List<Vivienda> listVivienda) {
		this.listVivienda = listVivienda;
	}

	public Vivienda getVivienda() {
		return vivienda;
	}

	public void setVivienda(Vivienda vivienda) {
		this.vivienda = vivienda;
	}

	public void listar() {
		try {
			listVivienda = viviendaImpl.findAll();
		} catch (Exception e) {
			System.out.println("Error en: " + e.getMessage());
			e.printStackTrace();
		}

	}

	public void crear() {
		try {
			viviendaImpl.create(vivienda);
			listVivienda = viviendaImpl.findAll();
			init();
		} catch (Exception e) {
			System.out.println("Error al crear: " + e.getMessage());
			e.printStackTrace();
		}
	}

	public void editar() {
		try {
			viviendaImpl.edit(vivienda);
			listVivienda = viviendaImpl.findAll();
			init();
		} catch (Exception e) {
			System.out.println("Error al editar: " + e.getMessage());
			e.printStackTrace();
		}
	}

	public void llenar(Vivienda v) {
		vivienda = v;
	}

	public void eliminar(Vivienda v) {
		try {
			viviendaImpl.remove(v);
			listVivienda = viviendaImpl.findAll();
			init();
		} catch (Exception e) {
			System.out.println("Error al eliminar: " + e.getMessage());
			e.printStackTrace();
		}
	}

}
