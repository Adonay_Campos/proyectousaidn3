package com.spring.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.model.Vivienda;
import com.spring.util.AbstractFacade;
import com.spring.util.Dao;

@Repository
public class ViviendaImpl extends AbstractFacade<Vivienda> implements Dao<Vivienda> {

	public ViviendaImpl() {
		super(Vivienda.class);
	}

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	protected SessionFactory sessionFactory() {
		return sessionFactory;
	}

}
