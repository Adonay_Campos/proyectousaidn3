package com.seguros.confs;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.seguros.impl.BeneficiarioImpl;
import com.seguros.impl.CancelarDaoImpl;
import com.seguros.impl.CategoriaSeguroImpl;
import com.seguros.impl.DepartamentoImpl;
import com.seguros.impl.EstadocivilImpl;
import com.seguros.impl.IncidenciasCubiertasImpl;
import com.seguros.impl.IncidenciasImpl;
import com.seguros.impl.MarcaImpl;
import com.seguros.impl.ModeloImpl;
import com.seguros.impl.MunicipiosImpl;
import com.seguros.impl.PagoSegurosImpl;
import com.seguros.impl.PaqueteImpl;
import com.seguros.impl.PersonaImpl;
import com.seguros.impl.SucursalesImpl;
import com.seguros.impl.TalleresImpl;
import com.seguros.impl.TipoAutoImpl;
import com.seguros.impl.TipoSeguroImpl;
import com.seguros.impl.TipoUsuarioImpl;
import com.seguros.impl.UsuarioImpl;
import com.seguros.impl.VehiculoImpl;

@Configuration
@ComponentScan(basePackages = { "com.seguros" })
@EnableTransactionManagement(proxyTargetClass = true)
public class AppContextConfig {

	@Bean(name = "dataSource")
	public DataSource getDataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl(
				"jdbc:mysql://usam-sql.sv.cds:3306/seguros?allowPublicKeyRetrieval=true&useSSL=false&sessionVariables=LC_TIME_NAMES='es_SV'");
		dataSource.setUsername("kz");
		dataSource.setPassword("kzroot");
		return dataSource;
	}

	private Properties getHibernateProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		properties.put("hibernate.format_sql", "hibernate.format_sql");
		return properties;
	}

	@Bean(name = "sessionFactory")
	public SessionFactory getsessionFactory(DataSource dataSource) {
		LocalSessionFactoryBuilder sessionBuilder = new LocalSessionFactoryBuilder(dataSource);
		sessionBuilder.scanPackages("com.seguros.model");
		sessionBuilder.addProperties(getHibernateProperties());
		return sessionBuilder.buildSessionFactory();
	}

	@Bean(name = "transactionManager")
	public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory) {
		HibernateTransactionManager transactionManager = new HibernateTransactionManager(sessionFactory);
		return transactionManager;
	}

	@Bean
	public BeneficiarioImpl beneficiario() {
		return new BeneficiarioImpl();
	}

	@Bean
	public CancelarDaoImpl cancelacion() {
		return new CancelarDaoImpl();
	}

	@Bean
	public CategoriaSeguroImpl cateSeguros() {
		return new CategoriaSeguroImpl();
	}

	@Bean
	public DepartamentoImpl departamento() {
		return new DepartamentoImpl();
	}

	@Bean
	public EstadocivilImpl estadoCivil() {
		return new EstadocivilImpl();
	}

	@Bean
	public IncidenciasCubiertasImpl inciCubiertas() {
		return new IncidenciasCubiertasImpl();
	}

	@Bean
	public IncidenciasImpl incidencias() {
		return new IncidenciasImpl();
	}

	@Bean
	public ModeloImpl modelo() {
		return new ModeloImpl();
	}

	@Bean
	public MunicipiosImpl municipio() {
		return new MunicipiosImpl();
	}

	@Bean
	public PagoSegurosImpl pagoSeguros() {
		return new PagoSegurosImpl();
	}

	@Bean
	public PersonaImpl persona() {
		return new PersonaImpl();
	}

	@Bean
	public SucursalesImpl sucursales() {
		return new SucursalesImpl();
	}

	@Bean
	public TalleresImpl talleres() {
		return new TalleresImpl();
	}

	@Bean
	public TipoAutoImpl tipoAuto() {
		return new TipoAutoImpl();
	}

	@Bean
	public TipoUsuarioImpl tipoUsuario() {
		return new TipoUsuarioImpl();
	}

	@Bean
	public UsuarioImpl usuario() {
		return new UsuarioImpl();
	}

	@Bean
	public VehiculoImpl vehiculo() {
		return new VehiculoImpl();
	}

	@Bean
	public MarcaImpl marca() {
		return new MarcaImpl();
	}

	@Bean
	public TipoSeguroImpl tipoSeguro() {
		return new TipoSeguroImpl();

	}

	@Bean
	public PaqueteImpl paquete() {
		return new PaqueteImpl();
	}

}
