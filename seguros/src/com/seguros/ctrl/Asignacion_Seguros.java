package com.seguros.ctrl;

import java.io.Serializable;
import java.time.Instant;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.Tuple;

import org.primefaces.PrimeFaces;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.CategoryAxis;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.seguros.impl.BeneficiarioImpl;
import com.seguros.impl.CancelarDaoImpl;
import com.seguros.impl.CategoriaSeguroImpl;
import com.seguros.impl.PaqueteImpl;
import com.seguros.impl.PersonaImpl;
import com.seguros.impl.SucursalesImpl;
import com.seguros.impl.TipoSeguroImpl;
import com.seguros.impl.UsuarioImpl;
import com.seguros.impl.VehiculoImpl;
import com.seguros.model.AsignacionSeguros;
import com.seguros.model.Beneficiarios;
import com.seguros.model.CategoriaSeguros;
import com.seguros.model.Paquetes;
import com.seguros.model.Personas;
import com.seguros.model.Sucursales;
import com.seguros.model.TipoSeguros;
import com.seguros.model.Usuarios;
import com.seguros.model.Vehiculos;

@Component
@ManagedBean
@SessionScoped
public class Asignacion_Seguros implements Serializable {

	private static final long serialVersionUID = 1L;
	@Autowired
	private VehiculoImpl dvehiculo;

	@Autowired
	private CancelarDaoImpl cancelardao;

	@Autowired
	private UsuarioImpl userdao;

	@Autowired
	private PersonaImpl persondao;

	@Autowired
	private TipoSeguroImpl dtipoSeguro;

	@Autowired
	private VehiculoImpl vehiculosdao;

	@Autowired
	private CategoriaSeguroImpl dcseguros;

	@Autowired
	private SucursalesImpl sucurdao;

	@Autowired
	private PaqueteImpl dpaquete;

	@Autowired
	private BeneficiarioImpl dbeneficiario;
	@Autowired
	private CancelarDaoImpl asignacionSeguros;

	private Beneficiarios beneficiarios;
	private AsignacionSeguros cancelarSeguros;
	private Usuarios usuarios;
	private Personas personas;
	private TipoSeguros tipoSeguros;
	private Vehiculos vehiculos;
	private CategoriaSeguros catSeguros;
	private Paquetes paquetes;
	private Sucursales sucursales;

	private List<AsignacionSeguros> listaSA;
	private List<AsignacionSeguros> listaSV;
	private List<Usuarios> listaUsuarios;
	private List<Personas> listaPersonas;
	private List<Vehiculos> listaVehiculos;
	private List<Sucursales> listaSucursales;
	private List<Paquetes> listaPaquetes;
	private List<CategoriaSeguros> listCatSeguros;
	private List<TipoSeguros> listaTS;
	private List<Beneficiarios> listaBeneficiario;
	private List<Beneficiarios> listactbene;
	private List<Tuple> listaTuple;

	private BarChartModel grafico;

	public BarChartModel getGrafico() {
		return grafico;
	}

	public void setGrafico(BarChartModel grafico) {
		this.grafico = grafico;
	}

	public List<Tuple> getListaTuple() {
		return listaTuple;
	}

	public void setListaTuple(List<Tuple> listaTuple) {
		this.listaTuple = listaTuple;
	}

	public List<Beneficiarios> getListactbene() {
		return listactbene;
	}

	public void setListactbene(List<Beneficiarios> listactbene) {
		this.listactbene = listactbene;
	}

	String mensaje = "";
	private String placa;
	private String dui;

	public String getDui() {
		return dui;
	}

	public void setDui(String dui) {
		this.dui = dui;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public List<TipoSeguros> getListaTS() {
		this.listaTS = dtipoSeguro.findAll();
		return listaTS;
	}

	public void setListaTS(List<TipoSeguros> listaTS) {
		this.listaTS = listaTS;
	}

	public AsignacionSeguros getCancelarSeguros() {
		return cancelarSeguros;
	}

	public void setCancelarSeguros(AsignacionSeguros cancelarSeguros) {
		this.cancelarSeguros = cancelarSeguros;
	}

	public Usuarios getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(Usuarios usuarios) {
		this.usuarios = usuarios;
	}

	public Personas getPersonas() {
		return personas;
	}

	public void setPersonas(Personas personas) {
		this.personas = personas;
	}

	public TipoSeguros getTipoSeguros() {
		return tipoSeguros;
	}

	public void setTipoSeguros(TipoSeguros tipoSeguros) {
		this.tipoSeguros = tipoSeguros;
	}

	public Vehiculos getVehiculos() {
		return vehiculos;
	}

	public void setVehiculos(Vehiculos vehiculos) {
		this.vehiculos = vehiculos;
	}

	public CategoriaSeguros getCatSeguros() {
		return catSeguros;
	}

	public void setCatSeguros(CategoriaSeguros catSeguros) {
		this.catSeguros = catSeguros;
	}

	public Paquetes getPaquetes() {
		return paquetes;
	}

	public void setPaquetes(Paquetes paquetes) {
		this.paquetes = paquetes;
	}

	public Sucursales getSucursales() {
		return sucursales;
	}

	public void setSucursales(Sucursales sucursales) {
		this.sucursales = sucursales;
	}

	public List<AsignacionSeguros> getListaSA() {
		this.listaSA = cancelardao.listaSA();
		return listaSA;
	}

	public void setListaSA(List<AsignacionSeguros> listaSA) {
		this.listaSA = listaSA;
	}

	public List<AsignacionSeguros> getListaSV() {
		this.listaSV = cancelardao.listaSV();
		return listaSV;
	}

	public void setListaSV(List<AsignacionSeguros> listaSV) {
		this.listaSV = listaSV;
	}

	public List<Usuarios> getListaUsuarios() {
		this.listaUsuarios = userdao.findAll();
		return listaUsuarios;
	}

	public void setListaUsuarios(List<Usuarios> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}

	public List<Personas> getListaPersonas() {
		this.listaPersonas = persondao.findAll();
		return listaPersonas;
	}

	public void setListaPersonas(List<Personas> listaPersonas) {
		this.listaPersonas = listaPersonas;
	}

	public List<Vehiculos> getListaVehiculos() {
		this.listaVehiculos = vehiculosdao.findAll();
		return listaVehiculos;
	}

	public void setListaVehiculos(List<Vehiculos> listaVehiculos) {
		this.listaVehiculos = listaVehiculos;
	}

	public List<Sucursales> getListaSucursales() {
		this.listaSucursales = sucurdao.findAll();
		return listaSucursales;
	}

	public void setListaSucursales(List<Sucursales> listaSucursales) {
		this.listaSucursales = listaSucursales;
	}

	public List<Paquetes> getListaPaquetes() {
		this.listaPaquetes = dpaquete.findAll();
		return listaPaquetes;
	}

	public void setListaPaquetes(List<Paquetes> listaPaquetes) {
		this.listaPaquetes = listaPaquetes;
	}

	public List<CategoriaSeguros> getListCatSeguros() {
		this.listCatSeguros = dcseguros.findAll();
		return listCatSeguros;
	}

	public void setListCatSeguros(List<CategoriaSeguros> listCatSeguros) {
		this.listCatSeguros = listCatSeguros;
	}

	private List<Personas> listaClientes2;

	public List<Personas> getListaClientes2() {
		listaClientes2 = persondao.listarClientes();
		return listaClientes2;
	}

	public void setListaClientes2(List<Personas> listaClientes2) {
		this.listaClientes2 = listaClientes2;
	}

	@PostConstruct
	public void init() {
		cancelarSeguros = new AsignacionSeguros();
		tipoSeguros = new TipoSeguros();
		catSeguros = new CategoriaSeguros();
		paquetes = new Paquetes();
		personas = new Personas();
		sucursales = new Sucursales();
		usuarios = new Usuarios();
		vehiculos = new Vehiculos();
		beneficiarios = new Beneficiarios();
		grafico = new BarChartModel();
		grafico1 = new BarChartModel();
		grafico2 = new BarChartModel();
		graficoS= new BarChartModel();
	}

	// CRUD ASIGNACION SEGUROS
	private Usuarios user2;

	public void crearAsignacionSegurosVida() {
		try {
			FacesContext contex = FacesContext.getCurrentInstance();
			user2 = (Usuarios) contex.getExternalContext().getSessionMap().get("us");
			user2.getId();
			cancelarSeguros.setUsuarios(user2);
			cancelarSeguros.setTipoSeguros(tipoSeguros);
			cancelarSeguros.setPaquetes(paquetes);
			cancelarSeguros.setSucursales(sucursales);
			cancelarSeguros.setPersonas(personas);
			cancelarSeguros.setBeneficiarios(beneficiarios);
			cancelarSeguros.setFecha(Date.from(Instant.now()));
			String p = "Seguro de Vida";
			cancelarSeguros.setPoliza(cancelardao.generarNumPoliza(p)); /// colocamos metodo que guardara el numero de
																		/// poliza generado
			cancelardao.create(cancelarSeguros);
			cancelarSeguros = new AsignacionSeguros();
			mensaje = "Asignacion Exitosa";
		} catch (Exception e) {
			mensaje = "Importante!! Debe llenar todos los campos";
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void crearAsignacionSegurosAutos() {
		try {
			FacesContext contex = FacesContext.getCurrentInstance();
			user2 = (Usuarios) contex.getExternalContext().getSessionMap().get("us");
			user2.getId();
			cancelarSeguros.setUsuarios(user2);
			cancelarSeguros.setTipoSeguros(tipoSeguros);
			cancelarSeguros.setPaquetes(paquetes);
			cancelarSeguros.setSucursales(sucursales);
			cancelarSeguros.setVehiculos(vehiculos);
			cancelarSeguros.setPersonas(personas);
			cancelarSeguros.setFecha(Date.from(Instant.now()));
			String p = "Seguro de Auto";
			cancelarSeguros.setPoliza(cancelardao.generarNumPoliza(p)); /// colocamos metodo que guardara el numero de
																		/// poliza generado
			cancelardao.create(cancelarSeguros);
			cancelarSeguros = new AsignacionSeguros();
			mensaje = "Asignacion Exitosa";
		} catch (Exception e) {
			mensaje = "Importante!! Debe llenar todos los campos";
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void editarAsignacionSegurosVida() {
		try {
			cancelarSeguros.setTipoSeguros(tipoSeguros);
			cancelarSeguros.setPaquetes(paquetes);
			cancelarSeguros.setSucursales(sucursales);
			cancelarSeguros.setPersonas(personas);
			cancelarSeguros.setBeneficiarios(beneficiarios);
			cancelardao.edit(cancelarSeguros);
			cancelarSeguros = new AsignacionSeguros();
			mensaje = "Asignacion Editada";
		} catch (Exception e) {
			mensaje = "Error:" + e.getMessage();
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void editarAsignacionSegurosAutos() {
		try {
			cancelarSeguros.setTipoSeguros(tipoSeguros);
			cancelarSeguros.setPaquetes(paquetes);
			cancelarSeguros.setSucursales(sucursales);
			cancelarSeguros.setVehiculos(vehiculos);
			cancelarSeguros.setPersonas(personas);
			cancelardao.edit(cancelarSeguros);
			cancelarSeguros = new AsignacionSeguros();
			mensaje = "Asignacion Editada";
		} catch (Exception e) {
			mensaje = "Error:" + e.getMessage();
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void eliminarAsignacionSeguros(AsignacionSeguros v) {
		try {
			v.setEstado(false);
			cancelardao.edit(v);
			mensaje = "Asignacion Eliminada";
			init();
		} catch (Exception e) {
			mensaje = "Error:" + e.getMessage();
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void eliminarAsignacionSegurosSA(AsignacionSeguros as) {
		try {
			cancelarSeguros.setUsuarios(as.getUsuarios());
			cancelarSeguros.setTipoSeguros(as.getTipoSeguros());
			cancelarSeguros.setPaquetes(as.getPaquetes());
			cancelarSeguros.setSucursales(as.getSucursales());
			cancelarSeguros.setVehiculos(as.getVehiculos());
			cancelarSeguros.setPersonas(as.getPersonas());
			cancelarSeguros.setEstado(false);
			cancelarSeguros.setId(as.getId());
			System.out.println(cancelarSeguros.getEstado());
			cancelardao.edit(cancelarSeguros);
			mensaje = "Asignacion Eliminada";
		} catch (Exception e) {
			mensaje = "Error:" + e.getMessage();
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public String cargarPersona(Personas lp) {
		personas = lp;
		System.out.println(lp.getNombre());
		return "beneficiarios?faces-redirect=true";
	}

	public void cargarAsignacionSegurosV(AsignacionSeguros v) {

		usuarios = v.getUsuarios();
		tipoSeguros = v.getTipoSeguros();
		paquetes = v.getPaquetes();
		sucursales = v.getSucursales();
		personas = v.getPersonas();
		beneficiarios = v.getBeneficiarios();
		cancelarSeguros = v;
	}

	public void cargarAsignacionSegurosA(AsignacionSeguros a) {

		usuarios = a.getUsuarios();
		tipoSeguros = a.getTipoSeguros();
		paquetes = a.getPaquetes();
		sucursales = a.getSucursales();
		vehiculos = a.getVehiculos();
		personas = a.getPersonas();
		cancelarSeguros = a;
	}

	// CRUD TIPO SEGUROS
	public void crearTS() {
		try {
			dtipoSeguro.create(tipoSeguros);
			tipoSeguros = new TipoSeguros();
			mensaje = "Tipo de Seguro Creado";
		} catch (Exception e) {
			mensaje = "Importante!! Debe llenar todos los campos";
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);

	}

	public void editarTS() {
		try {
			dtipoSeguro.edit(tipoSeguros);
			tipoSeguros = new TipoSeguros();
			mensaje = "Tipo de Seguro Editado";
		} catch (Exception e) {
			mensaje = "Error:" + e.getMessage();
			e.printStackTrace();

		}

		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);

	}

	public void eliminarTS(TipoSeguros ts) {
		try {
			dtipoSeguro.remove(ts);
			mensaje = "Tipo de Seguro Eliminado";
		} catch (Exception e) {
			mensaje = "Error:" + e.getMessage();
			e.printStackTrace();
		}

		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);

	}

	public void cargarTS(TipoSeguros ts) {
		tipoSeguros = ts;

	}

	// CRUD CATEGORIA SEGUROS
	public void crearCatSe() {
		try {
			catSeguros.setPaquetes(paquetes);
			catSeguros.setTipoSeguros(tipoSeguros);
			dcseguros.create(catSeguros);
			mensaje = "Categoria Creada";
		} catch (Exception e) {
			mensaje = "Importante!! Debe llenar todos los campos";
			e.printStackTrace();
		}

		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);

	}

	public void editarCatSe() {
		try {
			catSeguros.setPaquetes(paquetes);
			catSeguros.setTipoSeguros(tipoSeguros);
			dcseguros.edit(catSeguros);
			mensaje = "Categoria Editada";
		} catch (Exception e) {
			mensaje = "Error:" + e.getMessage();
			e.printStackTrace();

		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);

	}

	public void eliminarCatSeguros(CategoriaSeguros cat) {
		try {
			dcseguros.remove(cat);
			mensaje = "Categoria Eliminada";
		} catch (Exception e) {
			mensaje = "Error:" + e.getMessage();
			e.printStackTrace();

		}

		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);

	}

	public void cargarCatSe(CategoriaSeguros cat) {
		tipoSeguros.setId(cat.getId());
		paquetes.setId(cat.getId());
		catSeguros = cat;

	}

	public void actvehi() {
		listPPLA = dvehiculo.busquedaPorPlaca(placa);
		PrimeFaces.current().ajax().update("datos:ve");
	}

	public void actper() {
		listBDUI = persondao.search(dui);
		PrimeFaces.current().ajax().update("datos:pe");
	}

	public Beneficiarios getBeneficiarios() {
		return beneficiarios;
	}

	public void setBeneficiarios(Beneficiarios beneficiarios) {
		this.beneficiarios = beneficiarios;
	}

	public List<Beneficiarios> getListaBeneficiario() {
		this.listaBeneficiario = dbeneficiario.findAll();
		return listaBeneficiario;
	}

	public void setListaBeneficiario(List<Beneficiarios> listaBeneficiario) {
		this.listaBeneficiario = listaBeneficiario;
	}

	// CRUD BENEFICIARIO
	public void crearBeneficiario() {
		try {
			beneficiarios.setPersonas(personas);
			dbeneficiario.create(beneficiarios);
			beneficiarios = new Beneficiarios();
			init();
			mensaje = "Beneficiario Creado";
		} catch (Exception e) {
			mensaje = "Importante!! Debe llenar todos los campos";
			e.printStackTrace();
		}

		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void editarBeneficiario() {
		try {
			System.out.println("entrooooooo");
			beneficiarios.setPersonas(personas);
			dbeneficiario.edit(beneficiarios);
			init();
			mensaje = "Beneficiario Editado";
		} catch (Exception e) {
			mensaje = "Error al Editar";
			e.printStackTrace();
		}

		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void eliminarBeneficiario(Beneficiarios be) {
		try {
			be.setEstado(false);
			dbeneficiario.edit(be);
			mensaje = "Beneficiario Eliminado";
		} catch (Exception e) {
			mensaje = "Error al Eliminar";
			e.printStackTrace();
		}

		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void cargarBeneficiario(Beneficiarios be) {
		personas.setId(be.getId());
		beneficiarios = be;
	}

	private List<Personas> listBDUI;

	public List<Personas> getListBDUI() {
		return listBDUI;
	}

	public void setListBDUI(List<Personas> listBDUI) {
		this.listBDUI = listBDUI;
	}

	private List<Vehiculos> listPPLA;

	public List<Vehiculos> getListPPLA() {
		return listPPLA;
	}

	public void setlistPPLA(List<Vehiculos> listPPLA) {
		this.listPPLA = listPPLA;
	}

	public void actbene() {
		personas.getId();
		System.out.println(personas.getId());
		listactbene = dbeneficiario.actbene(personas);
		PrimeFaces.current().ajax().update("datos:bnet");

	}

	private int anio;

	public int getAnio() {
		return anio;
	}

	public void setAnio(int anio) {
		this.anio = anio;
	}

	private BarChartModel grafico1;
	private BarChartModel grafico2;

	public BarChartModel getGrafico2() {
		return grafico2;
	}

	public void setGrafico2(BarChartModel grafico2) {
		this.grafico2 = grafico2;
	}

	public BarChartModel getGrafico1() {
		return grafico1;
	}

	public void setGrafico1(BarChartModel grafico1) {
		this.grafico1 = grafico1;
	}

	public void graficarPaqueteSA() {
		try {
			String valor = "Ventas Anuales";
			listaTuple = asignacionSeguros.paquetesxyearSA(anio);
			BarChartModel model = new BarChartModel();
			ChartSeries cs = new ChartSeries();
			cs = new ChartSeries();

			for (int i = 0; i < listaTuple.size(); i++) {
				cs.setLabel("seguros");
				cs.set(listaTuple.get(i).get(1).toString() + " " + listaTuple.get(i).get(0),
						(long) listaTuple.get(i).get(2));
				Axis yAxis = grafico.getAxis(AxisType.Y);

				yAxis.setTickCount(4);
				yAxis.setMin(0);
				yAxis.setMax((Number) listaTuple.get(i).get(2));

			}

			model.addSeries(cs);
			model.setShowDatatip(false);

			grafico = model;
			grafico.setTitle(valor);
			grafico.setAnimate(true);
			grafico.setBarWidth(10);
			grafico.setLegendPosition("n");
			grafico.setShowDatatip(true);
			grafico.setBarMargin(10);
			grafico.setBarPadding(2);
			grafico.setShowDatatip(false);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void comparartipo() {
		try {
			String titulo = "Seguro de Autos, A�o: " + a;
			String titulo1="Seguro de Personas, A�o:"+a;
			listatuple1 = cancelardao.comparartipo1(a);
			listatuple2 = cancelardao.comparartipo2(a);
			BarChartModel model = new BarChartModel();
			ChartSeries cs = new ChartSeries();
			BarChartModel model2 = new BarChartModel();
			ChartSeries cs2 = new ChartSeries();
			cs = new ChartSeries();

			for (int i = 0; i < listatuple1.size(); i++) {
				cs.setLabel("seguros");
				cs.set(listatuple1.get(i).get(0).toString() + "", (long) listatuple1.get(i).get(1));
				Axis yAxis = grafico1.getAxis(AxisType.Y);

				yAxis.setTickCount(4);
				yAxis.setMin(0);
				yAxis.setMax((Number) listatuple1.get(i).get(1));
			}

			model.addSeries(cs);
			model.setShowDatatip(false);

			grafico1 = model;
			grafico1.setTitle(titulo);
			grafico1.setAnimate(true);
			grafico1.setBarWidth(30);
			grafico1.setLegendPosition("n");
			grafico1.setShowDatatip(true);
			grafico1.setBarMargin(12);
			grafico1.setBarPadding(2);
			grafico1.setShowDatatip(false);

			for (int i = 0; i < listatuple2.size(); i++) {
				cs2.setLabel("seguros");
				cs2.set(listatuple2.get(i).get(0).toString() + "", (long) listatuple2.get(i).get(1));
				Axis yAxis = grafico2.getAxis(AxisType.Y);

				yAxis.setTickCount(4);
				yAxis.setMin(0);
				yAxis.setMax((Number) listatuple2.get(i).get(1));
			}

			model2.addSeries(cs2);
			model2.setShowDatatip(false);

			grafico2 = model2;
			grafico2.setTitle(titulo1);
			grafico2.setAnimate(true);
			grafico2.setBarWidth(30);
			grafico2.setLegendPosition("n");
			grafico2.setShowDatatip(true);
			grafico2.setBarMargin(12);
			grafico2.setBarPadding(2);
			grafico2.setShowDatatip(false);

		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	int a;
	private List<Tuple> listatuple1;
	private List<Tuple> listatuple2;

	public int getA() {
		return a;
	}

	public void setA(int a) {
		this.a = a;
	}

	public List<Tuple> getListatuple1() {
		return listatuple1;
	}

	public void setListatuple1(List<Tuple> listatuple1) {
		this.listatuple1 = listatuple1;
	}

	public List<Tuple> getListatuple2() {
		return listatuple2;
	}

	public void setListatuple2(List<Tuple> listatuple2) {
		this.listatuple2 = listatuple2;
	}

	private int year;

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	private BarChartModel graficoS;

	public BarChartModel getGraficoS() {
		return graficoS;
	}

	public void setGraficoS(BarChartModel graficoS) {
		this.graficoS = graficoS;
	}

	private List<Tuple> listaTupleS;

	public List<Tuple> getListaTupleS() {
		return listaTupleS;
	}

	public void setListaTupleS(List<Tuple> listaTupleS) {
		this.listaTupleS = listaTupleS;
	}

	public void graficarSucursal() {
		try {
			String valor = "Ventas Anuales por Sucursal";
			listaTupleS = asignacionSeguros.ventas(year);
			BarChartModel model = new BarChartModel();
			ChartSeries cs = new ChartSeries();
			cs = new ChartSeries();

			for (int i = 0; i < listaTupleS.size(); i++) {
				cs.setLabel("Sucursales");
				cs.set(listaTupleS.get(i).get(1) + " " + listaTupleS.get(i).get(0), (Number) listaTupleS.get(i).get(2));
				Axis yAxis = graficoS.getAxis(AxisType.Y);

				yAxis.setTickCount(4);
				yAxis.setMin(0);
				yAxis.setMax((Number) listaTupleS.get(i).get(2));

			}

			model.addSeries(cs);
			model.setShowDatatip(false);

			graficoS = model;
			graficoS.setTitle(valor);
			graficoS.setAnimate(true);
			graficoS.setBarWidth(30);
			graficoS.setLegendPosition("n");
			graficoS.setShowDatatip(true);
			graficoS.setBarMargin(10);
			graficoS.setBarPadding(2);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}