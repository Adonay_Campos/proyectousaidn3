package com.seguros.ctrl;

import java.io.Serializable;
import java.sql.Date;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.Tuple;

import org.primefaces.PrimeFaces;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.CategoryAxis;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;
import org.primefaces.model.chart.PieChartModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.seguros.impl.CancelarDaoImpl;
import com.seguros.impl.PagoSegurosImpl;
import com.seguros.impl.SucursalesImpl;
import com.seguros.impl.UsuarioImpl;
import com.seguros.model.AsignacionSeguros;
import com.seguros.model.PagoSeguros;
import com.seguros.model.Sucursales;
import com.seguros.model.Usuarios;

@Component
@ManagedBean
@SessionScoped
public class Cobros implements Serializable {
	private static final long serialVersionUID = 1L;

	@Autowired
	private PagoSegurosImpl pagodao;
	private AsignacionSeguros asignacionSeguros;
	private PagoSeguros pago;
	private List<PagoSeguros> listP;

	@Autowired
	private CancelarDaoImpl asignaciondao;
	private AsignacionSeguros seguros;
	private List<AsignacionSeguros> listAS;

	@Autowired
	private UsuarioImpl usuarioImpl;
	private Usuarios usuario;
	private List<Usuarios> listU;
	@Autowired
	private SucursalesImpl sucursalesImpl;
	private Sucursales sucursal;
	private List<Sucursales> listS;

	private int anio;
	private List<Tuple> listTuple;
	private List<Tuple> listTuple1;
	private List<Tuple> listTuple2;
	private LineChartModel grafTotalRecibido;
	private PieChartModel graficoTotal;

	String mensaje;

	public PagoSeguros getPago() {
		return pago;
	}

	public void setPago(PagoSeguros pago) {
		this.pago = pago;
	}

	public List<PagoSeguros> getListP() {
		return listP;
	}

	public void setListP(List<PagoSeguros> listP) {
		this.listP = listP;
	}

	public AsignacionSeguros getSeguros() {
		return seguros;
	}

	public void setSeguros(AsignacionSeguros seguros) {
		this.seguros = seguros;
	}

	public List<AsignacionSeguros> getListAS() {
		return listAS;
	}

	public void setListAS(List<AsignacionSeguros> listAS) {

		this.listAS = listAS;
	}

	public Usuarios getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuarios usuario) {
		this.usuario = usuario;
	}

	public List<Usuarios> getListU() {
		listU = usuarioImpl.findAll();
		return listU;
	}

	public void setListU(List<Usuarios> listU) {
		this.listU = listU;
	}

	public Sucursales getSucursal() {
		return sucursal;
	}

	public void setSucursal(Sucursales sucursal) {
		this.sucursal = sucursal;
	}

	public List<Sucursales> getListS() {
		listS = sucursalesImpl.findAll();
		return listS;
	}

	public void setListS(List<Sucursales> listS) {
		this.listS = listS;
	}

	public AsignacionSeguros getAsignacionSeguros() {
		return asignacionSeguros;
	}

	public void setAsignacionSeguros(AsignacionSeguros asignacionSeguros) {
		this.asignacionSeguros = asignacionSeguros;
	}

	@PostConstruct

	public void init() {
		asignacionSeguros = new AsignacionSeguros();
		pago = new PagoSeguros();
		sucursal = new Sucursales();
		seguros = new AsignacionSeguros();
		usuario = new Usuarios();
		grafTotalRecibido = new LineChartModel();
		graficoTotal = new PieChartModel();
	}

	// CRUD TIPO PAGOS SEGUROS

	private Usuarios user2;

	public void create() {
		try {
			FacesContext contex = FacesContext.getCurrentInstance();
			user2 = (Usuarios) contex.getExternalContext().getSessionMap().get("us");
			user2.getId();
			pago.setUsuarios(user2);
			pago.setAsignacionSeguros(asignacionSeguros);
			pago.setFecha(Date.from(Instant.now()));
			pagodao.create(pago);
			System.out.println(pago.getAsignacionSeguros());
			mensaje = "Pago Hecho con exito";
			pago = new PagoSeguros();
			asignacionSeguros = new AsignacionSeguros();

		} catch (Exception e) {
			mensaje = "Error al realizar su pago";
		}
	}

	public void llenar(AsignacionSeguros as) {
		try {
			System.out.println("Entro");
			asignacionSeguros = as;
			System.out.println(as.getPoliza());
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}

	}

	public String editar(String pol) {
		buscarPoliza(pol);
		listarxPol(pol);
		return "pagoseguro?faces-redirect=true	";
	}

	private List<PagoSeguros> listPa;

	public List<PagoSeguros> getListPa() {
		return listPa;
	}

	public void setListPa(List<PagoSeguros> listPa) {
		this.listPa = listPa;
	}

	public void listarxPol(String pol) {
		try {
			listPa = pagodao.listarxPoliza(pol);
		} catch (Exception e) {
			System.out.println("Error al realizar la busqueda de poliza, por: " + e.getMessage());
		}
	}

	public void edit() {
		try {
			pago.setUsuarios(usuario);
			pago.setSucursales(sucursal);
			pagodao.edit(pago);
			mensaje = "Actualizado con exito";
		} catch (Exception e) {
			mensaje = "Error en actualizar";
		}
	}

	public void eliminar(PagoSeguros pagos) {
		this.pago = pagos;
		try {

			pagodao.remove(pagos);
			mensaje = "Archivo eliminado";
		} catch (Exception e) {
			mensaje = "Error a ala hora de eliminar el archivo";
		}
	}

	public void cargarpagos(PagoSeguros pagos) {
		usuario.setId(pagos.getUsuarios().getId());
		asignacionSeguros.setPoliza(pagos.getAsignacionSeguros().getPoliza());
		sucursal.setId(pagos.getSucursales().getId());
		this.pago = pagos;

	}

	private List<Tuple> listT;

	public List<Tuple> getListT() {
		return listT;
	}

	public void setListT(List<Tuple> listT) {
		this.listT = listT;
	}

	public void list(String id, String pa, int idpa) {
		try {
			listT = pagodao.cuotaXid(id);
			listTD = pagodao.totalPagadoxId(id, pa, idpa);
		} catch (Exception e) {
			System.out.println("Error en: " + e.getMessage());
			e.printStackTrace();
		}
	}

	private List<Tuple> listTD;

	public List<Tuple> getListTD() {
		return listTD;
	}

	public void debe(String id, String pa, int idpa) {
		try {
			listTD = pagodao.totalPagadoxId(id, pa, idpa);
		} catch (Exception e) {
			System.out.println("Error en: " + e.getMessage());
			e.printStackTrace();
		}
	}

	public String pol;

	public String getPol() {
		return pol;
	}

	public void setPol(String pol) {
		this.pol = pol;
	}

	public void buscarPoliza(String comienza) {
		try {
			System.out.println("Entro");
			System.out.println(comienza);
			listAS = asignaciondao.consPSeguro(comienza);
		} catch (Exception e) {
			System.out.println("Error al realizar la busqueda de poliza, por: " + e.getMessage());
		}
	}

	public int getAnio() {
		return anio;
	}

	public void setAnio(int anio) {
		this.anio = anio;
	}

	public List<Tuple> getListTuple() {
		return listTuple;
	}

	public void setListTuple(List<Tuple> listTuple) {
		this.listTuple = listTuple;
	}

	public LineChartModel getGrafTotalRecibido() {
		return grafTotalRecibido;
	}

	public void setGrafTotalRecibido(LineChartModel grafTotalRecibido) {
		this.grafTotalRecibido = grafTotalRecibido;
	}

	public PieChartModel getgraficoTotal() {
		return graficoTotal;
	}

	public void setGraficoTotal(PieChartModel graficoTotal) {
		this.graficoTotal = graficoTotal;
	}

	public List<Tuple> getListTuple1() {
		return listTuple1;
	}

	public void setListTuple1(List<Tuple> listTuple1) {
		this.listTuple1 = listTuple1;
	}

	public List<Tuple> getListTuple2() {
		return listTuple2;
	}

	public void setListTuple2(List<Tuple> listTuple2) {
		this.listTuple2 = listTuple2;
	}

	// Graficas
	public void graficaPastel() {
		try {
			listTuple2 = new ArrayList<>();
			graficoTotal = new PieChartModel();
			String valor = "Total de seguros vendidos durante el a�o " + anio;
			listTuple2 = pagodao.totalRecibidoxTipoS(anio);
			for (int i = 0; i < listTuple2.size(); i++) {
				graficoTotal.set(listTuple2.get(i).get(0).toString(), (Number) listTuple2.get(i).get(1));
			}
			graficoTotal.setShowDataLabels(true);
			graficoTotal.setFill(false);
			graficoTotal.setTitle(valor);
			graficoTotal.setDiameter(350);
			graficoTotal.setShowDatatip(true);
			graficoTotal.setLegendPosition("w");
			graficoTotal.setShowDataLabels(true);
		} catch (Exception e) {
			System.out.println("Error al graficar en: " + e.getMessage());
			e.printStackTrace();
		}
	}

	public void total() {
		try {
			listTuple = new ArrayList<>();
			listTuple1 = new ArrayList<>();
			grafTotalRecibido = new LineChartModel();
			String valor = "Total recaudado en el a�o " + anio;
			listTuple = pagodao.graficarTotalRecibido(anio);
			listTuple1 = pagodao.suma(anio);
			LineChartModel model = new LineChartModel();
			LineChartSeries m = new LineChartSeries();
			for (int i = 0; i < listTuple.size(); i++) {
				Axis yA = grafTotalRecibido.getAxis(AxisType.Y);
				yA.setLabel("CANTIDAD");
				yA.setTickInterval("5");
				yA.setMin(0);
				if (i == 0) {
					m = new LineChartSeries();
					m.setLabel(listTuple.get(0).get(0).toString());
					m.set(listTuple.get(0).get(1).toString().toUpperCase(), (Double) listTuple.get(0).get(2));
					for (int j = 0; j < listTuple.size(); j++) {
						if (j == 0) {
						} else {
							if (listTuple.get(0).get(1).toString().trim()
									.equals(listTuple.get(j).get(1).toString().trim())) {
								if (listTuple.get(0).get(0).toString().trim()
										.equals(listTuple.get(j).get(0).toString().trim())) {
									m.set(listTuple.get(j).get(1).toString().toUpperCase(),
											(double) listTuple.get(j).get(2));
									yA.setMax(listTuple.get(j).get(2));
								} else {
								}
							} else {
								if (listTuple.get(0).get(0).toString().trim()
										.equals(listTuple.get(j).get(0).toString().trim())) {
									m.set(listTuple.get(j).get(1).toString().toUpperCase(),
											(double) listTuple.get(j).get(2));
									yA.setMax(listTuple.get(j).get(2));
								} else {
								}
							}
						}
					}
				} else if (i == 1) {
					model.addSeries(m);
					m = new LineChartSeries();
					m.setLabel(listTuple.get(1).get(0).toString());
					for (int j = 0; j < listTuple.size(); j++) {
						if (listTuple.get(1).get(1).toString().trim()
								.equals(listTuple.get(j).get(1).toString().trim())) {
							if (listTuple.get(1).get(0).toString().trim()
									.equals(listTuple.get(j).get(0).toString().trim())) {
								m.set(listTuple.get(j).get(1).toString().toUpperCase(),
										(double) listTuple.get(j).get(2));
								yA.setMax(listTuple.get(j).get(2));
							} else {
							}
						} else {
							if (listTuple.get(1).get(0).toString().trim()
									.equals(listTuple.get(j).get(0).toString().trim())) {
								m.set(listTuple.get(j).get(1).toString().toUpperCase(),
										Double.parseDouble(listTuple.get(j).get(2).toString()));
								yA.setMax(listTuple.get(j).get(2));
							} else {
							}
						}
					}
				} else {
				}
			}
			model.addSeries(m);
			model.setShowDatatip(true);

			grafTotalRecibido = model;
			grafTotalRecibido.setLegendPosition("ne");
			grafTotalRecibido.setTitle(valor);
			grafTotalRecibido.setShowPointLabels(true);
			grafTotalRecibido.setAnimate(true);
			grafTotalRecibido.getAxes().put(AxisType.X, new CategoryAxis("MESES"));
		} catch (Exception e) {
			System.out.println("Error al graficar en: " + e.getMessage());
			e.printStackTrace();
		}
	}
}