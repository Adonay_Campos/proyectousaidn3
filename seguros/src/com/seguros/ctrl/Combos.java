package com.seguros.ctrl;


import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.seguros.impl.CategoriaSeguroImpl;
import com.seguros.impl.IncidenciasImpl;
import com.seguros.impl.PaqueteImpl;
import com.seguros.impl.TipoSeguroImpl;
import com.seguros.model.CategoriaSeguros;
import com.seguros.model.Incidencias;
import com.seguros.model.Paquetes;
import com.seguros.model.TipoSeguros;

@Component
@ManagedBean
@SessionScoped
public class Combos implements Serializable {

	private static final long serialVersionUID = 1L;
	@Autowired
	private PaqueteImpl paqueteImpl;
	private List<Paquetes> listaPaquetes;
	private Paquetes paquetes;

	@Autowired
	private TipoSeguroImpl tipoSeguroImpl;
	private List<TipoSeguros> listaTipoSeguro;
	private TipoSeguros tipoSeguros;

	@Autowired
	private CategoriaSeguroImpl categoriaSeguroImpl;
	private List<CategoriaSeguros> listaCategoriaSeguros;
	private CategoriaSeguros categoriaSeguros;

	@Autowired
	private IncidenciasImpl incidenciasImpl;
	private List<Incidencias> listaIncidencias;
	private Incidencias incidencias;

	private String nombre;
	private List<Paquetes> listaPaquetesConsulta;

	public List<Paquetes> getListaPaquetesConsulta() {
		return listaPaquetesConsulta;
	}

	public void setListaPaquetesConsulta(List<Paquetes> listaPaquetesConsulta) {
		this.listaPaquetesConsulta = listaPaquetesConsulta;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public IncidenciasImpl getIncidenciasImpl() {
		return incidenciasImpl;
	}

	public void setIncidenciasImpl(IncidenciasImpl incidenciasImpl) {
		this.incidenciasImpl = incidenciasImpl;
	}

	public List<Incidencias> getListaIncidencias() {
		listaIncidencias = incidenciasImpl.findAll();
		return listaIncidencias;
	}

	public void setListaIncidencias(List<Incidencias> listaIncidencias) {
		this.listaIncidencias = listaIncidencias;
	}

	public Incidencias getIncidencias() {
		return incidencias;
	}

	public void setIncidencias(Incidencias incidencias) {
		this.incidencias = incidencias;
	}

	public List<CategoriaSeguros> getListaCategoriaSeguros() {
		listaCategoriaSeguros = categoriaSeguroImpl.findAll();
		return listaCategoriaSeguros;
	}

	public void setListaCategoriaSeguros(List<CategoriaSeguros> listaCategoriaSeguros) {
		this.listaCategoriaSeguros = listaCategoriaSeguros;
	}

	public PaqueteImpl getPaqueteImpl() {
		return paqueteImpl;
	}

	public CategoriaSeguroImpl getCategoriaSeguroImpl() {
		return categoriaSeguroImpl;
	}

	public void setCategoriaSeguroImpl(CategoriaSeguroImpl categoriaSeguroImpl) {
		this.categoriaSeguroImpl = categoriaSeguroImpl;
	}

	public CategoriaSeguros getCategoriaSeguros() {
		return categoriaSeguros;
	}

	public void setCategoriaSeguros(CategoriaSeguros categoriaSeguros) {
		this.categoriaSeguros = categoriaSeguros;
	}

	public void setPaqueteImpl(PaqueteImpl paqueteImpl) {
		this.paqueteImpl = paqueteImpl;
	}

	public List<Paquetes> getListaPaquetes() {
		listaPaquetes = paqueteImpl.findAll();
		return listaPaquetes;
	}

	public void setListaPaquetes(List<Paquetes> listaPaquetes) {
		this.listaPaquetes = listaPaquetes;
	}

	public Paquetes getPaquetes() {
		return paquetes;
	}

	public void setPaquetes(Paquetes paquetes) {
		this.paquetes = paquetes;
	}

	public TipoSeguroImpl getTipoSeguroImpl() {
		return tipoSeguroImpl;
	}

	public void setTipoSeguroImpl(TipoSeguroImpl tipoSeguroImpl) {
		this.tipoSeguroImpl = tipoSeguroImpl;
	}

	public List<TipoSeguros> getListaTipoSeguro() {
		listaTipoSeguro = tipoSeguroImpl.findAll();
		return listaTipoSeguro;
	}

	public void setListaTipoSeguro(List<TipoSeguros> listaTipoSeguro) {
		this.listaTipoSeguro = listaTipoSeguro;
	}

	public TipoSeguros getTipoSeguros() {
		return tipoSeguros;
	}

	public void setTipoSeguros(TipoSeguros tipoSeguros) {
		this.tipoSeguros = tipoSeguros;
	}

	String mensaje = "";

	@PostConstruct
	public void init() {
		categoriaSeguros = new CategoriaSeguros();
		paquetes = new Paquetes();
		tipoSeguros = new TipoSeguros();
		incidencias = new Incidencias();
	}

	// CONSULTA PARA BUSCAR POR NOMBRE DE PAQUETE
	public void busquedaxpaquete() {
		try {
			listaPaquetesConsulta = paqueteImpl.busquedaxPaquete(nombre);
		} catch (Exception e) {
			System.out.println("Error");
		}
	}

	// CRUD PAQUETES
	public void crearPaquetes() {
		try {
			paquetes.setTipoSeguros(tipoSeguros);
			paquetes.setIncidencias(incidencias);
			paqueteImpl.create(paquetes);
			mensaje = "Combo Creardo con Exito";
		} catch (Exception e) {
			System.out.println("Error al crear: " + e.getMessage());
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void editaPaquete() {
		try {
			paquetes.setTipoSeguros(tipoSeguros);
			paquetes.setIncidencias(incidencias);
			paqueteImpl.edit(paquetes);
			mensaje = "Combo Editado con Exito";
		} catch (Exception e) {
			System.out.println("Error al crear: " + e.getMessage());
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void llenarPaquete(Paquetes paq) {
		tipoSeguros.setId(paq.getId());
		incidencias.setId(paq.getId());
		paquetes = paq;
	}

	public void eliminarPaquete(Paquetes paq) {
		try {
			System.out.println("funciona");
			mensaje = "Se ha eliminado con éxito los datos";
			paqueteImpl.remove(paq);
			paquetes = new Paquetes();
		} catch (Exception e) {
			System.out.println("Error: " + e);
		}
		FacesMessage msj = new FacesMessage(mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	// CRUD TIPO SEGURO
	public void crearTipoSeguro() {
		try {
			tipoSeguroImpl.create(tipoSeguros);
			mensaje = "Combo Creardo con Exito";
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void editarTipoSeguro() {
		try {
			tipoSeguroImpl.edit(tipoSeguros);
			mensaje = "Combo Editado con Exito";
		} catch (Exception e) {
			System.out.println("Error al editar: " + e.getMessage());
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void llenarTipoSeguro(TipoSeguros ts) {
		tipoSeguros = ts;
	}

	public void eliminarTipoSeguro(TipoSeguros ts) {
		try {
			tipoSeguroImpl.remove(ts);
		} catch (Exception e) {
		}
	}

	// CRUD CATEGORIA SEGURO

	public void crearCategoriaSeguro() {
		try {
			categoriaSeguros.setTipoSeguros(tipoSeguros);
			categoriaSeguros.setPaquetes(paquetes);
			categoriaSeguroImpl.create(categoriaSeguros);
			mensaje = "Combo Creado con Exito";
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void editarCategoriaSeguro() {
		try {
			categoriaSeguros.setTipoSeguros(tipoSeguros);
			categoriaSeguros.setPaquetes(paquetes);
			categoriaSeguroImpl.edit(categoriaSeguros);
			mensaje = "Combo Editado con Exito";
		} catch (Exception e) {
		}
	}

	public void llenarCategoriaSeguro(CategoriaSeguros cs) {
		tipoSeguros.setId(cs.getId());
		paquetes.setId(cs.getId());
		categoriaSeguros = cs;
	}

	public void eliminarCategoriaSeguro(CategoriaSeguros cs) {
		try {
			categoriaSeguroImpl.remove(cs);
			mensaje = "Se ha eliminado con Exito los datos";
		} catch (Exception e) {
		}
	}

}
