package com.seguros.ctrl;

import java.io.Serializable;
import java.time.Instant;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.seguros.impl.CancelarDaoImpl;
import com.seguros.impl.IncidenciasCubiertasImpl;
import com.seguros.impl.IncidenciasImpl;
import com.seguros.impl.TalleresImpl;
import com.seguros.model.AsignacionSeguros;
import com.seguros.model.Incidencias;
import com.seguros.model.IncidenciasCubiertas;
import com.seguros.model.Talleres;

@Component
@SessionScoped
@ManagedBean(name = "incidenciasA")
public class IncidenciasA implements Serializable {

	private static final long serialVersionUID = 1L;
	@Autowired
	private TalleresImpl talleres;
	@Autowired
	private IncidenciasImpl incidencias;
	@Autowired
	private IncidenciasCubiertasImpl inciCubiertas;
	@Autowired
	private CancelarDaoImpl asSeguros;
	String mensaje = "";

	private List<Talleres> listaTalleres;
	private Talleres taller;

	private List<Incidencias> listaIncidencias;
	private Incidencias incidencia;

	private List<IncidenciasCubiertas> listaICubiertas;
	private IncidenciasCubiertas iCubierta;

	private List<AsignacionSeguros> listaAS;
	private AsignacionSeguros asSeguro;

	private List<AsignacionSeguros> listaA;

	/*****************************************************************************/

	
	
	public List<Talleres> getListaTalleres() {
		this.listaTalleres = talleres.findAll();
		return listaTalleres;
	}

	public List<AsignacionSeguros> getListaA() {
		this.listaA= asSeguros.listaSA(); 
		return listaA;
	}

	public void setListaA(List<AsignacionSeguros> listaA) {
		this.listaA = listaA;
	}

	public void setListaTalleres(List<Talleres> listaTalleres) {
		this.listaTalleres = listaTalleres;
	}

	public Talleres getTaller() {
		return taller;
	}

	public void setTalleres(Talleres taller) {
		this.taller = taller;
	}

	public List<Incidencias> getListaIncidencias() {
		this.listaIncidencias = incidencias.findAll();
		return listaIncidencias;
	}

	public void setListaIncidencias(List<Incidencias> listaIncidencias) {
		this.listaIncidencias = listaIncidencias;
	}

	public Incidencias getIncidencia() {
		return incidencia;
	}

	public void setIncidencia(Incidencias incidencia) {
		this.incidencia = incidencia;
	}

	public List<IncidenciasCubiertas> getListaICubiertas() {
		this.listaICubiertas = inciCubiertas.findAll();
		return listaICubiertas;
	}

	public void setListaICubiertas(List<IncidenciasCubiertas> listaICubiertas) {
		this.listaICubiertas = listaICubiertas;
	}

	public IncidenciasCubiertas getiCubierta() {
		return iCubierta;
	}

	public void setiCubierta(IncidenciasCubiertas iCubierta) {
		this.iCubierta = iCubierta;
	}

	public List<AsignacionSeguros> getListaAS() {
		this.listaAS = asSeguros.findAll();
		return listaAS;
	}

	public void setListaAS(List<AsignacionSeguros> listaAS) {
		this.listaAS = listaAS;
	}

	public AsignacionSeguros getAsSeguro() {
		return asSeguro;
	}

	public void setAsSeguro(AsignacionSeguros asSeguro) {
		this.asSeguro = asSeguro;
	}

	/*****************************************************************************/

	@PostConstruct
	public void init() {
		iCubierta = new IncidenciasCubiertas();
		incidencia = new Incidencias();
		taller = new Talleres();
		asSeguro = new AsignacionSeguros();
	}

	// crud talleres
	/***************************************************************************/
	public void crearTaller() {
		try {
			talleres.create(taller);
			taller = new Talleres();
			mensaje = "Taller creado";
		} catch (Exception e) {
			mensaje = "Error: " + e.getMessage();
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void editarTaller() {
		try {
			talleres.edit(taller);
			taller = new Talleres();
			System.out.println("xxxxxxxxxx");
			mensaje = "Taller Editado";
		} catch (Exception e) {
			mensaje = "Error: " + e.getMessage();
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void eliminarTaller(Talleres t) {
		try {
			talleres.remove(t);
			mensaje = "Taller Eliminado";
		} catch (Exception e) {
			mensaje = "Error: " + e.getMessage();
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void cargarTaller(Talleres t) {
		taller = t;
	}

	/***************************************************************************/
	// crud incidencia
	public void crearIncidncia() {
		try {
			incidencias.create(incidencia);
			mensaje = "Incidencia creada";
		} catch (Exception e) {
			mensaje = "Error: " + e.getMessage();
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void editarIncidencia() {
		try {
			incidencias.edit(incidencia);
			mensaje = "Incidencia editada";
		} catch (Exception e) {
			mensaje = "Error: " + e.getMessage();
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void eliminarIncidencia(Incidencias i) {
		try {
			incidencias.remove(i);
			mensaje = "Incidencia eliminada";
		} catch (Exception e) {
			mensaje = "Error: " + e.getMessage();
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void cargarIncidencia(Incidencias i) {
		incidencia = i;
	}

	/****************************************************************/
	// crud incidencias cubiertas
	public void crearICubiertas() {
		try {
			iCubierta.setAsignacionSeguros(asSeguro);
			iCubierta.setIncidencias(incidencia);
			iCubierta.setTalleres(taller);
			iCubierta.setFecha(Date.from(Instant.now()));
			inciCubiertas.create(iCubierta);
			mensaje = "Incidencia Cubierta Creada";
		} catch (Exception e) {
			mensaje = "Error: " + e.getMessage();
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void editarICubiertas() {
		try {
			iCubierta.setAsignacionSeguros(asSeguro);
			iCubierta.setIncidencias(incidencia);
			iCubierta.setTalleres(taller);
			inciCubiertas.edit(iCubierta);
			mensaje = "Incidencia Cubierta Editada";

		} catch (Exception e) {
			mensaje = "Error: " + e.getMessage();
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void eliminarICubierta(IncidenciasCubiertas ic) {
		try {
			inciCubiertas.remove(ic);
			mensaje = "Incidencia Cubierta Eliminada";
		} catch (Exception e) {
			mensaje = "Error: " + e.getMessage();
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void cargarICubierta(IncidenciasCubiertas inCu) {
		asSeguro.setId(inCu.getAsignacionSeguros().getId());
		incidencia.setId(inCu.getIncidencias().getId());
		taller.setId(inCu.getTalleres().getId());
		iCubierta = inCu;
	}

	/**************************************************************************/
	// Filtros

	private List<IncidenciasCubiertas> listic;
	private String estado;

	public List<IncidenciasCubiertas> getListic() {
		return listic;
	}

	public void setListic(List<IncidenciasCubiertas> listic) {
		this.listic = listic;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public void buscarxestado() {
		try {
			listic = inciCubiertas.busquedaxEstado(estado);
		} catch (Exception e) {
			System.out.println("Error en " + e.getMessage());
			e.printStackTrace();
		}
	}
}
