package com.seguros.ctrl;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.primefaces.PrimeFaces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.seguros.impl.MarcaImpl;
import com.seguros.impl.ModeloImpl;
import com.seguros.impl.TipoAutoImpl;
import com.seguros.impl.VehiculoImpl;
import com.seguros.model.Marcas;
import com.seguros.model.Modelos;
import com.seguros.model.TipoAutos;
import com.seguros.model.Vehiculos;

@Component
@ManagedBean
@SessionScoped
public class SeguroAutos implements Serializable {

	private static final long serialVersionUID = 1L;
	@Autowired
	private MarcaImpl dmarca;
	private List<Marcas> listaMarcas;
	private Marcas marcas;

	@Autowired
	private TipoAutoImpl dtauto;
	private List<TipoAutos> listaTA;
	private TipoAutos tipoAutos;

	@Autowired
	private ModeloImpl dmodelo;
	private List<Modelos> listaModelos;
	private Modelos modelos;

	@Autowired
	private VehiculoImpl dvehiculo;
	private List<Vehiculos> listaVehiculos;
	private Vehiculos vehiculos;
	private String placa;

	String mensaje = "";

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public List<Marcas> getListaMarcas() {
		this.listaMarcas = dmarca.findAll();
		return listaMarcas;
	}

	public void setListaMarcas(List<Marcas> listaMarcas) {
		this.listaMarcas = listaMarcas;
	}

	public Marcas getMarcas() {
		return marcas;
	}

	public void setMarcas(Marcas marcas) {
		this.marcas = marcas;
	}

	public List<TipoAutos> getListaTA() {
		this.listaTA = dtauto.findAll();
		return listaTA;
	}

	public void setListaTA(List<TipoAutos> listaTA) {
		this.listaTA = listaTA;
	}

	public TipoAutos getTipoAutos() {
		return tipoAutos;
	}

	public void setTipoAutos(TipoAutos tipoAutos) {
		this.tipoAutos = tipoAutos;
	}

	public List<Modelos> getListaModelos() {
		this.listaModelos = dmodelo.findAll();
		return listaModelos;
	}

	public void setListaModelos(List<Modelos> listaModelos) {
		this.listaModelos = listaModelos;
	}

	public Modelos getModelos() {
		return modelos;
	}

	public void setModelos(Modelos modelos) {
		this.modelos = modelos;
	}

	public List<Vehiculos> getListaVehiculos() {
		this.listaVehiculos = dvehiculo.findAll();
		return listaVehiculos;
	}

	public void setListaVehiculos(List<Vehiculos> listaVehiculos) {
		this.listaVehiculos = listaVehiculos;
	}

	public Vehiculos getVehiculos() {
		return vehiculos;
	}

	public void setVehiculos(Vehiculos vehiculos) {
		this.vehiculos = vehiculos;
	}

	@PostConstruct
	public void init() {
		marcas = new Marcas();
		modelos = new Modelos();
		tipoAutos = new TipoAutos();
		vehiculos = new Vehiculos();
	}

	// CRUD TIPO AUTOS
	public void crearTA() {
		try {
			dtauto.create(tipoAutos);
			tipoAutos = new TipoAutos();
			mensaje = "Tipo de Auto Creado";
			marcas = new Marcas();
			modelos = new Modelos();
			tipoAutos = new TipoAutos();
			vehiculos = new Vehiculos();
		} catch (Exception e) {
			mensaje = "ˇˇImportante!! Debe llenar todos los campos";
			e.printStackTrace();
		}

		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void editarTA() {
		try {
			dtauto.edit(tipoAutos);
			mensaje = "Tipo de Auto Editado";
			marcas = new Marcas();
			modelos = new Modelos();
			tipoAutos = new TipoAutos();
			vehiculos = new Vehiculos();
		} catch (Exception e) {
			mensaje = "Error:" + e.getMessage();
			e.printStackTrace();
		}

		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void eliminarTA(TipoAutos ta) {
		try {
			dtauto.remove(ta);
			mensaje = "Tipo de Auto Eliminado";
			marcas = new Marcas();
			modelos = new Modelos();
			tipoAutos = new TipoAutos();
			vehiculos = new Vehiculos();
		} catch (Exception e) {
			mensaje = "Error:" + e.getMessage();
			e.printStackTrace();

		}

		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void cargarTA(TipoAutos ta) {
		tipoAutos = ta;
	}

	// CRUD MARCAS
	public void crearMarcas() {
		try {
			dmarca.create(marcas);
			marcas = new Marcas();
			mensaje = "Marca Creada";
			marcas = new Marcas();
			modelos = new Modelos();
			tipoAutos = new TipoAutos();
			vehiculos = new Vehiculos();
		} catch (Exception e) {
			mensaje = "Error:" + e.getMessage();
			e.printStackTrace();
		}

		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void editarMarcas() {
		try {
			dmarca.edit(marcas);
			marcas = new Marcas();
			mensaje = "Marca Editada";
			marcas = new Marcas();
			modelos = new Modelos();
			tipoAutos = new TipoAutos();
			vehiculos = new Vehiculos();
		} catch (Exception e) {
			mensaje = "Error:" + e.getMessage();
			e.printStackTrace();
		}

		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void eliminarMarcas(Marcas ma) {
		try {
			dmarca.remove(ma);
			mensaje = "Marca Eliminada";
			marcas = new Marcas();
			modelos = new Modelos();
			tipoAutos = new TipoAutos();
			vehiculos = new Vehiculos();
		} catch (Exception e) {
			mensaje = "Error:" + e.getMessage();
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void cargarMarcas(Marcas ma) {
		marcas = ma;
	}

	// CRUD MODELOS
	public void crearModelos() {
		try {
			modelos.setMarcas(marcas);
			dmodelo.create(modelos);
			modelos = new Modelos();
			mensaje = "Modelo Creado";
			marcas = new Marcas();
			modelos = new Modelos();
			tipoAutos = new TipoAutos();
			vehiculos = new Vehiculos();
		} catch (Exception e) {
			mensaje = "Error:" + e.getMessage();
			e.printStackTrace();
		}

		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void editarModelos() {
		try {
			modelos.setMarcas(marcas);
			dmodelo.edit(modelos);
			modelos = new Modelos();
			mensaje = "Modelo Editado";
			marcas = new Marcas();
			modelos = new Modelos();
			tipoAutos = new TipoAutos();
			vehiculos = new Vehiculos();
		} catch (Exception e) {
			mensaje = "Error:" + e.getMessage();
			e.printStackTrace();
		}

		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void eliminarModelos(Modelos mo) {
		try {
			dmodelo.remove(mo);
			mensaje = "Modelo Eliminado";
			marcas = new Marcas();
			modelos = new Modelos();
			tipoAutos = new TipoAutos();
			vehiculos = new Vehiculos();
		} catch (Exception e) {
			mensaje = "Error:" + e.getMessage();
			e.printStackTrace();
		}

		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void cargarModelos(Modelos mo) {
		marcas.setId(mo.getId());
		modelos = mo;
	}

	// CRUD VEHICULOS
	public void crearVehiculo() {
		try {
			vehiculos.setModelos(modelos);
			vehiculos.setTipoAutos(tipoAutos);
			dvehiculo.create(vehiculos);
			mensaje = "Vehiculo Creado";
			vehiculos = new Vehiculos();
			marcas = new Marcas();
			modelos = new Modelos();
			tipoAutos = new TipoAutos();
			vehiculos = new Vehiculos();
		} catch (Exception e) {
			mensaje = "ˇˇImportante!! Debe llenar todos los campos";
			e.printStackTrace();
		}

		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void editarVehiculos() {
		try {
			vehiculos.setModelos(modelos);
			vehiculos.setTipoAutos(tipoAutos);
			dvehiculo.edit(vehiculos);
			mensaje = "Vehiculo Editado";
			marcas = new Marcas();
			modelos = new Modelos();
			tipoAutos = new TipoAutos();
			vehiculos = new Vehiculos();
		} catch (Exception e) {
			mensaje = "Error:" + e.getMessage();
			e.printStackTrace();
		}

		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void eliminarVehiculo(Vehiculos v) {
		try {
			dvehiculo.remove(v);
			mensaje = "Vehiculo Eliminado";
			marcas = new Marcas();
			modelos = new Modelos();
			tipoAutos = new TipoAutos();
			vehiculos = new Vehiculos();
		} catch (Exception e) {
			mensaje = "Error:" + e.getMessage();
			e.printStackTrace();
		}

		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void cargarVehiculo(Vehiculos ve) {
		marcas.setId(ve.getId());
		tipoAutos.setId(ve.getId());
		vehiculos = ve;
	}

	private List<Modelos> listModexM;

	public List<Modelos> getListModexM() {
		return listModexM;
	}

	public void setListModexM(List<Modelos> listModexM) {
		this.listModexM = listModexM;
	}

	public void atcModel() {
		marcas.getId();
		listModexM = dmodelo.modebymar(marcas);
		PrimeFaces.current().ajax().update("datos:mo");

	}

}