package com.seguros.ctrl;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import org.primefaces.PrimeFaces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.seguros.impl.BeneficiarioImpl;
import com.seguros.impl.DepartamentoImpl;
import com.seguros.impl.EstadocivilImpl;
import com.seguros.impl.MunicipiosImpl;
import com.seguros.impl.PersonaImpl;
import com.seguros.impl.TipoUsuarioImpl;
import com.seguros.model.Beneficiarios;
import com.seguros.model.Departamentos;
import com.seguros.model.EstadoCivil;
import com.seguros.model.Municipios;
import com.seguros.model.Personas;
import com.seguros.model.TipoUsuarios;

@Component
@RequestScoped
@ManagedBean
public class SeguroVida implements Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	private DepartamentoImpl daode;

	@Autowired
	private TipoUsuarioImpl dao;

	@Autowired
	private MunicipiosImpl daoMuni;

	@Autowired
	private PersonaImpl daoper;

	@Autowired
	private EstadocivilImpl daoestado;

	@Autowired
	private BeneficiarioImpl daobene;
	private String mensaje = "";

	private List<Personas> listaPersonas;
	private Personas persona;

	private List<Beneficiarios> listaBeneficiario;
	private Beneficiarios beneficiario;
	private EstadoCivil estadoCivil;
	private List<EstadoCivil> listaestadocivil;

	private Municipios municipio;
	private List<Municipios> listamunicipios;

	private Departamentos d;

	private TipoUsuarios Tusuarios;
	private List<TipoUsuarios> listaTipoususarios;

	private List<Personas> listaxtipo;
	private String tipo;
	private String dui;
	private List<Departamentos> listadepa;

	public List<Departamentos> getListadepa() {
		listadepa = daode.findAll();
		return listadepa;
	}

	public void setListadepa(List<Departamentos> listadepa) {
		this.listadepa = listadepa;
	}

	public Departamentos getD() {
		return d;
	}

	public void setD(Departamentos d) {
		this.d = d;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public List<Personas> getListaxtipo() {
		return listaxtipo;
	}

	public void setListaxtipo(List<Personas> listaxtipo) {
		this.listaxtipo = listaxtipo;
	}

	public List<EstadoCivil> getListaestadocivil() {
		listaestadocivil = daoestado.findAll();
		return listaestadocivil;
	}

	public void setListaestadocivil(List<EstadoCivil> listaestadocivil) {
		this.listaestadocivil = listaestadocivil;
	}

	public List<TipoUsuarios> getListaTipoususarios() {
		this.listaTipoususarios = dao.findAll();
		return listaTipoususarios;
	}

	public void setListaTipoususarios(List<TipoUsuarios> listaTipoususarios) {

		this.listaTipoususarios = listaTipoususarios;
	}

	// listado de municipios
	public List<Municipios> getListamunicipios() {
		// this.listamunicipios = daoMuni.findAll();
		return listamunicipios;
	}

	public void setListamunicipios(List<Municipios> listamunicipios) {
		this.listamunicipios = listamunicipios;
	}

	public TipoUsuarios getTusuarios() {
		return Tusuarios;
	}

	public void setTusuarios(TipoUsuarios Tusuarios) {
		this.Tusuarios = Tusuarios;
	}

	public EstadoCivil getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(EstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public Municipios getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipios municipio) {
		this.municipio = municipio;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public List<Personas> getListaPersonas() {
		listaPersonas = daoper.findAll();
		return listaPersonas;
	}

	public void setListaPersonas(List<Personas> listaPersonas) {
		this.listaPersonas = listaPersonas;
	}

	public Personas getPersona() {
		return persona;
	}

	public void setPersona(Personas persona) {
		this.persona = persona;
	}

	public List<Beneficiarios> getListaBeneficiario() {
		listaBeneficiario = daobene.findAll();
		return listaBeneficiario;
	}

	public void setListaBeneficiario(List<Beneficiarios> listaBeneficiario) {
		this.listaBeneficiario = listaBeneficiario;
	}

	public Beneficiarios getBeneficiario() {
		return beneficiario;
	}

	public void setBeneficiario(Beneficiarios beneficiario) {
		this.beneficiario = beneficiario;
	}

	public PersonaImpl getDaoper() {
		return daoper;
	}

	public void setDaoper(PersonaImpl daoper) {
		this.daoper = daoper;
	}

	@PostConstruct
	public void init() {
		beneficiario = new Beneficiarios();
		persona = new Personas();
		Tusuarios = new TipoUsuarios();
		estadoCivil = new EstadoCivil();
		municipio = new Municipios();
		d = new Departamentos();
	}

	// CONSULTAS
	public void BxTipoUsuario() {
		try {
			listaxtipo = daoper.busquedaxTipoUsuario(tipo);
		} catch (Exception e) {
		}
	}

	public void consultarPorIDpersona(Personas per) {
		persona.setId(per.getId());
		estadoCivil.setId(per.getEstadoCivil().getId());
		municipio.setId(per.getMunicipios().getId());
		Tusuarios.setId(per.getTipoUsuarios().getId());
		this.persona = per;
	}

	public void consultarIDbeneinsert(Personas p) {

		beneficiario.setPersonas(p);
	}

	//////////////////////// crud personas //////////////////
	public void createVidapersona() {
		try {
			persona.setMunicipios(municipio);
			persona.setEstadoCivil(estadoCivil);
			persona.setTipoUsuarios(Tusuarios);
			daoper.create(persona);
			mensaje = "Registrado";
			init();
		} catch (Exception e) {
			mensaje = "ˇˇImportante!! Debe llenar todos los campos";
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void editVidapersona() {
		try {
			persona.setEstadoCivil(estadoCivil);
			persona.setTipoUsuarios(Tusuarios);
			persona.setMunicipios(municipio);
			daoper.edit(persona);
			init();
			mensaje = "Editado";
		} catch (Exception e) {
			mensaje = "Error" + e.getMessage();
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void deleteVidapersona(Personas p) {
		try {
			daoper.remove(p);
			init();
			mensaje = "eliminado";
		} catch (Exception e) {
			mensaje = "error" + e.getMessage();
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	/////
	public void listarPersona() {
		listaPersonas = daoper.findAll();
	}

	/////
	public void listarmunicipiopersonas() {
		listamunicipios = daoMuni.findAll();
	}
	///////////////////////// crud beneficiario/////////////////

	public void createBeneficiario() {
		try {
			daobene.create(beneficiario);
			init();
			mensaje = "creado";
		} catch (Exception e) {
			mensaje = "error" + e.getMessage();
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void editBeneficiario() {
		try {
			beneficiario.setPersonas(persona);
			daobene.edit(beneficiario);
			init();
			mensaje = "editado";
		} catch (Exception e) {
			mensaje = "error" + e.getMessage();
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	public void eliminarBeneficiario(Beneficiarios b) {
		try {
			daobene.remove(b);
			init();
			mensaje = "eliminado";
		} catch (Exception e) {
			mensaje = "error" + e.getMessage();
			e.printStackTrace();
		}
		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);
	}

	//
	public void listarBeneficiario() {
		listaBeneficiario = daobene.findAll();
	}

	public void consultarPorIDbeneficiario(Beneficiarios ben) {
		persona.setId(ben.getId());
		this.beneficiario = ben;
	}

	public String getDui() {
		return dui;
	}

	public void setDui(String dui) {
		this.dui = dui;
	}

	public void search() {
		try {
			listClientes = daoper.search(dui);
			System.out.println("xxxxxxxxxxxxxxxxxxxxx");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private List<Personas> listaPorId;

	public List<Personas> getListaPorId() {
		return listaPorId;
	}

	public void setListaPorId(List<Personas> listaPorId) {
		this.listaPorId = listaPorId;
	}

	public void listarP(int id) {
		try {
			listaPorId = daoper.busquedaPorId(id);
		} catch (Exception e) {
		}

	}

	private List<Personas> listClientes;

	public List<Personas> getListClientes() {
		// listClientes = daoper.listarClientes();
		return listClientes;
	}

	private List<Personas> listaClientes2;

	public List<Personas> getListaClientes2() {
		listaClientes2 = daoper.listarClientes();
		return listaClientes2;
	}

	public void setListaClientes2(List<Personas> listaClientes2) {
		this.listaClientes2 = listaClientes2;
	}

	public void actMuni() {
		d.getId();
		// mu = (Municipios) daomu.muniByDepa(d);
		listamunicipios = daoMuni.muniByDepa(d);
		PrimeFaces.current().ajax().update("datos:mu");
	}

	public void editMuni() {
		d.getId();
		listamunicipios = daoMuni.muniByDepa(d);
		PrimeFaces.current().ajax().update("editar:mo");
	}

	public void setListClientes(List<Personas> listClientes) {
		this.listClientes = listClientes;
	}

	private List<Personas> listEmpleados;

	public List<Personas> getListEmpleados() {
		listEmpleados = daoper.listarEmpleados();
		return listEmpleados;
	}

	public void setListEmpleados(List<Personas> listEmpleados) {
		this.listEmpleados = listEmpleados;
	}

	private List<Personas> listaEmpleado;

	public List<Personas> getListaEmpleado() {
		this.listaEmpleado = daoper.listarEmpleados();
		return listaEmpleado;
	}
 
	public void setListaEmpleado(List<Personas> listaEmpleado) {
		this.listaEmpleado = listaEmpleado;
	}

	public void eliminar(Personas lp) {
		try {
			lp.setEstado(false);
			daoper.edit(lp);
			init();
			System.out.println("Ha eliminado con exito");
		} catch (Exception e) {
			System.out.println("Error al eliminar por "+e.getMessage());
		}
	}
	
}
