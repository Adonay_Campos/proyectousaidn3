package com.seguros.ctrl;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.PrimeFaces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.seguros.impl.DepartamentoImpl;
import com.seguros.impl.MunicipiosImpl;
import com.seguros.impl.SucursalesImpl;
import com.seguros.model.Departamentos;
import com.seguros.model.Municipios;
import com.seguros.model.Sucursales;

@Component
@ManagedBean
@SessionScoped
public class UbicacionS implements Serializable {

	private static final long serialVersionUID = 1L;
	@Autowired
	private DepartamentoImpl depa;
	@Autowired
	private MunicipiosImpl mun;
	@Autowired
	private SucursalesImpl sucursales;
	private List<Municipios> listMu;
	private List<Departamentos> lsitDe;
	private List<Sucursales> listSucu;
	private Departamentos departamentos;
	private Municipios municipios;
	private Sucursales sucursal;

	public List<Departamentos> getLsitDe() {
		lsitDe = depa.findAll();
		return lsitDe;
	}

	public void setLsitDe(List<Departamentos> lsitDe) {
		this.lsitDe = lsitDe;
	}

	public Departamentos getDepartamentos() {
		return departamentos;
	}

	public void setDepartamentos(Departamentos departamentos) {
		this.departamentos = departamentos;
	}

	public List<Municipios> getListMu() {
		return listMu;
	}

	public void setListMu(List<Municipios> listMu) {
		this.listMu = listMu;
	}

	public Municipios getMunicipios() {
		return municipios;
	}

	public void setMunicipios(Municipios municipios) {
		this.municipios = municipios;
	}

	public List<Sucursales> getListSucu() {
		listSucu = sucursales.findAll();
		return listSucu;
	}

	public void setListSucu(List<Sucursales> listSucu) {
		this.listSucu = listSucu;
	}

	public Sucursales getSucursal() {
		return sucursal;
	}

	public void setSucursal(Sucursales sucursal) {
		this.sucursal = sucursal;
	}

	@PostConstruct
	public void init() {
		departamentos = new Departamentos();
		municipios = new Municipios();
		sucursal = new Sucursales();
	}

	public void insertarDept() {
		try {
			depa.create(departamentos);
			departamentos = new Departamentos();
			lsitDe = depa.findAll();
		} catch (Exception e) {
			System.out.println("Error al crear un nuevo : Departamento, por " + e.getMessage());
			e.printStackTrace();
		}
	}

	public void editarDept() {
		try {
			depa.edit(departamentos);
			departamentos = new Departamentos();
			lsitDe = depa.findAll();
		} catch (Exception e) {
			System.out.println("Error al crear un nuevo : Departamento, por " + e.getMessage());
			e.printStackTrace();
		}
	}

	public void eliminarDept(Departamentos dep) {
		try {
			depa.remove(dep);
			departamentos = new Departamentos();
			lsitDe = depa.findAll();
		} catch (Exception e) {
			System.out.println("Error al eliminar : " + "Departamento, por " + e.getMessage());
			e.printStackTrace();
		}
	}

	public void llenarDepa(Departamentos dep) {
		departamentos = dep;
	}

	public void insertarMun() {
		try {
			municipios.setDepartamentos(departamentos);
			mun.create(municipios);
			municipios = new Municipios();
			departamentos = new Departamentos();
			listMu = mun.findAll();
		} catch (Exception e) {
			System.out.println("Error al crear un nuevo : Municipio, por " + e.getMessage());
			e.printStackTrace();
		}
	}

	public void editarMun() {
		try {
			municipios.setDepartamentos(departamentos);
			mun.edit(municipios);
			municipios = new Municipios();
			departamentos = new Departamentos();
			listMu = mun.findAll();
		} catch (Exception e) {
			System.out.println("Error al crear un nuevo : Municipio, por " + e.getMessage());
			e.printStackTrace();
		}
	}

	public void eliminarMun(Municipios m) {
		try {
			mun.remove(m);
			municipios = new Municipios();
			listMu = mun.findAll();
		} catch (Exception e) {
			System.out.println("Error al eliminar : " + "Municipio, por " + e.getMessage());
			e.printStackTrace();
		}
	}

	public void llenarMun(Municipios m) {
		departamentos.setId(m.getDepartamentos().getId());
		municipios = m;
	}

	public void insertarSucu() {
		try {
			sucursal.setMunicipios(municipios);
			sucursales.create(sucursal);
			municipios = new Municipios();
			sucursal = new Sucursales();
			listSucu = sucursales.findAll();
		} catch (Exception e) {
			System.out.println("Error al crear un nuevo : Sucursal, por " + e.getMessage());
			e.printStackTrace();
		}
	}

	public void editarSucu() {
		try {
			sucursal.setMunicipios(municipios);
			sucursales.edit(sucursal);
			municipios = new Municipios();
			sucursal = new Sucursales();
			listSucu = sucursales.findAll();
		} catch (Exception e) {
			System.out.println("Error al crear un nuevo : Sucursal, por " + e.getMessage());
			e.printStackTrace();
		}
	}

	public void eliminarSucu(Sucursales s) {
		try {
			sucursales.remove(s);
			municipios = new Municipios();
			sucursal = new Sucursales();
			listSucu = sucursales.findAll();
		} catch (Exception e) {
			System.out.println("Error al eliminar : " + "Sucursal, por " + e.getMessage());
			e.printStackTrace();
		}
	}

	public void llenarSucu(Sucursales s) {
		municipios.setId(s.getMunicipios().getId());
		sucursal = s;
	}

	public void actMuni() {
		departamentos.getId();
		listMu = mun.muniByDepa(departamentos);
		PrimeFaces.current().ajax().update("su:mu");
	}

	private List<Sucursales> busq;

	public List<Sucursales> getbusq() {
		return busq;
	}

	public String nomb;

	public String getNomb() {
		return nomb;
	}

	public void setNomb(String nomb) {
		this.nomb = nomb;
	}

	public void busqueda(int m) {
		try {
			busq = sucursales.busquedaxDepaMuni(m);
		} catch (Exception e) {
			System.out.println("Error al hacer busqueda en : Sucursales, por " + e.getMessage());

		}
	}

	public void busqueda1(String m) {
		try {
			busq = sucursales.busquedaxNombre(m);
		} catch (Exception e) {
			System.out.println("Error al hacer busqueda en : Sucursales, por " + e.getMessage());

		}
	}

}
