package com.seguros.ctrl;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.PrimeFaces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.seguros.impl.DepartamentoImpl;
import com.seguros.impl.EstadocivilImpl;
import com.seguros.impl.MunicipiosImpl;
import com.seguros.impl.PersonaImpl;
import com.seguros.impl.TipoUsuarioImpl;
import com.seguros.impl.UsuarioImpl;
import com.seguros.model.Departamentos;
import com.seguros.model.EstadoCivil;
import com.seguros.model.Municipios;
import com.seguros.model.Personas;
import com.seguros.model.TipoUsuarios;
import com.seguros.model.Usuarios;

/**
 *
 * @author rene.fuentesusam
 */
@Component
@ManagedBean
@SessionScoped
public class UsuariosC implements Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	public DepartamentoImpl daod;

	@Autowired
	public MunicipiosImpl daomu;

	@Autowired
	public EstadocivilImpl daoec;

	@Autowired
	public TipoUsuarioImpl daptp;

	@Autowired
	public UsuarioImpl daousu;

	@Autowired
	public PersonaImpl daoperso;

	private List<Departamentos> listad;
	private List<TipoUsuarios> listatu;
	private List<EstadoCivil> listaec;
	private List<Municipios> listamu;
	private List<Usuarios> listausuarios;
	private Usuarios usuarios;
	private Personas personas;
	private List<Personas> listapersonas;
	private String usuario;
	private String pass;
	private TipoUsuarios tp;
	private EstadoCivil ec;
	private Municipios mu;
	private Departamentos d;
	private String dui;

	public List<Departamentos> getListad() {
		listad = daod.findAll();
		return listad;
	}

	public void setListad(List<Departamentos> listad) {
		this.listad = listad;
	}

	public List<TipoUsuarios> getListatu() {
		listatu = daptp.findAll();
		return listatu;
	}

	public void setListatu(List<TipoUsuarios> listatu) {
		this.listatu = listatu;
	}

	public List<EstadoCivil> getListaec() {
		listaec = daoec.findAll();
		return listaec;
	}

	public void setListaec(List<EstadoCivil> listaec) {
		this.listaec = listaec;
	}

	public List<Municipios> getListamu() {
		// listamu = daomu.findAll();
		return listamu;
	}

	public void setListamu(List<Municipios> listamu) {
		this.listamu = listamu;
	}

	public String getDui() {
		return dui;
	}

	public void setDui(String dui) {
		this.dui = dui;
	}

	public Departamentos getD() {
		return d;
	}

	public void setD(Departamentos d) {
		this.d = d;
	}

	public TipoUsuarios getTp() {
		return tp;
	}

	public void setTp(TipoUsuarios tp) {
		this.tp = tp;
	}

	public EstadoCivil getEc() {
		return ec;
	}

	public void setEc(EstadoCivil ec) {
		this.ec = ec;
	}

	public Municipios getMu() {
		return mu;
	}

	public void setMu(Municipios mu) {
		this.mu = mu;
	}

	public Personas getPersonas() {
		return personas;
	}

	public void setPersonas(Personas personas) {
		this.personas = personas;
	}

	public List<Personas> getListapersonas() {
		listapersonas = daoperso.findAll();
		return listapersonas;
	}

	public void setListapersonas(List<Personas> listapersonas) {
		this.listapersonas = listapersonas;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public List<Usuarios> getListausuarios() {
		listausuarios = daousu.findAll();
		return listausuarios;
	}

	public void setListausuarios(List<Usuarios> listausuarios) {
		this.listausuarios = listausuarios;
	}

	public Usuarios getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(Usuarios usuarios) {
		this.usuarios = usuarios;
	}

	private Usuarios user2;

	@PostConstruct
	public void init() {
		personas = new Personas();
		usuarios = new Usuarios();

		tp = new TipoUsuarios();
		mu = new Municipios();
		ec = new EstadoCivil();
		d = new Departamentos();
	}

	public String access() {
		FacesContext fc = FacesContext.getCurrentInstance();
		usuarios = daousu.access(usuario, pass);
		if (usuarios != null) {
			user2 = usuarios;
			fc.getExternalContext().getSessionMap().put("us", user2);
			return "menu?faces-redirect=true";
		} else {
			fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Contrase�a y	 Usuario Incorrectos!",
					"No esta registrado, verifique si su contrase�a y su usuario son correctos"));
			return "index?faces-redirect=false";
		}
	}

	@SuppressWarnings("unlikely-arg-type")
	public String close() {
		FacesContext fc = FacesContext.getCurrentInstance();
		user2 = (Usuarios) fc.getExternalContext().getSessionMap().get("us");
		fc.getExternalContext().invalidateSession();
		fc.getExternalContext().getSessionMap().remove(usuarios.getId());
		System.out.println("Verficar: " + usuarios.getId());
		return "index?faces-redirect=true";

	}

	public void insert() {
		try {
			personas.setTipoUsuarios(tp);
			personas.setMunicipios(mu);
			personas.setEstadoCivil(ec);
			daoperso.create(personas);
			usuarios = new Usuarios();
			listapersonas = daoperso.search(personas.getDui());
			System.out.println("se ejecuto" + personas.getId());

		} catch (Exception e) {
			System.out.println("Error" + e.getMessage());
			e.printStackTrace();
		}

	}

	public void search() {
		try {
			System.out.println("antes de buscar:");
			listapersonas = daoperso.search(dui);
			System.out.println("despues de buscar:" + dui);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error" + e.getMessage());
		}
	}

	public void getperson(Personas per) {
		try {
			personas = daousu.getperson(per);
			System.out.println("me lo llevo:" + per.getId());
			if (personas != null) {
				System.out.println("encontrado");
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("per", personas);
			}
			if (per.getId() != null) {
				ExternalContext dc = FacesContext.getCurrentInstance().getExternalContext();
				dc.redirect(dc.getRequestContextPath() + "/faces/CrearUsuario.xhtml");
			}
		} catch (Exception e) {
			System.out.println("Error" + e.getMessage());
			e.printStackTrace();
		}
	}

	public void verificarSesion() {
		try {
			FacesContext contex = FacesContext.getCurrentInstance();
			user2 = (Usuarios) contex.getExternalContext().getSessionMap().get("us");
			if (user2 == null) {
				contex.getExternalContext().redirect("index.xhtml");
			}
		} catch (Exception e) {
			System.out.println("Error : " + e.getMessage());
			e.printStackTrace();
		}
	}

	// CRUD PARA CREAR NUEVO USUARIO//////

	public void insertuser() {
		try {
			usuarios.setPersonas(personas);
			usuarios.setEstado(true);
			daousu.create(usuarios);
			usuarios = new Usuarios();
			mensaje = "Usuario Registrado con Exito";
		} catch (Exception e) {
			mensaje = "Error al crear Usuario" + e.getMessage();
			e.printStackTrace();
		}

		FacesMessage msj = new FacesMessage(this.mensaje);
		FacesContext.getCurrentInstance().addMessage(null, msj);

	}

	public void editarusuario() {
		try {
			usuarios.setPersonas(personas);
			System.out.println("antes de insertar");
			daousu.edit(usuarios);
			System.out.println("Se ingreso");
			init();
			mensaje = "modificado";

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void eliminarusuario(Usuarios us) {
		try {
			us.setEstado(false);
			System.out.println("antes de insertar");
			daousu.edit(us);
			System.out.println("Se ingreso");
			mensaje = "eliminado";
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void cargarporIDusuario(Usuarios u) {
		try {
			personas.setId(u.getPersonas().getId());
			usuarios = u;
		} catch (Exception e) {
		}
	}

	private String mensaje;

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	/////////////////////
	private List<Usuarios> listausuarioId;

	public List<Usuarios> getListausuarioId() {
		return listausuarioId;
	}

	public void setListausuarioId(List<Usuarios> listausuarioId) {
		this.listausuarioId = listausuarioId;
	}

	public void preedit(Personas p) {
		try {

			mu.setId(p.getMunicipios().getId());
			tp.setId(p.getTipoUsuarios().getId());
			ec.setId(p.getEstadoCivil().getId());
			personas = p;

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void editar() {
		try {
			personas.setTipoUsuarios(tp);
			personas.setMunicipios(mu);
			personas.setEstadoCivil(ec);
			daoperso.edit(personas);
			listapersonas = daoperso.search(personas.getDui());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error" + e.getMessage());
		}
	}


	private List<Personas> listaEmpleados;


	public List<Personas> getListaEmpleados() {
		this.listaEmpleados= daousu.listarEmpleados();
		return listaEmpleados;
	}

	public void setListaEmpleados(List<Personas> listaEmpleados) {
		this.listaEmpleados = listaEmpleados;
	}
	
	
	

}