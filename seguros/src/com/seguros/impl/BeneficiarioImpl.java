package com.seguros.impl;

import com.seguros.model.Beneficiarios;
import com.seguros.model.Municipios;
import com.seguros.model.Personas;
import com.seguros.util.AbstractFacade;
import com.seguros.util.Dao;

import net.bytebuddy.asm.Advice.Return;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.seguros.model.Beneficiarios;
import com.seguros.model.Personas;
import com.seguros.util.Dao;

@Repository("beneficiario")
public class BeneficiarioImpl extends AbstractFacade<Beneficiarios> implements Dao<Beneficiarios> {

	@Autowired
	private SessionFactory sessionFactory;

	public BeneficiarioImpl() {
		super(Beneficiarios.class);
	}

	public BeneficiarioImpl(SessionFactory sf, Class<Beneficiarios> entityClass) {
		super(entityClass);
//        this.sessionFactory = sessionFactory;
	}

    @Override
    protected SessionFactory sessionFactory() {
        return sessionFactory;
    }
    
    @Transactional
    public List<Beneficiarios> actbene(Personas p){
    	List<Beneficiarios> list = new ArrayList<>();
    	try {
			list = sessionFactory.getCurrentSession().createNativeQuery("select * from beneficiarios where beneficiario ="+ p.getId(),Beneficiarios.class).getResultList();
		} catch (Exception e) {
		}
    	return list;
    }
    
}
