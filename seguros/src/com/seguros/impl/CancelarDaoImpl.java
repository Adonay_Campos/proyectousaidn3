package com.seguros.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.seguros.model.AsignacionSeguros;
import com.seguros.model.Paquetes;
import com.seguros.model.Sucursales;
import com.seguros.model.TipoSeguros;
import com.seguros.util.AbstractFacade;
import com.seguros.util.Dao;

@Repository("cancelacion")
public class CancelarDaoImpl extends AbstractFacade<AsignacionSeguros> implements Dao<AsignacionSeguros> {

	@Autowired
	private SessionFactory sessionFactory;

	public CancelarDaoImpl(SessionFactory sessionFactory, Class<AsignacionSeguros> AsignacionSeguros) {
		super(AsignacionSeguros);
		this.sessionFactory = sessionFactory;
	}

	public CancelarDaoImpl() {
		super(AsignacionSeguros.class);
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sessionFactory;
	}

	/// Metodo que genera numero de poliza
	@Transactional
	public String generarNumPoliza(String p) {

		String polizanueva = "Error", polizaanterior, seguro = p, part1, part2;
		String[] parts;
		int numeroant, numeronuevo;

		if (seguro == "Seguro de Vida") {
			polizaanterior = ultimaPoliza("SV");

			if (polizaanterior == "nulo") {
				polizanueva = "SV1";
			} else {
				parts = polizaanterior.split("(?<=V)"); // me divide el string ejemplo SV0001 --> part1=SV y part2=0001
				part1 = parts[0];
				part2 = parts[1];

				numeroant = Integer.parseInt(part2);
				numeronuevo = numeroant + 1;
				polizanueva = part1 + Integer.toString(numeronuevo);
			}
		} else if (seguro == "Seguro de Auto") {
			polizaanterior = ultimaPoliza("SA");

			if (polizaanterior == "nulo") {
				polizanueva = "SA1";
			} else {
				parts = polizaanterior.split("(?<=A)"); // me divide el string ejemplo SA0001 --> part1=SA y part2=0001
				part1 = parts[0];
				part2 = parts[1];

				numeroant = Integer.parseInt(part2);
				numeronuevo = numeroant + 1;
				polizanueva = part1 + Integer.toString(numeronuevo);
			}
		}

		return polizanueva;
	}

	/// Metodo que obtiene la ultima poliza guardada
	@Transactional
	public String ultimaPoliza(String c) {
		String polizaUtil; // almacenaremos la ultima poliza guardada en la BD
		List<AsignacionSeguros> listaPol = new ArrayList<>();
		;
		int idmayor, a = 0;
		listaPol = consPSeguro(c); // ejecuto el metodo de busqueda

		if (listaPol.size() == 0) {
			return polizaUtil = "nulo";
		} else {
			idmayor = listaPol.get(0).getId(); // asigno el primer id de la lista a idmayor

			for (int i = 0; i < listaPol.size(); i++) { // mientras i sea menor que el tamaa�o de la lista que ejecute
														// el for
				if (idmayor < listaPol.get(i).getId()) { // si idmayor es menor que el id de la lista correspondiente
															// que asigne este valor a idmayor
					idmayor = listaPol.get(i).getId();
				} else {
					idmayor = idmayor;

				}
				a = i;
			}
			polizaUtil = listaPol.get(a).getPoliza(); // asigno el numero de poliza mayor de la lista a polizaUtil

			return polizaUtil;
		}
	}

	/// metodo para consultar lista de polizas guardadas(vida o auto)

	@SuppressWarnings("unchecked")
	@Transactional
	public List<AsignacionSeguros> consPSeguro(String comienza) {
		List<AsignacionSeguros> listaPolizas = new ArrayList<>();
		try {
			CriteriaBuilder cb = sessionFactory.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<AsignacionSeguros> cq = cb.createQuery(AsignacionSeguros.class);
			Root<AsignacionSeguros> r = cq.from(AsignacionSeguros.class);
			cq.select(r);
			System.out.println(comienza);
			cq.where(cb.like(r.get("poliza"), comienza.concat("%")));
			Query q = sessionFactory.getCurrentSession().createQuery(cq);
			listaPolizas = q.getResultList();
			return listaPolizas;
		} catch (Exception e) {
			System.out.println("Error" + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Transactional
	public List<AsignacionSeguros> listaSA() {
		List<AsignacionSeguros> listasig = new ArrayList<>();
		try {
			CriteriaBuilder cb = sessionFactory.getCriteriaBuilder();
			CriteriaQuery cq = cb.createQuery();
			Root<AsignacionSeguros> root = cq.from(AsignacionSeguros.class);
			cq.select(root);
			cq.where(cb.equal(root.get("tipoSeguros"), 1));
			Query q = sessionFactory.getCurrentSession().createQuery(cq);
			listasig = q.getResultList();
			return listasig;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Transactional
	public List<AsignacionSeguros> listaSV() {
		List<AsignacionSeguros> listasig = new ArrayList<>();
		try {
			CriteriaBuilder cb = sessionFactory.getCriteriaBuilder();
			CriteriaQuery cq = cb.createQuery();
			Root<AsignacionSeguros> root = cq.from(AsignacionSeguros.class);
			cq.select(root);
			cq.where(cb.equal(root.get("tipoSeguros"), 2));
			Query q = sessionFactory.getCurrentSession().createQuery(cq);
			listasig = q.getResultList();
			return listasig;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}
	
	@Transactional
	public List<Tuple> paquetesxyearSA(int anio){
		List<Tuple> listupSA = new ArrayList<>();
		try {
			session= sessionFactory.getCurrentSession();
			CriteriaBuilder cb = session.getCriteriaBuilder();
			CriteriaQuery<Tuple> cq = cb.createTupleQuery();
			Root<AsignacionSeguros> root = cq.from(AsignacionSeguros.class);
			System.out.println("entro y paso");
			Expression<Long> ventas = cb.count(root.get("id"));
			Join<AsignacionSeguros, Paquetes> jAP = root.join("paquetes");
			Join<Paquetes, TipoSeguros> jPT = jAP.join("tipoSeguros");
			System.out.println("paso el join");
			
			
			cq.multiselect(jAP.get("nombre"), jPT.get("nombre"), ventas);
			cq.where(
					cb.equal(cb.function("YEAR", Integer.class, root.get("fecha")), anio));
			
			cq.groupBy(root.get("paquetes"));
			System.out.println("paso la consulta");		
			
			TypedQuery<Tuple> q= session.createQuery(cq);
			
			listupSA = q.getResultList();
			return listupSA;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/* tipo seguro 1
	 * ---------------------------------------------------------*/
	
	
	@Transactional
	public List<Tuple> comparartipo1(int a){
		List<Tuple> list = new ArrayList<>(); 
	try {
		CriteriaBuilder cb = sessionFactory.getCriteriaBuilder();
		CriteriaQuery<Tuple> cq = cb.createTupleQuery();
		Root<AsignacionSeguros> root = cq.from(AsignacionSeguros.class);
		Expression<Long> countsa =  cb.count(root.get("id"));	
		Expression<String> mes = cb.function("MONTHNAME", String.class, root.get("fecha"));
		cq.multiselect(mes,countsa);
		cq.where(
				cb.equal(root.get("tipoSeguros"), 1),
				cb.equal(cb.function("YEAR", Integer.class, root.get("fecha")), a)).groupBy(mes);
		TypedQuery<Tuple> tp = sessionFactory.getCurrentSession().createQuery(cq);
		list = tp.getResultList();
		
		return list;
		} catch (Exception e) {
	System.out.println("Error: " + e.getMessage());
	return null;
	
	 
	}
	}
	
	/* tipo seguro 2
	 * ---------------------------------------------------------*/
	
	@Transactional
	public List<Tuple> comparartipo2(int a){
		List<Tuple> list = new ArrayList<>(); 
	try {
		CriteriaBuilder cb = sessionFactory.getCriteriaBuilder();
		CriteriaQuery<Tuple> cq = cb.createTupleQuery();
		Root<AsignacionSeguros> root = cq.from(AsignacionSeguros.class);
		Expression<Long> countsa =  cb.count(root.get("id"));	
		Expression<String> mes = cb.function("MONTHNAME", String.class, root.get("fecha"));
		cq.multiselect(mes,countsa);
		cq.where(
				cb.equal(root.get("tipoSeguros"), 2),
				cb.equal(cb.function("YEAR", Integer.class, root.get("fecha")), a)).groupBy(mes);
		TypedQuery<Tuple> tp = sessionFactory.getCurrentSession().createQuery(cq);
		list = tp.getResultList();
		
		return list;
		} catch (Exception e) {
	System.out.println("Error: " + e.getMessage());
	return null;
	
	 
	}
	
}

	//consulta de ventas por sucursal
		@Transactional
		public List<Tuple> ventas(int year) {
			List<Tuple> listupSS = new ArrayList<>();
			try {
				session = sessionFactory.getCurrentSession();
				CriteriaBuilder cb = session.getCriteriaBuilder();
				CriteriaQuery<Tuple> cq = cb.createTupleQuery();
				Root<AsignacionSeguros> root = cq.from(AsignacionSeguros.class);

				Expression<Long> ventas = cb.count(root.get("id"));

				Join<AsignacionSeguros, Sucursales> jPS = root.join("sucursales");
				Join<AsignacionSeguros, TipoSeguros> jPT = root.join("tipoSeguros");
			
				cq.multiselect(jPS.get("nombre"), jPT.get("nombre"), ventas);
				cq.where(cb.equal(cb.function("YEAR", Integer.class, root.get("fecha")), year));

				cq.groupBy(root.get("sucursales"));
			
				TypedQuery<Tuple> q = session.createQuery(cq);

				listupSS = q.getResultList();
				
				return listupSS;
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
	
}
