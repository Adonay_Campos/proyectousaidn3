/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seguros.impl;

import com.seguros.model.CategoriaSeguros;
import com.seguros.util.AbstractFacade;
import com.seguros.util.Dao;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("cateSeguros")
public class CategoriaSeguroImpl extends AbstractFacade<CategoriaSeguros> implements Dao<CategoriaSeguros> {

    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    protected SessionFactory sessionFactory() {
        return sessionFactory;
    }

    public CategoriaSeguroImpl() {
        super(CategoriaSeguros.class);
    }

    
    
}
