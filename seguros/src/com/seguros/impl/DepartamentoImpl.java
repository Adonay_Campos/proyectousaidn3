package com.seguros.impl;

import java.util.List;
import javax.persistence.criteria.CriteriaQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.seguros.model.Departamentos;
import com.seguros.util.AbstractFacade;
import com.seguros.util.Dao;

@Repository("departamento")
public class DepartamentoImpl extends AbstractFacade<Departamentos> implements Dao<Departamentos> {

    @Autowired
    private SessionFactory sessionFactory;

    public DepartamentoImpl() {
        super(Departamentos.class);
//        this.sessionFactory = sessionFactory;
    }

    public DepartamentoImpl(SessionFactory sessionFactory, Class<Departamentos> entityClass) {
        super(entityClass);
        this.sessionFactory = sessionFactory;
    }

    @Override
    protected SessionFactory sessionFactory() {
        return sessionFactory;
    }

	@SuppressWarnings({ "unchecked", "rawtypes" })
    @Transactional
    public List<Departamentos> orden() {
//        try {
//            String consulta = "Select d.nombre, d.id FROM Departamentos as d order by d.nombre asc ";
//            Query q = session.createNativeQuery(consulta);
//            List<Departamentos> listDepa = new ArrayList<>();
//            listDepa = q.getResultList();
//            return listDepa;
//        } catch (Exception e) {
//            System.out.println("Error en la consulta: " + e.getMessage());
//            return null;
//        }
        session = sessionFactory().getCurrentSession();
        CriteriaQuery crq = session.getCriteriaBuilder().createQuery();
        crq.select(crq.from(Departamentos.class));
        return session.createQuery(crq).getResultList();
    }
}
