package com.seguros.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.seguros.model.EstadoCivil;
import com.seguros.util.AbstractFacade;
import com.seguros.util.Dao;

@Repository("estadoCivil")
public class EstadocivilImpl extends AbstractFacade<EstadoCivil> implements Dao<EstadoCivil>{
    
    @Autowired
    private SessionFactory sessionFactory;
    
    public EstadocivilImpl(SessionFactory sessionFactory, Class<EstadoCivil> entityClass){
        super(entityClass);
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    protected SessionFactory sessionFactory(){
        return sessionFactory;
    }

    public EstadocivilImpl() {
        super(EstadoCivil.class);
    }
    
    
}
