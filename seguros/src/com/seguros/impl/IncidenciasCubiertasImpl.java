package com.seguros.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.seguros.model.AsignacionSeguros;
import com.seguros.model.Incidencias;
import com.seguros.model.IncidenciasCubiertas;
import com.seguros.model.Talleres;
import com.seguros.model.Vehiculos;
import com.seguros.util.AbstractFacade;
import com.seguros.util.Dao;

@SuppressWarnings("deprecation")
@Repository("inciCubiertas")
public class IncidenciasCubiertasImpl extends AbstractFacade<IncidenciasCubiertas>
		implements Dao<IncidenciasCubiertas> {

	@Autowired
	private SessionFactory sessionFactory;

	public IncidenciasCubiertasImpl(SessionFactory sessionFactory, Class<IncidenciasCubiertas> entityClass) {
		super(entityClass);
		this.sessionFactory = sessionFactory;
	}

	public IncidenciasCubiertasImpl() {
		super(IncidenciasCubiertas.class);
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sessionFactory;
	}

//    metodo para filtro de busqueda por estado de incidencias cubiertas
	@SuppressWarnings({ "unchecked", "rawtypes"})
	public List<IncidenciasCubiertas> busquedaxEstado(String estado) {
		Session s = sessionFactory.openSession();
		List<IncidenciasCubiertas> listaInCubiertas = new ArrayList<>();
		String sqq = "select a_s.poliza,v.placa,i.descripcion,t.nombre_taller,t.direccion,i_c.fecha from vehiculos as v inner join asignacion_seguros as a_s on v.id=a_s.vehiculo inner join incidencias_cubiertas as i_c on a_s.id=i_c.n_seguro inner join talleres as t on i_c.taller=t.id inner join incidencias as i on i_c.codigo=i.id where i_c.estado='"
				+ estado + "';";
		try {
			SQLQuery query = s.createSQLQuery(sqq);
			List<Object[]> rows = query.list();

			Object v[];
			for (int i = 0; rows.size() > i; i++) {
				
				IncidenciasCubiertas icub = new IncidenciasCubiertas();
				v = rows.get(i);

				Vehiculos ve = new Vehiculos();
				ve.setPlaca(v[1].toString());
				AsignacionSeguros asSeg = new AsignacionSeguros();
				asSeg.setPoliza(v[0].toString());
				asSeg.setVehiculos(ve);
				Incidencias in = new Incidencias();
				in.setDescripcion(v[2].toString());
				Talleres t = new Talleres();
				t.setNombreTaller(v[3].toString());
				t.setDireccion(v[4].toString());
				icub.setAsignacionSeguros(asSeg);
				icub.setIncidencias(in);
				icub.setTalleres(t);
				icub.setFecha((Date) v[5]);

				listaInCubiertas.add(icub);
			}
			return listaInCubiertas;

		} catch (Exception e) {
			System.out.println("Error" + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
}
