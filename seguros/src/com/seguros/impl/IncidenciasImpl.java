package com.seguros.impl;

import com.seguros.model.Incidencias;
import com.seguros.util.AbstractFacade;
import com.seguros.util.Dao;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("incidencias")
public class IncidenciasImpl extends AbstractFacade<Incidencias> implements Dao<Incidencias> {

	@Autowired
	private SessionFactory sessionFactory;

	public IncidenciasImpl() {
		super(Incidencias.class);
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sessionFactory;
	}
}
