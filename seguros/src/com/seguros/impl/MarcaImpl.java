/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seguros.impl;

import com.seguros.model.Marcas;
import com.seguros.util.AbstractFacade;
import com.seguros.util.Dao;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("marca")
public class MarcaImpl extends AbstractFacade<Marcas> implements Dao<Marcas> {

    @Autowired
    private SessionFactory sessionFactory;

    public MarcaImpl() {
        super(Marcas.class);
    }

    @Override
    protected SessionFactory sessionFactory() {
        return sessionFactory;
    }
    
    

}
