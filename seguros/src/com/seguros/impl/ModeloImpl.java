/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seguros.impl;

import com.seguros.model.Marcas;
import com.seguros.model.Modelos;
import com.seguros.util.AbstractFacade;
import com.seguros.util.Dao;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("modelo")
public class ModeloImpl extends AbstractFacade<Modelos> implements Dao<Modelos> {

    @Autowired
    public SessionFactory sessionFactory;

    public ModeloImpl() {
        super(Modelos.class);
    }

    @Override
    protected SessionFactory sessionFactory() {
        return sessionFactory;
    }

    @Transactional
    public List<Modelos> modebymar(Marcas ma){
    	List<Modelos>  list = new ArrayList<>();
    	try {
			list = sessionFactory.getCurrentSession().createNativeQuery("select * from modelos where marca =" + ma.getId(), Modelos.class).getResultList();
		} catch (Exception e) {
			
		}
    	return list;
    }

}
