package com.seguros.impl;

import com.seguros.model.Departamentos;
import com.seguros.model.Municipios;
import com.seguros.util.AbstractFacade;
import com.seguros.util.Dao;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("municipio")
public class MunicipiosImpl extends AbstractFacade<Municipios> implements Dao<Municipios>{

    @Autowired
    private SessionFactory sessionFactory;
    
    public MunicipiosImpl() {
        super(Municipios.class);
    }

    @Override
    protected SessionFactory sessionFactory() {
        return  sessionFactory;
    }
    
    @Transactional
    public List<Municipios> muniByDepa(Departamentos d)
    {
        List<Municipios> list=new ArrayList<>();
        try {
            list=sessionFactory.getCurrentSession().createNativeQuery("select * from municipios where departamento="+d.getId(),Municipios.class).getResultList();
        } catch (Exception e) {
        }
        return list;
    }
}
