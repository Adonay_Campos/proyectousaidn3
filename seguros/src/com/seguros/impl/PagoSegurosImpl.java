package com.seguros.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.seguros.model.AsignacionSeguros;
import com.seguros.model.CategoriaSeguros;
import com.seguros.model.PagoSeguros;
import com.seguros.model.Paquetes;
import com.seguros.model.Personas;
import com.seguros.model.TipoSeguros;
import com.seguros.model.Usuarios;
import com.seguros.util.AbstractFacade;
import com.seguros.util.Dao;

@Repository("pagoSeguros")
public class PagoSegurosImpl extends AbstractFacade<PagoSeguros> implements Dao<PagoSeguros> {

	@Autowired
	private SessionFactory sessionFactory;

	public PagoSegurosImpl() {
		super(PagoSeguros.class);
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sessionFactory;
	}

	@Transactional
	public List<Tuple> cuotaXid(String id) {
		List<Tuple> lisTupl = new ArrayList<>();
		try {
			session = sessionFactory.getCurrentSession();
			CriteriaBuilder cb = session.getCriteriaBuilder();
			CriteriaQuery<Tuple> cqPagos = cb.createTupleQuery();
			Root<PagoSeguros> rPagoS = cqPagos.from(PagoSeguros.class);
			Join<PagoSeguros, AsignacionSeguros> jPSA = rPagoS.join("asignacionSeguros");
			Join<PagoSeguros, Usuarios> jPSU = rPagoS.join("usuarios");
			Join<Usuarios, Personas> jUP = jPSU.join("personas");
			Expression<Object> monto = cb.selectCase()
					.when(cb.equal(rPagoS.get("estado"), 2), (cb.prod(rPagoS.get("cantidad"), 0.93)))
					.otherwise(rPagoS.get("cantidad"));
			Expression<Date> fecha = rPagoS.get("fecha");
			Expression<String> usuario = jUP.get("nombre");
			Expression<String> apel = jUP.get("apellido");
			cqPagos.multiselect(monto, fecha, usuario, apel);
			cqPagos.where(cb.equal(jPSA.get("poliza"), id));
			cqPagos.orderBy(cb.asc(fecha));
			TypedQuery<Tuple> q = session.createQuery(cqPagos);
			lisTupl = q.getResultList();
			return lisTupl;
		} catch (Exception e) {
			System.out.println("Error al hacer el calculo por: " + e.getMessage());
			return null;
		}
	}

	@Transactional
	public List<PagoSeguros> listarxPoliza(String id) {
		List<PagoSeguros> lisTupl = new ArrayList<PagoSeguros>();
		try {
			session = sessionFactory.getCurrentSession();
			CriteriaBuilder cb = session.getCriteriaBuilder();
			CriteriaQuery<PagoSeguros> cqPagos = cb.createQuery(PagoSeguros.class);
			Root<PagoSeguros> rPagoS = cqPagos.from(PagoSeguros.class);
			Join<PagoSeguros, AsignacionSeguros> jPSA = rPagoS.join("asignacionSeguros");
			cqPagos.select(rPagoS);
			cqPagos.where(cb.equal(jPSA.get("poliza"), id));
			cqPagos.orderBy(cb.asc(rPagoS.get("fecha")));
			Query q = session.createQuery(cqPagos);
			lisTupl = q.getResultList();
			return lisTupl;
		} catch (Exception e) {
			System.out.println("Error al hacer el calculo por: " + e.getMessage());
			return null;
		}
	}

	@Transactional
	public List<Tuple> totalPagadoxId(String id, String pa, int idpa) {
		List<Tuple> lisTupl = new ArrayList<>();
		try {
			session = sessionFactory.getCurrentSession();
			CriteriaBuilder cb = session.getCriteriaBuilder();
			CriteriaQuery<Tuple> cqPagos = cb.createTupleQuery();
			Root<PagoSeguros> rPagoS = cqPagos.from(PagoSeguros.class);
			Join<PagoSeguros, AsignacionSeguros> jPSA = rPagoS.join("asignacionSeguros");
			Expression<Double> monto = cb.<Double>selectCase()
					.when(cb.equal(rPagoS.get("estado"), 2), (cb.prod(rPagoS.get("cantidad"), 0.93)))
					.otherwise(rPagoS.get("cantidad"));
			Expression<Double> totalPagado = cb.sum(monto);
			Double suma;
			suma = Double.parseDouble(adeber(id, pa, idpa).get(0).get(0).toString());
			Expression<Double> totalDebe = cb.diff(suma, totalPagado);
			cqPagos.multiselect(totalPagado, totalDebe);
			cqPagos.where(cb.equal(jPSA.get("poliza"), id));
			TypedQuery<Tuple> q = session.createQuery(cqPagos);
			lisTupl = q.getResultList();
			return lisTupl;
		} catch (Exception e) {
			System.out.println("Error al hacer el calculo, en TOTALPAGADOxID  por: " + e.getMessage());
			return null;
		}
	}

	@Transactional
	public List<Tuple> adeber(String id, String pa, int idpa) {
		List<Tuple> lisTupl1 = new ArrayList<>();
		try {
			session = sessionFactory.getCurrentSession();
			CriteriaBuilder cb = session.getCriteriaBuilder();
			CriteriaQuery<Tuple> cqPagos1 = cb.createTupleQuery();
			Root<CategoriaSeguros> rCS = cqPagos1.from(CategoriaSeguros.class);
			Join<CategoriaSeguros, Paquetes> jCP = rCS.join("paquetes");
			Expression<Double> aPagar = cb.prod(rCS.get("anio"), 12.00);
			Expression<Double> pagara = cb.prod(aPagar, jCP.get("monto"));
			cqPagos1.multiselect(pagara);
			cqPagos1.where(cb.equal(jCP.get("nombre"), pa), cb.equal(jCP.get("id"), idpa));
			TypedQuery<Tuple> q1 = session.createQuery(cqPagos1);
			lisTupl1 = q1.getResultList();
			return lisTupl1;
		} catch (Exception e) {
			System.out.println("Error al hacer el calculo en, ADEBER por: " + e.getMessage());
			return null;
		}
	}

	@Transactional
	public List<Tuple> graficarTotalRecibido(int anio) {
		List<Tuple> listG = new ArrayList<>();
		try {
			session = sessionFactory.getCurrentSession();
			CriteriaBuilder cb = session.getCriteriaBuilder();
			CriteriaQuery<Tuple> cqRecibidos = cb.createTupleQuery();
			Root<PagoSeguros> rPagoS = cqRecibidos.from(PagoSeguros.class);
			Expression<String> nombreMes = cb.function("MONTHNAME", String.class, rPagoS.get("fecha"));
			Expression<Number> sumaRecibido = cb.sum(cb.<Number>selectCase()
					.when(cb.equal(rPagoS.get("estado"), 2), (cb.prod(rPagoS.get("cantidad"), 0.93)))
					.otherwise(rPagoS.get("cantidad")));
			Expression<Integer> numMes = cb.function("MONTH", Integer.class, rPagoS.get("fecha"));
			Join<PagoSeguros, AsignacionSeguros> jPA = rPagoS.join("asignacionSeguros");
			Join<AsignacionSeguros, TipoSeguros> jAT = jPA.join("tipoSeguros");
			Expression<String> tipoS = jAT.get("nombre");
			cqRecibidos.multiselect(tipoS, nombreMes, sumaRecibido);
			cqRecibidos.where(cb.equal(cb.function("YEAR", Integer.class, rPagoS.get("fecha")), anio))
					.groupBy(nombreMes, tipoS).orderBy(cb.asc(numMes));
			TypedQuery<Tuple> q = session.createQuery(cqRecibidos);
			listG = q.getResultList();
			return listG;
		} catch (Exception e) {
			System.out.println("Error al realizar la consulta, 'graficarTotalRecibidoxAnio' por: " + e.getMessage());
			return null;
		}
	}

	@Transactional
	public List<Tuple> suma(int anio) {
		List<Tuple> listG = new ArrayList<>();
		try {
			session = sessionFactory.getCurrentSession();
			CriteriaBuilder cb = session.getCriteriaBuilder();
			CriteriaQuery<Tuple> cqRecibidos = cb.createTupleQuery();
			Root<PagoSeguros> rPagoS = cqRecibidos.from(PagoSeguros.class);
			Expression<Number> sumaRecibido = cb.sum(cb.<Number>selectCase()
					.when(cb.equal(rPagoS.get("estado"), 2), (cb.prod(rPagoS.get("cantidad"), 0.93)))
					.otherwise(rPagoS.get("cantidad")));
			Join<PagoSeguros, AsignacionSeguros> jPA = rPagoS.join("asignacionSeguros");
			Join<AsignacionSeguros, TipoSeguros> jAT = jPA.join("tipoSeguros");
			Expression<String> tipoS = jAT.get("nombre");
			cqRecibidos.multiselect(tipoS, sumaRecibido);
			cqRecibidos.where(cb.equal(cb.function("YEAR", Integer.class, rPagoS.get("fecha")), anio)).groupBy(tipoS);
			TypedQuery<Tuple> q = session.createQuery(cqRecibidos);
			listG = q.getResultList();
			return listG;
		} catch (Exception e) {
			System.out.println("Error al realizar la consulta, 'suma' por: " + e.getMessage());
			return null;
		}
	}

	@Transactional
	public List<Tuple> totalRecibidoxTipoS(int anio) {
		List<Tuple> listG = new ArrayList<>();
		try {
			session = sessionFactory.getCurrentSession();
			CriteriaBuilder cb = session.getCriteriaBuilder();
			CriteriaQuery<Tuple> cqRecibidos = cb.createTupleQuery();
			Root<AsignacionSeguros> rPagoS = cqRecibidos.from(AsignacionSeguros.class);
			Join<AsignacionSeguros, TipoSeguros> jAT = rPagoS.join("tipoSeguros");
			Expression<String> tipoSeguro = jAT.get("nombre");
			Expression<Long> total = cb.count(rPagoS.get("id"));
			cqRecibidos.multiselect(tipoSeguro, total);
			cqRecibidos.where(cb.equal(cb.function("YEAR", Integer.class, rPagoS.get("fecha")), anio))
					.groupBy(tipoSeguro);
			TypedQuery<Tuple> q = session.createQuery(cqRecibidos);
			listG = q.getResultList();
			return listG;
		} catch (Exception e) {
			System.out.println("Error al realizar la consulta, 'graficarTotalRecibidoxAnio' por: " + e.getMessage());
			return null;
		}
	}

}
