package com.seguros.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.seguros.model.Paquetes;
import com.seguros.util.AbstractFacade;
import com.seguros.util.Dao;

@SuppressWarnings("unchecked")
@Repository("paquete")
public class PaqueteImpl extends AbstractFacade<Paquetes> implements Dao<Paquetes> {

	@Autowired
	private SessionFactory sessionFactory;

	public PaqueteImpl() {
		super(Paquetes.class);
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sessionFactory;
	}

	// CONSULTAS PARA BUSCAR POR NOMBRE DE PAQUETE
		@SuppressWarnings("rawtypes")
		@Transactional
		public List<Paquetes> busquedaxPaquete(String nombre) {
			List<Paquetes> listaxpaquete = new ArrayList<>();
			try {
				CriteriaBuilder cb = sessionFactory.getCriteriaBuilder();
				CriteriaQuery<Paquetes> cq = cb.createQuery(Paquetes.class);
				Root<Paquetes> rootPaquetes = cq.from(Paquetes.class);
				cq.select(rootPaquetes);
				cq.where(cb.like(rootPaquetes.get("nombre"), nombre.concat("%")));

				Query q = sessionFactory.getCurrentSession().createQuery(cq);
				listaxpaquete = q.getResultList();
				System.out.println("Termino Listar " + listaxpaquete);
				return listaxpaquete;
			} catch (Exception e) {
				System.out.println("Error" + e.getMessage());
				return null;
			}
		}
}
