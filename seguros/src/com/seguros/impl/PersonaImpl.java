package com.seguros.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.seguros.model.Personas;
import com.seguros.model.TipoUsuarios;
import com.seguros.util.AbstractFacade;
import com.seguros.util.Dao;

@Repository("persona")
public class PersonaImpl extends AbstractFacade<Personas> implements Dao<Personas> {

	@Autowired
	private SessionFactory sessionFactory;

	public PersonaImpl(SessionFactory sessionFactory, Class<Personas> entityClass) {
		super(entityClass);
		this.sessionFactory = sessionFactory;
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sessionFactory;
	}

	public PersonaImpl() {
		super(Personas.class);
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<Personas> search(String dui) {
		List<Personas> listaper = new ArrayList<>();
		try {
			session = sessionFactory.getCurrentSession();
			CriteriaBuilder cb = session.getCriteriaBuilder();
			CriteriaQuery<Personas> cq = cb.createQuery(Personas.class);
			Root<Personas> root = cq.from(Personas.class);
			Join<Personas, TipoUsuarios> join01 = root.join("tipoUsuarios");
			cq.select(root);
			cq.where(cb.equal(join01.get("id"), 2), cb.like(root.get("dui"), dui + "%"));
			Query q = session.createQuery(cq);
			listaper = q.getResultList();
			return listaper;

		} catch (Exception e) {
			System.out.println("Error" + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<Personas> busquedaxTipoUsuario(String tipo) {
		List<Personas> listxTipoUsuario = new ArrayList<>();
		try {
			CriteriaBuilder cb = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Personas> cq = cb.createQuery(Personas.class);
			Root<Personas> rootPersonas = cq.from(Personas.class);
			Join<Personas, TipoUsuarios> join01 = rootPersonas.join("tipoUsuarios");
			cq.select(rootPersonas);
			// cq.where(cb.equal(rootPersonas.get("tipoUsuarios"), tipo));
			cq.where(cb.like(join01.get("tipo"), tipo));
			Query q = sessionFactory.getCurrentSession().createQuery(cq);
			listxTipoUsuario = q.getResultList();
			return listxTipoUsuario;

		} catch (Exception e) {
			System.out.println("Error" + e.getMessage());
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<Personas> listarClientes() {
		List<Personas> listarClientes = new ArrayList<>();
		try {
			session = sessionFactory.getCurrentSession();
			CriteriaBuilder cb = session.getCriteriaBuilder();
			CriteriaQuery<Personas> cq = cb.createQuery(Personas.class);
			Root<Personas> rootPersonas = cq.from(Personas.class);
			Join<Personas, TipoUsuarios> join01 = rootPersonas.join("tipoUsuarios");
			cq.select(rootPersonas);
			cq.where(cb.equal(rootPersonas.get("estado"), 1),
					
					cb.equal(join01.get("id"), 2));
			Query q = session.createQuery(cq);
			listarClientes = q.getResultList();
			return listarClientes;
		} catch (Exception e) {
			System.out.println("Error" + e.getMessage());
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<Personas> listarEmpleados() {
		List<Personas> listarEmpleados = new ArrayList<>();
		try {
			CriteriaBuilder cb = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Personas> cq = cb.createQuery(Personas.class);
			Root<Personas> rootPersonas = cq.from(Personas.class);
			Join<Personas, TipoUsuarios> join01 = rootPersonas.join("tipoUsuarios");
			cq.select(rootPersonas);
			cq.where(cb.not(cb.equal(join01.get("id"), 2)));
			Query q = sessionFactory.getCurrentSession().createQuery(cq);
			listarEmpleados = q.getResultList();
			return listarEmpleados;
		} catch (Exception e) {
			System.out.println("Error" + e.getMessage());
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<Personas> busquedaPorId(int id) {
		List<Personas> listaP = new ArrayList<>();
		try {
			CriteriaBuilder cb = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Personas> cq = cb.createQuery(Personas.class);
			Root<Personas> rootPersonas = cq.from(Personas.class);
			cq.select(rootPersonas);

			cq.where(cb.equal(rootPersonas.get("id"), id)

			);
			Query q = sessionFactory.getCurrentSession().createQuery(cq);
			listaP = q.getResultList();
			return listaP;
		} catch (Exception e) {
			System.out.println("Error" + e.getMessage());
		}
		return null;
	}

}
