package com.seguros.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.seguros.model.Municipios;
import com.seguros.model.Sucursales;
import com.seguros.util.AbstractFacade;
import com.seguros.util.Dao;

@Repository("sucursales")
public class SucursalesImpl extends AbstractFacade<Sucursales> implements Dao<Sucursales> {

	@Autowired
	private SessionFactory sessionFactory;

	public SucursalesImpl() {
		super(Sucursales.class);
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sessionFactory;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<Sucursales> busquedaxDepaMuni(int mun) {
		List<Sucursales> lista = new ArrayList<>();
		try {
			session = sessionFactory.getCurrentSession();
			CriteriaBuilder cb = session.getCriteriaBuilder();
			CriteriaQuery<Sucursales> cqSucursales = cb.createQuery(Sucursales.class);
			Root<Sucursales> rSucursales = cqSucursales.from(Sucursales.class);
			Join<Sucursales, Municipios> jSM = rSucursales.join("municipios");
			cqSucursales.select(rSucursales);
			cqSucursales.where(cb.equal(jSM.get("id"), mun));
			Query query = session.createQuery(cqSucursales);
			lista = query.getResultList();
			return lista;
		} catch (Exception e) {
			System.out.println("Error al buscar Sucursal por B1, por: " + e.getMessage());
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<Sucursales> busquedaxNombre(String nomb) {
		List<Sucursales> lista = new ArrayList<>();
		try {
			session = sessionFactory.getCurrentSession();
			CriteriaBuilder cb = session.getCriteriaBuilder();
			CriteriaQuery<Sucursales> cqSucursales = cb.createQuery(Sucursales.class);
			Root<Sucursales> rSucursales = cqSucursales.from(Sucursales.class);
			cqSucursales.select(rSucursales);
			cqSucursales.where(cb.like(rSucursales.get("nombre"), nomb.concat("%")));
			Query query = session.createQuery(cqSucursales);
			lista = query.getResultList();
			return lista;
		} catch (Exception e) {
			System.out.println("Error al buscar Sucursal por B1, por: " + e.getMessage());
			return null;
		}
	}
}
