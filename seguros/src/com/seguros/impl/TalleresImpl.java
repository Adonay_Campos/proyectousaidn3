/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seguros.impl;

import com.seguros.model.Talleres;
import com.seguros.util.AbstractFacade;
import com.seguros.util.Dao;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author erick.gomezusam
 */
@Repository("talleres")
public class TalleresImpl extends AbstractFacade<Talleres> implements Dao<Talleres> {

    @Autowired
    private SessionFactory sessionFactory;

    public TalleresImpl(SessionFactory sessionFactory, Class<Talleres> entityClass) {
        super(entityClass);
        this.sessionFactory = sessionFactory;
    }

    @Override
    protected SessionFactory sessionFactory() {
        return sessionFactory;
    }

    public TalleresImpl() {
        super(Talleres.class);
    }
    
    

}
