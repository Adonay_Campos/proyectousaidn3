/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seguros.impl;

import com.seguros.model.TipoAutos;
import com.seguros.util.AbstractFacade;
import com.seguros.util.Dao;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("tipoAuto")
public class TipoAutoImpl extends AbstractFacade<TipoAutos> implements Dao<TipoAutos>{
 
    @Autowired
    private SessionFactory sessionFactory;

    public TipoAutoImpl() {
        super(TipoAutos.class);
    }

    @Override
    protected SessionFactory sessionFactory() {
        return sessionFactory;
    }
 
}
