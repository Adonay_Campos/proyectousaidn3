package com.seguros.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.seguros.model.TipoSeguros;
import com.seguros.util.AbstractFacade;
import com.seguros.util.Dao;

@Repository("tipoSeguro")
public class TipoSeguroImpl extends AbstractFacade<TipoSeguros> implements Dao<TipoSeguros> {

	@Autowired
	private SessionFactory sessionFactory;

	public TipoSeguroImpl() {
		super(TipoSeguros.class);

	}

	@Override
	protected SessionFactory sessionFactory() {
		return sessionFactory;
	}

}
