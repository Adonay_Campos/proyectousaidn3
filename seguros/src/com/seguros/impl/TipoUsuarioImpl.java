package com.seguros.impl;

import com.seguros.model.TipoUsuarios;
import com.seguros.util.AbstractFacade;
import com.seguros.util.Dao;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("tipoUsuario")
public class TipoUsuarioImpl extends AbstractFacade<TipoUsuarios> implements Dao<TipoUsuarios> {

	@Autowired
	private SessionFactory sessionFactory;

	public TipoUsuarioImpl(SessionFactory sessionFactory, Class<TipoUsuarios> entityClass) {
		super(entityClass);
		this.sessionFactory = sessionFactory;
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sessionFactory;
	}

	public TipoUsuarioImpl() {
		super(TipoUsuarios.class);
	}
}
