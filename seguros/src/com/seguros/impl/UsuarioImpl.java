package com.seguros.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

//import javax.persistence.Query;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.seguros.model.Personas;
import com.seguros.model.TipoUsuarios;
import com.seguros.model.Usuarios;
import com.seguros.util.AbstractFacade;
import com.seguros.util.Dao;

@Repository("usuario")
public class UsuarioImpl extends AbstractFacade<Usuarios> implements Dao<Usuarios> {

	@Autowired
	private SessionFactory sessionFactory;

	public UsuarioImpl(SessionFactory sessionFactory, Class<Usuarios> entityClass) {
		super(entityClass);
		this.sessionFactory = sessionFactory;
	}

	public UsuarioImpl() {
		super(Usuarios.class);
	}

	@Override
	protected SessionFactory sessionFactory() {
		return sessionFactory;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Transactional
	public Usuarios access(String usuario, String pass) {

		Usuarios usu = null;
		try {
			System.out.println("Antes de la consulta:");
			String consulta = "SELECT u.id FROM Usuarios as u WHERE u.usuario=? and u.pass =aes_encrypt(?,'afe')";
			Query q = sessionFactory.getCurrentSession().createNativeQuery(consulta);
			q.setParameter(1, usuario);
			q.setParameter(2, pass);
			System.out.println("Se ejecuto");
			List<Usuarios> listausuarios = q.getResultList();

			if (listausuarios.size() > 0) {
				String sql2 = "SELECT u FROM Usuarios u WHERE u.usuario=?1";
				Query q2 = sessionFactory.getCurrentSession().createQuery(sql2);
				q2.setParameter(1, usuario);
				List<Usuarios> list2 = q2.getResultList();
				usu = list2.get(0);
				System.out.println("Usuario" + usu.getUsuario());
				System.out.println("Usuario*******" + usu.getPersonas().getTipoUsuarios().getTipo());
			}
		} catch (Exception e) {
			System.out.println("Error" + e.getMessage());
			e.printStackTrace();
		}
		return usu;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Transactional
	public Personas getperson(Personas per) {
		Personas persona = null;
		try {
			String consulta = "SELECT p FROM Personas p WHERE p.id = ?1";
			Query q = sessionFactory.getCurrentSession().createQuery(consulta);
			q.setParameter(1, per.getId());
			List<Personas> listaper = q.getResultList();
			if (!listaper.isEmpty()) {
				persona = listaper.get(0);
				System.out.println("I get it:" + persona.getId());
			}
		} catch (Exception e) {
			System.out.println("Error:" + e.getMessage());
			e.printStackTrace();
		}
		return persona;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<Personas> listarEmpleados() {
		List<Personas> listarEmpleados = new ArrayList<>();
		try {
			CriteriaBuilder cb = sessionFactory.getCriteriaBuilder();
			CriteriaQuery<Personas> cq = cb.createQuery(Personas.class);
			Root<Personas> rootPersonas = cq.from(Personas.class);
			Join<Personas, TipoUsuarios> join01 = rootPersonas.join("tipoUsuarios");
			cq.select(rootPersonas);
			cq.where(cb.not(cb.equal(join01.get("id"), 2)));
			Query q = sessionFactory.getCurrentSession().createQuery(cq);
			listarEmpleados = q.getResultList();
			return listarEmpleados;
		} catch (Exception e) {
			System.out.println("Error" + e.getMessage());
			return null;
		}
	}

}
