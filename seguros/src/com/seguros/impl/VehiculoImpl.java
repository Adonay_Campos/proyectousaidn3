package com.seguros.impl;

import com.seguros.model.Vehiculos;
import com.seguros.util.AbstractFacade;
import com.seguros.util.Dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("vehiculo")
public class VehiculoImpl extends AbstractFacade<Vehiculos> implements Dao<Vehiculos> {

    @Autowired
    public SessionFactory sessionFactory;

    public VehiculoImpl() {
        super(Vehiculos.class);

    }

    @Override
    protected SessionFactory sessionFactory() {
        return sessionFactory;
    }

	@SuppressWarnings({ "unchecked", "rawtypes" })
    @Transactional
    public List<Vehiculos> busquedaPorPlaca(String placa) {
        List<Vehiculos> listaVe = new ArrayList<>();

        try {
            CriteriaBuilder cb = sessionFactory.getCriteriaBuilder();
            CriteriaQuery<Vehiculos> cq = cb.createQuery(Vehiculos.class);
            Root<Vehiculos> rv = cq.from(Vehiculos.class);
            cq.where(
                    cb.like(rv.<String>get("placa"), placa + "%")
            );
            Query q = sessionFactory.getCurrentSession().createQuery(cq);
            listaVe = q.getResultList();
            return listaVe;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
