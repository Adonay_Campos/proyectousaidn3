package com.seguros.model;
// Generated 11-08-2019 02:48:07 PM by Hibernate Tools 4.3.1


import static javax.persistence.GenerationType.IDENTITY;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Sucursales generated by hbm2java
 */
@Entity
@Table(name="sucursales"
    ,catalog="seguros"
)
public class Sucursales  implements java.io.Serializable {


     private Integer id;
     private Municipios municipios;
     private String nombre;

    public Sucursales() {
    }

    public Sucursales(Municipios municipios, String nombre, Set<PagoSeguros> pagoSeguroses, Set<AsignacionSeguros> asignacionSeguroses) {
       this.municipios = municipios;
       this.nombre = nombre;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="id", unique=true, nullable=false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

@ManyToOne(fetch=FetchType.EAGER,optional = true)
    @JoinColumn(name="municipio")
    public Municipios getMunicipios() {
        return this.municipios;
    }
    
    public void setMunicipios(Municipios municipios) {
        this.municipios = municipios;
    }

    
    @Column(name="nombre", length=50)
    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}


