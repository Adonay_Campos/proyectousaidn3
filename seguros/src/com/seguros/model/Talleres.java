package com.seguros.model;
// Generated 11-08-2019 02:48:07 PM by Hibernate Tools 4.3.1

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Talleres generated by hbm2java
 */
@Entity
@Table(name = "talleres", catalog = "seguros")
public class Talleres implements java.io.Serializable {

	private Integer id;
	private String nombreTaller;
	private String codigo;
	private String direccion;

	public Talleres() {
	}

	public Talleres(String nombreTaller, String codigo, String direccion) {
		this.nombreTaller = nombreTaller;
		this.codigo = codigo;
		this.direccion = direccion;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "nombre_taller", length = 50)
	public String getNombreTaller() {
		return this.nombreTaller;
	}

	public void setNombreTaller(String nombreTaller) {
		this.nombreTaller = nombreTaller;
	}

	@Column(name = "codigo", length = 20)
	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	@Column(name = "direccion", length = 50)
	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
}
