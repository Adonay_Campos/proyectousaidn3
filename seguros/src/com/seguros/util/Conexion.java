package com.seguros.util;

import java.sql.Connection;
import java.sql.DriverManager;

public class Conexion {


	    private String db = "seguros";
	    private String user = "kz";
	    private String pass = "kzroot";
	    private String url = "jdbc:mysql://usam-sql.sv.cds:3306/seguros?allowPublicKeyRetrieval=true&useSSL=false";
	    private Connection conn;

	    public Connection getConn() {
	        return conn;
	    }

	    public void setConn(Connection conn) {
	        this.conn = conn;
	    }

	    public Connection conectar() {
	        try {
	            Class.forName("com.mysql.jdbc.Driver");
	            conn = DriverManager.getConnection(url, user, pass);
	            if (conn != null) {
	                System.out.println("Exitos!!!!!");
	            }
	            return conn;
	        } catch (Exception e) {
	            e.printStackTrace();
	            return null;
	        }
	    }

	    public void desconectar() {
	        try {
	            conn.close();
	        } catch (Exception e) {

	        }
	    }

	}
