package com.kz.facturacion.conf;



import com.kz.facturacion.dao.ClienteAc;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import com.kz.facturacion.utils.Dao;

@Configuration
@ComponentScan(basePackages = {"com.kz.facturacion.dao","com.kz.facturacion.ctrl"})
//@ComponentScan(basePackages = {"com.kz"})
public class Config {

    @Bean
    public Dao daoCliente() {
        return new ClienteAc();
    }
    
    @Bean
    public Connection getConex(){
        Connection c;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            //Class.forName("com.mysql.jdbc.Driver");
            c = DriverManager.getConnection("jdbc:mysql://localhost:3306/facturacion?useSSL=false", "root", "admin");
            //c = DriverManager.getConnection("jdbc:mysql://localhost:3306/facturacion?useSSL=false", "root", "rootroot");
        } catch (SQLException e) {
            c = null;
        }
        catch(ClassNotFoundException cx){
            c = null;
        }
        return c;
    }
}
