package com.kz.facturacion.dao;

import com.kz.facturacion.utils.Dao;
import com.kz.facturacion.utils.Enlace;
import com.kz.usespring.models.Factura;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author Elena
 */
@Repository
public class FacturaAc implements Dao<Factura> {

    @Autowired
    Connection getConex;
    private PreparedStatement p;
    private String[] ar = {
        "insert into factura(codigo,fecha,num_pago,id_cliente) values(?,?,?,?)",
        "select * from factura order by num_factura",
        "select * from factura order by num_factura where num_factura=?",
        "update factura set codigo=?, fecha=?, num_pago=?, id_cliente=? where num_factura=?",
        "delete from factura where num_factura=?"};

    private Factura f;
    private ClienteAc cl = new ClienteAc();
    private ModoPagoAc mp = new ModoPagoAc();
    private List<Factura> facs;

    @Override
    public void create(Factura t) {
        try {
            SimpleDateFormat sd = new SimpleDateFormat("yyyy-mm-dd");
            p = getConex.prepareStatement(ar[0]);
            p.setString(1, t.getCodigo());
            p.setString(2, sd.format(t.getFecha()));
            p.setLong(3, t.getModPago().getNumPago());
            p.setLong(4, t.getClient().getId());
            p.executeUpdate();
        } catch (Exception e) {
            System.out.println("Error en Factura: Al crear");
        } finally {
            Enlace.cierra();
        }
    }

    @Override
    public List<Factura> read() {
        try {
            // p = Enlace.conecta().prepareStatement(ar[0]);
            p = getConex.prepareStatement(ar[1]);
            ResultSet rs = p.executeQuery();
            facs = new ArrayList<Factura>();
            SimpleDateFormat sd = new SimpleDateFormat("yyyy-mm-dd");
            while (rs.next()) {
                f = new Factura();
                f.setNumFactura(rs.getLong("num_factura"));
                f.setCodigo(rs.getString("codigo"));
                f.setClient(cl.readBy(rs.getLong("id_cliente")));
                f.setFecha(new Date(rs.getString("fecha")));
                f.setModPago(mp.readBy(rs.getLong("num_pago")));
                facs.add(f);
            }
            return facs;
        } catch (Exception e) {
            System.out.println("Error en Factura: Al leer");
            return null;
        } finally {
            Enlace.cierra();
        }
    }

    @Override
    public Factura readBy(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(Factura t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Factura t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
