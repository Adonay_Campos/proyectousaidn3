package com.kz.facturacion.dao;

import com.kz.facturacion.utils.Dao;
import com.kz.facturacion.utils.Enlace;
import com.kz.usespring.models.ModoPago;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author Elena
 */
@Repository
public class ModoPagoAc implements Dao<ModoPago> {

    @Autowired
    Connection getConex;
    private PreparedStatement p;
    private String[] ar = {
        "insert into modo_pago(nombre,otros_detalles) values(?,?)",
        "select * from modo_pago order by num_pago",
        "select * from modo_pago order by num_pago where num_pago=?",
        "update modo_pago set nombre=?, otros_detalles=? where num_pago=?",
        "delete from modo_pago where num_pago=?"};

    private ModoPago pago;
    private List<ModoPago> t;

    @Override
    public void create(ModoPago t) {
        try {
            p = getConex.prepareStatement(ar[0]);
            p.setString(1, t.getNombre());
            p.setString(2, t.getOtrosDetalles());
            p.executeUpdate();
        } catch (Exception e) {
            System.out.println("Error en ModoPago: Al crear");
        } finally {
            Enlace.cierra();
        }
    }

    @Override
    public List<ModoPago> read() {
        try {
            // p = Enlace.conecta().prepareStatement(ar[0]);
            p = getConex.prepareStatement(ar[1]);
            ResultSet rs = p.executeQuery();
            t = new ArrayList<ModoPago>();
            while (rs.next()) {
                pago = new ModoPago();
                pago.setNumPago(rs.getLong("num_pago"));
                pago.setNombre(rs.getString("nombre"));
                pago.setOtrosDetalles(rs.getString("otros_detalles"));
                t.add(pago);
            }
            return t;
        } catch (Exception e) {
            System.out.println("Error en ModoPago: Al leer");
            return null;
        } finally {
            Enlace.cierra();
        }
    }

    @Override
    public ModoPago readBy(long id) {
        try {
            // p = Enlace.conecta().prepareStatement(ar[0]);
            p = getConex.prepareStatement(ar[2]);
            ResultSet rs = p.executeQuery();
            pago = new ModoPago();
            pago.setNumPago(rs.getLong("num_pago"));
            pago.setNombre(rs.getString("nombre"));
            pago.setOtrosDetalles(rs.getString("otros_detalles"));
            return pago;
        } catch (Exception e) {
            System.out.println("Error en ModoPago: Al leer");
            return null;
        } finally {
            Enlace.cierra();
        }
    }

    @Override
    public void update(ModoPago t) {
        try {
            p = getConex.prepareStatement(ar[3]);
            p.setString(1, t.getNombre());
            p.setString(2, t.getOtrosDetalles());
            p.executeUpdate();
        } catch (Exception e) {
            System.out.println("Error en ModoPago: Al actualizar");
        } finally {
            Enlace.cierra();
        }
    }

    @Override
    public void delete(ModoPago t) {
        try {
            p = getConex.prepareStatement(ar[4]);
            p.setLong(1, t.getNumPago());
            p.executeUpdate();
        } catch (Exception e) {
            System.out.println("Error en ModoPago: Al eliminar");
        } finally {
            Enlace.cierra();
        }
    }

}
