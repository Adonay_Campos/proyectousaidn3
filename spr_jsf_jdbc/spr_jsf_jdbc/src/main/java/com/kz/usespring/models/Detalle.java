
package com.kz.usespring.models;

/**
 * @author Elena
 */
public class Detalle {
    private long numDetalle;
    private Factura f;
    private Producto p;
    private int cantidad;
    private double precio;

    public long getNumDetalle() {
        return numDetalle;
    }

    public void setNumDetalle(long numDetalle) {
        this.numDetalle = numDetalle;
    }

    public Factura getF() {
        return f;
    }

    public void setF(Factura f) {
        this.f = f;
    }

    public Producto getP() {
        return p;
    }

    public void setP(Producto p) {
        this.p = p;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
    
    
}
