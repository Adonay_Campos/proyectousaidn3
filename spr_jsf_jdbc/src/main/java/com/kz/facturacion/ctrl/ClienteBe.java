package com.kz.facturacion.ctrl;

import com.kz.usespring.models.Cliente;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import org.springframework.stereotype.Component;
import com.kz.facturacion.utils.Dao;

@Component
@RequestScoped
@ManagedBean(name = "kzBean")
public class ClienteBe implements Serializable {

//    @ManagedProperty("#{daoCliente}")
    @ManagedProperty(value = "#{daoCliente}")
    private Dao daoCliente;
    private Cliente c;

    public void setDaoCliente(Dao daoCliente) {
        this.daoCliente = daoCliente;
    }

    public Dao getDaoCliente() {
        return daoCliente;
    }

    public Cliente getC() {
        return c;
    }

    public void setC(Cliente c) {
        this.c = c;
    }

    public ClienteBe() {
    }
    
    public List<Cliente> listar() {
        try {
            List<Cliente> ls = daoCliente.read();
            return ls;
        } catch (Exception x) {
            return null;
        }

    }

    public void llenar(Cliente cliente) {
        c = cliente;
        System.out.println("Lo lleno");
    }
}
