package com.kz.facturacion.dao;

import com.kz.facturacion.utils.Dao;
import com.kz.facturacion.utils.Enlace;
import com.kz.usespring.models.Categoria;
import com.kz.usespring.models.Cliente;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author Elena
 */
@Repository
public class CategoriaAc implements Dao<Categoria> {

    @Autowired
    Connection getConex;
    private PreparedStatement p;
    private String[] ar = {
        "insert into categoria(nombre,descripcion) values(?,?)",
        "select * from categoria order by id_categoria",
        "select * from categoria order by id_categoria where id_categoria=?",
        "update categoria set nombre=?, descripcion=? where id_categoria=?",
        "delete from categoria where id_categoria=?"};

    private Categoria ca;
    private List<Categoria> s;
    
    @Override
    public void create(Categoria t) {
        try {
            p = getConex.prepareStatement(ar[0]);
            p.setString(1, t.getNombre());
            p.setString(2, t.getDescripcion());
            p.executeUpdate();
        } catch (Exception e) {
            System.out.println("Error en Categoria: Al crear");
        } finally {
            Enlace.cierra();
        }
    }

    @Override
    public List<Categoria> read() {
        try {
            // p = Enlace.conecta().prepareStatement(ar[0]);
            p = getConex.prepareStatement(ar[1]);
            ResultSet rs = p.executeQuery();
            s = new ArrayList<Categoria>();
            while (rs.next()) {
                ca = new Categoria();
                ca.setId(rs.getLong("id_categoria"));
                ca.setNombre(rs.getString("nombre"));
                ca.setDescripcion(rs.getString("descripcion"));
                s.add(ca);
            }
            return s;
        } catch (Exception e) {
            System.out.println("Error en Categoria: Al leer");
            return null;
        } finally {
            Enlace.cierra();
        }
    }

    @Override
    public Categoria readBy(long id) {
        try {
            p = getConex.prepareStatement(ar[2]);
            p.setLong(1, id);
            ResultSet rs = p.executeQuery();
            ca = new Categoria();
            ca.setId(rs.getLong("id_categoria"));
            ca.setNombre(rs.getString("nombre"));
            ca.setDescripcion(rs.getString("descripcion"));
            return ca;
        } catch (Exception e) {
            System.out.println("Error en Cliente: Al leer por id");
            return null;
        } finally {
            Enlace.cierra();
        }
    }

    @Override
    public void update(Categoria t) {
        try {
            p = getConex.prepareStatement(ar[3]);
            p.setString(1, t.getNombre());
            p.setString(2, t.getDescripcion());
            p.setLong(3, t.getId());
            p.executeUpdate();
        } catch (Exception e) {
            System.out.println("Error en Categoria: Al actualizar");
        } finally {
            Enlace.cierra();
        }
        
    }

    @Override
    public void delete(Categoria t) {
        try {
            p = getConex.prepareStatement(ar[4]);
            p.setLong(1, t.getId());
            p.executeUpdate();
        } catch (Exception e) {
            System.out.println("Error en Categoria: Al eliminar");
        } finally {
            Enlace.cierra();
        }
    }
    
}
