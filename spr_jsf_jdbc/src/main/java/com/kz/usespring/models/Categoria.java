package com.kz.usespring.models;

import java.util.List;

/**
 * @author Elena
 */
public class Categoria {
    private long id;
    private String nombre;
    private String descripcion;
    private List<Producto> lsProds;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<Producto> getLsProds() {
        return lsProds;
    }

    public void setLsProds(List<Producto> lsProds) {
        this.lsProds = lsProds;
    }

}
